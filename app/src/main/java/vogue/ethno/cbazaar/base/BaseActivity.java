package vogue.ethno.cbazaar.base;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;


import com.bumptech.glide.Glide;

import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.data.localdb.pref.AppPreference;

public  class BaseActivity extends AppCompatActivity {
    // public KProressHUD kProgressHUD;
   private  Dialog kProgressHUD;
    private View views;
    private View myLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ////hideKeyBoard();
        if (kProgressHUD == null)
            kProgressHUD = new Dialog(this);
        kProgressHUD.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_layout));
    }

    public void showDialog(String label, boolean cancellable, View view) {
       /* if (view==null)
            return;
        this.views=view;
        LayoutInflater inflater = getLayoutInflater();
          myLayout = inflater.inflate(R.layout.shimmer_view, null);
         if (view instanceof RelativeLayout)
            ((RelativeLayout) view).addView(myLayout);
        else if (view instanceof LinearLayout)
            ((LinearLayout) view).addView(myLayout);
        else ((ScrollView) view).addView(myLayout);
        ShimmerLayout shimmerText = (ShimmerLayout )myLayout. findViewById(R.id.shimmer_text);
        shimmerText.startShimmerAnimation();*/

        if (kProgressHUD != null && !kProgressHUD.isShowing()) {
            kProgressHUD.setCancelable(cancellable);
            kProgressHUD.setContentView(R.layout.app_loading_dialog);
            ImageView imageView = kProgressHUD.findViewById(R.id.img_loading_gif);
            //  kProgressHUD.setLabel(Utils.cutNull(label).isEmpty() ? getString(R.string.loading) : label);
            Glide.with(this)
                    .load(R.drawable.lazyloading)
                    .load(R.drawable.loader)
//                    .apply(RequestOptions.placeholderOf(R.drawable.ev_logo))
//                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))  // you may not need this
                    .into(imageView);
            kProgressHUD.show();
            //}

        }
             //}
    }

    public void hideDialog(boolean isRefreshable) {
      /*  if (isRefreshable) {
            ImageView imageView = new ImageView(this);
            imageView.setBackgroundResource(R.drawable.refresh);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();
            //kProgressHUD.create(this).setCustomView(imageView).setLabel(getString(R.string.retry));

        } else {
            if (kProgressHUD != null && kProgressHUD.isShowing())
                kProgressHUD.dismiss();
        }*//* if (views==null)
            return;
        if (views instanceof RelativeLayout)
            ((RelativeLayout) views).removeView(myLayout);
        else if (views instanceof LinearLayout)
            ((LinearLayout) views).removeView(myLayout);
        else  ((ScrollView) views).removeView(myLayout);*/
        if (kProgressHUD != null && kProgressHUD.isShowing())
            kProgressHUD.dismiss();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //  hideKeyBoard();
    }

    public void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }
    }

    public void showKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public void showToast(String s) {
        if (s != null && !s.trim().isEmpty())
            Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    public boolean isUserLoginIn() {
        return !Utils.cutNull(AppPreference.getPrefsHelper().getPref(AppPreference.USER_LOGIN_DATA, "")).isEmpty();
    }
}
