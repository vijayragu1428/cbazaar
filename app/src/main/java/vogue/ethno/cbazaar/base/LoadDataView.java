package vogue.ethno.cbazaar.base;

import android.content.Context;

public interface LoadDataView {
    void showLoading();

    void hideLoading(boolean status);

    Context getActivityContext();

    void showError(String string);

    void sessionError(String string);
}
