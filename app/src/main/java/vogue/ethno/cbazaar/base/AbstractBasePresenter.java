package vogue.ethno.cbazaar.base;

import retrofit2.HttpException;

public abstract class AbstractBasePresenter<V extends LoadDataView> implements BasePresenter<V> {
    private V views;

    @Override
    public void destroyView() {
        views = null;
    }

    @Override
    public void setView(V view) {
        this.views = view;
    }

    public void exceptionHandling(Object o, TokenExpireCommunicator comunicator) {
        try {
            Throwable throwable = (Throwable) o;
            if (throwable instanceof HttpException) {
                if (((HttpException) throwable).code() == 401) {
                    comunicator.callTokenApi();
                } else comunicator.showError(o);
            } else comunicator.showError(o);
        } catch (Exception e) {
            comunicator.showError(o);
        }
    }

}
/*    *   if (throwable is HttpException) {
            if (throwable.code() == 400 || throwable.code() == 500) {
                var appErrorResponse = gson.fromJson(throwable.response().errorBody()?.string(), AppErrorResponseModel::class.java)
                view!!.showServerError(appErrorResponse)

            }
        } else if (throwable is ConnectException)
            view!!.showError(AppConstant.NO_NETWORK)
        else if (throwable is TimeoutException)
            view!!.showError(AppConstant.PLEASE_TRY_AGAIN)
        else if (throwable is Exception)
            view!!.showError(throwable.message!!)
    }*/