package vogue.ethno.cbazaar.base;

public interface TokenExpireCommunicator {
    void callTokenApi();

    void showError(Object o);
}
