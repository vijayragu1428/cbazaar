package vogue.ethno.cbazaar.base;

public interface BasePresenter <V> {
    void destroyView();
    void setView(V view);
}
