package vogue.ethno.cbazaar.model;

import java.io.Serializable;
import java.util.ArrayList;

public class HomeDetailsResponseModel implements Serializable {

    private ArrayList<Banner> banners;

    public ArrayList<Banner> getBanners() {
        return this.banners;
    }

    public void setBanners(ArrayList<Banner> banners) {
        this.banners = banners;
    }

    private ArrayList<Popularcategory> popularcategories;

    public ArrayList<Popularcategory> getPopularcategories() {
        return this.popularcategories;
    }

    public void setPopularcategories(ArrayList<Popularcategory> popularcategories) {
        this.popularcategories = popularcategories;
    }

    public class Image {
        private String image;

        public String getImage() {
            return this.image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }

    public class Banner {
        private String url;

        public String getUrl() {
            return this.url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        private ArrayList<Image> images;

        public ArrayList<Image> getImages() {
            return this.images;
        }

        public void setImages(ArrayList<Image> images) {
            this.images = images;
        }
    }

    public class Popularcategory {
        private String image;

        public String getImage() {
            return this.image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        private String url;

        public String getUrl() {
            return this.url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        private String displaytext;

        public String getDisplaytext() {
            return this.displaytext;
        }

        public void setDisplaytext(String displaytext) {
            this.displaytext = displaytext;
        }
    }
}
