package vogue.ethno.cbazaar.model.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.model.HomeDetailsResponseModel;
import vogue.ethno.cbazaar.view.homescreen.OnItemSelectedCallback;

public class PopularAdapter extends RecyclerView.Adapter<PopularAdapter.PopularViewHolder> {

    private ArrayList<HomeDetailsResponseModel.Popularcategory> popularList;
    private Context mContext;
    private OnItemSelectedCallback onItemSelectedCallback;

    public PopularAdapter(Context mContext, ArrayList<HomeDetailsResponseModel.Popularcategory> popularList, OnItemSelectedCallback onItemSelectedCallback) {
        this.popularList = popularList;
        this.mContext = mContext;
        this.onItemSelectedCallback = onItemSelectedCallback;
    }

    @NonNull
    @Override
    public PopularAdapter.PopularViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.popular_adapter, parent, false);
        return new PopularAdapter.PopularViewHolder(viewGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull PopularAdapter.PopularViewHolder holder, int position) {
        final HomeDetailsResponseModel.Popularcategory product = popularList.get(position);

        Glide.with(mContext).load(Utils.cutNull(product.getImage()))
                .apply(RequestOptions.placeholderOf(R.drawable.lazyloading))
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .into(holder.imageView);
        holder.name.setText(product.getDisplaytext());

        holder.itemLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemSelectedCallback.onItemSelected(product.getDisplaytext(), product.getUrl());
            }
        });
    }

    @Override
    public int getItemCount() {
        return popularList.size();
    }

    class PopularViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView name;
        LinearLayout itemLinearLayout;

        PopularViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_popular);
            name = itemView.findViewById(R.id.tv_name);
            itemLinearLayout = itemView.findViewById(R.id.ll_item);
        }
    }
}
