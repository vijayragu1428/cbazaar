package vogue.ethno.cbazaar.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductListModel implements Serializable {

    private boolean IsLastPage;

    public boolean isLastPage() {
        return IsLastPage;
    }

    public void setLastPage(boolean lastPage) {
        IsLastPage = lastPage;
    }

    private boolean IsRequestInvalid;

    public boolean getIsRequestInvalid() {
        return this.IsRequestInvalid;
    }

    public void setIsRequestInvalid(boolean IsRequestInvalid) {
        this.IsRequestInvalid = IsRequestInvalid;
    }

    private boolean IsRequestToBeRedirected;

    public boolean getIsRequestToBeRedirected() {
        return this.IsRequestToBeRedirected;
    }

    public void setIsRequestToBeRedirected(boolean IsRequestToBeRedirected) {
        this.IsRequestToBeRedirected = IsRequestToBeRedirected;
    }

    private Object UrlToRedirect;

    public Object getUrlToRedirect() {
        return this.UrlToRedirect;
    }

    public void setUrlToRedirect(Object UrlToRedirect) {
        this.UrlToRedirect = UrlToRedirect;
    }

    private ArrayList<Product> Products;

    public ArrayList<Product> getProducts() {
        return this.Products;
    }

    public void setProducts(ArrayList<Product> Products) {
        this.Products = Products;
    }

    private ArrayList<Facet> Facets;

    public ArrayList<Facet> getFacets() {
        return this.Facets;
    }

    public void setFacets(ArrayList<Facet> Facets) {
        this.Facets = Facets;
    }

    private Object RelatedLinks;

    public Object getRelatedLinks() {
        return this.RelatedLinks;
    }

    public void setRelatedLinks(Object RelatedLinks) {
        this.RelatedLinks = RelatedLinks;
    }

    private Object RelatedLinksHeading;

    public Object getRelatedLinksHeading() {
        return this.RelatedLinksHeading;
    }

    public void setRelatedLinksHeading(Object RelatedLinksHeading) {
        this.RelatedLinksHeading = RelatedLinksHeading;
    }

    private Object BreadCrumbs;

    public Object getBreadCrumbs() {
        return this.BreadCrumbs;
    }

    public void setBreadCrumbs(Object BreadCrumbs) {
        this.BreadCrumbs = BreadCrumbs;
    }

    private Hierarchy Hierarchy;

    public Hierarchy getHierarchy() {
        return this.Hierarchy;
    }

    public void setHierarchy(Hierarchy Hierarchy) {
        this.Hierarchy = Hierarchy;
    }

    private String CanonicalUrl;

    public String getCanonicalUrl() {
        return this.CanonicalUrl;
    }

    public void setCanonicalUrl(String CanonicalUrl) {
        this.CanonicalUrl = CanonicalUrl;
    }

    private ArrayList<String> SortOptions;

    public ArrayList<String> getSortOptions() {
        return this.SortOptions;
    }

    public void setSortOptions(ArrayList<String> SortOptions) {
        this.SortOptions = SortOptions;
    }

    private ArrayList<String> InterlinkFacets;

    public ArrayList<String> getInterlinkFacets() {
        return this.InterlinkFacets;
    }

    public void setInterlinkFacets(ArrayList<String> InterlinkFacets) {
        this.InterlinkFacets = InterlinkFacets;
    }

    private int Count;

    public int getCount() {
        return this.Count;
    }

    public void setCount(int Count) {
        this.Count = Count;
    }

    private double Rating;

    public double getRating() {
        return this.Rating;
    }

    public void setRating(double Rating) {
        this.Rating = Rating;
    }

    private int VisitCount;

    public int getVisitCount() {
        return this.VisitCount;
    }

    public void setVisitCount(int VisitCount) {
        this.VisitCount = VisitCount;
    }

    private int MinPrice;

    public int getMinPrice() {
        return this.MinPrice;
    }

    public void setMinPrice(int MinPrice) {
        this.MinPrice = MinPrice;
    }

    private int MaxPrice;

    public int getMaxPrice() {
        return this.MaxPrice;
    }

    public void setMaxPrice(int MaxPrice) {
        this.MaxPrice = MaxPrice;
    }

    private int MinDays;

    public int getMinDays() {
        return this.MinDays;
    }

    public void setMinDays(int MinDays) {
        this.MinDays = MinDays;
    }

    private int MaxDays;

    public int getMaxDays() {
        return this.MaxDays;
    }

    public void setMaxDays(int MaxDays) {
        this.MaxDays = MaxDays;
    }

    private String OutOfStockImage;

    public String getOutOfStockImage() {
        return this.OutOfStockImage;
    }

    public void setOutOfStockImage(String OutOfStockImage) {
        this.OutOfStockImage = OutOfStockImage;
    }

    private boolean ShowCommercialMessage;

    public boolean getShowCommercialMessage() {
        return this.ShowCommercialMessage;
    }

    public void setShowCommercialMessage(boolean ShowCommercialMessage) {
        this.ShowCommercialMessage = ShowCommercialMessage;
    }

    private String CommercialMessage;

    public String getCommercialMessage() {
        return this.CommercialMessage;
    }

    public void setCommercialMessage(String CommercialMessage) {
        this.CommercialMessage = CommercialMessage;
    }

    private Object PECData;

    public Object getPECData() {
        return this.PECData;
    }

    public void setPECData(Object PECData) {
        this.PECData = PECData;
    }

    private boolean HideSortOption;

    public boolean getHideSortOption() {
        return this.HideSortOption;
    }

    public void setHideSortOption(boolean HideSortOption) {
        this.HideSortOption = HideSortOption;
    }

    private boolean HidePagination;

    public boolean getHidePagination() {
        return this.HidePagination;
    }

    public void setHidePagination(boolean HidePagination) {
        this.HidePagination = HidePagination;
    }

    private Object NoProductsMessage;

    public Object getNoProductsMessage() {
        return this.NoProductsMessage;
    }

    public void setNoProductsMessage(Object NoProductsMessage) {
        this.NoProductsMessage = NoProductsMessage;
    }

    private String Title;

    public String getTitle() {
        return this.Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    private String Meta;

    public String getMeta() {
        return this.Meta;
    }

    public void setMeta(String Meta) {
        this.Meta = Meta;
    }

    private String Description;

    public String getDescription() {
        return this.Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    private String Heading;

    public String getHeading() {
        return this.Heading;
    }

    public void setHeading(String Heading) {
        this.Heading = Heading;
    }

    private Object LMC;

    public Object getLMC() {
        return this.LMC;
    }

    public void setLMC(Object LMC) {
        this.LMC = LMC;
    }

    private ArrayList<Object> SelectedFacets;

    public ArrayList<Object> getSelectedFacets() {
        return this.SelectedFacets;
    }

    public void setSelectedFacets(ArrayList<Object> SelectedFacets) {
        this.SelectedFacets = SelectedFacets;
    }

    private String EffectiveURL;

    public String getEffectiveURL() {
        return this.EffectiveURL;
    }

    public void setEffectiveURL(String EffectiveURL) {
        this.EffectiveURL = EffectiveURL;
    }

    private String BaseImageUrl;

    public String getBaseImageUrl() {
        return this.BaseImageUrl;
    }

    public void setBaseImageUrl(String BaseImageUrl) {
        this.BaseImageUrl = BaseImageUrl;
    }

    private String PriceFacetName;

    public String getPriceFacetName() {
        return this.PriceFacetName;
    }

    public void setPriceFacetName(String PriceFacetName) {
        this.PriceFacetName = PriceFacetName;
    }

    private String DeliveryFacetName;

    public String getDeliveryFacetName() {
        return this.DeliveryFacetName;
    }

    public void setDeliveryFacetName(String DeliveryFacetName) {
        this.DeliveryFacetName = DeliveryFacetName;
    }

    private int RedirectionType;

    public int getRedirectionType() {
        return this.RedirectionType;
    }

    public void setRedirectionType(int RedirectionType) {
        this.RedirectionType = RedirectionType;
    }

    private int ProductCount;

    public int getProductCount() {
        return this.ProductCount;
    }

    public void setProductCount(int ProductCount) {
        this.ProductCount = ProductCount;
    }

    private String UnbxdAPI;

    public String getUnbxdAPI() {
        return this.UnbxdAPI;
    }

    public void setUnbxdAPI(String UnbxdAPI) {
        this.UnbxdAPI = UnbxdAPI;
    }

    public class Product {
        private String Code;

        public String getCode() {
            return this.Code;
        }

        public void setCode(String Code) {
            this.Code = Code;
        }

        private String Name;

        public String getName() {
            return this.Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        private String ProductHisImage;

        public String getProductHisImage() {
            return this.ProductHisImage;
        }

        public void setProductHisImage(String ProductHisImage) {
            this.ProductHisImage = ProductHisImage;
        }

        private String LargeImageUrl;

        public String getLargeImageUrl() {
            return this.LargeImageUrl;
        }

        public void setLargeImageUrl(String LargeImageUrl) {
            this.LargeImageUrl = LargeImageUrl;
        }

        private String UpperLargeImage;

        public String getUpperLargeImage() {
            return this.UpperLargeImage;
        }

        public void setUpperLargeImage(String UpperLargeImage) {
            this.UpperLargeImage = UpperLargeImage;
        }

        private String LowerLargeImage;

        public String getLowerLargeImage() {
            return this.LowerLargeImage;
        }

        public void setLowerLargeImage(String LowerLargeImage) {
            this.LowerLargeImage = LowerLargeImage;
        }

        private Object GifLargeImage;

        public Object getGifLargeImage() {
            return this.GifLargeImage;
        }

        public void setGifLargeImage(Object GifLargeImage) {
            this.GifLargeImage = GifLargeImage;
        }

        private String VendorType;

        public String getVendorType() {
            return this.VendorType;
        }

        public void setVendorType(String VendorType) {
            this.VendorType = VendorType;
        }

        private String TypeShortCode;

        public String getTypeShortCode() {
            return this.TypeShortCode;
        }

        public void setTypeShortCode(String TypeShortCode) {
            this.TypeShortCode = TypeShortCode;
        }

        private boolean OutOfStock;

        public boolean getOutOfStock() {
            return this.OutOfStock;
        }

        public void setOutOfStock(boolean OutOfStock) {
            this.OutOfStock = OutOfStock;
        }

        private boolean HasReadyMadeSizes;

        public boolean getHasReadyMadeSizes() {
            return this.HasReadyMadeSizes;
        }

        public void setHasReadyMadeSizes(boolean HasReadyMadeSizes) {
            this.HasReadyMadeSizes = HasReadyMadeSizes;
        }

        private boolean IsNew;

        public boolean getIsNew() {
            return this.IsNew;
        }

        public void setIsNew(boolean IsNew) {
            this.IsNew = IsNew;
        }

        private int Discount;

        public int getDiscount() {
            return this.Discount;
        }

        public void setDiscount(int Discount) {
            this.Discount = Discount;
        }

        private int VisitCount;

        public int getVisitCount() {
            return this.VisitCount;
        }

        public void setVisitCount(int VisitCount) {
            this.VisitCount = VisitCount;
        }

        private boolean IsFreeShipping;

        public boolean getIsFreeShipping() {
            return this.IsFreeShipping;
        }

        public void setIsFreeShipping(boolean IsFreeShipping) {
            this.IsFreeShipping = IsFreeShipping;
        }

        private String ProductBigLink;

        public String getProductBigLink() {
            return this.ProductBigLink;
        }

        public void setProductBigLink(String ProductBigLink) {
            this.ProductBigLink = ProductBigLink;
        }

        private boolean IsReadyToShip;

        public boolean getIsReadyToShip() {
            return this.IsReadyToShip;
        }

        public void setIsReadyToShip(boolean IsReadyToShip) {
            this.IsReadyToShip = IsReadyToShip;
        }

        private int ActualPrice;

        public int getActualPrice() {
            return this.ActualPrice;
        }

        public void setActualPrice(int ActualPrice) {
            this.ActualPrice = ActualPrice;
        }

        private int SalePrice;

        public int getSalePrice() {
            return this.SalePrice;
        }

        public void setSalePrice(int SalePrice) {
            this.SalePrice = SalePrice;
        }

        private int DeliveryDays;

        public int getDeliveryDays() {
            return this.DeliveryDays;
        }

        public void setDeliveryDays(int DeliveryDays) {
            this.DeliveryDays = DeliveryDays;
        }

        private int CashBackAmount;

        public int getCashBackAmount() {
            return this.CashBackAmount;
        }

        public void setCashBackAmount(int CashBackAmount) {
            this.CashBackAmount = CashBackAmount;
        }

        private int PromotionTypeID;

        public int getPromotionTypeID() {
            return this.PromotionTypeID;
        }

        public void setPromotionTypeID(int PromotionTypeID) {
            this.PromotionTypeID = PromotionTypeID;
        }

        private int Is7DD;

        public int getIs7DD() {
            return this.Is7DD;
        }

        public void setIs7DD(int Is7DD) {
            this.Is7DD = Is7DD;
        }

        private String ListingImage;

        public String getListingImage() {
            return this.ListingImage;
        }

        public void setListingImage(String ListingImage) {
            this.ListingImage = ListingImage;
        }

        private boolean IsProductAnimation;

        public boolean getIsProductAnimation() {
            return this.IsProductAnimation;
        }

        public void setIsProductAnimation(boolean IsProductAnimation) {
            this.IsProductAnimation = IsProductAnimation;
        }

        private String PromotionsIcon;

        public String getPromotionsIcon() {
            return this.PromotionsIcon;
        }

        public void setPromotionsIcon(String PromotionsIcon) {
            this.PromotionsIcon = PromotionsIcon;
        }

        private boolean IsInWishlist;

        public boolean getIsInWishlist() {
            return this.IsInWishlist;
        }

        public void setIsInWishlist(boolean IsInWishlist) {
            this.IsInWishlist = IsInWishlist;
        }

        private String DispatchDateDisplay;

        public String getDispatchDateDisplay() {
            return this.DispatchDateDisplay;
        }

        public void setDispatchDateDisplay(String DispatchDateDisplay) {
            this.DispatchDateDisplay = DispatchDateDisplay;
        }

        private boolean StrikeActualPrice;

        public boolean getStrikeActualPrice() {
            return this.StrikeActualPrice;
        }

        public void setStrikeActualPrice(boolean StrikeActualPrice) {
            this.StrikeActualPrice = StrikeActualPrice;
        }

        private String Exclusive;

        public String getExclusive() {
            return this.Exclusive;
        }

        public void setExclusive(String Exclusive) {
            this.Exclusive = Exclusive;
        }

        private boolean IsCustomizable;

        public boolean getIsCustomizable() {
            return this.IsCustomizable;
        }

        public void setIsCustomizable(boolean IsCustomizable) {
            this.IsCustomizable = IsCustomizable;
        }

        private String OfferImageUrl;

        public String getOfferImageUrl() {
            return this.OfferImageUrl;
        }

        public void setOfferImageUrl(String OfferImageUrl) {
            this.OfferImageUrl = OfferImageUrl;
        }

        private String AssuredTitle;

        public String getAssuredTitle() {
            return this.AssuredTitle;
        }

        public void setAssuredTitle(String AssuredTitle) {
            this.AssuredTitle = AssuredTitle;
        }

        private String DoodleImageListing;

        public String getDoodleImageListing() {
            return this.DoodleImageListing;
        }

        public void setDoodleImageListing(String DoodleImageListing) {
            this.DoodleImageListing = DoodleImageListing;
        }

        private Object AlternateTextListing;

        public Object getAlternateTextListing() {
            return this.AlternateTextListing;
        }

        public void setAlternateTextListing(Object AlternateTextListing) {
            this.AlternateTextListing = AlternateTextListing;
        }

        private String Ethnostylist;

        public String getEthnostylist() {
            return this.Ethnostylist;
        }

        public void setEthnostylist(String Ethnostylist) {
            this.Ethnostylist = Ethnostylist;
        }
    }

    public class Value {
        private String Value;

        public String getValue() {
            return this.Value;
        }

        public void setValue(String Value) {
            this.Value = Value;
        }

        private String Value2;

        public String getValue2() {
            return this.Value2;
        }

        public void setValue2(String Value2) {
            this.Value2 = Value2;
        }

        private String DisplayName;

        public String getDisplayName() {
            return this.DisplayName;
        }

        public void setDisplayName(String DisplayName) {
            this.DisplayName = DisplayName;
        }

        private int Count;

        public int getCount() {
            return this.Count;
        }

        public void setCount(int Count) {
            this.Count = Count;
        }

        private String Href;

        public String getHref() {
            return this.Href;
        }

        public void setHref(String Href) {
            this.Href = Href;
        }

        private boolean IsSelected;

        public boolean getIsSelected() {
            return this.IsSelected;
        }

        public void setIsSelected(boolean IsSelected) {
            this.IsSelected = IsSelected;
        }

        private int Priority;

        public int getPriority() {
            return this.Priority;
        }

        public void setPriority(int Priority) {
            this.Priority = Priority;
        }
    }

    public class Facet {
        private String Name;

        public String getName() {
            return this.Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        private String Heading;

        public String getHeading() {
            return this.Heading;
        }

        public void setHeading(String Heading) {
            this.Heading = Heading;
        }

        private int Style;

        public int getStyle() {
            return this.Style;
        }

        public void setStyle(int Style) {
            this.Style = Style;
        }

        private int FacetType;

        public int getFacetType() {
            return this.FacetType;
        }

        public void setFacetType(int FacetType) {
            this.FacetType = FacetType;
        }

        private ArrayList<Value> Values;

        public ArrayList<Value> getValues() {
            return this.Values;
        }

        public void setValues(ArrayList<Value> Values) {
            this.Values = Values;
        }
    }

    public class Level1 {
        private String DisplayName;

        public String getDisplayName() {
            return this.DisplayName;
        }

        public void setDisplayName(String DisplayName) {
            this.DisplayName = DisplayName;
        }

        private String Href;

        public String getHref() {
            return this.Href;
        }

        public void setHref(String Href) {
            this.Href = Href;
        }

        private boolean IsSelected;

        public boolean getIsSelected() {
            return this.IsSelected;
        }

        public void setIsSelected(boolean IsSelected) {
            this.IsSelected = IsSelected;
        }

        private boolean IsSelectedRoot;

        public boolean getIsSelectedRoot() {
            return this.IsSelectedRoot;
        }

        public void setIsSelectedRoot(boolean IsSelectedRoot) {
            this.IsSelectedRoot = IsSelectedRoot;
        }
    }

    public class Level2 {
        private String DisplayName;

        public String getDisplayName() {
            return this.DisplayName;
        }

        public void setDisplayName(String DisplayName) {
            this.DisplayName = DisplayName;
        }

        private String Href;

        public String getHref() {
            return this.Href;
        }

        public void setHref(String Href) {
            this.Href = Href;
        }

        private boolean IsSelected;

        public boolean getIsSelected() {
            return this.IsSelected;
        }

        public void setIsSelected(boolean IsSelected) {
            this.IsSelected = IsSelected;
        }

        private boolean IsSelectedRoot;

        public boolean getIsSelectedRoot() {
            return this.IsSelectedRoot;
        }

        public void setIsSelectedRoot(boolean IsSelectedRoot) {
            this.IsSelectedRoot = IsSelectedRoot;
        }
    }

    public class Level3 {
        private String DisplayName;

        public String getDisplayName() {
            return this.DisplayName;
        }

        public void setDisplayName(String DisplayName) {
            this.DisplayName = DisplayName;
        }

        private String Href;

        public String getHref() {
            return this.Href;
        }

        public void setHref(String Href) {
            this.Href = Href;
        }

        private boolean IsSelected;

        public boolean getIsSelected() {
            return this.IsSelected;
        }

        public void setIsSelected(boolean IsSelected) {
            this.IsSelected = IsSelected;
        }

        private boolean IsSelectedRoot;

        public boolean getIsSelectedRoot() {
            return this.IsSelectedRoot;
        }

        public void setIsSelectedRoot(boolean IsSelectedRoot) {
            this.IsSelectedRoot = IsSelectedRoot;
        }
    }

    public class Level4 {
        private String DisplayName;

        public String getDisplayName() {
            return this.DisplayName;
        }

        public void setDisplayName(String DisplayName) {
            this.DisplayName = DisplayName;
        }

        private String Href;

        public String getHref() {
            return this.Href;
        }

        public void setHref(String Href) {
            this.Href = Href;
        }

        private boolean IsSelected;

        public boolean getIsSelected() {
            return this.IsSelected;
        }

        public void setIsSelected(boolean IsSelected) {
            this.IsSelected = IsSelected;
        }

        private boolean IsSelectedRoot;

        public boolean getIsSelectedRoot() {
            return this.IsSelectedRoot;
        }

        public void setIsSelectedRoot(boolean IsSelectedRoot) {
            this.IsSelectedRoot = IsSelectedRoot;
        }
    }

    public class Hierarchy {
        private ArrayList<Level1> Level1;

        public ArrayList<Level1> getLevel1() {
            return this.Level1;
        }

        public void setLevel1(ArrayList<Level1> Level1) {
            this.Level1 = Level1;
        }

        private ArrayList<Level2> Level2;

        public ArrayList<Level2> getLevel2() {
            return this.Level2;
        }

        public void setLevel2(ArrayList<Level2> Level2) {
            this.Level2 = Level2;
        }

        private ArrayList<Level3> Level3;

        public ArrayList<Level3> getLevel3() {
            return this.Level3;
        }

        public void setLevel3(ArrayList<Level3> Level3) {
            this.Level3 = Level3;
        }

        private ArrayList<Level4> Level4;

        public ArrayList<Level4> getLevel4() {
            return this.Level4;
        }

        public void setLevel4(ArrayList<Level4> Level4) {
            this.Level4 = Level4;
        }
    }
}
