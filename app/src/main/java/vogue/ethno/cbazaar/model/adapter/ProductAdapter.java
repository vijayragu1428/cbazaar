package vogue.ethno.cbazaar.model.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.model.ProductModel;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    ArrayList<ProductModel> productList;
    Context mContext;

    public ProductAdapter(ArrayList<ProductModel> productList, Context mContext) {
        this.productList = productList;
        this.mContext = mContext;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.product_adapter, parent, false);
        return new ProductAdapter.ProductViewHolder(viewGroup);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        ProductModel product = productList.get(position);
        holder.imageView.setBackgroundResource(product.getImageResource());
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        ProductViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_product);
        }
    }
}
