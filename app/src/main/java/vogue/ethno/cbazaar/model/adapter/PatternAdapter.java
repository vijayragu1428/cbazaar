package vogue.ethno.cbazaar.model.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.model.PatternModel;

public class PatternAdapter extends RecyclerView.Adapter<PatternAdapter.PatternViewHolder> {

    ArrayList<PatternModel> patternList;
    Context mContext;

    public PatternAdapter(ArrayList<PatternModel> patternList, Context mContext) {
        this.patternList = patternList;
        this.mContext = mContext;
    }

    @Override
    public PatternAdapter.PatternViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.pattern_adapter, parent, false);
        return new PatternAdapter.PatternViewHolder(viewGroup);
    }

    @Override
    public void onBindViewHolder(PatternAdapter.PatternViewHolder holder, int position) {
        PatternModel product = patternList.get(position);
        holder.imageView.setBackgroundResource(product.getImageResource());
    }

    @Override
    public int getItemCount() {
        return patternList.size();
    }

    class PatternViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        PatternViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_pattern);
        }
    }
}
