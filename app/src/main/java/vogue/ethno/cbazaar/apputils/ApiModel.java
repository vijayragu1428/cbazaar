package vogue.ethno.cbazaar.apputils;

public class ApiModel {
    private static ApiModel instance;
    public static String BASE_URL = "https://testapi.cbazaar.com/";
    public static String BASE_IMAGE_URL = "https://images.cbazaar.com/images/";
    public static String USER_NAME = "";
    public static String PASSWORD = "";
    public static String GRAND_TYPE = "";
    public static String CLIENT_ID = "";

    public ApiModel() {
        init();
    }

    public static ApiModel getInstance() {
        if (null == instance) {
            instance = new ApiModel();
        }
        return instance;
    }

    public ApiModel init() {
        if (true)
            setLiveServer();
        else
            setTestServer();
        setCommonUrl();
        return this;
    }

    public void clear() {
        instance = null;
        getInstance();
    }

    private void setLiveServer() {
        BASE_URL = "https://testapi.cbazaar.com/";
        USER_NAME = "cbazaarserviceconsumer";
        PASSWORD = "cbsc#1@";
        GRAND_TYPE = "password";
        CLIENT_ID = "f52ab395e98749c9a1f0dc531eaa98f0";
    }

    private void setTestServer() {
        BASE_URL = "https://testapi.cbazaar.com/";
        USER_NAME = "cbazaarserviceconsumer";
        PASSWORD = "cbsc#1@";
        GRAND_TYPE = "password";
        CLIENT_ID = "f52ab395e98749c9a1f0dc531eaa98f0";
    }

    private void setCommonUrl() {

    }
}
