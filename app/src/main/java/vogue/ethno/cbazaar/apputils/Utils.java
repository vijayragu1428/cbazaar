package vogue.ethno.cbazaar.apputils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import java.text.DecimalFormat;

import vogue.ethno.cbazaar.app.CbzaarApplication;

public class Utils {
    public static boolean isReallyConnectedToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) CbzaarApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
         return false;
    }

    public static void appLog(String TAG, String msg) {
        Log.d(cutNull(TAG), cutNull(msg));
    }

    public static String cutNull(Object o) {
        return o == null ? "" : o.toString().trim().equalsIgnoreCase("") ? "" :
                o.toString().trim().equalsIgnoreCase("null") ? "" : o.toString();
    }

    @SuppressLint("DefaultLocale")
    public static String getAmountString(double cost) {
        try {
            DecimalFormat twoPlaces = new DecimalFormat("0");
            String amt = twoPlaces.format(cost);
            return "\u20B9" + amt;//+ String.format("%,.2f", amt);
        } catch (Exception e) {
            return cutNull(String.valueOf(cost));
        }
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}
