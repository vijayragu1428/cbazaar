package vogue.ethno.cbazaar.apputils;

public interface GlobalConstant {
    String ESTORE_ID = "10";
    String PLEASE_TRY_AGAIN = "";
    int TOKEN_ID = -1;
    String REDIRECT_URL = "REDIRECT_URL";
    String PRODUCT_CODE = "PRODUCT_CODE";
    String PRODUCT_NAME = "PRODUCT_NAME";
    String PRODUCT_DETAILS = "PRODUCT_DETAILS";
 }
