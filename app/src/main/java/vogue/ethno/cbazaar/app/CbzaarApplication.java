package vogue.ethno.cbazaar.app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import vogue.ethno.cbazaar.apputils.ApiModel;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.data.network.Network;
import vogue.ethno.cbazaar.data.network.RetrofitService;

public class CbzaarApplication extends Application implements Application.ActivityLifecycleCallbacks {
    public static RetrofitService retrofitService;
    public static Context getContext;
    public static String productId = "";
    public static int size = 0;
    public static String cartCount="0";
    public String fromWhere;
    @Override
    public void onCreate() {
        super.onCreate();
        getContext = this;
        retrofitService = new Network().getStaticService();
        new ApiModel();
    }

    public static Context getContext() {
        if (getContext == null)
            getContext = new CbzaarApplication();
        return getContext;
    }

    public static RetrofitService getRetrofitService() {
        if (retrofitService == null)
            retrofitService = new Network().getStaticService();
        return retrofitService;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        Utils.appLog("currentActivity", activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}
