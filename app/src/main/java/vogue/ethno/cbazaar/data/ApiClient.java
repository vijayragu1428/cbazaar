package vogue.ethno.cbazaar.data;

import java.util.HashMap;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.app.CbzaarApplication;
import vogue.ethno.cbazaar.apputils.ApiModel;
import vogue.ethno.cbazaar.apputils.GlobalConstant;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.classmodel.requestmodel.address.AddressRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.addtocart.AddToCartRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.bodyheight.BodyHeightRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.confirmcart.ConfirmCartRequest;
import vogue.ethno.cbazaar.classmodel.requestmodel.enquiryform.EnquiryFormRequest;
import vogue.ethno.cbazaar.classmodel.requestmodel.getcartdetails.GetCartRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.otpverified.VerifyOtpRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.readysize.NaturalWaistRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.resent.ReSendOtpRequest;
import vogue.ethno.cbazaar.classmodel.requestmodel.storecreatepay.StoreCreatePayRequest;
import vogue.ethno.cbazaar.classmodel.requestmodel.topstyle.TopStyleRequestModel;
import vogue.ethno.cbazaar.data.localdb.pref.AppPreference;
import vogue.ethno.cbazaar.data.network.HeaderValues;
import vogue.ethno.cbazaar.data.network.Network;
import vogue.ethno.cbazaar.data.network.NetworkCallback;
import vogue.ethno.cbazaar.data.network.RetrofitService;

public class ApiClient implements Callback {
    private RetrofitService service;
    private NetworkCallback callback;
    private int api = 0;

    public ApiClient(NetworkCallback service) {
        this.service = CbzaarApplication.getRetrofitService();
        this.callback = service;
    }

    @Override
    public void onResponse(Call call, Response response) {
        if (response.isSuccessful())
            callback.onSuccess(response.body(), api);
        else if (response.code() == 401 || response.message().contains("Authorization has been denied for this request")) {
            HeaderValues.ACESS_TOKEN = "";
            getAccessToken();
        } else callback.onErrorBody(response.errorBody(), api);
    }


    @Override
    public void onFailure(Call call, Throwable t) {
        if (!Utils.isReallyConnectedToInternet())
            callback.onFailure(CbzaarApplication.getContext().getResources().getString(R.string.no_internet));
        else if (t instanceof TimeoutException)
            callback.onFailure(CbzaarApplication.getContext().getResources().getString(R.string.some_thing_went_wrong));
        else
            callback.onFailure(t.getMessage());
    }

    public void getAccessToken() {
        this.api = GlobalConstant.TOKEN_ID;
        service.getAccessToken(ApiModel.USER_NAME, ApiModel.PASSWORD, ApiModel.GRAND_TYPE, ApiModel.CLIENT_ID).enqueue(this);
    }

    public void getProduct(int which, String id) {
        this.api = which;
        service.getProductDetails(id).enqueue(this);
    }

    public void getBodyHeight(int which, BodyHeightRequestModel requestModel) {
        this.api = which;
        Network.getDynamicHeaderService().getBodyHeight(HeaderValues.getHeader(), requestModel).enqueue(this);
    }

    public void getHomeList(int which) {
        this.api = which;
        service.getHomeDetails().enqueue(this);
    }

    public void getNatualWaist(NaturalWaistRequestModel waist, int whichApi) {
        this.api = whichApi;
        Network.getDynamicHeaderService().getNaturalWaist(HeaderValues.getHeader(), waist).enqueue(this);

    }

    public void getProductList(int which, String redirectUrl) {
        this.api = which;
        service.getProductListDetails(redirectUrl).enqueue(this);
    }

    public void geTopStyle(int whichApi, TopStyleRequestModel topStyleData) {
        this.api = whichApi;
        Network.getDynamicHeaderService().getTopStyle(HeaderValues.getHeader(), topStyleData).enqueue(this);
    }

    public void geMenuList(int whichApi) {
        this.api = whichApi;
        service.getMenuDetails().enqueue(this);
    }

    public void getCircumference(int whichApi, String waiseCircumferenceId) {
        this.api = whichApi;
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Type", "waistcircumference");
        hashMap.put("Value", waiseCircumferenceId);
        Network.getDynamicHeaderService().getCircumference(HeaderValues.getHeader(), hashMap).enqueue(this);
    }

    public void getWaistFloor(int whichApi, String waiseCircumferenceId) {
        this.api = whichApi;
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Type", "waisttofloor");
        hashMap.put("Value", waiseCircumferenceId);
        Network.getDynamicHeaderService().getCircumference(HeaderValues.getHeader(), hashMap).enqueue(this);
    }

    public void addCartItem(int whichApi, AddToCartRequestModel addToCartResquestModel) {
        this.api = whichApi;
        Network.getDynamicHeaderService().addToCart(HeaderValues.getHeader(), addToCartResquestModel).enqueue(this);
    }

    public void getCartDetails(int whichApi, GetCartRequestModel cartData) {
        this.api = whichApi;
        Network.getDynamicHeaderService().getCartDetails(HeaderValues.getHeader(), cartData).enqueue(this);
    }

    public void removeCart(int whichApi, GetCartRequestModel s) {
        this.api = whichApi;
        Network.getDynamicHeaderService().removeCart(HeaderValues.getHeader(), s).enqueue(this);
    }

    public void updateCart(int whichApi, GetCartRequestModel s) {
        this.api = whichApi;
        Network.getDynamicHeaderService().updateCart(HeaderValues.getHeader(), s).enqueue(this);
    }

    public void getAddress(int whichApi, HashMap<String, Object> cartAddressData) {
        this.api = whichApi;
        Network.getDynamicHeaderService().address(HeaderValues.getHeader(), AppPreference.getPrefsHelper().getPref(AppPreference.USER_ID,""), cartAddressData).enqueue(this);
    }

    public void addressOperation(int whichApi, AddressRequestModel addressRequestModel) {
        this.api = whichApi;
        Network.getDynamicHeaderService().addressOperation(HeaderValues.getHeader(), AppPreference.getPrefsHelper().getPref(AppPreference.USER_ID,""), addressRequestModel).enqueue(this);
    }

    public void submitEnquiry(int which, EnquiryFormRequest enquiryFormRequest) {
        this.api = which;
        Network.getDynamicHeaderService().enquiryForm(HeaderValues.getHeader(), enquiryFormRequest).enqueue(this);
    }

    public void agentLogin(int whichApi, String loginId, String password, boolean isGuest) {
        this.api = whichApi;
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("LoginId", loginId);
        hashMap.put("Password", password);
        hashMap.put("IsGuest", isGuest);
        Network.getDynamicHeaderService().agentLogin(HeaderValues.getHeader(), hashMap).enqueue(this);
    }

    public void getStateList(int whichApi) {
        this.api = whichApi;
        service.getStateList().enqueue(this);
    }


    public void confirmCart(int whichApi, ConfirmCartRequest confirmCartRequest) {
        this.api = whichApi;
        Network.getDynamicHeaderService().confirmCart(HeaderValues.getHeader(), AppPreference.getPrefsHelper().getPref(AppPreference.USER_ID,""), confirmCartRequest).enqueue(this);
    }

    public void userLogin(int whichApi, String actionMode, String password, boolean isGuest) {
        this.api = whichApi;
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("ActionMode", actionMode);
        hashMap.put("Password", password);
        hashMap.put("IsGuest", isGuest);
        Network.getDynamicHeaderService().userLogin(HeaderValues.getHeader(), hashMap).enqueue(this);
    }

    public void cashOndelivery(int whichApi, HashMap<String, Object> hashMap) {
        this.api = whichApi;
        Network.getDynamicHeaderService().cashOnDelivery(HeaderValues.getHeader(), AppPreference.getPrefsHelper().getPref(AppPreference.USER_ID,""), hashMap).enqueue(this);

    }

    public void verifyOtp(int whichApi, VerifyOtpRequestModel verifyOtpRequestModel) {
        this.api = whichApi;
        Network.getDynamicHeaderService().verifyOtp(HeaderValues.getHeader(), AppPreference.getPrefsHelper().getPref(AppPreference.USER_ID,""), verifyOtpRequestModel).enqueue(this);

    }

    public void storeCretePayment(int whichApi, StoreCreatePayRequest storeCreatePayRequest) {
        this.api = whichApi;
        Network.getDynamicHeaderService().storeCreatePayment(HeaderValues.getHeader(), AppPreference.getPrefsHelper().getPref(AppPreference.USER_ID,""), storeCreatePayRequest).enqueue(this);

    }

    public void userRegister(int whichApi, String password, boolean isGuest) {
        this.api = whichApi;
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Password", password);
        hashMap.put("IsGuest", isGuest);
        Network.getDynamicHeaderService().userRegister(HeaderValues.getHeader(), hashMap).enqueue(this);
    }
    public void resendOtp(int whichApi, ReSendOtpRequest storeCreatePayRequest) {
        this.api = whichApi;
        Network.getDynamicHeaderService().reSendOtp(HeaderValues.getHeader(), storeCreatePayRequest).enqueue(this);

    }

    public void getRsaKey(int whichApi,String orderId) {
        this.api = whichApi;
        Network.getDynamicHeaderService().rsaKey(HeaderValues.getHeader(), orderId,"AVSI07CL98BN36ISNB").enqueue(this);
    }
}


///365565     572610
