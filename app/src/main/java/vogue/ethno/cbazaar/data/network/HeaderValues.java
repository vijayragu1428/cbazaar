package vogue.ethno.cbazaar.data.network;

import android.provider.Settings;

import java.util.HashMap;
import java.util.UUID;

import vogue.ethno.cbazaar.BuildConfig;
import vogue.ethno.cbazaar.app.CbzaarApplication;
import vogue.ethno.cbazaar.data.localdb.pref.AppPreference;

public class HeaderValues {

    public static String ACESS_TOKEN = "";
    public static String DEVICE_ID = Settings.Secure.getString(CbzaarApplication.getContext().getContentResolver(),Settings.Secure.ANDROID_ID);
    public static String TOKEN_TYPE = "";
    public static String UNIQUE_ID = UUID.randomUUID().toString().replaceAll("-","");
    public static String E_STORE_ID = "10";
   // public static String EMAIL = "karthick.karthick901@gmail.com";
    public static String EMAIL = AppPreference.getPrefsHelper().getPref(AppPreference.USER_MAIL_ID,"");
    public static String COUNTRY_CODE = "IN";
    public static String CURRENCY_CODE = "INR";
    public static String CURRENCY_FACTOR = "38";
    public static String CURRENCY_SYMBOL = "Rs";
    public static String REGION = "IN";
    public static String APP_VERSION = String.valueOf(BuildConfig.VERSION_CODE);
    public static String APP_SOURCE = "9";
    public static String USER_ID = AppPreference.getPrefsHelper().getPref(AppPreference.USER_ID,"");

    public static HashMap<String, Object> getHeader() {
        HashMap<String, Object> hashMap = new HashMap<>(0);
        hashMap.put("Content-Type", "application/json");
        hashMap.put("Authorization", TOKEN_TYPE + " " + ACESS_TOKEN);
        hashMap.put("X-Device-ID", DEVICE_ID);
        hashMap.put("X-Unique-ID", UNIQUE_ID);
        hashMap.put("X-Country-Code", COUNTRY_CODE);
        hashMap.put("X-Currency-Code", CURRENCY_CODE);
        hashMap.put("X-EStore-ID", E_STORE_ID);
        hashMap.put("X-App-Version", APP_VERSION);
        hashMap.put("X-Currency-Symbol", CURRENCY_SYMBOL);
        hashMap.put("X-Currency-Factor", CURRENCY_FACTOR);
        hashMap.put("X-App-Source", APP_SOURCE);
        hashMap.put("X-Region", REGION);
        hashMap.put("X-Email", EMAIL);
       // hashMap.put("X-User-ID", USER_ID);
        return hashMap;
    }
}
