package vogue.ethno.cbazaar.data.localdb.room.login;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface LoginDao {
    @Query("SELECT * FROM loginmodel")
    List<LoginModel> getAllLoginDetails();

}
