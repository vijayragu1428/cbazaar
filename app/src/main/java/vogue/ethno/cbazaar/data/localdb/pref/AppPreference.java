package vogue.ethno.cbazaar.data.localdb.pref;

import android.content.Context;
import android.content.SharedPreferences;

import vogue.ethno.cbazaar.app.CbzaarApplication;

import static android.content.Context.MODE_PRIVATE;

public class AppPreference {
    public static String AGENT_ID = "AGENT_ID";
    public static String USER_MAIL_ID = "USER_MAIL_ID";
    public static String USER_LOGIN_DATA = "USER_LOGIN_DATA";
    public static String USER_ID = "USER_ID";
    public static String AGENT_NAME = "AGENT_NAME";
    public static String AGENT_MOBILE_NUMBER = "AGENT_MOBILE_NUMBER";
    private final SharedPreferences.Editor editor;
    private SharedPreferences sharedPreferences;
    public static AppPreference instance;

    public AppPreference(Context context) {
        String prefsFile = context.getPackageName();
        sharedPreferences = context.getSharedPreferences(prefsFile, MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public static AppPreference getPrefsHelper() {
        if (null == instance)
            instance = new AppPreference(CbzaarApplication.getContext());
        return instance;
    }

    public void delete(String key) {

        if (sharedPreferences.contains(key)) {
            editor.remove(key).commit();
        }
    }

    public void savePref(String key, Object value) {
        getPrefsHelper();
        delete(key);
        if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Enum) {
            editor.putString(key, value.toString());
        } else if (value != null) {
            throw new RuntimeException("Attempting to save non-primitive preference");
        }
        editor.commit();
    }


    public <T> T getPref(String key, T defValue) {
        getPrefsHelper();
        T returnValue = (T) sharedPreferences.getAll().get(key);
        return returnValue == null ? defValue : returnValue;
    }

    public void clearPref() {
        editor.clear().commit();
    }
}
