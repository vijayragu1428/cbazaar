package vogue.ethno.cbazaar.data.network;

public interface NetworkCallback {

    void onSuccess(Object o, int api);

    void onFailure(String body);

    void onErrorBody(Object o, int api);


 }
