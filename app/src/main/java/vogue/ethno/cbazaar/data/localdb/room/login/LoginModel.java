package vogue.ethno.cbazaar.data.localdb.room.login;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;

@Entity
public class LoginModel implements Serializable {

    @PrimaryKey
    @NonNull
    private int ID;
    private String Name;
    private boolean IsRegisteredWithPassword;
    private String Message;
    private String Status;
    private String RfmValue;
    private boolean IsGuideShopUser;
    private ArrayList<Object> TrackNumbers;

    public LoginModel(@NonNull int ID, String name, boolean isRegisteredWithPassword,
                      String message, String status, String rfmValue, boolean isGuideShopUser,
                      ArrayList<Object> trackNumbers) {
        this.ID = ID;
        Name = name;
        IsRegisteredWithPassword = isRegisteredWithPassword;
        Message = message;
        Status = status;
        RfmValue = rfmValue;
        IsGuideShopUser = isGuideShopUser;
        TrackNumbers = trackNumbers;
    }

    public int getID() {
        return this.ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return this.Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public boolean getIsRegisteredWithPassword() {
        return this.IsRegisteredWithPassword;
    }

    public void setIsRegisteredWithPassword(boolean IsRegisteredWithPassword) {
        this.IsRegisteredWithPassword = IsRegisteredWithPassword;
    }

    public String getMessage() {
        return this.Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getStatus() {
        return this.Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }


    public String getRfmValue() {
        return this.RfmValue;
    }


    public void setRfmValue(String RfmValue) {
        this.RfmValue = RfmValue;
    }

    public ArrayList<Object> getTrackNumbers() {
        return this.TrackNumbers;
    }

    public boolean getIsGuideShopUser() {
        return this.IsGuideShopUser;
    }

    public void setIsGuideShopUser(boolean IsGuideShopUser) {
        this.IsGuideShopUser = IsGuideShopUser;
    }

    public void setTrackNumbers(ArrayList<Object> TrackNumbers) {
        this.TrackNumbers = TrackNumbers;
    }
}
