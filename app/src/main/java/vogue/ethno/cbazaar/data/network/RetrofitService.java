package vogue.ethno.cbazaar.data.network;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Path;
import vogue.ethno.cbazaar.classmodel.requestmodel.address.AddressRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.addtocart.AddToCartRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.bodyheight.BodyHeightRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.confirmcart.ConfirmCartRequest;
import vogue.ethno.cbazaar.classmodel.requestmodel.enquiryform.EnquiryFormRequest;
import vogue.ethno.cbazaar.classmodel.requestmodel.getcartdetails.GetCartRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.otpverified.VerifyOtpRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.readysize.NaturalWaistRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.resent.ReSendOtpRequest;
import vogue.ethno.cbazaar.classmodel.requestmodel.storecreatepay.StoreCreatePayRequest;
import vogue.ethno.cbazaar.classmodel.requestmodel.topstyle.TopStyleRequestModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.accesstoken.TokenResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.addaddress.AddAddressResponse;
import vogue.ethno.cbazaar.classmodel.responsemodel.addtocart.AddToCartDetailsResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.bodyheight.BodyHeightResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.cashondelivery.CashOnDelivryResponse;
import vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart.ConfirmCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.getaddress.AddressResponse;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.ProductDetailsResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.topstyle.TopStyleResponse;
import vogue.ethno.cbazaar.classmodel.responsemodel.userlogin.UserLoginResponse;
import vogue.ethno.cbazaar.classmodel.responsemodel.verifyotp.VerifyOtpResponse;
import vogue.ethno.cbazaar.model.HomeDetailsResponseModel;
import vogue.ethno.cbazaar.model.ProductListModel;
import vogue.ethno.cbazaar.view.menu.MenuModel;

public interface RetrofitService {

    @GET("/users/{user}/repos")
    Call<List<Object>> getUserRepos(@Path("user") String user);

    @FormUrlEncoded
    @POST("oauth2/token")
    Call<TokenResponseModel> getAccessToken(@Field("username") String userName,
                                            @Field("password") String password,
                                            @Field("grant_type") String grandtype,
                                            @Field("client_id") String clintId);

    @GET("products/products-detail/{id}")
    Call<ProductDetailsResponseModel> getProductDetails(@Path("id") String id);

    @POST("products/get-body-height")
    Call<BodyHeightResponseModel> getBodyHeight(@HeaderMap HashMap<String, Object> map, @Body BodyHeightRequestModel requestModel);

    @GET("evstore/get-homepage")
    Call<HomeDetailsResponseModel> getHomeDetails();

    @POST("products/get-readysize-inches")
    Call<JsonArray> getNaturalWaist(@HeaderMap HashMap<String, Object> map, @Body NaturalWaistRequestModel requestModel);

    @GET("productsv2/get")
    Call<ProductListModel> getProductListDetails(@Header("X-Url") String redirectUrl);

    @POST("products/get-top-style")
    Call<TopStyleResponse> getTopStyle(@HeaderMap HashMap<String, Object> map, @Body TopStyleRequestModel requestModel);

    @GET("evstore/get-menu")
    Call<MenuModel> getMenuDetails();

    @POST("products/get-readysize-inches")
    Call<JsonArray> getCircumference(@HeaderMap HashMap<String, Object> map, @Body HashMap<String, Object> requestModel);

    @POST("cart/add-to-cart")
    Call<AddToCartDetailsResponseModel> addToCart(@HeaderMap HashMap<String, Object> map, @Body AddToCartRequestModel toCartRequestModel);

    @POST("cartv2/get-cart")
    Call<GetCartResponseModel> getCartDetails(@HeaderMap HashMap<String, Object> map, @Body GetCartRequestModel toCartRequestModel);

    @POST("cartv2/get-cart")
    Call<GetCartResponseModel> removeCart(@HeaderMap HashMap<String, Object> map, @Body GetCartRequestModel toCartRequestModel);

    @POST("cartv2/get-cart")
    Call<GetCartResponseModel> updateCart(@HeaderMap HashMap<String, Object> map, @Body GetCartRequestModel toCartRequestModel);

    @POST("cartv2/get-address-detail")
    Call<AddressResponse> address(@HeaderMap HashMap<String, Object> map, @Header("X-User-ID") String id, @Body HashMap<String, Object> toCartRequestModel);

    @GET("cartv2/get-ccavenue-rsakey/{OrderNumber}/{paymentgateway}")
    Call<JsonArray> rsaKey(@HeaderMap HashMap<String, Object> map, @Path("OrderNumber") String id,@Path("paymentgateway") String paymentFate);

    //@FormUrlEncoded
    @POST("user/enquiry-form")
    Call<JsonObject> enquiryForm(@HeaderMap HashMap<String, Object> map, @Body EnquiryFormRequest toEnquiryFormRequestModel);

    @POST("/cartv2/address-operations")
    Call<AddAddressResponse> addressOperation(@HeaderMap HashMap<String, Object> map, @Header("X-User-ID") String id, @Body AddressRequestModel addressRequestModel);

    @POST("/cartv2/confirm-cart")
    Call<ConfirmCartResponseModel> confirmCart(@HeaderMap HashMap<String, Object> map, @Header("X-User-ID") String id, @Body ConfirmCartRequest addressRequestModel);

    @POST("user/agent-login")
    Call<JsonObject> agentLogin(@HeaderMap HashMap<String, Object> map, @Body HashMap<String, Object> requestModel);

    @GET("localization/get-states")
    Call<JsonArray> getStateList();

    @POST("user/login")
    Call<UserLoginResponse> userLogin(@HeaderMap HashMap<String, Object> map, @Body HashMap<String, Object> requestModel);


    @POST("/cartv2/selected-payment-gateway")
    Call<CashOnDelivryResponse> cashOnDelivery(@HeaderMap HashMap<String, Object> map, @Header("X-User-ID") String id, @Body HashMap<String, Object> hashMap);

    @POST("/cartv2/verify-otp")
    Call<VerifyOtpResponse> verifyOtp(@HeaderMap HashMap<String, Object> map, @Header("X-User-ID") String id, @Body VerifyOtpRequestModel verifyOtpResponseModel);

    @POST("/cartv2/use-store-credit")
    Call<AddAddressResponse> storeCreatePayment(@HeaderMap HashMap<String, Object> map, @Header("X-User-ID") String id, @Body StoreCreatePayRequest verifyOtpResponseModel);

    @POST("user/register")
    Call<UserLoginResponse> userRegister(@HeaderMap HashMap<String, Object> map, @Body HashMap<String, Object> requestModel);

    @POST("/cartv2/send-otp")
    Call<UserLoginResponse> reSendOtp(@HeaderMap HashMap<String, Object> map, @Body ReSendOtpRequest requestModel);


}
