package vogue.ethno.cbazaar.data.network;


import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import vogue.ethno.cbazaar.apputils.ApiModel;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.BuildConfig;

public class Network {


    public RetrofitService getStaticService() {
        return getRetrofit(ApiModel.BASE_URL).create(RetrofitService.class);
    }


    public RetrofitService getDynamicService(String baseUrl) {
        return getRetrofit(baseUrl).create(RetrofitService.class);
    }



    private static Interceptor getNetworkInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder builder = original.newBuilder();
                Request request;
                if (Utils.cutNull(HeaderValues.ACESS_TOKEN).trim().isEmpty())
                    return chain.proceed(builder.method(original.method(), original.body()).build());
                request = builder
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Accept", "application/json")
                        .addHeader("Authorization", HeaderValues.TOKEN_TYPE + " " + HeaderValues.ACESS_TOKEN)
                        .addHeader("X-Device-ID", HeaderValues.DEVICE_ID)
                        .addHeader("X-Unique-ID", HeaderValues.UNIQUE_ID)
                        .addHeader("X-EStore-ID", HeaderValues.E_STORE_ID)
                        .addHeader("X-Email", HeaderValues.EMAIL)
                        .addHeader("X-Country-Code", HeaderValues.COUNTRY_CODE)
                        .addHeader("X-Currency-Code", HeaderValues.CURRENCY_CODE)
                        .addHeader("X-Currency-Factor", HeaderValues.CURRENCY_FACTOR)
                        .addHeader("X-Currency-Symbol", HeaderValues.CURRENCY_SYMBOL)
                        .addHeader("X-Region", HeaderValues.REGION)
                        .addHeader("X-App-Version", HeaderValues.APP_VERSION)
                        .addHeader("X-App-Source", HeaderValues.APP_SOURCE)
                        .method(original.method(), original.body())
                        .build();
                return chain.proceed(request);
            }

            ;
        };
    }

    private static Interceptor getInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        return interceptor;
    }

    private Retrofit getRetrofit(String s) {
        return new Retrofit.Builder()
                .baseUrl(s)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    public static RetrofitService getDynamicHeaderService() {
        return getRetrofitForDynamicHeader(ApiModel.BASE_URL).create(RetrofitService.class);
    }

    private static Retrofit getRetrofitForDynamicHeader(String s) {
        return new Retrofit.Builder()
                .baseUrl(s)
                .client(getOkHttpDynamicHeaderClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    private static OkHttpClient getOkHttpClient() {
        try {
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.readTimeout(60, TimeUnit.SECONDS);
            builder.writeTimeout(60, TimeUnit.SECONDS);
            builder.sslSocketFactory(sslSocketFactory);
            builder.addInterceptor(getInterceptor());
            builder.addNetworkInterceptor(getNetworkInterceptor());
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static OkHttpClient getOkHttpDynamicHeaderClient() {
        try {
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.readTimeout(60, TimeUnit.SECONDS);
            builder.writeTimeout(60, TimeUnit.SECONDS);
            builder.sslSocketFactory(sslSocketFactory);
            builder.addInterceptor(getInterceptor());
            // builder.addNetworkInterceptor(getNetworkInterceptor());
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}