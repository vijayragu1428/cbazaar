package vogue.ethno.cbazaar.view.directpayment;

import android.content.Context;
import android.widget.EditText;

import java.util.HashMap;

import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.base.AbstractBasePresenter;
import vogue.ethno.cbazaar.classmodel.requestmodel.getcartdetails.GetCartRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.otpverified.VerifyOtpRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.resent.ReSendOtpRequest;
import vogue.ethno.cbazaar.classmodel.responsemodel.accesstoken.TokenResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart.ConfirmCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.verifyotp.VerifyOtpResponse;
import vogue.ethno.cbazaar.data.ApiClient;
import vogue.ethno.cbazaar.data.localdb.pref.AppPreference;
import vogue.ethno.cbazaar.data.network.HeaderValues;
import vogue.ethno.cbazaar.data.network.NetworkCallback;

import static vogue.ethno.cbazaar.data.localdb.pref.AppPreference.AGENT_MOBILE_NUMBER;

public class DirectPaymentPresenter extends AbstractBasePresenter<DirectListener> implements NetworkCallback {
    private DirectListener view;
    private Context mContext;
    private int apiFrom = 0;
    private ApiClient apiClient;
    private GetCartRequestModel model;
    private HashMap<String, Object> hashMap;
    private VerifyOtpRequestModel verifyOtpRequestModel;

    @Override
    public void setView(DirectListener view) {
        super.setView(view);
        this.view = view;
        this.mContext = view.getActivityContext();
        this.apiClient = new ApiClient(this);
    }


    public void apiCall(int whichApi) {
        loadingDialog(true);
        this.apiFrom = whichApi;
        switch (whichApi) {
            case 1:
                apiClient.verifyOtp(whichApi, verifyOtpRequestModel);
                break;
            case 2:
                apiClient.cashOndelivery(whichApi, hashMap);
                break;
            case 3:
                apiClient.resendOtp(whichApi, getResentRequest());
                break;

        }
    }

    private ReSendOtpRequest getResentRequest() {
        ReSendOtpRequest reSendOtpRequest = new ReSendOtpRequest();
        reSendOtpRequest.setMode("BOOMMALL");
        reSendOtpRequest.setAction("insert");
        reSendOtpRequest.setOperation("send");
        reSendOtpRequest.setTrackNumber(view.getTrackNumber());
        reSendOtpRequest.setOtpVerificationCode("");
        reSendOtpRequest.setMobileNumber(AppPreference.getPrefsHelper().getPref(AGENT_MOBILE_NUMBER, ""));
        return reSendOtpRequest;
    }

    public void setRequestData(EditText edtEnterOtp, ConfirmCartResponseModel cashOnDelivryResponse) {
        verifyOtpRequestModel = new VerifyOtpRequestModel();
        verifyOtpRequestModel.setMode("BOOMMALL");
        verifyOtpRequestModel.setAction("verify");
        verifyOtpRequestModel.setOperation("received");
        verifyOtpRequestModel.setTrackNumber(String.valueOf(Utils.cutNull(cashOnDelivryResponse.getTemporaryTransaction().getTrackNumber())));
        verifyOtpRequestModel.setOtpVerificationCode(edtEnterOtp.getText().toString());
        apiCall(1);
    }

    @Override
    public void onSuccess(Object o, int api) {
        if (apiFrom != -1)
            loadingDialog(false);
        try {
            if (o != null) {
                switch (apiFrom) {
                    case -1:
                        accessToken(o);
                        break;
                    case 1:
                        verifyotp(o);
                        break;
                    case 2:
                        cashOndelivery(o);
                        break;
                    case 3:
                        resendOtpSuccess(o);
                        break;

                }
            } else failureAction(mContext.getString(R.string.some_thing_went_wrong));
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void resendOtpSuccess(Object o) {
        view.resentSuccess();
    }


    private void cashOndelivery(Object o) {
        try {
            GetCartResponseModel responseModel = (GetCartResponseModel) o;

        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void verifyotp(Object o) {
        try {
            VerifyOtpResponse responseModel = (VerifyOtpResponse) o;
            if (responseModel != null) {
                if (!responseModel.isIsProceedToCOD()&&(Utils.cutNull(responseModel.getMessage()).equalsIgnoreCase("Verification Failed") || Utils.cutNull(responseModel.getMessage()).contains("Verification Failed"))){
                    failureAction(mContext.getString(R.string.verification_failed));
                }else {
                    view.proceedToCod(responseModel);
                }

                //  view.updateCartCount(responseModel);
            }
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void accessToken(Object o) {
        try {
            TokenResponseModel responseModel = (TokenResponseModel) o;
            if (responseModel != null && !cutNull(responseModel.getAccessToken()).isEmpty() && !cutNull(responseModel.getTokenType()).isEmpty()) {
                HeaderValues.ACESS_TOKEN = responseModel.getAccessToken();
                HeaderValues.TOKEN_TYPE = responseModel.getTokenType();
                apiCall(apiFrom);
            } else {
                HeaderValues.ACESS_TOKEN = "";
                HeaderValues.TOKEN_TYPE = "";
                tokenError(o);
            }
        } catch (Exception e) {
            HeaderValues.TOKEN_TYPE = "";
            HeaderValues.ACESS_TOKEN = "";
            tokenError(o);
        }
    }

    private void tokenError(Object o) {
        loadingDialog(false);
        if (view != null)
            view.sessionError("");
    }

    private String cutNull(Object o) {
        return Utils.cutNull(o);
    }

    @Override
    public void onFailure(String body) {
        failureAction(body);
    }

    @Override
    public void onErrorBody(Object o, int api) {
        loadingDialog(false);
        switch (apiFrom) {
            case -1:
                tokenError(o);
                break;
            case 1:
                failureAction(mContext.getString(R.string.some_thing_went_wrong));
                break;
            case 2:
                failureAction(mContext.getString(R.string.some_thing_went_wrong));
                break;
            case 3:
                failureAction(mContext.getString(R.string.some_thing_went_wrong));
                break;
        }
    }

    private void loadingDialog(boolean status) {
        if (view != null) {
            if (status)
                view.showLoading();
            else view.hideLoading(status);
        }
    }

    private void failureAction(String s) {
        loadingDialog(false);
        if (view != null) {
            view.showError(s);
        }
    }
}