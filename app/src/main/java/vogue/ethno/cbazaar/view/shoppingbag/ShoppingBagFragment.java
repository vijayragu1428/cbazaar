package vogue.ethno.cbazaar.view.shoppingbag;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.app.CbzaarApplication;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.CartDetailItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.CartSummaries;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;
import vogue.ethno.cbazaar.view.address.AddressFragment;
import vogue.ethno.cbazaar.view.homescreen.FragmentListener;
import vogue.ethno.cbazaar.view.homescreen.HomeActivity;
import vogue.ethno.cbazaar.view.homescreen.fragment.home.HomeFragment;
import vogue.ethno.cbazaar.view.login.userlogin.UserLoginActivity;
import vogue.ethno.cbazaar.view.shoppingbag.bottomsheetremoveitem.BottomSheetCommunicator;
import vogue.ethno.cbazaar.view.shoppingbag.bottomsheetremoveitem.RemoveItemDialogFragment;

import static vogue.ethno.cbazaar.apputils.GlobalConstant.PRODUCT_DETAILS;

public class ShoppingBagFragment extends Fragment implements ShoppingBagListener, ShoppingBagAdapter.DeleteClick, BottomSheetCommunicator {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    Unbinder unbinder;
    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.rv_bag_details)
    RecyclerView rvBagDetails;
    @BindView(R.id.tv_summary)
    TextView tvSummary;
    @BindView(R.id.txt_gross_order_total)
    TextView txtGrossOrderTotal;
    @BindView(R.id.txt_gross_order_total_amount)
    TextView txtGrossOrderTotalAmount;
    @BindView(R.id.img_hint)
    ImageView imgHint;
    @BindView(R.id.txt_product_discount)
    TextView txtProductDiscount;
    @BindView(R.id.txt_product_discount_amount)
    TextView txtProductDiscountAmount;
    @BindView(R.id.txt_order_sub_total)
    TextView txtOrderSubTotal;
    @BindView(R.id.txt_order_sub_total_amount)
    TextView txtOrderSubTotalAmount;
    @BindView(R.id.txt_shipping_charges)
    TextView txtShippingCharges;
    @BindView(R.id.txt_shipping_charges_amount)
    TextView txtShippingChargesAmount;
    @BindView(R.id.txt_order_total)
    TextView txtOrderTotal;
    @BindView(R.id.txt_order_total_amount)
    TextView txtOrderTotalAmount;
    @BindView(R.id.tv_you_have_saved)
    TextView tvYouHaveSaved;
    @BindView(R.id.lin_summary)
    LinearLayout linSummary;
    @BindView(R.id.txt_amount)
    TextView txtAmount;
    @BindView(R.id.btn_continue)
    Button btnContinue;
    @BindView(R.id.lin_total_amount)
    LinearLayout linTotalAmount;
    @BindView(R.id.rel_total_view)
    RelativeLayout relTotalView;
    @BindView(R.id.txt_something)
    TextView txtSomething;
    @BindView(R.id.txt_empty)
    TextView txtEmpty;
    @BindView(R.id.rel_empty_cart)
    RelativeLayout relEmptyCart;
 @BindView(R.id.rel_root)
    RelativeLayout rel_root;

    private FragmentListener mListener;
    private HomeActivity activity;
    private ShoppingBagPresenter shoppingBagPresenter;
    private ShoppingBagAdapter shoppingBagAdapter;
    private Context mContext;
    private HashMap<Object, String> map = new HashMap<>(0);
    private GetCartResponseModel responseModel;
    private LinearLayoutManager lastScrollPosition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shopping_bag, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            mListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
         shoppingBagPresenter.destroyView();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

    }

    private void initUi() {
        mContext = getContext();
        activity = (HomeActivity) getActivity();
        shoppingBagPresenter = new ShoppingBagPresenter();
        shoppingBagPresenter.setView(this);
        shoppingBagPresenter.apiCall(1);
        activity.showHideToolbar(true);
        toolBar.setNavigationIcon(getContext().getResources().getDrawable(R.drawable.ic_baseline_close));
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        intRv();
    }

    @SuppressLint("SetTextI18n")
    private void uiListener(GetCartResponseModel getCartResponseModel) {
        this.responseModel = getCartResponseModel;
        if (getCartResponseModel.getCartDetail() != null && getCartResponseModel.getCartDetail().size() > 0) {
            shoppingBagAdapter.loadData(getCartResponseModel.getCartDetail());
            relTotalView.setVisibility(View.VISIBLE);
            txtSomething.setVisibility(View.GONE);
            relEmptyCart.setVisibility(View.GONE);
        } else {
            relTotalView.setVisibility(View.GONE);
            txtSomething.setVisibility(View.GONE);
            relEmptyCart.setVisibility(View.VISIBLE);
            return;
        }
        if (getCartResponseModel != null && getCartResponseModel.getCartSummaries() != null) {
            final CartSummaries summaries = getCartResponseModel.getCartSummaries();
            txtGrossOrderTotalAmount.setText(Utils.getAmountString(summaries.getGrossOrderTotal()));
            txtProductDiscountAmount.setText(Utils.getAmountString(summaries.getProductDiscounts()));
            txtOrderSubTotalAmount.setText(Utils.getAmountString(summaries.getSubTotal()));
            txtOrderTotalAmount.setText(Utils.getAmountString(summaries.getGrandTotal()));
            txtAmount.setText(getString(R.string.total) + " " + Utils.getAmountString(summaries.getGrandTotal()));
            if (getCartResponseModel.getShippingCost() != null) {
                if (getCartResponseModel.getShippingCost().getShippingCost() == 0.0 || getCartResponseModel.getShippingCost().getShippingCost() == 0)
                    txtShippingChargesAmount.setText(getString(R.string.free));
                else
                    txtShippingChargesAmount.setText(Utils.getAmountString(getCartResponseModel.getShippingCost().getShippingCost()));
                if (summaries.getSavings() == 0 || summaries.getSavings() == 0.0)
                    tvYouHaveSaved.setText(getString(R.string.you_have_saved) + " " + Utils.getAmountString(0));
                else
                    tvYouHaveSaved.setText(getString(R.string.you_have_saved) + " " + Utils.getAmountString(summaries.getSavings()));
            }

        }
    }

    private void intRv() {
        shoppingBagAdapter = new ShoppingBagAdapter(this, getContext());
        lastScrollPosition = new LinearLayoutManager(mContext);
        rvBagDetails.setLayoutManager(lastScrollPosition);
        rvBagDetails.setAdapter(shoppingBagAdapter);
       /* rvBagDetails.addOnScrollListener(new RecyclerView.OnScrollListener() {
            private int pos;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                pos = lastScrollPosition.findLastCompletelyVisibleItemPosition();

                if (shoppingBagAdapter.getItemCount() - 1 != shoppingBagAdapter.pos) {
                    if (dy > 0 || dy < 0 && linSummary.isShown()) {
                        linSummary.setVisibility(View.GONE);
                    }
                }else linSummary.setVisibility(View.VISIBLE);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (shoppingBagAdapter.getItemCount() - 1 != shoppingBagAdapter.pos) {
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        linSummary.setVisibility(View.VISIBLE);
                    }
                } else linSummary.setVisibility(View.VISIBLE);
                super.onScrollStateChanged(recyclerView, newState);
            }
        });*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        activity.showHideToolbar(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.showHideToolbar(true);
    }


    @Override
    public void updateCartCount(GetCartResponseModel responseModel) {
        try {
            if (responseModel != null) {
                relTotalView.setVisibility(View.VISIBLE);
                txtSomething.setVisibility(View.GONE);


                if (responseModel != null && responseModel.getCartDetail() != null) {
                    int count = 0;
                    for (CartDetailItem detailItem : responseModel.getCartDetail()) {
                        count = count + detailItem.getProductQuantity();
                    }
                    activity.upDateCartCont(String.valueOf(count));
                } else activity.upDateCartCont("0");
                uiListener(responseModel);
            } else {
                relTotalView.setVisibility(View.GONE);
                txtSomething.setVisibility(View.VISIBLE);
                showError(getString(R.string.some_thing_went_wrong));
                mListener.nextFragment(new HomeFragment());
            }
        } catch (Exception e) {
            e.printStackTrace();
            showError(getString(R.string.some_thing_went_wrong));
            mListener.nextFragment(new HomeFragment());
        }

    }

    @Override
    public void showLoading() {
        activity.showDialog("", false,rel_root);
    }

    @Override
    public void hideLoading(boolean status) {
        activity.hideDialog(status);
    }

    @Override
    public Context getActivityContext() {
        return getContext();
    }

    @Override
    public void showError(String string) {
        activity.showToast(string);
    }


    @Override
    public void sessionError(String string) {
        mListener.nextFragment(new HomeFragment());
    }

    @Override
    public void onDeleteClick(CartDetailItem detailItem) {
        RemoveItemDialogFragment removeItemDialogFragment = new RemoveItemDialogFragment(this, detailItem, "main");
        removeItemDialogFragment.show(getActivity().getSupportFragmentManager(), "show");
    }

    @Override
    public void onSpinnerDefaultValues(String defaultQuantity, int adapterPostion) {
        map.put(adapterPostion, defaultQuantity);
    }

    @Override
    public void onSpinnerChanged(CartDetailItem detailItem, String defaultValues, String str, int adapterPos) {

     /*   String quantity = map.get(adapterPos);
        if (!quantity.equals(str)) {
            int defaultQuantity = Integer.parseInt(quantity);
            int selectedQuantity = Integer.parseInt(str);
            if (defaultQuantity <= selectedQuantity) {
                shoppingBagPresenter.inCreaseCount(str,detailItem.getCartID(),detailItem.getProductRate(),detailItem.getProductCode());
            } else {
                shoppingBagPresenter.deCreaseCount(str,detailItem.getCartID(),detailItem.getProductRate(),detailItem.getProductCode());
            }
        }*/
        int defaultPos = Integer.parseInt(defaultValues);
        int selectedQuantity = Integer.parseInt(str);
        if (defaultPos != selectedQuantity) {

            if (defaultPos <= selectedQuantity) {
                shoppingBagPresenter.inCreaseCount(str, detailItem.getCartID(), detailItem.getProductRate(), detailItem.getProductCode());
            } else {
                shoppingBagPresenter.deCreaseCount(str, detailItem.getCartID(), detailItem.getProductRate(), detailItem.getProductCode());
            }
        }

    }

    @OnClick(R.id.btn_continue)
    public void onViewClicked() {
        if (responseModel != null) {
            if (activity.isUserLoginIn()) {
                AddressFragment fragment = new AddressFragment(responseModel);
                mListener.nextFragment(fragment);
            } else {
                ((CbzaarApplication) activity.getApplicationContext()).fromWhere = PRODUCT_DETAILS;
                startActivityForResult(new Intent(activity, UserLoginActivity.class), 100);
            }
        } else showError(getString(R.string.some_thing_went_wrong));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            if (responseModel != null) {
                if (activity.isUserLoginIn()) {
                    AddressFragment fragment = new AddressFragment(responseModel);
                    mListener.nextFragment(fragment);
                } else {
                    ((CbzaarApplication) activity.getApplicationContext()).fromWhere = PRODUCT_DETAILS;
                    startActivityForResult(new Intent(activity, UserLoginActivity.class), 100);
                }
            } else showError(getString(R.string.some_thing_went_wrong));
        }

    }
}
