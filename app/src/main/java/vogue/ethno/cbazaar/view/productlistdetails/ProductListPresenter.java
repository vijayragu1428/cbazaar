package vogue.ethno.cbazaar.view.productlistdetails;

import android.content.Context;

import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.base.AbstractBasePresenter;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.classmodel.responsemodel.accesstoken.TokenResponseModel;
import vogue.ethno.cbazaar.data.ApiClient;
import vogue.ethno.cbazaar.model.ProductListModel;
import vogue.ethno.cbazaar.data.network.HeaderValues;
import vogue.ethno.cbazaar.data.network.NetworkCallback;

public class ProductListPresenter extends AbstractBasePresenter<ProductListInterface> implements NetworkCallback {

    ProductListInterface view;
    private int apiFrom = 0;
    private ApiClient apiClient;
    private Context mContext;

    @Override
    public void setView(ProductListInterface view) {
        super.setView(view);
        this.view = view;
        this.mContext = view.getActivityContext();
        this.apiClient = new ApiClient(this);
    }

    public void apiCall(int whichApi, String redirectUrl) {
        loadingDialog(true);
        this.apiFrom = whichApi;
        switch (whichApi) {
            case 1:
                apiClient.getProductList(whichApi, redirectUrl);
                break;
        }
    }

    private void failureAction(String s) {
        loadingDialog(false);
        if (view != null) {
            view.showError(s);
        }
    }

    private void loadingDialog(boolean status) {
        if (view != null) {
            if (status)
                view.showLoading();
            else
                view.hideLoading(status);
        }
    }

    @Override
    public void onSuccess(Object o, int apiFrom) {
        if (apiFrom != -1)
            loadingDialog(false);
        try {
            if (o != null) {
                switch (apiFrom) {
                    case -1:
                        accessToken(o);
                        break;
                    case 1:
                        productListDetails(o);
                        break;
                }
            } else failureAction(mContext.getString(R.string.some_thing_went_wrong));
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }


    private void accessToken(Object o) {
        try {
            TokenResponseModel responseModel = (TokenResponseModel) o;
            if (responseModel != null && !cutNull(responseModel.getAccessToken()).isEmpty() && !cutNull(responseModel.getTokenType()).isEmpty()) {
                HeaderValues.ACESS_TOKEN = responseModel.getAccessToken();
                HeaderValues.TOKEN_TYPE = responseModel.getTokenType();
                apiCall(apiFrom, "");
            } else {
                HeaderValues.ACESS_TOKEN = "";
                HeaderValues.TOKEN_TYPE = "";
                tokenError(o);
            }
        } catch (Exception e) {
            HeaderValues.TOKEN_TYPE = "";
            HeaderValues.ACESS_TOKEN = "";
            tokenError(o);
        }
    }

    private void productListDetails(Object o) {
        try {
            ProductListModel responseModel = (ProductListModel) o;
            if (responseModel != null) {
                view.successFromServer(responseModel);
            } else {
                failureAction(mContext.getString(R.string.some_thing_went_wrong));
            }
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    @Override
    public void onFailure(String body) {
        failureAction(body);
    }

    @Override
    public void onErrorBody(Object o, int apiFrom) {
        loadingDialog(false);
        switch (apiFrom) {
            case -1:
                tokenError(o);
                break;
            case 1:
                homeDetailsError(o);
                break;
        }
    }

    private void tokenError(Object o) {
        if (view != null)
            view.sessionError("");
    }

    private void homeDetailsError(Object o) {
        failureAction(mContext.getString(R.string.some_thing_went_wrong));
    }

    private String cutNull(Object o) {
        return Utils.cutNull(o);
    }
}
