package vogue.ethno.cbazaar.view.productdetails.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import vogue.ethno.cbazaar.R;

public class ViewPagerAdapter extends PagerAdapter {
    private Context mContext;
    private List<String> imageUrl;

    public ViewPagerAdapter(Context context, List<String> imageUrl) {
        this.imageUrl = imageUrl;
        this.mContext = context;
    }


    @Override
    public int getCount() {
        return imageUrl == null ? 0 : imageUrl.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (LinearLayout) object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.view_pager_product_details, container, false);
        ImageView slider = view.findViewById(R.id.img_slider);
        Glide.with(mContext).load(imageUrl.get(position))
                .apply(RequestOptions.placeholderOf(R.drawable.ev_logo))
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .into(slider);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
