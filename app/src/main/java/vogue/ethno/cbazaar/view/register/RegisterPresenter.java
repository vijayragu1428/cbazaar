package vogue.ethno.cbazaar.view.register;

import android.content.Context;

import com.google.gson.Gson;

import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.base.AbstractBasePresenter;
import vogue.ethno.cbazaar.classmodel.responsemodel.accesstoken.TokenResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.userlogin.UserLoginResponse;
import vogue.ethno.cbazaar.data.ApiClient;
import vogue.ethno.cbazaar.data.localdb.pref.AppPreference;
import vogue.ethno.cbazaar.data.network.HeaderValues;
import vogue.ethno.cbazaar.data.network.NetworkCallback;

public class RegisterPresenter extends AbstractBasePresenter<RegisterListener> implements NetworkCallback {

    private RegisterListener view;
    private int apiFrom;
    private Context mContext;
    private ApiClient apiClient;

    @Override
    public void setView(RegisterListener view) {
        super.setView(view);
        this.view = view;
        this.mContext = view.getActivityContext();
        this.apiClient = new ApiClient(this);
    }

    @Override
    public void onSuccess(Object o, int api) {
        try {
            UserLoginResponse userLoginResponse = (UserLoginResponse) o;
            if (userLoginResponse != null && userLoginResponse.isIsRegisteredWithPassword()) {
                AppPreference.getPrefsHelper().savePref(AppPreference.USER_LOGIN_DATA, new Gson().toJson(userLoginResponse));
                AppPreference.getPrefsHelper().savePref(AppPreference.USER_MAIL_ID, view.getUserId());
                AppPreference.getPrefsHelper().savePref(AppPreference.USER_ID, String.valueOf(userLoginResponse.getID()));
                HeaderValues.EMAIL=view.getUserId();

                //HeaderValues.USER_ID = AppPreference.getPrefsHelper().getPref(AppPreference.USER_ID,"");
                view.registerSuccess();
            } else {
                onFailure(view.getActivityContext().getString(R.string.some_thing_went_wrong));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onFailure(String body) {
        loadingDialog(false);
        view.showError(body);
    }

    @Override
    public void onErrorBody(Object o, int api) {

    }

    public void apiCall(int item) {
        loadingDialog(true);
        apiFrom = item;
        switch (item) {
            case 1:
                apiClient.userRegister(item, view.getPassword(), view.getIsGuest());
                break;
        }
    }

    private void loadingDialog(boolean status) {
        if (view != null) {
            if (status)
                view.showLoading();
            else view.hideLoading(status);
        }
    }

    private void tokenError(Object o) {
        loadingDialog(false);
        if (view != null)
            view.sessionError("");
    }

    private void accessToken(Object o) {
        try {
            TokenResponseModel responseModel = (TokenResponseModel) o;
            if (responseModel != null && !Utils.cutNull(responseModel.getAccessToken()).isEmpty() && !Utils.cutNull(responseModel.getTokenType()).isEmpty()) {
                HeaderValues.ACESS_TOKEN = responseModel.getAccessToken();
                HeaderValues.TOKEN_TYPE = responseModel.getTokenType();
                apiCall(apiFrom);
            } else {
                HeaderValues.ACESS_TOKEN = "";
                HeaderValues.TOKEN_TYPE = "";
                tokenError(o);
            }
        } catch (Exception e) {
            HeaderValues.TOKEN_TYPE = "";
            HeaderValues.ACESS_TOKEN = "";
            tokenError(o);
        }
    }
}
