package vogue.ethno.cbazaar.view.homescreen.fragment.home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import vogue.ethno.cbazaar.apputils.GlobalConstant;
import vogue.ethno.cbazaar.apputils.ItemDecorationGridColumns;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.classmodel.responsemodel.rsakey.RsaKeyResponseModel;
import vogue.ethno.cbazaar.model.HomeDetailsResponseModel;
import vogue.ethno.cbazaar.model.adapter.PopularAdapter;
import vogue.ethno.cbazaar.view.homescreen.FragmentListener;
import vogue.ethno.cbazaar.view.homescreen.HomeActivity;
import vogue.ethno.cbazaar.view.homescreen.HomeDetailsListener;
import vogue.ethno.cbazaar.view.homescreen.HomeDetailsPresenter;
import vogue.ethno.cbazaar.view.homescreen.OnItemSelectedCallback;
import vogue.ethno.cbazaar.view.productlistdetails.ProductListFragment;

public class HomeFragment extends Fragment implements HomeDetailsListener, OnItemSelectedCallback {

    @BindView(R.id.rv_popular)
    RecyclerView rvPopular;
    @BindView(R.id.ll_container)
    LinearLayout rel_root;
    /*@BindView(R.id.nsv_parent)
    NestedScrollView nsvParent;*/
    @BindView(R.id.tv_lable_popular)
    TextView tvPopular;
    @BindView(R.id.iv_banner)
    ImageView ivBanner;
    Unbinder unbinder;

    ArrayList<HomeDetailsResponseModel.Popularcategory> popularList;
    PopularAdapter popularAdapter;

    int spanCount = 3;
    private FragmentListener mListener;
    private HomeActivity activity;
    private Context mContext;
    private HomeDetailsPresenter homeDetailsPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        initUi();
        return view;
    }

    private void initUi() {
        activity = (HomeActivity) getActivity();
        mContext = getContext();
        initRv();
        homeDetailsPresenter = new HomeDetailsPresenter();
        homeDetailsPresenter.setView(this);
        homeDetailsPresenter.apiCall(1);
        //homeDetailsPresenter.apiCall(2);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.app_name));
    }

    private void initRv() {
        popularList = new ArrayList<>();
        rvPopular.setHasFixedSize(true);
        //rvPopular.setNestedScrollingEnabled(false);
        GridLayoutManager productLayoutManager = new GridLayoutManager(mContext, spanCount, GridLayoutManager.VERTICAL, false);
        rvPopular.setLayoutManager(productLayoutManager);
        rvPopular.addItemDecoration(new ItemDecorationGridColumns(15, 3));
        popularAdapter = new PopularAdapter(mContext, popularList, this);
        rvPopular.setAdapter(popularAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            mListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mListener = null;
        homeDetailsPresenter.destroyView();
        unbinder.unbind();
    }

    @Override
    public void showLoading() {
        activity.showDialog("", false,rel_root);
    }

    @Override
    public void hideLoading(boolean status) {
        activity.hideDialog(status);
    }

    @Override
    public Context getActivityContext() {
        return getContext();
    }

    @Override
    public void showError(String string) {
        activity.showToast(string);
    }

    @Override
    public void sessionError(String string) {
        mListener.nextFragment(new HomeFragment());
    }

    @Override
    public void successFromServer(HomeDetailsResponseModel responseModel) {
        tvPopular.setVisibility(View.VISIBLE);
        if (responseModel.getBanners() != null && responseModel.getBanners().size() > 0) {
            Glide.with(mContext).load(Utils.cutNull(responseModel.getBanners().get(0).getImages().get(3)))
                    .apply(RequestOptions.placeholderOf(R.drawable.ev_logo))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                    .into(ivBanner);
        }

        if (responseModel.getPopularcategories() != null && responseModel.getPopularcategories().size() > 0) {
            popularList = responseModel.getPopularcategories();
        }
        popularAdapter = new PopularAdapter(mContext, popularList, this);
        rvPopular.setAdapter(popularAdapter);
        popularAdapter.notifyDataSetChanged();
    }

    @Override
    public void successFromServerRSA(List<RsaKeyResponseModel> rsaKeyResponseModel) {







           /* Intent intent = new Intent(getActivity(),CcAvanueFragment.class);
            intent.putExtra(AvenuesParams.ACCESS_CODE, rsaKeyResponseModel.get(3).getValue());
            intent.putExtra(AvenuesParams.MERCHANT_ID, "8694");
            intent.putExtra(AvenuesParams.ORDER_ID,"1431432602");
            intent.putExtra(AvenuesParams.CURRENCY, "inr");
            intent.putExtra(AvenuesParams.AMOUNT, "1");
            intent.putExtra(AvenuesParams.REDIRECT_URL, rsaKeyResponseModel.get(1).getValue());
            intent.putExtra(AvenuesParams.CANCEL_URL, rsaKeyResponseModel.get(2).getValue());
            intent.putExtra(AvenuesParams.RSA_KEY_URL, rsaKeyResponseModel.get(0).getValue());
            startActivity(intent);*/
    }

    @Override
    public void onItemSelected(String name, String redirectUrl) {

        Log.e("HomeFragment", name);
        Log.e("HomeFragment", redirectUrl);

        ProductListFragment productListFragment = new ProductListFragment();
        Bundle arg = new Bundle();
        arg.putString(GlobalConstant.REDIRECT_URL, redirectUrl);
        arg.putString("name", name);
        productListFragment.setArguments(arg);
        mListener.nextFragment(productListFragment);
        /*getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, productListFragment, ProductListFragment.class.getName())
                .addToBackStack(ProductListFragment.class.getName())
                .commit();*/
    }
}
