package vogue.ethno.cbazaar.view.login;

import android.content.Context;

import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;

import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.base.AbstractBasePresenter;
import vogue.ethno.cbazaar.classmodel.responsemodel.accesstoken.TokenResponseModel;
import vogue.ethno.cbazaar.data.ApiClient;
import vogue.ethno.cbazaar.data.localdb.pref.AppPreference;
import vogue.ethno.cbazaar.data.network.HeaderValues;
import vogue.ethno.cbazaar.data.network.NetworkCallback;
import vogue.ethno.cbazaar.view.enquiryform.EnquiryFormListener;

public class LoginPresenter extends AbstractBasePresenter<LoginListener> implements NetworkCallback {

    private LoginListener views;
    private int apiFrom;
    private Context mContext;
    private ApiClient apiClient;

    @Override
    public void setView(LoginListener view) {
        super.setView(view);
        this.views = view;
        this.mContext = view.getActivityContext();
        this.apiClient = new ApiClient(this);
    }

    @Override
    public void onSuccess(Object o, int api) {
        loadingDialog(false);

        if (apiFrom != -1)
            loadingDialog(false);
        try {
            if (o != null) {
                switch (api) {
                    case -1:
                        accessToken(o);
                        break;
                    case 1:
                        loginResponse(o);
                        break;

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onFailure(String body) {
        loadingDialog(false);
        views.showError(body);
    }

    @Override
    public void onErrorBody(Object o, int api) {
        loadingDialog(false);
        switch (apiFrom) {
            case -1:
                tokenError(o);
                break;

        }
    }

    public void apiCall(int item) {
        loadingDialog(true);
        apiFrom = item;
        switch (item) {
            case 1:
                HeaderValues.EMAIL=views.getLoginId();
                apiClient.agentLogin(item, views.getLoginId(), views.getPassword(), views.getIsGuest());
                break;
        }
    }

    private void loadingDialog(boolean status) {
        if (views != null) {
            if (status)
                views.showLoading();
            else views.hideLoading(status);
        }
    }

    private void tokenError(Object o) {
        loadingDialog(false);
        if (views != null)
            views.sessionError("");
    }

    private void accessToken(Object o) {
        try {
            TokenResponseModel responseModel = (TokenResponseModel) o;
            if (responseModel != null && !Utils.cutNull(responseModel.getAccessToken()).isEmpty() && !Utils.cutNull(responseModel.getTokenType()).isEmpty()) {
                HeaderValues.ACESS_TOKEN = responseModel.getAccessToken();
                HeaderValues.TOKEN_TYPE = responseModel.getTokenType();
                apiCall(apiFrom);
            } else {
                HeaderValues.ACESS_TOKEN = "";
                HeaderValues.TOKEN_TYPE = "";
                tokenError(o);
            }
        } catch (Exception e) {
            HeaderValues.TOKEN_TYPE = "";
            HeaderValues.ACESS_TOKEN = "";
            tokenError(o);
        }
    }

    private void loginResponse(Object o) {
        if (o instanceof JsonObject) {
            try {
                if ((((JsonObject) o).get("Status").toString().contains("Success"))) {
                    AppPreference.getPrefsHelper().savePref(AppPreference.AGENT_ID, ((JsonObject) o).get("UserId").toString().replaceAll("\"", ""));
                    AppPreference.getPrefsHelper().savePref(AppPreference.AGENT_NAME, ((JsonObject) o).get("Name").toString().replaceAll("\"", ""));
                    AppPreference.getPrefsHelper().savePref(AppPreference.AGENT_MOBILE_NUMBER, ((JsonObject) o).get("MobileNumber").toString().replaceAll("\"", ""));
                    views.loginSuccess();
                } else {

                    onFailure(views.getActivityContext().getString(R.string.invalid_user_or_password));
                }

            } catch (JsonIOException e) {
                e.printStackTrace();
            }
        }
    }
}
