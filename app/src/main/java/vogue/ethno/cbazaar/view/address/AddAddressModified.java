package vogue.ethno.cbazaar.view.address;

import java.util.List;

import vogue.ethno.cbazaar.classmodel.responsemodel.getaddress.RelationItem;

public interface AddAddressModified {
    void isAddressModified(List<RelationItem> relationItems);
}
