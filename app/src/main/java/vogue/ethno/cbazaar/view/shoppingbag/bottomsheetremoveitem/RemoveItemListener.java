package vogue.ethno.cbazaar.view.shoppingbag.bottomsheetremoveitem;

import vogue.ethno.cbazaar.base.LoadDataView;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.CartDetailItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;

public interface RemoveItemListener extends LoadDataView {

    String getType();
    CartDetailItem getCart();

    void updateCartCount(GetCartResponseModel responseModel);
}
