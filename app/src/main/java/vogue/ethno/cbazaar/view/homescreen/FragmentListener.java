package vogue.ethno.cbazaar.view.homescreen;


import android.support.v4.app.Fragment;

public interface FragmentListener {
    void nextFragment(Fragment fragment, int... ints);

    void setTitle(CharSequence title);
}
