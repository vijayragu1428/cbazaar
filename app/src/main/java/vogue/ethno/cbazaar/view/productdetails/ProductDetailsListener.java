package vogue.ethno.cbazaar.view.productdetails;

import java.util.List;

import vogue.ethno.cbazaar.base.LoadDataView;
import vogue.ethno.cbazaar.classmodel.responsemodel.addtocart.AddToCartDetailsResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.bodyheight.BodyHeightResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.ProductDetailsResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.ProductSizeItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.readysize.NaturalWaistResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.topstyle.TopStyleResponse;

public interface ProductDetailsListener extends LoadDataView {
    void successFromServer(ProductDetailsResponseModel responseModel);

    String getProductId();

    ProductSizeItem getHeightRequest();

    void successFromServerBodyHeight(BodyHeightResponseModel responseModel);

    void successFromServerNaturalWaist(List<NaturalWaistResponseModel> responseModel);

    void successFromCircumference(List<NaturalWaistResponseModel> posts);

    void successFromWaist(List<NaturalWaistResponseModel> posts);

    void successFromTopStyle(TopStyleResponse posts);

    void updateCartCount(GetCartResponseModel responseModel);

    void buyNowCart(GetCartResponseModel responseModel);
}
