package vogue.ethno.cbazaar.view.address;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.classmodel.requestmodel.address.AddressRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.confirmcart.ShippingAddress;
import vogue.ethno.cbazaar.classmodel.responsemodel.addaddress.AddAddressResponse;
import vogue.ethno.cbazaar.classmodel.responsemodel.addaddress.RelationInfoItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart.ConfirmCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.getaddress.AddressResponse;
import vogue.ethno.cbazaar.classmodel.responsemodel.getaddress.RelationItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.getaddress.ShippingCountryListItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;
import vogue.ethno.cbazaar.view.addaddress.AddAddressFragment;
import vogue.ethno.cbazaar.view.homescreen.FragmentListener;
import vogue.ethno.cbazaar.view.homescreen.HomeActivity;
import vogue.ethno.cbazaar.view.homescreen.fragment.home.HomeFragment;
import vogue.ethno.cbazaar.view.payment.PaymentFragment;
import vogue.ethno.cbazaar.view.shoppingbag.bottomsheetremoveitem.BottomSheetCommunicator;
import vogue.ethno.cbazaar.view.shoppingbag.bottomsheetremoveitem.RemoveItemDialogFragment;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class AddressFragment extends Fragment implements AddAddressListener, AddressAdapter.OnSelect, BottomSheetCommunicator, AddAddressModified {


    @BindView(R.id.txt_no_address)
    TextView noAddress;
    @BindView(R.id.btn_add_addrss)
    Button btnAddAddrss;
    @BindView(R.id.rel_add_new)
    RelativeLayout relAddNew;
    @BindView(R.id.rv_address)
    RecyclerView rvAddress;
    @BindView(R.id.check_box_same_address)
    CheckBox checkBoxSameAddress;
    @BindView(R.id.spin_type)
    Spinner spinType;
    @BindView(R.id.edt_first_name)
    EditText edtFirstName;
    @BindView(R.id.edt_last_name)
    EditText edtLastName;
    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_addres_line_one)
    EditText edtAddresLineOne;
    @BindView(R.id.edt_addres_line_two)
    EditText edtAddresLineTwo;
    @BindView(R.id.edt_code)
    EditText edtCode;
    @BindView(R.id.edt_city)
    EditText edtCity;
    @BindView(R.id.edt_county)
    EditText edtCounty;
    @BindView(R.id.edt_state)
    EditText edtState;
    @BindView(R.id.edt_mobile_number)
    EditText edtMobileNumber;
    @BindView(R.id.edt_alter_number)
    EditText edtAlterNumber;
    @BindView(R.id.btn_continue)
    Button btnContinue;
    @BindView(R.id.lin_biiling_adress)
    LinearLayout linearLayoutBillingAddress;
    Unbinder unbinder;
    @BindView(R.id.lin_rv)
    LinearLayout linRv;
    @BindView(R.id.spin_country)
    Spinner spinCountry;
    @BindView(R.id.spin_state)
    Spinner spinState;

    @BindView(R.id.rel_root)
    RelativeLayout rel_root;

    private AddAddressPresenter presenter;
    private HomeActivity activity;
    private FragmentListener fragmentListener;
    private AddressAdapter adapter;
    private AddressResponse addressResponse;
    private RelationItem relationDetais;
    private int currentItem;
    private GetCartResponseModel model;
    private RelationItem relationDetaisDefault;
     public AddressFragment(GetCartResponseModel responseModel) {
        this.model = responseModel;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_address, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            fragmentListener = (FragmentListener) context;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (fragmentListener != null)
            fragmentListener = null;
       // activity.showHideToolbar(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        activity = (HomeActivity) getActivity();
        presenter = new AddAddressPresenter();
        presenter.setView(this);
        presenter.apiCall(1);
        initRv();
        uiListener();
    }

    private void uiListener() {
        checkBoxSameAddress.setChecked(true); linearLayoutBillingAddress.setVisibility(View.GONE);
        checkBoxSameAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) linearLayoutBillingAddress.setVisibility(View.GONE);
                else linearLayoutBillingAddress.setVisibility(View.VISIBLE);

            }
        });
    }

    private void initRv() {
        adapter = new AddressAdapter(this, getContext());
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvAddress.setLayoutManager(linearLayoutManager);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(rvAddress);
        rvAddress.setNestedScrollingEnabled(false);
        rvAddress.setAdapter(adapter);
        rvAddress.setHasFixedSize(true);
        rvAddress.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstVisibleItemPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                adapter.setSeletedItem(firstVisibleItemPosition);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();  activity.showHideToolbar(false);
        fragmentListener.setTitle(getString(R.string.address));//.toUpperCase());
    }

    @Override
    public void showLoading() {
        activity.showDialog("", false,rel_root);
    }

    @Override
    public void hideLoading(boolean status) {
        activity.hideDialog(status);
    }

    @Override
    public Context getActivityContext() {
        return getContext();
    }

    @Override
    public void showError(String string) {
        activity.showToast(string);
    }


    @Override
    public void sessionError(String string) {
        fragmentListener.nextFragment(new HomeFragment());
    }

    @Override
    public void successAddress(AddressResponse responseModel) {

         if (responseModel==null){
             activity.showToast(getString(R.string.some_thing_went_wrong));
             return;
         }
        this.addressResponse = responseModel;
        adapter.loadData(null);
        if (responseModel.getRelation() != null && responseModel.getRelation().size() > 0) {
            setSpinerData();
            adapter.loadData(responseModel.getRelation());
            noAddress.setVisibility(View.GONE);
            checkBoxSameAddress.setVisibility(View.VISIBLE);
            rvAddress.setVisibility(View.VISIBLE);
        } else {
            checkBoxSameAddress.setVisibility(View.GONE);
            noAddress.setVisibility(View.VISIBLE);
            rvAddress.setVisibility(View.GONE);
        }
    }

    @Override
    public AddressRequestModel model() {
        return null;
    }

    @Override
    public void addAddressResponse(AddAddressResponse addAddressResponse) {

    }

    @Override
    public void removeAddress(List<RelationInfoItem> relationInfo) {

        if (relationInfo != null && relationInfo.size() > 0) {
            noAddress.setVisibility(View.GONE);
            checkBoxSameAddress.setVisibility(View.VISIBLE);
            rvAddress.setVisibility(View.VISIBLE);
        } else {
            checkBoxSameAddress.setVisibility(View.GONE);
            noAddress.setVisibility(View.VISIBLE);
            rvAddress.setVisibility(View.GONE);
        }
        adapter.itemRemove(currentItem);
    }

    @Override
    public void cartResponseModel(ConfirmCartResponseModel cartResponseModel, ShippingAddress shippingAddress) {

        if (cartResponseModel != null) {
            fragmentListener.nextFragment(new PaymentFragment(cartResponseModel, shippingAddress));
        } else showError(getString(R.string.some_thing_went_wrong));

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btn_continue, R.id.btn_add_addrss})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_continue:
                btnSubmitAction();
                break;
            case R.id.btn_add_addrss:
                addAddressAction(null);
                break;
        }
    }



    private void setSpinerData() {
        if (addressResponse.getShippingStateList() != null && addressResponse.getShippingStateList().size() > 0) {
            ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, addressResponse.getShippingStateList());
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinState.setAdapter(stateAdapter);

        }
        if (addressResponse.getShippingCountryList() != null && addressResponse.getShippingCountryList().size() > 0) {
            List<String> list = new ArrayList<>();
            for (ShippingCountryListItem shippingCountryListItem : addressResponse.getShippingCountryList()) {
                list.add(shippingCountryListItem.getCountry());
            }
            ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, list);
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinCountry.setAdapter(stateAdapter);

        }
        if (addressResponse.getAddressType() != null && addressResponse.getAddressType().size() > 0) {
            ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, addressResponse.getAddressType());
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinType.setAdapter(stateAdapter);
        }
    }

    private boolean validation() {
        if (Utils.cutNull(edtFirstName.getText().toString()).isEmpty()) {
            showError(getString(R.string.please_enter_first_name));
            return false;
        }
        if (Utils.cutNull(edtLastName.getText().toString()).isEmpty()) {
            showError(getString(R.string.please_enter_last_name));
            return false;
        }
        if (Utils.cutNull(edtEmail.getText().toString()).isEmpty()) {
            showError(getString(R.string.please_enter_email));
            return false;
        }
        if (!Utils.isValidEmail(edtEmail.getText().toString())) {
            showError(getString(R.string.please_enter_valid_email));
            return false;
        }
        if (Utils.cutNull(edtAddresLineOne.getText().toString()).isEmpty()) {
            showError(getString(R.string.please_enter_address1));
            return false;
        }
        if (Utils.cutNull(edtAddresLineTwo.getText().toString()).isEmpty()) {
            showError(getString(R.string.please_enter_address2));
            return false;
        }
        if (Utils.cutNull(edtCode.getText().toString()).isEmpty()) {
            showError(getString(R.string.please_enter_post));
            return false;
        }
        if (edtCode.getText().toString().length() < 5) {
            showError(getString(R.string.please_zip_6));
            return false;
        }
        if (Utils.cutNull(edtCity.getText().toString()).isEmpty()) {
            showError(getString(R.string.please_enter_city));
            return false;
        }
        if (Utils.cutNull(edtMobileNumber.getText().toString()).isEmpty()) {
            showError(getString(R.string.please_enter_mobilet));
            return false;
        }
        if (edtMobileNumber.getText().toString().length() < 9) {
            showError(getString(R.string.please_enter_mobilet_10));
            return false;
        }
        if (!Utils.cutNull(edtMobileNumber.getText().toString()).isEmpty() && edtAlterNumber.getText().toString().length() > 0) {
            if (edtAlterNumber.getText().toString().length() < 9) {
                showError(getString(R.string.please_enter_mobilet_10));
                return false;
            }
        }
        return true;
    }

    private void addAddressAction(RelationItem relationItem) {
        fragmentListener.nextFragment(new AddAddressFragment(addressResponse, relationItem, this) );
    }

    @Override
    public void onAddressEdit(RelationItem relationItem) {
        addAddressAction(relationItem);
    }

    @Override
    public void onAddressDelete(RelationItem relationItem, int pos) {
        this.currentItem = pos;
        this.relationDetais = relationItem;
        RemoveItemDialogFragment removeItemDialogFragment = new RemoveItemDialogFragment(this);
        removeItemDialogFragment.show(getActivity().getSupportFragmentManager(), "show");
    }

    private void btnSubmitAction() {
        if (adapter.getItemCount() == 0) {
            showError(getString(R.string.please_add_address));
            return;
        }
        if (checkBoxSameAddress.isChecked()) {
            presenter.confirmCart(relationDetaisDefault, null, model, addressResponse);
            // fragmentListener.nextFragment(new PaymentFragment(model, relationDetais), );
        } else {
            if (validation()) {
                RelationItem address = new RelationItem();
                address.setRelationID(0);
                address.setRelationTypeId(0);
                address.setRelationFirstName(edtFirstName.getText().toString());
                address.setRelationLastName(edtFirstName.getText().toString());
                address.setAddress1(edtAddresLineOne.getText().toString());
                address.setAddress2(edtAddresLineTwo.getText().toString());
                address.setCity(edtCity.getText().toString());
                address.setState(spinState.getSelectedItem().toString());

                for (ShippingCountryListItem countryListItem : addressResponse.getShippingCountryList()) {
                    if (countryListItem.getCountry().equalsIgnoreCase(spinCountry.getSelectedItem().toString())) {
                        address.setRelationCountryId(countryListItem.getCountryId());
                        address.setRelationCountryCode(countryListItem.getCountryCode());
                        break;
                    }
                }
                address.setCountry(spinCountry.getSelectedItem().toString());
                address.setZipcode(edtCode.getText().toString());
                address.setPhoneNumber(edtMobileNumber.getText().toString());
                address.setCellNumber(edtAlterNumber.getText().toString());
                address.setRelationEmailId(edtEmail.getText().toString());
                address.setAddressType(spinType.getSelectedItem().toString());
                presenter.confirmCart(relationDetaisDefault, address, model, addressResponse);
                //fragmentListener.nextFragment(new PaymentFragment(model, address), );
            }
        }
    }

    @Override
    public void onDefaultSelection(RelationItem relationItem) {
        this.relationDetaisDefault = relationItem;
    }

    @Override
    public void updateCartCount(GetCartResponseModel getCartResponseModel) {
        presenter.deleteAddress(relationDetais);
    }

    @Override
    public void isAddressModified(List<RelationItem> relationItems) {
        presenter.apiCall(1);
    }


}
