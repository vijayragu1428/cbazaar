package vogue.ethno.cbazaar.view.register;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.base.BaseActivity;
import vogue.ethno.cbazaar.data.network.HeaderValues;

import static vogue.ethno.cbazaar.app.CbzaarApplication.getContext;

public class RegisterActivity extends BaseActivity implements RegisterListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btn_register)
    Button btnRegister;
    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_password)
    EditText edtPassword;
    @BindView(R.id.edt_confirm_password)
    EditText edtConfirmPassword;
    @BindView(R.id.rel_root)
    RelativeLayout rel_root;

    private RegisterPresenter registerPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        initUI();

    }

    private void initUI() {
        registerPresenter = new RegisterPresenter();
        registerPresenter.setView(this);
        setupDrawer();
    }

    private void setupDrawer() {
        toolbar.setTitle(getString(R.string.register));
        toolbar.setNavigationIcon(getContext().getResources().getDrawable(R.drawable.ic_back_arrow));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @OnClick(R.id.btn_register)
    public void onViewClicked() {
        if (validation()) {
            HeaderValues.EMAIL = getUserId();
            registerPresenter.apiCall(1);
        }
    }

    private boolean validation() {
        if (edtEmail.getText().toString().isEmpty()) {
            showError(getString(R.string.please_enter_email));
            return false;
        }

        if (!Utils.isValidEmail(edtEmail.getText().toString())) {
            showError(getString(R.string.please_enter_valid_email));
            return false;
        }
        if (edtPassword.getText().toString().isEmpty()) {
            showError(getString(R.string.please_enter_pass));
            return false;
        }
        if (Utils.cutNull(edtPassword.getText().toString()).length() < 5) {
            showError(getString(R.string.please_enter_pass_6));
            return false;
        }
        if (!Utils.cutNull(edtPassword.getText().toString()).equals(Utils.cutNull(edtConfirmPassword.getText().toString()))) {
            showError(getString(R.string.please_enter_pass_mismatch));
            return false;
        }
        return true;
    }


    @Override
    public String getPassword() {
        return edtPassword.getText().toString();
    }

    @Override
    public boolean getIsGuest() {
        return false;
    }

    @Override
    public String getUserId() {
        return edtEmail.getText().toString();
    }

    @Override
    public void registerSuccess() {
        finish();
    }

    @Override
    public void showLoading() {
        showDialog("", false,rel_root);

    }

    @Override
    public void hideLoading(boolean status) {
        hideDialog(status);

    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public void showError(String string) {
        showToast(string);

    }

    @Override
    public void sessionError(String string) {

    }
}
