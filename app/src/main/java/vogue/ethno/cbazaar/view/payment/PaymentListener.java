package vogue.ethno.cbazaar.view.payment;

import java.util.List;

import vogue.ethno.cbazaar.base.LoadDataView;
import vogue.ethno.cbazaar.classmodel.responsemodel.cashondelivery.CashOnDelivryResponse;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.rsakey.RsaKeyResponseModel;

public interface PaymentListener extends LoadDataView {

    void successFromAddress();

    void cashOndeliverySuccess(CashOnDelivryResponse responseModel);

    String orderId();

    void successFromServerRSA(List<RsaKeyResponseModel> rsaKeyResponseModel);
}
