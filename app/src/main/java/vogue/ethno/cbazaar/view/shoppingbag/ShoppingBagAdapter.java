package vogue.ethno.cbazaar.view.shoppingbag;

import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.ApiModel;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.CartDetailItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.DeliveryOptionsItem;

public class ShoppingBagAdapter extends RecyclerView.Adapter<ShoppingBagAdapter.ViewHolder> {


    @BindView(R.id.img_delete)
    AppCompatImageView imgDelete;
    private List<CartDetailItem> cartDetail;
    private Context mContext;
    private DeleteClick deleteClick;
    public int pos;

    public ShoppingBagAdapter(DeleteClick deleteClick, Context context) {
        this.cartDetail = new ArrayList<>();
        this.mContext = context;
        this.deleteClick = deleteClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_shopping_bag_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final CartDetailItem detailItem = cartDetail.get(position);
        holder.onItemBind(detailItem);
        holder.txtOldPrice.setPaintFlags(holder.txtOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        this.pos = position;
        holder.txtOldPrice.setText(Utils.getAmountString(detailItem.getProductAmount()));
        holder.txtTitle.setText(cutNull(detailItem.getProductDescription()));
        holder.txtNewPrice.setText(Utils.getAmountString(detailItem.getProductNetAmount()));
        holder.txtSaving.setText(cutNull(detailItem.getSavingOfferMessage()).replace("[a-zA-Z]", "\u20B9 "));
        Glide.with(mContext).load(cutNull(ApiModel.BASE_IMAGE_URL + detailItem.getProductImage()))
                .apply(RequestOptions.placeholderOf(R.drawable.ev_logo))
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .into(holder.imgItemDetails);
        String deliveryOptions = "";
        if (holder.txtSaving.getText().toString().isEmpty()) {
            holder.imgPrice.setVisibility(View.GONE);
            holder.txtOldPrice.setVisibility(View.GONE);
        }
        for (DeliveryOptionsItem optionsItem : detailItem.getDeliveryOptions()) {
            deliveryOptions = deliveryOptions + optionsItem.getValue();
        }
        //holder.txtOldPrice.setVisibility(View.INVISIBLE);
        ///holder.imgPrice.setVisibility(View.INVISIBLE);
        holder.txtDelivery.setText(deliveryOptions);
        String[] arr = detailItem.getQuantity().toArray(new String[detailItem.getQuantity().size()]);
        deleteClick.onSpinnerDefaultValues(String.valueOf(detailItem.getProductQuantity()), position);
        int pos = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equalsIgnoreCase(String.valueOf(detailItem.getProductQuantity()))) {
                pos = i;
            }
        }
        ArrayAdapter aa = new ArrayAdapter(mContext, android.R.layout.simple_spinner_item, arr);
        aa.setDropDownViewResource(R.layout.simple_spinner_dropdown_item_app);
        holder.spinerItemCount.setAdapter(aa);
        holder.spinerItemCount.setSelection(pos);
        holder.spinerItemCount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                deleteClick.onSpinnerChanged(detailItem, String.valueOf(detailItem.getProductQuantity()), holder.spinerItemCount.getSelectedItem().toString(), position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return cartDetail == null ? 0 : cartDetail.size();
    }

    public void loadData(List<CartDetailItem> cartDetail) {
        this.cartDetail = cartDetail;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_item_details)
        AppCompatImageView imgItemDetails;
        @BindView(R.id.txt_title)
        TextView txtTitle;

        @BindView(R.id.img_price)
        AppCompatImageView imgPrice;
        @BindView(R.id.txt_old_price)
        TextView txtOldPrice;
        @BindView(R.id.txt_new_price)
        TextView txtNewPrice;
        @BindView(R.id.txt_saving)
        TextView txtSaving;
        @BindView(R.id.spiner_item_count)
        Spinner spinerItemCount;
        @BindView(R.id.img_delete)
        AppCompatImageView imgDelete;
        @BindView(R.id.txt_delivery)
        TextView txtDelivery;

        private CartDetailItem cartDetailItem;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.img_delete)
        public void onViewClicked() {
            deleteClick.onDeleteClick(cartDetailItem);
        }

        public void onItemBind(CartDetailItem detailItem) {
            this.cartDetailItem = detailItem;
        }
    }

    private String cutNull(Object o) {
        return Utils.cutNull(o);
    }

    public interface DeleteClick {
        void onDeleteClick(CartDetailItem detailItem);

        void onSpinnerDefaultValues(String defaultQuantity, int adapterPostion);

        void onSpinnerChanged(CartDetailItem detailItem, String f, String pos, int adapterPos);
    }

}
