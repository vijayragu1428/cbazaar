package vogue.ethno.cbazaar.view.productdetails.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.view.productdetails.ProductDetailsFragment;

public class ReadySizeAdapter extends RecyclerView.Adapter<ReadySizeAdapter.ViewHolder> {

    private List<String> list;

    public ReadySizeAdapter(ProductDetailsFragment productDetailsFragment) {
        this.list = new ArrayList<>(0);
    }

    public void loadData(List<String> strings) {
        this.list = strings;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_dropdown_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtTitle.setText(list.get(position));
        // holder.s.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_title)
        TextView txtTitle;
        @BindView(R.id.spinner_quantity)
        Spinner spinnerQuantity;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
