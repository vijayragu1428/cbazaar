package vogue.ethno.cbazaar.view.shoppingbag;

import android.content.Context;

import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.base.AbstractBasePresenter;
import vogue.ethno.cbazaar.classmodel.requestmodel.getcartdetails.GetCartRequestModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.accesstoken.TokenResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;
import vogue.ethno.cbazaar.data.ApiClient;
import vogue.ethno.cbazaar.data.network.HeaderValues;
import vogue.ethno.cbazaar.data.network.NetworkCallback;

public class ShoppingBagPresenter extends AbstractBasePresenter<ShoppingBagListener> implements NetworkCallback {
    private ShoppingBagListener view;
    private Context mContext;
    private int apiFrom = 0;
    private ApiClient apiClient;
    private GetCartRequestModel model;

    @Override
    public void setView(ShoppingBagListener view) {
        super.setView(view);
        this.view = view;
        this.mContext = view.getActivityContext();
        this.apiClient = new ApiClient(this);
    }


    public void apiCall(int whichApi) {
        loadingDialog(true);
        this.apiFrom = whichApi;
        switch (whichApi) {
            case 1:
                apiClient.getCartDetails(whichApi, getCartData());
                break;
            case 2:
                apiClient.updateCart(whichApi, model);
                break;
        }
    }

    private GetCartRequestModel getCartData() {
        GetCartRequestModel cartRequestModel = new GetCartRequestModel();
        cartRequestModel.setCartItemID(0);
        cartRequestModel.setCommand(2);
        cartRequestModel.setDeliveryMode(2);
        cartRequestModel.setGrandTotal(0);
        cartRequestModel.setIsCODEligible(false);
        cartRequestModel.setIsEDS(false);
        cartRequestModel.setIsMoveToShortlist(false);
        cartRequestModel.setPromoDiscountPercentage(0);
        cartRequestModel.setProductCode(0);
        return cartRequestModel;
    }

    @Override
    public void onSuccess(Object o, int api) {
        if (apiFrom != -1)
            loadingDialog(false);
        try {
            if (o != null) {
                switch (apiFrom) {
                    case -1:
                        accessToken(o);
                        break;
                    case 1:
                        getCartCount(o);
                        break;
                    case 2:
                        getCartCount(o);
                        break;
                }
            } else failureAction(mContext.getString(R.string.some_thing_went_wrong));
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void getCartCount(Object o) {
        try {
            GetCartResponseModel responseModel = (GetCartResponseModel) o;
            if (responseModel != null && responseModel.isIsSuccess()) {
                view.updateCartCount(responseModel);
            }
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void accessToken(Object o) {
        try {
            TokenResponseModel responseModel = (TokenResponseModel) o;
            if (responseModel != null && !cutNull(responseModel.getAccessToken()).isEmpty() && !cutNull(responseModel.getTokenType()).isEmpty()) {
                HeaderValues.ACESS_TOKEN = responseModel.getAccessToken();
                HeaderValues.TOKEN_TYPE = responseModel.getTokenType();
                apiCall(apiFrom);
            } else {
                HeaderValues.ACESS_TOKEN = "";
                HeaderValues.TOKEN_TYPE = "";
                tokenError(o);
            }
        } catch (Exception e) {
            HeaderValues.TOKEN_TYPE = "";
            HeaderValues.ACESS_TOKEN = "";
            tokenError(o);
        }
    }

    private void tokenError(Object o) {
        loadingDialog(false);
        if (view != null)
            view.sessionError("");
    }

    private String cutNull(Object o) {
        return Utils.cutNull(o);
    }

    @Override
    public void onFailure(String body) {
        failureAction(body);
    }

    @Override
    public void onErrorBody(Object o, int api) {
        loadingDialog(false);
        switch (apiFrom) {
            case -1:
                tokenError(o);
                break;
            case 1:
                failureAction(mContext.getString(R.string.some_thing_went_wrong));
                break;
            case 2:
                failureAction(mContext.getString(R.string.some_thing_went_wrong));
                break;
        }
    }

    private void loadingDialog(boolean status) {
        if (view != null) {
            if (status)
                view.showLoading();
            else view.hideLoading(status);
        }
    }

    private void failureAction(String s) {
        loadingDialog(false);
        if (view != null) {
            view.showError(s);
        }
    }

    public void inCreaseCount(String str, int cartID, double productRate, String productCode) {
        model = new GetCartRequestModel();
        model.setCommand(3);
        model.setQuantity(Integer.parseInt(str));
        model.setCartItemID(cartID);
        model.setDeliveryMode(2);
        model.setGrandTotal(productRate);
        model.setProductCode(0);
        apiCall(2);
    }

    public void deCreaseCount(String str, int cartID, double productRate, String productCode) {
        model = new GetCartRequestModel();
        model.setCommand(4);
        model.setQuantity(Integer.parseInt(str));
        model.setCartItemID(cartID);
        model.setDeliveryMode(2);
        model.setGrandTotal(productRate);
        model.setProductCode(0);
        apiCall(2);
    }
}
