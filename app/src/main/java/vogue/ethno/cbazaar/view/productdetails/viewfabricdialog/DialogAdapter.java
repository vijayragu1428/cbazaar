package vogue.ethno.cbazaar.view.productdetails.viewfabricdialog;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.PatternProductDetailItem;

public class DialogAdapter extends PagerAdapter {
    private List<PatternProductDetailItem> items;
    private Context mContext;
    private String code;

    public DialogAdapter(List<PatternProductDetailItem> productDetailItems, Context context, String productCode) {
        this.items = productDetailItems;
        this.mContext = context;
        this.code = productCode;
    }

    @Override
    public int getCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (RelativeLayout) object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.view_pager_view_used_fab, container, false);
        ImageView slider = view.findViewById(R.id.img_slider);
        TextView title = view.findViewById(R.id.txt_tilte);
        WebView des = view.findViewById(R.id.txt_description);
            des.getSettings().setJavaScriptEnabled(true);
            des.loadDataWithBaseURL("", items.get(position).getComponentDescription(), "text/html", "UTF-8", "");
             title.setText(items.get(position).getComponentName()+"("+code+")");
         Glide.with(mContext).load(items.get(position).getUpperImage())
                .apply(RequestOptions.placeholderOf(R.drawable.ev_logo))
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .into(slider);


        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    private class WebViewController extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
         }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
         }
    }
}
