package vogue.ethno.cbazaar.view.productdescription;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.AdditionalInfoItem;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductDescriptionFragment extends Fragment {

    @BindView(R.id.rv_decription)
    RecyclerView rvDecription;
    Unbinder unbinder;
    private List<AdditionalInfoItem> addInfo;
    private ProductDetailsAdapter detailsAdapter;

    public static ProductDescriptionFragment getInstance(List<AdditionalInfoItem> additionalInfoItems) {
        ProductDescriptionFragment descriptionFragment = new ProductDescriptionFragment();
        descriptionFragment.addInfo = additionalInfoItems;
        return descriptionFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.product_description));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_description, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        intUi();
    }

    private void intUi() {
        detailsAdapter=new ProductDetailsAdapter(addInfo);
        rvDecription.setLayoutManager(new LinearLayoutManager(getContext()));
        rvDecription.setAdapter(detailsAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
