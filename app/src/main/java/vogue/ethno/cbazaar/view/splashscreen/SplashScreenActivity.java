package vogue.ethno.cbazaar.view.splashscreen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.data.localdb.pref.AppPreference;
import vogue.ethno.cbazaar.view.homescreen.HomeActivity;
import vogue.ethno.cbazaar.view.login.LoginActivity;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;
                if (AppPreference.getPrefsHelper().getPref(AppPreference.AGENT_ID, "").isEmpty()) {
                    intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                 } else {
                    intent = new Intent(SplashScreenActivity.this, HomeActivity.class);
                }
                startActivity(intent);
                finish();
            }
        }, 2000);
    }
}
