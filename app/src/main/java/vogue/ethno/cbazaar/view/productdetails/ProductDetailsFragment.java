package vogue.ethno.cbazaar.view.productdetails;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.app.CbzaarApplication;
import vogue.ethno.cbazaar.apputils.GlobalConstant;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.classmodel.responsemodel.bodyheight.BodyHeightResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.bodyheight.MeasurementFeetItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.CartDetailItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.AddOnItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.AdditionalInfoItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.CommonDetail;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.MeasurementOptions;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.PatternProductDetailItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.ProductDetailsResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.ProductSizeItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.readysize.NaturalWaistResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.topstyle.TopStyleResponse;
import vogue.ethno.cbazaar.view.address.AddressFragment;
import vogue.ethno.cbazaar.view.commonwebview.CommonWebViewFragment;
import vogue.ethno.cbazaar.view.homescreen.FragmentListener;
import vogue.ethno.cbazaar.view.homescreen.HomeActivity;
import vogue.ethno.cbazaar.view.homescreen.fragment.home.ProductTypeFragment;
import vogue.ethno.cbazaar.view.login.userlogin.UserLoginActivity;
import vogue.ethno.cbazaar.view.productdescription.ProductDescriptionFragment;
import vogue.ethno.cbazaar.view.productdetails.adapters.AddOnsAdapter;
import vogue.ethno.cbazaar.view.productdetails.adapters.ProductSizeAdapter;
import vogue.ethno.cbazaar.view.productdetails.adapters.ReadySizeAdapter;
import vogue.ethno.cbazaar.view.productdetails.adapters.ViewPagerAdapter;
import vogue.ethno.cbazaar.view.productdetails.viewfabricdialog.DialogViewFabricFragment;
import vogue.ethno.cbazaar.view.productionstyle.ProductStyleFragment;

import static vogue.ethno.cbazaar.apputils.GlobalConstant.PRODUCT_DETAILS;


public class ProductDetailsFragment extends Fragment implements ProductDetailsListener, ProductSizeAdapter.OnSizeClick, AddOnsAdapter.OnChecked {

    @BindView(R.id.view_pager_slider)
    ViewPager viewPagerSlider;
    @BindView(R.id.lin_total)
    NestedScrollView linTotal;
    @BindView(R.id.lin_total_size)
    LinearLayout linTotalSize;
    @BindView(R.id.img_view_fabric)
    TextView patternDetails;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.txt_amount)
    TextView txtAmount;
    @BindView(R.id.txt_price)
    TextView txtPrice;
    @BindView(R.id.txt_total_price)
    TextView txtTotalPrice;
    @BindView(R.id.spinner_quantity)
    Spinner spinnerQuantity;
    @BindView(R.id.rv_add_ons)
    RecyclerView rvAddOns;
    @BindView(R.id.txt_bust_size)
    TextView txtBustSize;
    @BindView(R.id.lin_spin)
    LinearLayout linSpin;
    @BindView(R.id.txt_guide)
    TextView txtGuide;
    @BindView(R.id.rel_size_guide_check)
    RelativeLayout relSizeGuideCheck;
    @BindView(R.id.rv_ready_size)
    RecyclerView rvReadySize;
    @BindView(R.id.rv_size)
    RecyclerView rvSize;
    @BindView(R.id.btn_add_to_cart)
    Button btnAddToCart;
    @BindView(R.id.btn_buy_now)
    Button btnBuyNow;
    Unbinder unbinder;
    @BindView(R.id.indicator)
    CirclePageIndicator indicator;
    @BindView(R.id.txt_quantity_natural_waist)
    TextView txtQuantityNaturalWaist;
    @BindView(R.id.spinner_natural_waist)
    Spinner spinnerNaturalWaist;
    @BindView(R.id.lin_natural_waist)
    LinearLayout linNaturalWaist;
    @BindView(R.id.txt_circumference)
    TextView txtCircumference;
    @BindView(R.id.spinner_circumference)
    Spinner spinnerCircumference;
    @BindView(R.id.lin_circumference)
    LinearLayout linCircumference;
    @BindView(R.id.txt_height)
    TextView txtHeight;
    @BindView(R.id.spinner_height)
    Spinner spinnerHeight;
    @BindView(R.id.lin_height)
    LinearLayout linHeight;
    @BindView(R.id.txt_floor)
    TextView txtFloor;
    @BindView(R.id.spinner_floor)
    Spinner spinnerFloor;
    @BindView(R.id.lin_floor)
    LinearLayout linFloor;
    @BindView(R.id.txt_style)
    TextView txtStyle;
    @BindView(R.id.spinner_style)
    Spinner spinnerStyle;
    @BindView(R.id.lin_style)
    LinearLayout linStyle;
    @BindView(R.id.lin_bus_size)
    LinearLayout linBusSize;
    @BindView(R.id.txt_pro_des)
    TextView txtProDes;
    @BindView(R.id.txt_pro_style)
    TextView txtProStyle;
    @BindView(R.id.txt_select_height)
    TextView txtSelectHeight;
    @BindView(R.id.txt_discount_price)
    TextView txtDiscountPrice;

    private FragmentListener mListener;
    private HomeActivity activity;
    private Context mContext;
    private ProductDetailsPresenter presenter;
    private ViewPagerAdapter viewPagerAdapter;
    private ReadySizeAdapter readySizeAdapter;
    private AddOnsAdapter addOnsAdapter;
    private List<String> imageUrl;
    private double itemAmt = 0;
    private LinearLayout layout;
    private String productStyle = "";
    private List<AdditionalInfoItem> addInfo;
    private ProductSizeAdapter sizeAdapter;
    private String sizGuideLink = "";
    private List<PatternProductDetailItem> patternDetailsList;
    private ProductSizeItem productSizeItem;
    private CommonDetail readySize;
    private String productCode = "",productName="";
    private String redirectUrl = "";
    private int spinnerHeightListPos = 0;
    private PopupWindow heightPopupWindow;
    private ArrayList<String> heightListDropdown;
    private CommonDetail productCommonDetails;
    private MeasurementOptions mesurementOptions;
    private TopStyleResponse kameezStyle;
    private List<String> selectedAddon = new ArrayList<>(0);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();

        if (args != null && args.containsKey(GlobalConstant.REDIRECT_URL)) {
            productCode = args.getString(GlobalConstant.PRODUCT_CODE);
            redirectUrl = args.getString(GlobalConstant.REDIRECT_URL);
            productName = args.getString(GlobalConstant.PRODUCT_NAME);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            mListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        presenter.destroyView();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        getScreenSizes();
        linTotal.setVisibility(View.GONE);
        activity = (HomeActivity) getActivity();
        mContext = getContext();
        intAllRv();
        presenter = new ProductDetailsPresenter();
        presenter.setView(this);
        presenter.apiCall(1);
    }

    private void getScreenSizes() {
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        CbzaarApplication.size = metrics.heightPixels;
    }

    private void intAllRv() {
        /*view pager*/
        /*readySize Adapter*///heightListDropdown  like
        readySizeAdapter = new ReadySizeAdapter(this);
        rvReadySize.setLayoutManager(new LinearLayoutManager(mContext));
        rvReadySize.setAdapter(readySizeAdapter);

        /*add ons*/
        addOnsAdapter = new AddOnsAdapter(mContext, this);
        rvAddOns.setLayoutManager(new GridLayoutManager(mContext, 2));
        rvAddOns.setAdapter(addOnsAdapter);
        /*Productdetails adapter*/
        sizeAdapter = new ProductSizeAdapter(this, rvSize);
        rvSize.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        rvSize.setAdapter(sizeAdapter);
    }

    @Override
    public void showLoading() {
        activity.showDialog("", false,linTotal);
    }

    @Override
    public void hideLoading(boolean status) {
        activity.hideDialog(status);
    }

    @Override
    public Context getActivityContext() {
        return getContext();
    }

    @Override
    public void showError(String string) {
        activity.showToast(string);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void sessionError(String string) {
        mListener.nextFragment(new ProductTypeFragment() );
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btn_add_to_cart, R.id.rel_size_guide_check, R.id.btn_buy_now, R.id.txt_pro_des, R.id.txt_pro_style, R.id.img_view_fabric})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_add_to_cart:
                buyNow(false);
                break;
            case R.id.btn_buy_now:
                buyNow(true);
                break;
            case R.id.txt_pro_des:
                descriptionAction();
                break;
            case R.id.txt_pro_style:
                styleAction();
                break;
            case R.id.rel_size_guide_check:
                sizeGuideAction();
                break;
            case R.id.img_view_fabric:
                viewUsedFabric();
                break;
        }
    }

    private void buyNow(boolean b) {
        if (validation()) {
            if (b) {
                if (activity.isUserLoginIn()) {
                    presenter.addToCart(productCommonDetails, spinnerQuantity.getSelectedItem(), mesurementOptions, getProductId(), productSizeItem, b, spinnerHeight.getSelectedItem() != null ? spinnerHeight.getSelectedItem().toString().replaceAll(" ", "") : spinnerHeight.getSelectedItem(), spinnerNaturalWaist.getSelectedItem(), spinnerCircumference.getSelectedItem(), spinnerFloor.getSelectedItem(), selectedAddon);
                } else {
                    ((CbzaarApplication) activity.getApplicationContext()).fromWhere = PRODUCT_DETAILS;
                    startActivityForResult(new Intent(activity, UserLoginActivity.class), 100);
                }
            } else
                presenter.addToCart(productCommonDetails, spinnerQuantity.getSelectedItem(), mesurementOptions, getProductId(), productSizeItem, b, spinnerHeight.getSelectedItem() != null ? spinnerHeight.getSelectedItem().toString().replaceAll(" ", "") : spinnerHeight.getSelectedItem(), spinnerNaturalWaist.getSelectedItem(), spinnerCircumference.getSelectedItem(), spinnerFloor.getSelectedItem(), selectedAddon);
        }
    }
    /*private void addToCart() {
        if (validation())
            presenter.addToCart(productCommonDetails, spinnerQuantity.getSelectedItem(), mesurementOptions, getProductId(), productSizeItem, false);
    }*/

    private boolean validation() {
        if (readySize.isIsBodyHeight()) {
            if (spinnerHeight.getSelectedItemPosition() == 0) {
                activity.showToast(getString(R.string.please_select_height));
                return false;
            }
            if (readySize.isIsShowWaistToFloor())
                if (spinnerFloor.getSelectedItemPosition() == 0) {
                    activity.showToast(getString(R.string.please_select_floor));
                    return false;
                }
        }
        if (readySize.isIsShowNaturalWaist()) {
            if (spinnerNaturalWaist.getSelectedItemPosition() == 0) {
                activity.showToast(getString(R.string.please_select_waist));
                return false;
            }
            if (readySize.isIsShowWaistCircumference())
                if (spinnerCircumference.getSelectedItemPosition() == 0) {
                    activity.showToast(getString(R.string.please_select_circumference));

                    return false;
                }
        }
        return true;
    }


    private void viewUsedFabric() {
        if (patternDetailsList != null && patternDetailsList.size() > 0) {
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            DialogViewFabricFragment newFragment = new DialogViewFabricFragment(patternDetailsList, productCode);
            //Bundle transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity()).toBundle();
            ///newFragment.setArguments(transitionActivityOptions);
            newFragment.show(fragmentManager, "dialog");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            if (validation()) {
                if (activity.isUserLoginIn()) {
                    presenter.addToCart(productCommonDetails, spinnerQuantity.getSelectedItem(), mesurementOptions, getProductId(), productSizeItem, true, spinnerHeight.getSelectedItem(), spinnerNaturalWaist.getSelectedItem(), spinnerCircumference.getSelectedItem(), spinnerFloor.getSelectedItem(), selectedAddon);
                }
            }
        }
    }

    private void sizeGuideAction() {
        if (!Utils.cutNull(sizGuideLink).isEmpty())
            mListener.nextFragment(CommonWebViewFragment.getInstance(getString(R.string.size_chart), sizGuideLink) );
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.setTitle(productName);
        activity.showHideToolbar(false);
    }

    private void descriptionAction() {
        if (addInfo != null && addInfo.size() > 0)
            mListener.nextFragment(ProductDescriptionFragment.getInstance(addInfo), R.animator.slide_up_new,R.animator.slide_down,R.animator.slide_up_new,R.animator.slide_down);
    }

    private void styleAction() {
        if (!cutNull(productStyle).isEmpty())
            mListener.nextFragment(ProductStyleFragment.getInstance(productStyle), R.animator.slide_up_new,R.animator.slide_down,R.animator.slide_up_new,R.animator.slide_down);
    }

    @Override
    public void successFromServer(ProductDetailsResponseModel responseModel) {
        imageUrl = new ArrayList<>(0);
        try {
            if (cutNull(responseModel.getErrorMessage()).contains("Oops")) {
                showError(getString(R.string.this_product_out));
                linTotal.setVisibility(View.GONE);
                activity.onBackPressed();
                return;
            }


            linTotal.setVisibility(View.VISIBLE);
            txtAmount.setText(cutNull(responseModel.getCommonDetail().getDispatchDate()));
            if (responseModel.getProductDetail() != null) {


                this.mesurementOptions = responseModel.getMeasurementOptions();
                if (responseModel.getProductDetail().size() > 0) {
                    if (responseModel.getPatternProductDetail() != null && responseModel.getPatternProductDetail().size() > 0)
                        this.patternDetailsList = responseModel.getPatternProductDetail();
                    else patternDetails.setVisibility(View.GONE);
                    txtTitle.setText(cutNull(responseModel.getProductDetail().get(0).getName()));
                    txtPrice.setText(cutNull(Utils.getAmountString(responseModel.getProductDetail().get(0).getCost())));

                    if (responseModel.getProductDetail().get(0).getDiscount() > 0) {
                        itemAmt = responseModel.getProductDetail().get(0).getDiscountCost();
                        txtPrice.setPaintFlags(txtDiscountPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        txtDiscountPrice.setText(cutNull(Utils.getAmountString(responseModel.getProductDetail().get(0).getDiscountCost())));
                        txtDiscountPrice.setVisibility(View.VISIBLE);
                    } else {
                        txtDiscountPrice.setVisibility(View.GONE);
                        itemAmt = responseModel.getProductDetail().get(0).getCost();
                    }

                    imageUrl.add(cutNull(responseModel.getProductDetail().get(0).getLargeImageUrl()));
                    imageUrl.add(cutNull(responseModel.getProductDetail().get(0).getImageUrl()));
                    imageUrl.add(cutNull(responseModel.getProductDetail().get(0).getLowerLargeImageUrl()));
                    viewPagerAdapter = new ViewPagerAdapter(mContext, imageUrl);
                    viewPagerSlider.setAdapter(viewPagerAdapter);
                    indicator.setViewPager(viewPagerSlider);
                    indicator.setPageColor(ContextCompat.getColor(getContext(), R.color.gray));
                    indicator.setFillColor(ContextCompat.getColor(getContext(), R.color.appColor));
                }

                if (/*responseModel.getCommonDetail().isIsSize() &&*/responseModel.getCommonDetail() != null && responseModel.getCommonDetail().getProductSize() != null && responseModel.getCommonDetail().getProductSize().size() > 0) {
                    linBusSize.setVisibility(View.VISIBLE);
                    loadProductSize(responseModel.getCommonDetail().getProductSize());
                } else {
                    linBusSize.setVisibility(View.GONE);
                }
                setBodyMeasureMent(responseModel.getCommonDetail());
                if (responseModel.getCommonDetail() != null && responseModel.getCommonDetail().getProductStyle() != null)
                    this.productStyle = responseModel.getCommonDetail().getProductStyle();

                if (responseModel.getCommonDetail() != null && responseModel.getCommonDetail().getSizeGuideLink() != null)
                    this.sizGuideLink = responseModel.getCommonDetail().getSizeGuideLink();
                if (responseModel.getCommonDetail() != null && responseModel.getCommonDetail().getQuantity() != null && responseModel.getCommonDetail().getQuantity().size() > 0)
                    setUpSpinner(responseModel.getCommonDetail().getQuantity());
            }
            if (responseModel.getAdditionalInfo() != null && responseModel.getAdditionalInfo().size() > 0) {
                this.addInfo = responseModel.getAdditionalInfo();
            }
            if (responseModel.getAddOn() != null && responseModel.getAddOn().size() > 0) {
                addOnsAdapter.loadData(responseModel.getAddOn());
            }
        } catch (Exception e) {
            e.printStackTrace();
            showError(getString(R.string.this_product_out));
            activity.onBackPressed();
        }
    }

    @Override
    public String getProductId() {
        //  return "Ghbs1806810";
        // return "Slbs1808958";
        //  return "sadsf5264";
        return productCode;
    }

    @Override
    public ProductSizeItem getHeightRequest() {
        return productSizeItem;
    }

    @Override
    public void successFromServerBodyHeight(final BodyHeightResponseModel responseModel) {
        //set body heightListDropdown
        if (responseModel.getMeasurementFeet() != null && responseModel.getMeasurementFeet().size() > 0) {
            sizeAdapter.notifyDataSetChanged();
            heightListDropdown = new ArrayList<>(0);
            heightListDropdown.add(getString(R.string.select_height));
            for (MeasurementFeetItem feetItem : responseModel.getMeasurementFeet())
                if (!cutNull(feetItem).isEmpty())
                    heightListDropdown.add(feetItem.getText());
            String[] heightSpinner = heightListDropdown.toArray(new String[heightListDropdown.size()]);
            ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, heightSpinner);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerHeight.setAdapter(arrayAdapter);
            spinnerHeight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        if (!productCommonDetails.isIsKameezStyle()) {
                            presenter.waistFloor(responseModel.getMeasurementFeet().get(position).getValue());
                            spinnerFloor.setEnabled(true);
                            spinnerFloor.setClickable(true);
                        } else {
                            //presenter.getTopStyle(responseModel.getMeasurementFeet().get(position));
                            spinnerStyle.setEnabled(false);
                            spinnerStyle.setClickable(false);
                        }
                    } else {
                        if (!productCommonDetails.isIsKameezStyle()) {
                            spinnerFloor.setEnabled(false);
                            spinnerFloor.setClickable(false);
                            String[] heightSpinner = new String[1];
                            heightSpinner[0] = getString(R.string.select_waist_floor);
                            loadWaistSpinner(heightSpinner);
                        } else {
                            spinnerStyle.setEnabled(false);
                            spinnerStyle.setClickable(false);
                            String[] heightSpinner = new String[1];
                            heightSpinner[0] = getString(R.string.as_showun_in_the);
                            loadTopStyle(heightSpinner);
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

    }

    @Override
    public void successFromTopStyle(TopStyleResponse responseModel) {
        if (responseModel != null) {
            this.kameezStyle = responseModel;
            List<String> height = new ArrayList<>(0);
           /* for (NaturalWaistResponseModel feetItem : responseModel)
                if (!cutNull(feetItem).isEmpty())
                    height.add(feetItem.getInches());*/
            String[] heightSpinner;
            if (height.size() > 0)
                heightSpinner = height.toArray(new String[height.size()]);
            else {
                heightSpinner = new String[1];
                heightSpinner[0] = getString(R.string.as_showun_in_the);
            }
            loadTopStyle(heightSpinner);

        }
    }

    @Override
    public void updateCartCount(GetCartResponseModel responseModel) {
        if (responseModel != null && responseModel.getCartDetail() != null) {
            int count = 0;
            for (CartDetailItem detailItem : responseModel.getCartDetail()) {
                count = count + detailItem.getProductQuantity();
            }
            activity.upDateCartCont(String.valueOf(count));
            activity.onBackPressed();
        } else activity.upDateCartCont("0");
    }

    @Override
    public void buyNowCart(GetCartResponseModel responseModel) {

        //   PaymentFragment fragment = new PaymentFragment(responseModel);
        AddressFragment fragment = new AddressFragment(responseModel);
        mListener.nextFragment(fragment);
    }

    private void loadTopStyle(String[] heightSpinner) {
        ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, heightSpinner);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStyle.setAdapter(arrayAdapter);
        spinnerStyle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void successFromServerNaturalWaist(final List<NaturalWaistResponseModel> responseModel) {
        if (responseModel != null && responseModel.size() > 0) {
            sizeAdapter.notifyDataSetChanged();
            List<String> height = new ArrayList<>(0);
            height.add(getString(R.string.select_natural));
            for (NaturalWaistResponseModel feetItem : responseModel)
                if (!cutNull(feetItem).isEmpty())
                    height.add(feetItem.getInches());
            String[] heightSpinner = height.toArray(new String[height.size()]);
            ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, heightSpinner);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerNaturalWaist.setAdapter(arrayAdapter);
            spinnerNaturalWaist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        presenter.waiseCircumference(responseModel.get(position).getId());
                        spinnerCircumference.setEnabled(true);
                        spinnerCircumference.setClickable(true);
                    } else {
                        spinnerCircumference.setEnabled(false);
                        spinnerCircumference.setClickable(false);
                        String[] heightSpinner = new String[1];
                        heightSpinner[0] = getString(R.string.select_waist_cir);
                        loadcircumFerenceSpinner(heightSpinner);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void loadcircumFerenceSpinner(String[] heightSpinner) {
        ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, heightSpinner);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCircumference.setAdapter(arrayAdapter);
        spinnerCircumference.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // presenter.waistFloor(responseModel.get(position).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void successFromCircumference(final List<NaturalWaistResponseModel> responseModel) {
        if (responseModel != null && responseModel.size() > 0) {
            List<String> height = new ArrayList<>(0);
            height.add(getString(R.string.select_waist_cir));
            for (NaturalWaistResponseModel feetItem : responseModel)
                if (!cutNull(feetItem).isEmpty())
                    height.add(feetItem.getInches());
            String[] heightSpinner = height.toArray(new String[height.size()]);
            loadcircumFerenceSpinner(heightSpinner);
        }
    }

    @Override
    public void successFromWaist(final List<NaturalWaistResponseModel> responseModel) {
        if (responseModel != null && responseModel.size() > 0) {
            List<String> height = new ArrayList<>(0);
            height.add(getString(R.string.select_waist_floor));
            for (NaturalWaistResponseModel feetItem : responseModel)
                if (!cutNull(feetItem).isEmpty())
                    height.add(feetItem.getInches());
            String[] heightSpinner = height.toArray(new String[height.size()]);
            loadWaistSpinner(heightSpinner);
        }
    }

    private void loadWaistSpinner(String[] heightSpinner) {
        ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, heightSpinner);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFloor.setAdapter(arrayAdapter);
        spinnerFloor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void setBodyMeasureMent(CommonDetail commonDetail) {
        try {
            if (commonDetail != null) {
                this.productCommonDetails = commonDetail;
                boolean isShow = false;
                this.readySize = commonDetail;
                List<String> bodySizes = new ArrayList<>(0);
                spinnerCircumference.setVisibility(View.GONE);
                spinnerHeight.setVisibility(View.GONE);
                spinnerNaturalWaist.setVisibility(View.GONE);
                spinnerStyle.setVisibility(View.GONE);
                spinnerFloor.setVisibility(View.GONE);
                txtQuantityNaturalWaist.setVisibility(View.GONE);
                txtCircumference.setVisibility(View.GONE);
                txtHeight.setVisibility(View.GONE);
                txtFloor.setVisibility(View.GONE);
                txtStyle.setVisibility(View.GONE);
                if (commonDetail.isIsShowNaturalWaist()) {
                    isShow = true;
                    txtQuantityNaturalWaist.setVisibility(View.VISIBLE);
                    txtQuantityNaturalWaist.setText(getString(R.string.show_natural_waist));
                    spinnerNaturalWaist.setVisibility(View.VISIBLE);
                }
                if (commonDetail.isIsShowWaistCircumference()) {
                    txtCircumference.setText(getString(R.string.show_waist_circumference));
                    spinnerCircumference.setVisibility(View.VISIBLE);
                    txtCircumference.setVisibility(View.VISIBLE);
                    isShow = true;
                }

                if (commonDetail.isIsBodyHeight()) {
                    txtHeight.setVisibility(View.VISIBLE);
                    txtHeight.setText(getString(R.string.your_height));
                    isShow = true;
                    // TODO: 10/12/2018 modified.
                    spinnerHeight.setVisibility(View.VISIBLE);
                }
                if (commonDetail.isIsShowWaistToFloor() && commonDetail.isIsBodyHeight()) {
                    txtFloor.setVisibility(View.VISIBLE);
                    txtFloor.setText(getString(R.string.show_waist_floor));
                    spinnerFloor.setVisibility(View.VISIBLE);
                    isShow = true;
                }
                if (commonDetail.isIsKameezStyle()) {
                    txtStyle.setVisibility(View.VISIBLE);
                    txtStyle.setText(getString(R.string.top_style));
                    spinnerStyle.setVisibility(View.VISIBLE);
                    isShow = true;
                }
                if (!isShow && !commonDetail.isIsSize()) {
                    linTotalSize.setVisibility(View.GONE);
                } else linTotalSize.setVisibility(View.VISIBLE);
            } else {
                showError(getString(R.string.this_product_out));
                activity.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
            showError(getString(R.string.this_product_out));
            activity.onBackPressed();
        }
    }


    @SuppressLint("ResourceType")
    private void setUpSpinner(List<String> quantity) {
       /* PopupMenu popup = new PopupMenu(mContext, linSpin, Gravity.CENTER);
        popup.getMenuInflater().inflate(R.layout.list_item_add_on, popup.getMenu());
        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                 return true;
            }
        });
        popup.show();*/

        String[] arr = quantity.toArray(new String[quantity.size()]);
        ArrayAdapter aa = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, arr);
        aa.setDropDownViewResource(R.layout.simple_spinner_dropdown_item_app);
        spinnerQuantity.setAdapter(aa);
        spinnerQuantity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    int pos = Integer.parseInt(spinnerQuantity.getSelectedItem().toString());
                    double amt = pos * itemAmt;
                    txtTotalPrice.setText(cutNull(getString(R.string.total) + " " + Utils.getAmountString(amt)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadProductSize(List<ProductSizeItem> productSize) {
        sizeAdapter.loadData(productSize);
    }

    private String cutNull(Object o) {
        return Utils.cutNull(o);
    }

    @Override
    public void onSizeClickEvent(ProductSizeItem productSizeItem) {
        boolean isSelect = false;
        this.productSizeItem = productSizeItem;
        if (readySize.isIsBodyHeight()) {
            presenter.apiCall(2);
            isSelect = true;
        }
        if (readySize.isIsShowNaturalWaist()) {
            isSelect = true;
            presenter.apiCall(3);
        }
        if (!isSelect)
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    if (!rvSize.isComputingLayout()) {
                        sizeAdapter.notifyDataSetChanged();
                    }
                }
            });

        // sizeAdapter.notifyDataSetChanged();
//        if (!isNotSelect)
//            sizeAdapter.notifyDataSetChanged();

    }

    @Override
    public void onCheckedChangeListener(boolean status, AddOnItem onItem) {
        if (status)
            selectedAddon.add(onItem.getCode());
        else selectedAddon.remove(onItem.getCode());
    }


}
  /* LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            txtSelectHeight.setText(heightListDropdown.get(0));
            View layoutOfPopup = inflater.inflate(R.layout.popup_list, null);
            ListView listView = (ListView) layoutOfPopup.findViewById(R.id.list_pop);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                    android.R.layout.simple_list_item_1, android.R.id.text1, heightSpinner);
            listView.setAdapter(adapter);
            heightPopupWindow = new PopupWindow(layoutOfPopup, LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            heightPopupWindow.setContentView(layoutOfPopup);
            heightPopupWindow.setOutsideTouchable(true);
            heightPopupWindow.setFocusable(true);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {
                        return;
                    }

                    txtSelectHeight.setText(heightListDropdown.get(position));
                    if (heightPopupWindow != null && heightPopupWindow.isShowing())
                        heightPopupWindow.dismiss();
                    if (spinnerHeightListPos != position) {
                        spinnerHeightListPos = position;
                        presenter.getTopStyle(responseModel.getMeasurementFeet().get(position - 1));
                    }
                }
            });
            linHeight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    heightPopupWindow.showAsDropDown(linHeight);
                }
            });

*/
