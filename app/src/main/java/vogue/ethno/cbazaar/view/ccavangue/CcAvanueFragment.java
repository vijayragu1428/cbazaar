package vogue.ethno.cbazaar.view.ccavangue;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart.ConfirmCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.rsakey.RsaKeyResponseModel;
import vogue.ethno.cbazaar.view.ccavangue.utils.AvenuesParams;
import vogue.ethno.cbazaar.view.ccavangue.utils.Constants;
import vogue.ethno.cbazaar.view.ccavangue.utils.RSAUtility;
import vogue.ethno.cbazaar.view.ccavangue.utils.ServiceUtility;
import vogue.ethno.cbazaar.view.homescreen.FragmentListener;
import vogue.ethno.cbazaar.view.homescreen.HomeActivity;

@SuppressLint("ValidFragment")
public class CcAvanueFragment extends Fragment {
    String encVal;
    String vResponse;
    @BindView(R.id.webview)
    WebView webview;

    @BindView(R.id.rel_root)
    RelativeLayout relRoot;
    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.view)
    View view;


    private HomeActivity activity;
    private ConfirmCartResponseModel cartResponseModel;
    private String accessCode = "", merchantId = "", orderId = "", currency = "", amount = "0", redirectUrl = "", cancelUrl = "", rsaKey = "";
    private FragmentListener mListener;

    public CcAvanueFragment(List<RsaKeyResponseModel> rsaKeyResponseModel, ConfirmCartResponseModel model) {
        try {
            this.cartResponseModel = model;
            this.accessCode =rsaKeyResponseModel.get(3).getValue();//"AVBA02GC58AQ64ABQA"; //
            this.merchantId = "8694";
            this.orderId = model.getOrderNumber();
            this.currency = model.getCartdetail().getInfomations().getCurrencyCode();
            //  this.amount = String.valueOf(model.getCartdetail().getCartSummaries().getGrandTotal());
            this.amount = "1";
            this.redirectUrl = rsaKeyResponseModel.get(1).getValue();
            this.cancelUrl = rsaKeyResponseModel.get(2).getValue();
            this.rsaKey =rsaKeyResponseModel.get(0).getValue();// "26A127A81FA32F0A1A5251A56D810C1C";
        } catch (Exception e) {
            activity.showToast(getString(R.string.some_thing_went_wrong));
            activity.onBackPressed();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
         activity.showHideToolbar(true);
        toolBar.setNavigationIcon(getContext().getResources().getDrawable(R.drawable.ic_back_arrow));
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    /*    @Override
        public void onOptionsMenuClosed(Menu menu) {
            super.onOptionsMenuClosed(menu);
        }
        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
        {
            inflater.inflate(R.menu.main_menu, menu);
            menu.findItem(R.id.action_cart).setVisible(false);
            getActivity().invalidateOptionsMenu();
            //setHasOptionsMenu(false);
            super.onCreateOptionsMenu(menu,inflater);
        }*/
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            mListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_webview, container, false);
        ButterKnife.bind(this, view);
        initUi();
        return view;
    }

    private void initUi() {
        activity = (HomeActivity) getActivity();
        activity.showHideToolbar(false);
        new RenderView().execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private class RenderView extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            StringBuffer vEncVal = new StringBuffer("");
            vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.AMOUNT, amount));
            vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.CURRENCY, currency));
            encVal = Utils.cutNull(RSAUtility.encrypt(vEncVal.substring(0, vEncVal.length() - 1), rsaKey));  //encrypt amount and currency
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            hideLoading();
            @SuppressWarnings("unused")
            class MyJavaScriptInterface {
                @JavascriptInterface
                public void processHTML(String html) {
                    // process the html source code to get final status of transaction
                    String status = null;
                    if (html.indexOf("Failure") != -1) {
                        status = "Transaction Declined!";
                    } else if (html.indexOf("Success") != -1) {
                        status = "Transaction Successful!";
                    } else if (html.indexOf("Aborted") != -1) {
                        status = "Transaction Cancelled!";
                    } else {
                        status = "Status Not Known!";
                    }
                    //Toast.makeText(getApplicationContext(), status, Toast.LENGTH_SHORT).show();
                    //    Intent intent = new Intent(getApplicationContext(), StatusActivity.class);
                    //     intent.putExtra("transStatus", status);
                    //     startActivity(intent);
                }
            }

            webview.getSettings().setJavaScriptEnabled(true);
            webview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
            webview.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(webview, url);
                    hideLoading();
                    Log.d("urlurlonPageFinished", url);
                    if (url.indexOf("/ccavResponseHandler.jsp") != -1) {
                        webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
                    }
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    Log.d("urlurlonPageFinished", url);
                    showLoading();
                }
            });


            try {
                String postData = AvenuesParams.ACCESS_CODE + "=" + URLEncoder.encode(accessCode, "UTF-8")
                        + "&" + AvenuesParams.MERCHANT_ID + "=" + URLEncoder.encode(merchantId,
                        "UTF-8") + "&" + AvenuesParams.ORDER_ID + "=" + URLEncoder.encode(orderId,
                        "UTF-8") + "&" + AvenuesParams.REDIRECT_URL + "=" + URLEncoder.encode(redirectUrl,
                        "UTF-8") + "&" + AvenuesParams.CANCEL_URL + "=" + URLEncoder.encode(cancelUrl,
                        "UTF-8") + "&" + AvenuesParams.ENC_VAL + "=" + URLEncoder.encode(encVal, "UTF-8");
                Log.d("postData", postData);
                webview.postUrl(Constants.TRANS_URL, postData.getBytes());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        private void showLoading() {
            activity.showDialog("", false, relRoot);
        }

        private void hideLoading() {
            activity.hideLoading(false);
        }
    }
}