package vogue.ethno.cbazaar.view.homescreen;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.lang.reflect.Field;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.app.CbzaarApplication;
import vogue.ethno.cbazaar.apputils.GlobalConstant;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.base.BaseActivity;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.CartDetailItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.userlogin.UserLoginResponse;
import vogue.ethno.cbazaar.data.localdb.pref.AppPreference;
import vogue.ethno.cbazaar.view.enquiryform.EnquiryFormFragment;
import vogue.ethno.cbazaar.view.homescreen.fragment.home.HomeFragment;
import vogue.ethno.cbazaar.view.login.LoginActivity;
import vogue.ethno.cbazaar.view.login.userlogin.UserLoginActivity;
import vogue.ethno.cbazaar.view.menu.MenuAdapter;
import vogue.ethno.cbazaar.view.menu.MenuDetailsListener;
import vogue.ethno.cbazaar.view.menu.MenuModel;
import vogue.ethno.cbazaar.view.menu.MenuPresenter;
import vogue.ethno.cbazaar.view.paymentconfirm.PaymentConfirmationFragment;
import vogue.ethno.cbazaar.view.productlistdetails.ProductListFragment;
import vogue.ethno.cbazaar.view.register.RegisterActivity;
import vogue.ethno.cbazaar.view.shoppingbag.ShoppingBagFragment;

public class HomeActivity extends BaseActivity implements FragmentListener,
        FragmentManager.OnBackStackChangedListener, View.OnClickListener, MenuDetailsListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rel_root)
    LinearLayout rel_root;
    @BindView(R.id.content_frame)
    FrameLayout contentFrame;
    @BindView(R.id.img_logo)
    AppCompatImageView imgLogo;
    @BindView(R.id.txt_user_name)
    TextView txtUserName;
    @BindView(R.id.tv_user_log_out)
    TextView tvUserLogOut;
    @BindView(R.id.lin_txt)
    LinearLayout linTxt;
    @BindView(R.id.rv_cate_list)
    RecyclerView rvCateList;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    MenuAdapter adapter;
    MenuModel menuModel;
    MenuPresenter menuPresenter;
    private TextView txtUpdateCartCount;
    @BindView(R.id.tv_enquiry_form)
    TextView tvEnquiryForm;
    @BindView(R.id.txt_login)
    TextView txtLogin;
    @BindView(R.id.txt_register)
    TextView txtRegister;
    @BindView(R.id.lin_log_out)
    LinearLayout linLogOut;
    @BindView(R.id.lin_new_user)
    LinearLayout linNewUser;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;


    private EnquiryFormFragment enquiryFormFragment;
    private MenuItem qrCode, cartItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        ButterKnife.bind(this);
        initUi();
        setMenuItems();
    }

    private void setMenuItems() {
        menuModel = new MenuModel();
        adapter = new MenuAdapter(this, menuModel, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvCateList.setLayoutManager(layoutManager);
        rvCateList.setAdapter(adapter);
        enquiryFormFragment = new EnquiryFormFragment();
        tvEnquiryForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextFragment(enquiryFormFragment);
                closeDrawer();
            }
        });

    }

    private void initUi() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setupDrawer();
         /*getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.content_frame, new ProductTypeFragment(), ProductTypeFragment.class.getName())
                .addToBackStack(ProductTypeFragment.class.getName())
                .commitAllowingStateLoss();*/

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new HomeFragment(), HomeFragment.class.getName())
                .commitAllowingStateLoss();
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        menuPresenter = new MenuPresenter();
        menuPresenter.setView(this);
        menuPresenter.apiCall(1);
    }

    private void setupDrawer() {
        setSupportActionBar(toolbar);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                syncState();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                syncState();
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDrawerToggle.syncState();
        toolbar.setNavigationOnClickListener(this);
        makeActionOverflowMenuShown();
    }

    public void makeActionOverflowMenuShown() {
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            Log.d("", e.getLocalizedMessage());
        }
    }

    public void disableNavigation(boolean disabled) {
        int lockMode;
        if (disabled) {
            lockMode = DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            mDrawerToggle.setDrawerIndicatorEnabled(false);
        } else {
            lockMode = DrawerLayout.LOCK_MODE_UNLOCKED;
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            mDrawerToggle.setDrawerIndicatorEnabled(true);
        }
        mDrawerLayout.setDrawerLockMode(lockMode);
    }

    private void setAnimationOndrawerIcon() {
        ValueAnimator anim = ValueAnimator.ofFloat(1, 0);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float slideOffset = (Float) valueAnimator.getAnimatedValue();
                mDrawerToggle.onDrawerSlide(mDrawerLayout, slideOffset);
            }
        });
        anim.setInterpolator(new DecelerateInterpolator());
        anim.setDuration(800);
        anim.start();
    }

    public void setArrowIconInDrawer() {
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fragment != null && !(fragment instanceof PaymentConfirmationFragment))
            super.onBackPressed();
        //  super.onBackPressed();
    }

    public void openDrawer() {
        if (!mDrawerLayout.isDrawerOpen(GravityCompat.START))
            mDrawerLayout.openDrawer(GravityCompat.START);
    }

    public void closeDrawer() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
            mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
      //  qrCode = menu.findItem(R.id.action_qrcode);
        cartItem = menu.findItem(R.id.action_cart);
        final View cartCount = menu.findItem(R.id.action_cart).getActionView();
        txtUpdateCartCount = (TextView) cartCount.findViewById(R.id.hotlist_hot);
        cartCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtUpdateCartCount.getVisibility() == View.VISIBLE)
                    cartAction();
                else showToast(getString(R.string.cart_empty));
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        String btnName = null;
        switch (itemId) {
//            case R.id.action_qrcode:
//                btnName = "action_qrcode";
//                break;
            case R.id.action_cart:
                cartAction();
                break;
//            case R.id.action_language:
//                btnName = "action_language";
//                break;
        }
        return true;
    }

    private void cartAction() {
        nextFragment(new ShoppingBagFragment());
    }


    private int isFragmentInBackStack(FragmentManager fragmentManager, String fragmentTagName) {
        for (int entry = 0; entry < fragmentManager.getBackStackEntryCount() - 1; entry++) {
            if (fragmentTagName.equals(fragmentManager.getBackStackEntryAt(entry).getName())) {
                return entry + 1;
            }
        }
        return -1;
    }

    @Override
    public void nextFragment(Fragment fragment, int... anims) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Utils.appLog("currentActivity", fragment.getClass().getSimpleName());
        Fragment available = fragmentManager.findFragmentByTag(fragment.getClass().getName());
        if (available != null) {
            int stackPosition = isFragmentInBackStack(fragmentManager, fragment.getClass().getName());
            if (stackPosition != -1 && fragmentManager.getBackStackEntryCount() > stackPosition) {
                FragmentManager.BackStackEntry first = fragmentManager.getBackStackEntryAt(stackPosition);
                fragmentManager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        } else {
            /*if (anims.length == 2)
                fragmentTransaction
                        .setCustomAnimations(anims[0], anims[1]);
            else if (anims.length == 4)*/

                /**/
                fragmentTransaction
                        .setCustomAnimations(R.anim.slide_right_in, R.anim.slide_left_out,
                                R.anim.slide_left_in, R.anim.slide_right_out);
            fragmentTransaction
                    .add(R.id.content_frame, fragment, fragment.getClass().getName())
                    .addToBackStack(fragment.getClass().getName())
                    .commitAllowingStateLoss();
        }
    }


    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
    }

    @Override
    public void onBackStackChanged() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            setArrowIconInDrawer();
        } else {
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            setAnimationOndrawerIcon();
        }
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fragment != null)
            fragment.onResume();
        if (fragment instanceof HomeFragment) {
            disableNavigation(false);
        }
        upDateCartCont(CbzaarApplication.cartCount);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        fragment.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick({R.id.tv_user_log_out, R.id.txt_login, R.id.txt_register, R.id.tv_log_out})
    public void onViewClicked(View view) {
        closeDrawer();
        switch (view.getId()) {
            case R.id.txt_login: {
                ((CbzaarApplication) getApplicationContext()).fromWhere = "main";
                Intent intent = new Intent(this, UserLoginActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.txt_register: {

                Intent intent = new Intent(this, RegisterActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.tv_user_log_out:
                logoutClick();
                break;
            case R.id.tv_log_out:
                AppPreference.getPrefsHelper().clearPref();
                Intent intent = new Intent(this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
        }
    }

    private void logoutClick() {
        AppPreference.getPrefsHelper().savePref(AppPreference.USER_LOGIN_DATA, "");
        AppPreference.getPrefsHelper().savePref(AppPreference.USER_MAIL_ID, "");
        AppPreference.getPrefsHelper().savePref(AppPreference.USER_ID, "");
        linLogOut.setVisibility(View.GONE);
        linNewUser.setVisibility(View.VISIBLE);
        //nextFragment(new ProductDetailsFragment());
        //upDateCartCont("0");
        menuPresenter.apiCall(2);

    }

    @Override
    public void onClick(View v) {
        shouldDisplayHomeUp();
    }

    private void shouldDisplayHomeUp() {
        if (getSupportActionBar() != null) {
            if (shouldOpenDrawer()) {
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawers();
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                setArrowIconInDrawer();
                onBackPressed();
                shouldOpenDrawer();
            } else {
                mDrawerToggle.setDrawerIndicatorEnabled(true);
            }
        }
    }

    private boolean shouldOpenDrawer() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            return true;
        } else {
            return false;
        }
    }

    public void showHideToolbar(boolean status) {
        if (status)
            toolbar.setVisibility(View.GONE);
        else toolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void successFromServer(MenuModel responseModel) {
        setMenuItemsAdapter(responseModel);
        menuPresenter.apiCall(2);
    }

    @Override
    public void onMenuItemClick(String path) {
        if (!TextUtils.isEmpty(path)) {
            closeDrawer();
            ProductListFragment productListFragment = new ProductListFragment();
            Bundle arg = new Bundle();
            arg.putString(GlobalConstant.REDIRECT_URL, path);
            productListFragment.setArguments(arg);
            nextFragment(productListFragment);
        }
    }

    @Override
    public void updateCartCount(GetCartResponseModel responseModel) {
        if (responseModel != null && responseModel.getCartDetail() != null) {
            int count = 0;
            for (CartDetailItem detailItem : responseModel.getCartDetail()) {
                count = count + detailItem.getProductQuantity();
            }
            upDateCartCont(String.valueOf(count));
        } else upDateCartCont("0");
    }

    private void setMenuItemsAdapter(MenuModel responseModel) {
//        ArrayList<MenuModel.Menu> menuArrayList=new ArrayList<MenuModel.Menu>();
//        menuArrayList=responseModel.getMenu();
//        MenuModel.Menu menuObj=new MenuModel.Menu();
//        //menuObj.getUrl();
//        menuObj.setDisplaytext("Enquiry Form");
//        menuArrayList.add(menuObj);
//        responseModel.setMenu(menuArrayList);
        adapter = new MenuAdapter(this, responseModel, this);
        rvCateList.setAdapter(adapter);
    }

    @Override
    public void showLoading() {
        showDialog("", false,rel_root);
    }

    @Override
    public void hideLoading(boolean status) {
        hideDialog(status);
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public void showError(String string) {
        showToast(string);
    }

    @Override
    public void sessionError(String string) {

    }

    public void upDateCartCont(String count) {
        try {
            if (Utils.cutNull(count).equalsIgnoreCase("0"))
                txtUpdateCartCount.setVisibility(View.GONE);
            else txtUpdateCartCount.setVisibility(View.VISIBLE);
            txtUpdateCartCount.setText(Utils.cutNull(count));
            CbzaarApplication.cartCount = txtUpdateCartCount.getText().toString();
        } catch (Exception e) {
            e.printStackTrace();
            txtUpdateCartCount.setText("0");
            txtUpdateCartCount.setVisibility(View.GONE);
            CbzaarApplication.cartCount = txtUpdateCartCount.getText().toString();
        }
    }
  @Override
    protected void onResume() {
        super.onResume();
        if (AppPreference.getPrefsHelper().getPref(AppPreference.USER_LOGIN_DATA, "").isEmpty()) {
            linLogOut.setVisibility(View.GONE);
            linNewUser.setVisibility(View.VISIBLE);
        } else {
            linLogOut.setVisibility(View.VISIBLE);
            linNewUser.setVisibility(View.GONE);
            UserLoginResponse obj = new Gson().fromJson(AppPreference.getPrefsHelper().getPref(AppPreference.USER_LOGIN_DATA, ""), UserLoginResponse.class);
            txtUserName.setText(AppPreference.getPrefsHelper().getPref(AppPreference.USER_MAIL_ID, ""));
           // menuPresenter.apiCall(2);
        }

        tvUserName.setText(AppPreference.getPrefsHelper().getPref(AppPreference.AGENT_NAME, "") + "\n" + AppPreference.getPrefsHelper().getPref(AppPreference.AGENT_MOBILE_NUMBER, ""));
    }
}
