package vogue.ethno.cbazaar.view.productdescription;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.AdditionalInfoItem;

public class ProductDetailsAdapter extends RecyclerView.Adapter<ProductDetailsAdapter.ViewHolder> {

    private List<AdditionalInfoItem> addInfo;

    public ProductDetailsAdapter(List<AdditionalInfoItem> addInfo) {
        this.addInfo = addInfo;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_description, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final AdditionalInfoItem infoItem = addInfo.get(position);
        if (!Utils.cutNull(infoItem.getDescription()).contains("<p><font")) {
            holder.txtItemTitle.setText(infoItem.getTitle());
            holder.txtItemValues.setText(infoItem.getDescription());
            holder.webViews.setVisibility(View.GONE);
            holder.txtItemTitle.setVisibility(View.VISIBLE);
        } else {
            holder.txtItemTitle.setVisibility(View.GONE);
            holder.webViews.setVisibility(View.VISIBLE);
            holder.webViews.getSettings().setJavaScriptEnabled(true);
            holder.webViews.loadDataWithBaseURL("", infoItem.getDescription(), "text/html", "UTF-8", "");
        }
    }

    @Override
    public int getItemCount() {
        return addInfo == null ? 0 : addInfo.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_item_title)
        TextView txtItemTitle;
        @BindView(R.id.txt_item_values)
        TextView txtItemValues;
        @BindView(R.id.web_view)
        WebView webViews;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
