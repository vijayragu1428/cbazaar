package vogue.ethno.cbazaar.view.directpayment;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.classmodel.requestmodel.confirmcart.ShippingAddress;
import vogue.ethno.cbazaar.classmodel.responsemodel.cashondelivery.CashOnDelivryResponse;
import vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart.ConfirmCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.verifyotp.VerifyOtpResponse;
import vogue.ethno.cbazaar.view.homescreen.FragmentListener;
import vogue.ethno.cbazaar.view.homescreen.HomeActivity;
import vogue.ethno.cbazaar.view.homescreen.fragment.home.HomeFragment;
import vogue.ethno.cbazaar.view.payment.viewdetailsdialog.ViewDetailsFragment;
import vogue.ethno.cbazaar.view.paymentconfirm.PaymentConfirmationFragment;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class DirectPaymnetFragment extends Fragment implements DirectListener {


    Unbinder unbinder;
    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.rel_root)
    RelativeLayout rel_root;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.txt_total_amount)
    TextView txtTotalAmount;
    @BindView(R.id.lin_pay)
    LinearLayout linPay;
    @BindView(R.id.edt_enter_otp)
    EditText edtEnterOtp;
    @BindView(R.id.resend_otp)
    TextView resendOtp;
    @BindView(R.id.lin_otp)
    RelativeLayout linOtp;
    @BindView(R.id.btn_pay)
    Button btnPay;
    @BindView(R.id.txt_total)
    TextView txtTotal;
    @BindView(R.id.txt_view_details)
    TextView txtViewDetails;
    @BindView(R.id.lin_bottom)
    LinearLayout linBottom;
    private HomeActivity activity;
    private DirectPaymentPresenter directPaymentPresenter;

    private ConfirmCartResponseModel model;
    private CashOnDelivryResponse cashOnDelivryResponse;
    private ShippingAddress shippingAddress;
    private String addrss;
    private FragmentListener mListener;

    public DirectPaymnetFragment(CashOnDelivryResponse responseModel, ConfirmCartResponseModel model, String s, ShippingAddress shippingAddress) {
        this.model = model;
        this.cashOnDelivryResponse = responseModel;
        this.shippingAddress = shippingAddress;
        this.addrss = s;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_direct_paymnet, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void initUi() {
        directPaymentPresenter = new DirectPaymentPresenter();
        directPaymentPresenter.setView(this);
        activity = (HomeActivity) getActivity();
        activity.showHideToolbar(true);
        uiListener();
    }

    private void uiListener() {
        if (model.getCartdetail().getCartSummaries() != null)
            txtTotal.setText(getString(R.string.total) + " " + Utils.getAmountString(model.getCartdetail().getCartSummaries().getGrandTotal()));
        else
            txtTotal.setText(getString(R.string.total) + " " + Utils.getAmountString(0));
        txtTotalAmount.setText(model.getCartdetail().getInfomations().getCurrencyCode() + " " + Utils.getAmountString(cashOnDelivryResponse.getPayableAmount()));
        toolBar.setNavigationIcon(getContext().getResources().getDrawable(R.drawable.ic_back_arrow));
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        edtEnterOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().length()>5){
                    btnPay.performClick();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
       // activity.showHideToolbar(false);
    }

    private void paymentViewDetails() {
        ViewDetailsFragment viewDetailsFragment = new ViewDetailsFragment(model, addrss, shippingAddress);
        viewDetailsFragment.show(getActivity().getSupportFragmentManager(), "dialog");
    }

    @OnClick({R.id.txt_view_details, R.id.btn_pay, R.id.resend_otp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txt_view_details:
                paymentViewDetails();
                break;
            case R.id.btn_pay:
                payNow();
                break;
            case R.id.resend_otp:
                reSendOtp();
                break;
        }
    }

    private void reSendOtp() {
        directPaymentPresenter.apiCall(3);
    }

    private void payNow() {
        if (otpValidation())
            directPaymentPresenter.setRequestData(edtEnterOtp, model);
    }

    private boolean otpValidation() {
        if (edtEnterOtp.getText().toString().isEmpty()) {
            activity.showToast(getString(R.string.please_enter_otp));
            return false;
        }
        if (edtEnterOtp.getText().toString().length() < 6) {
            activity.showToast(getString(R.string.please_enter_otp_length));
            return false;
        }
        return true;
    }

    @Override
    public void showLoading() {
        activity.showDialog("", false,rel_root);
    }

    @Override
    public void hideLoading(boolean status) {
        activity.hideDialog(status);
    }

    @Override
    public Context getActivityContext() {
        return getContext();
    }

    @Override
    public void showError(String string) {
        activity.showToast(string);
    }


    @Override
    public void sessionError(String string) {
        mListener.nextFragment(new HomeFragment());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.setTitle(getString(R.string.direct_payment_cash));//.toUpperCase());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            mListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void proceedToCod(VerifyOtpResponse responseModel) {
        if (responseModel != null) {
            mListener.nextFragment(new PaymentConfirmationFragment(responseModel));
        }
    }

    @Override
    public String getTrackNumber() {
        return String.valueOf(model.getTemporaryTransaction().getTrackNumber());
    }

    @Override
    public void resentSuccess() {

    }
}
