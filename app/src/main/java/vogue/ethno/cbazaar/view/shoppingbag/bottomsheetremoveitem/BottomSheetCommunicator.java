package vogue.ethno.cbazaar.view.shoppingbag.bottomsheetremoveitem;

import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;

public interface BottomSheetCommunicator {
    void updateCartCount(GetCartResponseModel getCartResponseModel);
}
