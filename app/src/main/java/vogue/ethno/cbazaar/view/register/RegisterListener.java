package vogue.ethno.cbazaar.view.register;

import vogue.ethno.cbazaar.base.LoadDataView;

public interface RegisterListener extends LoadDataView {

    String getPassword();

    boolean getIsGuest();

    String getUserId();

    void registerSuccess();

}
