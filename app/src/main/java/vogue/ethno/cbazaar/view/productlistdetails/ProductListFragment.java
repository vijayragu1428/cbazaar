package vogue.ethno.cbazaar.view.productlistdetails;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.GlobalConstant;
import vogue.ethno.cbazaar.apputils.ItemDecorationGridColumns;
import vogue.ethno.cbazaar.model.ProductListModel;
import vogue.ethno.cbazaar.view.homescreen.FragmentListener;
import vogue.ethno.cbazaar.view.homescreen.HomeActivity;
import vogue.ethno.cbazaar.view.homescreen.OnProdutSelectedCallback;
import vogue.ethno.cbazaar.view.productdetails.ProductDetailsFragment;
import vogue.ethno.cbazaar.view.productlistdetails.adapter.ProductListAdapter;

public class ProductListFragment extends Fragment implements ProductListInterface, OnProdutSelectedCallback {

    @BindView(R.id.rv_product_list)
    RecyclerView rvProductList;
   @BindView(R.id.rel_root)
   RelativeLayout rel_root;
    @BindView(R.id.iv_refresh)
    ImageView refreshIV;
    Unbinder unbinder;
    int spanCount = 3;
    private FragmentListener mListener;
    private HomeActivity activity;
    private Context mContext;
    private ProductListPresenter productListPresenter;
    private ArrayList<ProductListModel.Product> productList;
    ProductListAdapter productListAdapter;
    private String redirectUrl = "";
    int tempItemSize = 0;
    int totalItemCount = 0;
    int initialValue = 1;
    private GridLayoutManager productLayoutManager;
    private boolean isLastPage;
    private String productName = "";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            mListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Bundle args = getArguments();

        super.onCreate(savedInstanceState);
        if (args != null && args.containsKey(GlobalConstant.REDIRECT_URL)) {
            redirectUrl = args.getString(GlobalConstant.REDIRECT_URL);
            productName = args.getString("name");

        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        initUi();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(productName);
    }

    private void initUi() {
        activity = (HomeActivity) getActivity();
        mContext = getContext();
        initRv();
        productListPresenter = new ProductListPresenter();
        productListPresenter.setView(this);
        productListPresenter.apiCall(1, redirectUrl);
    }

    private void initRv() {
        productList = new ArrayList<>();
        rvProductList.setHasFixedSize(true);
        productLayoutManager = new GridLayoutManager(mContext, spanCount, GridLayoutManager.VERTICAL, false);
        rvProductList.setLayoutManager(productLayoutManager);
        rvProductList.addItemDecoration(new ItemDecorationGridColumns(15, 3));
//        productListAdapter = new ProductListAdapter(mContext, productList, this);
//        rvProductList.setAdapter(productListAdapter);
    }

    @Override
    public void successFromServer(ProductListModel responseModel) {

        if (refreshIV.getVisibility() == View.VISIBLE) {
            refreshIV.setVisibility(View.GONE);
        }
        if (responseModel != null && responseModel.getProducts().size() > 0) {
            if (tempItemSize > 0) {
//                productList.addAll(responseModel.getProducts());
                isLastPage = responseModel.isLastPage();
                addNextProductList(responseModel.getProducts());

                if (tempItemSize < totalItemCount) {
                    tempItemSize = tempItemSize + responseModel.getProducts().size();
                    rvProductList.getLayoutManager().scrollToPosition(productLayoutManager.findFirstVisibleItemPosition());
                }
            } else {
                tempItemSize = tempItemSize + responseModel.getProducts().size();
                totalItemCount = responseModel.getCount();
                productList = responseModel.getProducts();
                productListAdapter = new ProductListAdapter(mContext, productList, this);
                rvProductList.setAdapter(productListAdapter);
            }

//            productListAdapter.notifyDataSetChanged();
            setProductListListener();
        }
    }

    private void addNextProductList(ArrayList<ProductListModel.Product> proList) {
        final int positionStart = this.productList.size() + 1;
        this.productList.addAll(proList);
        productListAdapter.notifyItemRangeInserted(positionStart, proList.size());
    }

    private void setProductListListener() {
        rvProductList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1)) {
                    /*if (tempItemSize >= totalItemCount) {

                    } else */
                    if (tempItemSize < totalItemCount && !isLastPage) {
                        ++initialValue;
                        productListPresenter.apiCall(1, redirectUrl.concat("&page=" + initialValue));
                        refreshIV.setVisibility(View.VISIBLE);
                        Glide.with(mContext).load(R.drawable.loader)
                                .into(refreshIV);
                    }
                }
            }
        });
    }

    @Override
    public void showLoading() {
        if (tempItemSize == 0)
            activity.showDialog("", false,rel_root);
    }

    @Override
    public void hideLoading(boolean status) {
        activity.hideDialog(status);
    }

    @Override
    public Context getActivityContext() {
        return getContext();
    }

    @Override
    public void showError(String string) {
        activity.showToast(string);
    }

    @Override
    public void sessionError(String string) {
        mListener.nextFragment(new ProductListFragment());
    }

    @Override
    public void onProductSelected(String productCode, String redirectUrl, String name) {
        Log.e("ProductListFragment", productCode);
        Log.e("ProductListFragment", redirectUrl);

        ProductDetailsFragment productDetailsFragment = new ProductDetailsFragment();
        Bundle arg = new Bundle();
        arg.putString(GlobalConstant.PRODUCT_CODE, productCode);
        arg.putString(GlobalConstant.REDIRECT_URL, redirectUrl);
        arg.putString(GlobalConstant.PRODUCT_NAME, name);
        productDetailsFragment.setArguments(arg);
        mListener.nextFragment(productDetailsFragment);
        /*getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, productDetailsFragment, ProductDetailsFragment.class.getName())
                .addToBackStack(ProductDetailsFragment.class.getName())
                .commit();*/
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
