package vogue.ethno.cbazaar.view.shoppingbag;

import vogue.ethno.cbazaar.base.LoadDataView;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;

public interface ShoppingBagListener extends LoadDataView {

    void updateCartCount(GetCartResponseModel responseModel);
}
