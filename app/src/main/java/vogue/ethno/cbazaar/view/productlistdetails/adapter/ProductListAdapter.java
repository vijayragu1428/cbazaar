package vogue.ethno.cbazaar.view.productlistdetails.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.ApiModel;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.model.ProductListModel;
import vogue.ethno.cbazaar.view.homescreen.OnProdutSelectedCallback;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductHolder> {

    Context mContext;
    private ArrayList<ProductListModel.Product> productList;
    OnProdutSelectedCallback onProductSelected;

    public ProductListAdapter(Context mContext, ArrayList<ProductListModel.Product> productList, OnProdutSelectedCallback onProductSelected) {
        this.mContext = mContext;
        this.productList = productList;
        this.onProductSelected = onProductSelected;
    }

    @NonNull
    @Override
    public ProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.product_list_adapter, parent, false);
        return new ProductListAdapter.ProductHolder(viewGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductHolder holder, int position) {
        final ProductListModel.Product product = productList.get(position);

        Log.e("img", ApiModel.BASE_IMAGE_URL + product.getListingImage());
        Glide.with(mContext).load(Utils.cutNull(ApiModel.BASE_IMAGE_URL + product.getListingImage()))
                .apply(RequestOptions.placeholderOf(R.drawable.lazyloading))
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .into(holder.productImage);
        holder.productName.setText(product.getName());
        if (product.getDiscount() > 0) {
            holder.salesPrice.setVisibility(View.VISIBLE);
            holder.productPrice.setText(Utils.getAmountString(product.getActualPrice()));
            holder.productPrice.setPaintFlags(holder.productPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.salesPrice.setText(Utils.getAmountString(product.getSalePrice()));
        } else {
            holder.productPrice.setText(Utils.getAmountString(product.getActualPrice()));
            holder.salesPrice.setVisibility(View.INVISIBLE);
        }

        holder.llItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onProductSelected.onProductSelected(product.getCode(), product.getProductBigLink(),product.getName());
            }
        });

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ProductHolder extends RecyclerView.ViewHolder {

        ImageView productImage;
        TextView productName;
        TextView productPrice, salesPrice;
        LinearLayout llItemLayout;

        public ProductHolder(View itemView) {
            super(itemView);
            productImage = itemView.findViewById(R.id.iv_product);
            productName = itemView.findViewById(R.id.tv_product_name);
            productPrice = itemView.findViewById(R.id.tv_product_price);
            salesPrice = itemView.findViewById(R.id.tv_sales_price);
            llItemLayout = itemView.findViewById(R.id.ll_item);
            //productName.setSelected(true);
        }
    }
}
