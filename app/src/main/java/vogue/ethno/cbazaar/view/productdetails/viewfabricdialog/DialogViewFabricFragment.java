package vogue.ethno.cbazaar.view.productdetails.viewfabricdialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.PatternProductDetailItem;

@SuppressLint("ValidFragment")
public class DialogViewFabricFragment extends DialogFragment {


    private List<PatternProductDetailItem> items;

    Unbinder unbinder;
    @BindView(R.id.img_close)
    ImageView imgClose;
    @BindView(R.id.img_right_arrow)
    ImageView imgRightArrow;
    @BindView(R.id.img_slider)
    ViewPager imgSlider;
    @BindView(R.id.img_left_arrow)
    ImageView imgLeftArrow;
    @BindView(R.id.rel_slider)
    RelativeLayout relSlider;
    private DialogAdapter dialogAdapter;
    private int nextPage = 0;
    private String productCode = "";

    public DialogViewFabricFragment(List<PatternProductDetailItem> items, String productCode) {
        this.items = items;
        this.productCode = productCode;

    }

    //  @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
        //  getDialog().getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        setStyle(STYLE_NO_TITLE, R.style.AppTheme);
        //    } else {
        //       setStyle(STYLE_NO_TITLE, R.style.Theme_AppCompat_Light_NoActionBar);
        //   }
        super.onCreate(savedInstanceState);
        //  items = (List<PatternProductDetailItem>) this.getArguments().getSerializable("data");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_view_fabric, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        intUi();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void intUi() {
        dialogAdapter = new DialogAdapter(items, getContext(),productCode);
        imgSlider.setAdapter(dialogAdapter);
        if (nextPage == 0) {
            imgLeftArrow.setEnabled(false);
            imgLeftArrow.setAlpha(0.5f);
            imgRightArrow.setEnabled(true);
            imgRightArrow.setAlpha(1f);
        }
        imgSlider.beginFakeDrag();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       /* if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.img_close, R.id.img_right_arrow, R.id.img_left_arrow})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_close:
                dismiss();
                break;
            case R.id.img_right_arrow:
                nextPage();
                break;
            case R.id.img_left_arrow:
                preciousPage();
                break;
        }
    }

    private void preciousPage() {
        nextPage = nextPage - 1;
        if (items.size() > nextPage)
            imgSlider.setCurrentItem(nextPage, true);
        if (nextPage == 0) {
            imgLeftArrow.setEnabled(false);
            imgLeftArrow.setAlpha(0.5f);
            imgRightArrow.setEnabled(true);
            imgRightArrow.setAlpha(1f);
        }
    }

    private void nextPage() {
        nextPage = nextPage + 1;
        if (items.size() > nextPage)
            imgSlider.setCurrentItem(imgSlider.getCurrentItem() + nextPage, true);
        if (nextPage == items.size() - 1) {
            imgRightArrow.setEnabled(false);
            imgRightArrow.setAlpha(0.5f);
            imgLeftArrow.setEnabled(true);
            imgLeftArrow.setAlpha(1f);
        }
    }


}
