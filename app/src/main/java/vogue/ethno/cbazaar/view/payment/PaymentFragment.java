package vogue.ethno.cbazaar.view.payment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.classmodel.requestmodel.confirmcart.ShippingAddress;
import vogue.ethno.cbazaar.classmodel.responsemodel.cashondelivery.CashOnDelivryResponse;
import vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart.ConfirmCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.rsakey.RsaKeyResponseModel;
import vogue.ethno.cbazaar.data.localdb.pref.AppPreference;
import vogue.ethno.cbazaar.view.ccavangue.CcAvanueFragment;
import vogue.ethno.cbazaar.view.ccavangue.utils.AvenuesParams;
import vogue.ethno.cbazaar.view.directpayment.DirectPaymnetFragment;
import vogue.ethno.cbazaar.view.homescreen.FragmentListener;
import vogue.ethno.cbazaar.view.homescreen.HomeActivity;
import vogue.ethno.cbazaar.view.homescreen.fragment.home.HomeFragment;
import vogue.ethno.cbazaar.view.payment.viewdetailsdialog.ViewDetailsFragment;

import static vogue.ethno.cbazaar.data.localdb.pref.AppPreference.AGENT_MOBILE_NUMBER;


@SuppressLint("ValidFragment")
public class PaymentFragment extends Fragment implements PaymentListener {

    @BindView(R.id.tool_bar)
    Toolbar toolBar;
   @BindView(R.id.rel_root)
    RelativeLayout rel_root;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.check_box)
    CheckBox checkBox;
    @BindView(R.id.txt_use_store_crate)
    TextView txtUseStoreCrate;
    @BindView(R.id.txt_you_have)
    TextView txtYouHave;
    @BindView(R.id.txt_credit_card)
    TextView txtCreditCard;
    @BindView(R.id.credit_select)
    TextView creditSelect;
    @BindView(R.id.rel_credit_card)
    RelativeLayout relCreditCard;
    @BindView(R.id.txt_cod)
    TextView txtCod;
    @BindView(R.id.txt_cod_select)
    TextView txtCodSelect;
    @BindView(R.id.rel_cash_on_delivery)
    RelativeLayout relCashOnDelivery;
    @BindView(R.id.lin_other_payment_option)
    LinearLayout linOtherPaymentOption;
    @BindView(R.id.txt_address_tit)
    TextView txtAddressTit;
    @BindView(R.id.txt_address)
    TextView txtAddress;
    @BindView(R.id.rel_address)
    RelativeLayout relAddress;
    @BindView(R.id.btn_continue_purchase)
    Button btnContinuePurchase;
    @BindView(R.id.txt_total)
    TextView txtTotal;
    @BindView(R.id.txt_view_details)
    TextView txtViewDetails;
    @BindView(R.id.lin_bottom)
    LinearLayout linBottom;
    Unbinder unbinder;
    private FragmentListener mListener;
    private ConfirmCartResponseModel model;
    private PaymentPresenter paymentPresenter;
    private HomeActivity activity;
    private ShippingAddress shippingAddress;

    public PaymentFragment(ConfirmCartResponseModel responseModel, ShippingAddress shippingAddress) {
        this.model = responseModel;
        this.shippingAddress = shippingAddress;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            mListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        activity = (HomeActivity) getActivity();

        paymentPresenter = new PaymentPresenter();
        paymentPresenter.setView(this);
        //   paymentPresenter.apiCall(1);
        uiListener();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        activity.showHideToolbar(false);
    }

    @SuppressLint("SetTextI18n")
    private void uiListener() {
        toolBar.setNavigationIcon(getContext().getResources().getDrawable(R.drawable.ic_back_arrow));
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (linOtherPaymentOption.getVisibility() == View.GONE)
                    checkBox.setChecked(false);
                else
                    getActivity().onBackPressed();
            }
        });
       checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    btnContinuePurchase.setVisibility(View.VISIBLE);
                    linOtherPaymentOption.setVisibility(View.GONE);
                } else {
                    linOtherPaymentOption.setVisibility(View.VISIBLE);
                    btnContinuePurchase.setVisibility(View.GONE);
                }
            }
        });
        txtAddress.setText(shippingAddress.getRelationFirstName() + "\n" + shippingAddress.getAddress1() + "\n" + shippingAddress.getAddress2());
        txtUseStoreCrate.setText(getString(R.string.use_store_credit) + " " + Utils.getAmountString(model.getCartdetail().getCartSummaries().getStoreCredit()));
        txtYouHave.setText(spannableString());
        if (model.getCartdetail().getCartSummaries() != null)
            txtTotal.setText(getString(R.string.total) + " " + Utils.getAmountString(model.getCartdetail().getCartSummaries().getGrandTotal()));
        else
            txtTotal.setText(getString(R.string.total) + " " + Utils.getAmountString(0));
    }

    private SpannableString spannableString() {
        SpannableString wordtoSpan = new SpannableString(getString(R.string.use_store_credit) + " " + amount() + " " + getString(R.string.this_order));
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), getString(R.string.use_store_credit).length() + 1, (getString(R.string.use_store_credit).length() + 1) + (amount().length() + 1), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return wordtoSpan;
    }

    private String amount() {
        if (model.getCartdetail().getCartSummaries() != null)
            return Utils.getAmountString(model.getCartdetail().getCartSummaries().getStoreCredit());
        return "0";
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.setTitle(getString(R.string.payment));//.toUpperCase());
        activity.showHideToolbar(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btn_continue_purchase, R.id.txt_view_details, R.id.rel_credit_card, R.id.rel_cash_on_delivery, R.id.rel_ckck_box})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_continue_purchase:
                orderByStoreDetails();
                break;
            case R.id.txt_view_details:
                paymentViewDetails();
                break;
            case R.id.rel_credit_card:
                creditCardOption();
                break;
            case R.id.rel_cash_on_delivery:
                cashOnDeliveryOption();
                break;
            case R.id.rel_ckck_box:
                chckBoxEvent();
                break;

        }
    }

    private void chckBoxEvent() {
        if (checkBox.isChecked()) {
            checkBox.setChecked(false);
            linOtherPaymentOption.setVisibility(View.VISIBLE);
            btnContinuePurchase.setVisibility(View.GONE);
        } else {
            checkBox.setChecked(true);
            btnContinuePurchase.setVisibility(View.VISIBLE);
            linOtherPaymentOption.setVisibility(View.GONE);
        }

}

    private void cashOnDeliveryOption() {
        if (model.getPaymentGateway() != null && model.getPaymentGateway().size() > 1) {

            paymentPresenter.cashOndelivery(model.getPaymentGateway().get(2).getIndex(), AppPreference.getPrefsHelper().getPref(AGENT_MOBILE_NUMBER, ""), model.getTemporaryTransaction().getTrackNumber());
        } else showError(getString(R.string.user_not_allow));
    }

    private void paymentViewDetails() {
     ViewDetailsFragment viewDetailsFragment = new ViewDetailsFragment(model, txtAddress.getText().toString(),shippingAddress);
       viewDetailsFragment.show(getActivity().getSupportFragmentManager(), "dialog");

      }

    private void creditCardOption() {
        paymentPresenter.apiCall(4);
    }

    private void orderByStoreDetails() {
        int totalPaidAmount = 0;
        if (model.getCartdetail().getCartSummaries() != null)
            totalPaidAmount = model.getCartdetail().getCartSummaries().getGrandTotal();
        int storeCredits = 0;
        if (model.getCartdetail().getCartSummaries() != null)
            storeCredits = model.getCartdetail().getCartSummaries().getStoreCredit();
        if (totalPaidAmount <= storeCredits) {
            paymentPresenter.storeCreatePay(model);
        } else {
            showError(getString(R.string.store_create_error));
            //not pay
        }

    }

    @Override
    public void successFromAddress() {

    }

    @Override
    public void cashOndeliverySuccess(CashOnDelivryResponse responseModel) {
        mListener.nextFragment(new DirectPaymnetFragment(responseModel, model, txtAddress.getText().toString(), shippingAddress));
    }

    @Override
    public String orderId() {
        return model.getOrderNumber();
    }

    @Override
    public void successFromServerRSA(List<RsaKeyResponseModel> rsaKeyResponseModel) {
           mListener.nextFragment(new CcAvanueFragment(rsaKeyResponseModel,model));

    }
    @Override
    public void showLoading() {
        activity.showDialog("", false,rel_root);
    }

    @Override
    public void hideLoading(boolean status) {
        activity.hideDialog(status);
    }

    @Override
    public Context getActivityContext() {
        return getContext();
    }

    @Override
    public void showError(String string) {
        activity.showToast(string);
    }


    @Override
    public void sessionError(String string) {
        mListener.nextFragment(new HomeFragment());
    }


}
