package vogue.ethno.cbazaar.view.shoppingbag.bottomsheetremoveitem;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.ApiModel;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.CartDetailItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;
import vogue.ethno.cbazaar.view.homescreen.HomeActivity;


@SuppressLint("ValidFragment")
public class RemoveItemDialogFragment extends BottomSheetDialogFragment implements RemoveItemListener {

    @BindView(R.id.img_item_data)
    AppCompatImageView imgItemData;
    @BindView(R.id.rel_root)
    LinearLayout rel_root;
    @BindView(R.id.img_close)
    AppCompatImageView imgClose;
    @BindView(R.id.txt_remove_item)
    TextView txtRemoveItem;
    @BindView(R.id.txt_remove)
    TextView txtRemove;
    @BindView(R.id.txt_content)
    TextView txtContent;
    Unbinder unbinder;
    private CartDetailItem detailItem;
    private RemoveItemSheetPresenter removeItemSheetPresenter;
    private HomeActivity activity;
    private String type;
    private BottomSheetCommunicator communicator;

    public RemoveItemDialogFragment(BottomSheetCommunicator sheetCommunicator, CartDetailItem item, String type) {
        this.detailItem = item;
        this.type = type;
        this.communicator = sheetCommunicator;
    }

    public RemoveItemDialogFragment(BottomSheetCommunicator sheetCommunicator) {
        this.communicator = sheetCommunicator;
        this.detailItem = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list_dialog, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        intUi();
    }

    private void intUi() {
        activity = (HomeActivity) getActivity();
        removeItemSheetPresenter = new RemoveItemSheetPresenter();
        removeItemSheetPresenter.setView(this);
        if (detailItem != null) {
            Glide.with(getContext()).load(Utils.cutNull(ApiModel.BASE_IMAGE_URL + detailItem.getProductImage()))
                    .apply(RequestOptions.placeholderOf(R.drawable.ev_logo))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                    .into(imgItemData);
            txtRemoveItem.setText(getString(R.string.remove_item));
            txtContent.setText(getString(R.string.are_sure_remove_the_bag));
            imgItemData.setVisibility(View.VISIBLE);
            txtRemove.setText(getString(R.string.remove));
        } else {
            txtRemoveItem.setText(getString(R.string.delete_address));
            txtContent.setText(getString(R.string.sure_delete_address));
            imgItemData.setVisibility(View.INVISIBLE);
            txtRemove.setText(getString(R.string.delete));
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       /* final Fragment parent = getParentFragment();
        if (parent != null) {
            mListener = (Listener) parent;
        } else {
            mListener = (Listener) context;
        }*/
    }

    @Override
    public void onDetach() {
        //  mListener = null;
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.txt_remove, R.id.img_close})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txt_remove:
                if (detailItem != null)
                    removeItemSheetPresenter.apiCall(1);
                else {
                    communicator.updateCartCount(null);
                    dismiss();
                }
                break;
            case R.id.img_close:
                dismiss();
                break;
        }
    }


    @Override
    public void showLoading() {
        activity.showDialog("", false,rel_root);
    }

    @Override
    public void hideLoading(boolean status) {
        activity.hideDialog(status);
    }

    @Override
    public Context getActivityContext() {
        return getContext();
    }

    @Override
    public void showError(String string) {
        activity.showToast(string);
    }


    @Override
    public void sessionError(String string) {
        activity.showToast(string);
        dismiss();
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public CartDetailItem getCart() {
        return detailItem;
    }

    @Override
    public void updateCartCount(GetCartResponseModel responseModel) {
        dismiss();
        communicator.updateCartCount(responseModel);
    }
}
