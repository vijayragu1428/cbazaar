package vogue.ethno.cbazaar.view.productionstyle;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import vogue.ethno.cbazaar.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductStyleFragment extends Fragment {

    @BindView(R.id.lin_des)
    LinearLayout linDes;
    Unbinder unbinder;
    private String productStyle;

    public static ProductStyleFragment getInstance(String productStyle) {
        ProductStyleFragment styleFragment = new ProductStyleFragment();
        styleFragment.productStyle = productStyle;
        return styleFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_style, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.product_styling));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        intUi();
    }

    private void intUi() {
        try {
            getActivity().getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;
            String[] strings = productStyle.split(",");
            LayoutInflater inflater = LayoutInflater.from(getContext());
            for (String sizeItem : strings) {
                final View view = inflater.inflate(R.layout.item_list_style, null, false);
                final TextView size = (TextView) view.findViewById(R.id.txt_item_title);
                final TextView values = (TextView) view.findViewById(R.id.txt_item_values);
                String[] str = sizeItem.split(":");
                size.setText(str[0]);
                values.setText(str[1]);
                linDes.addView(view);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
