package vogue.ethno.cbazaar.view.productlistdetails;

import vogue.ethno.cbazaar.base.LoadDataView;
import vogue.ethno.cbazaar.model.ProductListModel;

public interface ProductListInterface extends LoadDataView {
    void successFromServer(ProductListModel responseModel);
}
