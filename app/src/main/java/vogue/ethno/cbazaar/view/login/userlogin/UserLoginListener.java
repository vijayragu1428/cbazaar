package vogue.ethno.cbazaar.view.login.userlogin;

import vogue.ethno.cbazaar.base.LoadDataView;

public interface UserLoginListener extends LoadDataView {
    String getActionMode();

    String getPassword();

    boolean getIsGuest();

    String getUserId();

    void loginSuccess();
}
