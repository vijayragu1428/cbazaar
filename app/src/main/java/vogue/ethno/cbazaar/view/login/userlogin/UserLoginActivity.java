package vogue.ethno.cbazaar.view.login.userlogin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.app.CbzaarApplication;
import vogue.ethno.cbazaar.apputils.GlobalConstant;
import vogue.ethno.cbazaar.base.BaseActivity;
import vogue.ethno.cbazaar.data.network.HeaderValues;
import vogue.ethno.cbazaar.view.homescreen.HomeActivity;
import vogue.ethno.cbazaar.view.register.RegisterActivity;

import static vogue.ethno.cbazaar.app.CbzaarApplication.getContext;

public class UserLoginActivity extends BaseActivity implements UserLoginListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
  @BindView(R.id.rel_root)
  LinearLayout rel_root;
    @BindView(R.id.img_logo)
    ImageView imgLogo;
    @BindView(R.id.txt_login_title)
    TextView txtLoginTitle;
    @BindView(R.id.edt_user_id)
    EditText edtUserId;
    @BindView(R.id.ti_edt_user_id)
    TextInputLayout tiEdtUserId;
    @BindView(R.id.view_name)
    View viewName;
    @BindView(R.id.edt_password)
    EditText edtPassword;
    @BindView(R.id.ti_edt_password)
    TextInputLayout tiEdtPassword;
    @BindView(R.id.view_password)
    View viewPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.txt_register)
    TextView txtRegister;
    @BindView(R.id.txt_sign_up)
    TextView txtSignUp;

    private UserLoginPresenter userLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);
        ButterKnife.bind(this);
        initUI();
    }

    private void initUI() {
        userLoginPresenter = new UserLoginPresenter();
        userLoginPresenter.setView(this);
        setupDrawer();
    }

    private void setupDrawer() {
        toolbar.setTitle(getString(R.string.login));
        toolbar.setNavigationIcon(getContext().getResources().getDrawable(R.drawable.ic_back_arrow));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @OnClick({R.id.btn_login, R.id.txt_register, R.id.txt_sign_up})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_login: {
                if (validation()) {
                    HeaderValues.EMAIL = getUserId();
                    userLoginPresenter.apiCall(1);
                }
                break;
            }
            case R.id.txt_register: {
                break;
            }
            case R.id.txt_sign_up: {
                finish();
                Intent intent = new Intent(this, RegisterActivity.class);
                startActivity(intent);
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        hideKeyBoard();
    }

    @Override
    public String getActionMode() {
        return "L";
    }

    @Override
    public String getPassword() {
        return edtPassword.getText().toString();
    }

    @Override
    public boolean getIsGuest() {
        return false;
    }

    @Override
    public String getUserId() {
        return edtUserId.getText().toString();
    }

    @Override
    public void loginSuccess() {
        if (((CbzaarApplication) getApplicationContext()).fromWhere.equalsIgnoreCase(GlobalConstant.PRODUCT_DETAILS)) {
            Intent intent = new Intent();
            setResult(100, intent);
        }
        finish();
    }
    @Override
    public void showLoading() {
        showDialog("", false,rel_root);

    }

    @Override
    public void hideLoading(boolean status) {
        hideDialog(status);

    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public void showError(String string) {
        showToast(string);

    }

    @Override
    public void sessionError(String string) {

    }

    private boolean validation() {
        boolean flag = true;

        if (edtUserId.getText().toString().isEmpty()) {
            flag = false;
            edtUserId.setError("Please enter email or mobile number");
        }

        if (edtPassword.getText().toString().isEmpty()) {
            flag = false;
            edtPassword.setError("Please enter password");

        }
        return flag;
    }

}
