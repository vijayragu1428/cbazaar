package vogue.ethno.cbazaar.view.menu;

import java.util.ArrayList;

public class MenuModel {

    private ArrayList<Menu> menu;

    public ArrayList<Menu> getMenu() {
        return this.menu;
    }

    public void setMenu(ArrayList<Menu> menu) {
        this.menu = menu;
    }

    public static class Menu {
        private String url;

        public String getUrl() {
            return this.url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        private String displaytext;

        public String getDisplaytext() {
            return this.displaytext;
        }

        public void setDisplaytext(String displaytext) {
            this.displaytext = displaytext;
        }
    }
}
