package vogue.ethno.cbazaar.view.address;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.classmodel.responsemodel.getaddress.RelationItem;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

    private OnSelect onSelect;
    private List<RelationItem> relation;
    private int selectedPosition = 0;
    private Context mContext;

    public AddressAdapter(OnSelect onSelect, Context context) {
        this.onSelect = onSelect;
        this.relation = new ArrayList<>();
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_select_address, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final RelationItem relationItem = relation.get(position);
        holder.onBindItem(relationItem, position);
        holder.txtName.setText(relationItem.getRelationFirstName());
        holder.txtEmail.setText(relationItem.getRelationEmailId());
        holder.txtAddressLineOne.setText(relationItem.getAddress1());
        holder.txtAddressLineTwo.setText(relationItem.getAddress2());
        holder.txtNumber.setText(relationItem.getPhoneNumber());
        if (selectedPosition == position) {
            holder.linRoot.setBackground(mContext.getResources().getDrawable(R.drawable.border_layout_select));
            holder.viewLineOne.setBackgroundColor(mContext.getResources().getColor(R.color.appColor));
            holder.viewLineTwo.setBackgroundColor(mContext.getResources().getColor(R.color.appColor));
            holder.imgSelect.setImageResource(R.drawable.tick_selected);
            onSelect.onDefaultSelection(relationItem);
        } else {
            holder.linRoot.setBackground(mContext.getResources().getDrawable(R.drawable.border_layout));
            holder.viewLineOne.setBackgroundColor(mContext.getResources().getColor(R.color.gray));
            holder.viewLineTwo.setBackgroundColor(mContext.getResources().getColor(R.color.gray));
            holder.imgSelect.setImageResource(R.drawable.tick_unselected);
        }
    }

    @Override
    public int getItemCount() {
        return relation == null ? 0 : relation.size();
    }

    public void loadData(List<RelationItem> relation) {
        this.relation = relation;
        notifyDataSetChanged();
    }

    public void itemRemove(int currentItem) {
        relation.remove(currentItem);
        notifyDataSetChanged();
    }

    public void setSeletedItem(int firstVisibleItemPosition) {
        selectedPosition = firstVisibleItemPosition;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_select)
        ImageView imgSelect;
        @BindView(R.id.txt_name)
        TextView txtName;
        @BindView(R.id.txt_email)
        TextView txtEmail;
        @BindView(R.id.txt_address_line_one)
        TextView txtAddressLineOne;
        @BindView(R.id.txt_address_line_two)
        TextView txtAddressLineTwo;
        @BindView(R.id.txt_number)
        TextView txtNumber;
        @BindView(R.id.view_line_one)
        View viewLineOne;
        @BindView(R.id.txt_edit)
        TextView txtEdit;
        @BindView(R.id.view_line_two)
        View viewLineTwo;
        @BindView(R.id.txt_delete)
        TextView txtDelete;
        @BindView(R.id.lin_root)
        LinearLayout linRoot;
        private RelationItem relationItem;
        private int position = 0;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.txt_edit, R.id.txt_delete})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.txt_edit:
                    onSelect.onAddressEdit(relationItem);
                    break;
                case R.id.txt_delete:
                    onSelect.onAddressDelete(relationItem, position);
                    break;
            }
        }

        public void onBindItem(RelationItem relationItem, int pos) {
            this.relationItem = relationItem;
            this.position = pos;
        }
    }

    public interface OnSelect {
        void onAddressEdit(RelationItem relationItem);

        void onAddressDelete(RelationItem relationItem, int pos);

        void onDefaultSelection(RelationItem relationItem);
    }
}
