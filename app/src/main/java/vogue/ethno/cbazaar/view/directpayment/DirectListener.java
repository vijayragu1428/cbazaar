package vogue.ethno.cbazaar.view.directpayment;

import vogue.ethno.cbazaar.base.LoadDataView;
import vogue.ethno.cbazaar.classmodel.responsemodel.verifyotp.VerifyOtpResponse;

public interface DirectListener extends LoadDataView {
    void proceedToCod(VerifyOtpResponse responseModel);
    String getTrackNumber();

    void resentSuccess();
}
