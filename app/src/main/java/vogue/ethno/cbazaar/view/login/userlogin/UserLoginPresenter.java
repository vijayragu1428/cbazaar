package vogue.ethno.cbazaar.view.login.userlogin;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.base.AbstractBasePresenter;
import vogue.ethno.cbazaar.classmodel.responsemodel.accesstoken.TokenResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.userlogin.UserLoginResponse;
import vogue.ethno.cbazaar.data.ApiClient;
import vogue.ethno.cbazaar.data.localdb.pref.AppPreference;
import vogue.ethno.cbazaar.data.network.HeaderValues;
import vogue.ethno.cbazaar.data.network.NetworkCallback;

public class UserLoginPresenter extends AbstractBasePresenter<UserLoginListener> implements NetworkCallback {

    private UserLoginListener views;
    private int apiFrom;
    private Context mContext;
    private ApiClient apiClient;

    @Override
    public void setView(UserLoginListener view) {
        super.setView(view);
        views = view;
        apiClient = new ApiClient(this);
    }

    @Override
    public void onSuccess(Object o, int api) {
        loadingDialog(false);

        if (apiFrom != -1)
            loadingDialog(false);
        try {
            if (o != null) {
                switch (api) {
                    case -1:
                        accessToken(o);
                        break;
                    case 1:
                        loginResponse(o);
                        break;

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onFailure(String body) {
        loadingDialog(false);
        views.showError(body);
    }

    @Override
    public void onErrorBody(Object o, int api) {

    }

    public void apiCall(int item) {
        loadingDialog(true);
        apiFrom = item;
        switch (item) {
            case 1:
                apiClient.userLogin(item, views.getActionMode(), views.getPassword(), views.getIsGuest());
                break;
        }
    }

    private void loadingDialog(boolean status) {
        if (views != null) {
            if (status)
                views.showLoading();
            else views.hideLoading(status);
        }
    }

    private void accessToken(Object o) {
        try {
            TokenResponseModel responseModel = (TokenResponseModel) o;
            if (responseModel != null && !Utils.cutNull(responseModel.getAccessToken()).isEmpty() && !Utils.cutNull(responseModel.getTokenType()).isEmpty()) {
                HeaderValues.ACESS_TOKEN = responseModel.getAccessToken();
                HeaderValues.TOKEN_TYPE = responseModel.getTokenType();
                apiCall(apiFrom);
            } else {
                HeaderValues.ACESS_TOKEN = "";
                HeaderValues.TOKEN_TYPE = "";
                tokenError(o);
            }
        } catch (Exception e) {
            HeaderValues.TOKEN_TYPE = "";
            HeaderValues.ACESS_TOKEN = "";
            tokenError(o);
        }
    }

    private void loginResponse(Object o) {
        try {
            UserLoginResponse userLoginResponse = (UserLoginResponse) o;
            if (userLoginResponse != null && userLoginResponse.isIsRegisteredWithPassword()) {
                AppPreference.getPrefsHelper().savePref(AppPreference.USER_LOGIN_DATA, new Gson().toJson(userLoginResponse));
                AppPreference.getPrefsHelper().savePref(AppPreference.USER_MAIL_ID, views.getUserId());
                AppPreference.getPrefsHelper().savePref(AppPreference.USER_ID, String.valueOf(userLoginResponse.getID()));
              //  HeaderValues.USER_ID =  String.valueOf(userLoginResponse.getID());
                views.loginSuccess();
            } else {
                onFailure(views.getActivityContext().getString(R.string.invalid_user_or_password));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void tokenError(Object o) {
        loadingDialog(false);
        if (views != null)
            views.sessionError("");
    }
}
