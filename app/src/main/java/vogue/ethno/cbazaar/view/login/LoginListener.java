package vogue.ethno.cbazaar.view.login;

import vogue.ethno.cbazaar.base.LoadDataView;

public interface LoginListener extends LoadDataView {

    String getLoginId();

    String getPassword();

    boolean getIsGuest();

    void loginSuccess();
}
