package vogue.ethno.cbazaar.view.menu;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import vogue.ethno.cbazaar.R;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuHolder> {

    MenuModel menuModel;
    Context context;
    MenuDetailsListener menuDetailsListener;

    public MenuAdapter(Context context, MenuModel menuModel, MenuDetailsListener menuDetailsListener) {
        this.menuModel = menuModel;
        this.context = context;
        this.menuDetailsListener = menuDetailsListener;
    }

    @NonNull
    @Override
    public MenuAdapter.MenuHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater linearLayout = LayoutInflater.from(parent.getContext());
        ViewGroup viewGroup = (ViewGroup) linearLayout.inflate(R.layout.menu_adapter, parent, false);
        return new MenuHolder(viewGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuAdapter.MenuHolder holder, int position) {
        final int pos = position;
        holder.menuName.setText(menuModel.getMenu().get(position).getDisplaytext());
        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuDetailsListener.onMenuItemClick(menuModel.getMenu().get(pos).getUrl());
            }
        });
    }

    @Override
    public int getItemCount() {
        return (menuModel == null || menuModel.getMenu() == null ? 0 : menuModel.getMenu().size());
    }

    public class MenuHolder extends RecyclerView.ViewHolder {
        RelativeLayout itemLayout;
        ImageView menuIcon;
        TextView menuName;

        public MenuHolder(View itemView) {
            super(itemView);
            itemLayout = itemView.findViewById(R.id.rl_item_layout);
            menuIcon = itemView.findViewById(R.id.iv_menu_icon);
            menuName = itemView.findViewById(R.id.tv_menu_name);
        }
    }
}
