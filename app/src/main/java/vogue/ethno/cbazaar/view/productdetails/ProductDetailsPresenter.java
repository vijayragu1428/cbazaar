package vogue.ethno.cbazaar.view.productdetails;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.base.AbstractBasePresenter;
import vogue.ethno.cbazaar.classmodel.requestmodel.addtocart.AddToCartRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.bodyheight.BodyHeightRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.getcartdetails.GetCartRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.readysize.NaturalWaistRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.topstyle.TopStyleRequestModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.accesstoken.TokenResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.addtocart.AddToCartDetailsResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.bodyheight.BodyHeightResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.bodyheight.MeasurementFeetItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.CommonDetail;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.ItemsItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.MeasurementOptions;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.ProductDetailsResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.ProductSizeItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.readysize.NaturalWaistResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.topstyle.TopStyleResponse;
import vogue.ethno.cbazaar.data.ApiClient;
import vogue.ethno.cbazaar.data.network.HeaderValues;
import vogue.ethno.cbazaar.data.network.NetworkCallback;

public class ProductDetailsPresenter extends AbstractBasePresenter<ProductDetailsListener> implements NetworkCallback {

    ProductDetailsListener view;
    private int apiFrom = 0;
    private ApiClient apiClient;
    private Context mContext;
    private MeasurementFeetItem topStyleDataFeetItem;
    private String waiseCircumferenceId;
    private String waistFloorId;
    private AddToCartRequestModel addToCartResquestModel;
    private boolean isBuyNow;

    @Override
    public void setView(ProductDetailsListener view) {
        super.setView(view);
        this.view = view;
        this.mContext = view.getActivityContext();
        this.apiClient = new ApiClient(this);
    }

    /*
     * 1  productDetails
     * 2  body height
     * 3  natural wishlist
     * */
    public void apiCall(int whichApi) {
        loadingDialog(true);
        this.apiFrom = whichApi;
        switch (whichApi) {
            case 1:
                apiClient.getProduct(whichApi, view.getProductId());
                break;
            case 2:
                apiClient.getBodyHeight(whichApi, getHeight());
                break;
            case 3:
                apiClient.getNatualWaist(getWaist(), whichApi);
                break;
            case 4:
                //     apiClient.getBodyHeight(whichApi, getHeight());
                break;
            case 5:
                apiClient.geTopStyle(whichApi, getTopStyleData());
                break;
            case 6:
                apiClient.getCircumference(whichApi, waiseCircumferenceId);
                break;
            case 7:
                apiClient.getWaistFloor(whichApi, waistFloorId);
                break;
            case 8:
                apiClient.addCartItem(whichApi, addToCartResquestModel);
                break;
            case 9:
                apiClient.getCartDetails(whichApi, getCartData());
                break;
        }
    }

    private GetCartRequestModel getCartData() {
        GetCartRequestModel cartRequestModel = new GetCartRequestModel();
        cartRequestModel.setCartItemID(0);
        cartRequestModel.setCommand(2);
        cartRequestModel.setDeliveryMode(2);
        cartRequestModel.setGrandTotal(0);
        cartRequestModel.setIsCODEligible(false);
        cartRequestModel.setIsEDS(false);
        cartRequestModel.setIsMoveToShortlist(false);
        cartRequestModel.setPromoDiscountPercentage(0);
        cartRequestModel.setProductCode(0);
        return cartRequestModel;
    }

    private TopStyleRequestModel getTopStyleData() {
        TopStyleRequestModel styleRequestModel = new TopStyleRequestModel();
        styleRequestModel.setFeetAndInches(topStyleDataFeetItem.getValue());
        styleRequestModel.setProductCode(view.getProductId());
        styleRequestModel.setSizeValue(topStyleDataFeetItem.getSize());
        return styleRequestModel;
    }

    public void getTopStyle(MeasurementFeetItem measurementFeetItem) {
        this.topStyleDataFeetItem = measurementFeetItem;
        apiCall(5);
    }

    private BodyHeightRequestModel getHeight() {
        BodyHeightRequestModel bodyHeightRequestModel = new BodyHeightRequestModel();
        bodyHeightRequestModel.setProductCode(view.getProductId());
        bodyHeightRequestModel.setValue(view.getHeightRequest().getValue());
        return bodyHeightRequestModel;
    }

    private void failureAction(String s) {
        loadingDialog(false);
        if (view != null) {
            view.showError(s);
        }
    }

    private void loadingDialog(boolean status) {
        if (view != null) {
            if (status)
                view.showLoading();
            else view.hideLoading(status);
        }
    }

    @Override
    public void onSuccess(Object o, int apiFrom) {
        if (apiFrom != -1)
            loadingDialog(false);
        try {
            if (o != null) {
                switch (apiFrom) {
                    case -1:
                        accessToken(o);
                        break;
                    case 1:
                        productDetails(o);
                        break;
                    case 2:
                        getBodyHeight(o);
                        break;
                    case 3:
                        getNaturalwaist(o);
                        break;
                    case 5:
                        getTopStyleFromServer(o);
                        break;
                    case 6:
                        getCircumferenceData(o);
                        break;
                    case 7:
                        getWaistFloor(o);
                        break;
                    case 8:
                        addToCartSuccess(o);
                        break;
                    case 9:
                        getCartCount(o);
                        break;
                }
            } else failureAction(mContext.getString(R.string.some_thing_went_wrong));
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void getCartCount(Object o) {
        try {
            GetCartResponseModel responseModel = (GetCartResponseModel) o;
            if (responseModel != null && responseModel.isIsSuccess()) {
                if (isBuyNow)
                    view.buyNowCart(responseModel);
                else
                    view.updateCartCount(responseModel);
            }
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void addToCartSuccess(Object o) {
        try {
            AddToCartDetailsResponseModel responseModel = (AddToCartDetailsResponseModel) o;

            if (responseModel != null && responseModel.getStatusCode().equalsIgnoreCase("200")) {
                /// if (isBuyNow)
                //     view.buyNowCart(responseModel);
                //  else
                apiCall(9);
            } else failureAction(mContext.getString(R.string.some_thing_went_wrong));
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }

    }

    private void getWaistFloor(Object o) {
        try {
            if (o instanceof JsonArray) {
                JsonArray jsonArr = (JsonArray) o;
                Type listType = new TypeToken<List<NaturalWaistResponseModel>>() {
                }.getType();
                List<NaturalWaistResponseModel> posts = new Gson().fromJson(jsonArr, listType);

                view.successFromWaist(posts);
            } else failureAction(mContext.getString(R.string.some_thing_went_wrong));
            // }
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void getCircumferenceData(Object o) {
        try {
            if (o instanceof JsonArray) {
                JsonArray jsonArr = (JsonArray) o;
                Type listType = new TypeToken<List<NaturalWaistResponseModel>>() {
                }.getType();
                List<NaturalWaistResponseModel> posts = new Gson().fromJson(jsonArr, listType);

                view.successFromCircumference(posts);
            } else failureAction(mContext.getString(R.string.some_thing_went_wrong));
            // }
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void getTopStyleFromServer(Object o) {
        try {
            TopStyleResponse jsonArr = (TopStyleResponse) o;

            view.successFromTopStyle(jsonArr);
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void getNaturalwaist(Object o) {
        try {
            List<NaturalWaistResponseModel> responseModels = new ArrayList<>(0);
            if (o instanceof JsonArray) {
                JsonArray jsonArr = (JsonArray) o;
                Type listType = new TypeToken<List<NaturalWaistResponseModel>>() {
                }.getType();
                List<NaturalWaistResponseModel> posts = new Gson().fromJson(jsonArr, listType);               /* if (jsonArr != null) {
                    for (int i = 0; i < jsonArr.size(); i++) {
                        JsonObject jsonObj = jsonArr.getAsJsonObject(i);
                        NaturalWaistResponseModel model = new NaturalWaistResponseModel();
                        model.setInches(jsonObj.getString("Inches"));
                        model.setId(jsonObj.getString("Id"));
                        responseModels.add(model);
                    }*/
                view.successFromServerNaturalWaist(posts);
            } else failureAction(mContext.getString(R.string.some_thing_went_wrong));
            // }
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void getBodyHeight(Object o) {
        try {
            BodyHeightResponseModel responseModel = (BodyHeightResponseModel) o;
            if (responseModel != null) {
                view.successFromServerBodyHeight(responseModel);
            } else {
                failureAction(mContext.getString(R.string.some_thing_went_wrong));
            }
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }


    private void accessToken(Object o) {
        try {
            TokenResponseModel responseModel = (TokenResponseModel) o;
            if (responseModel != null && !cutNull(responseModel.getAccessToken()).isEmpty() && !cutNull(responseModel.getTokenType()).isEmpty()) {
                HeaderValues.ACESS_TOKEN = responseModel.getAccessToken();
                HeaderValues.TOKEN_TYPE = responseModel.getTokenType();
                apiCall(apiFrom);
            } else {
                HeaderValues.ACESS_TOKEN = "";
                HeaderValues.TOKEN_TYPE = "";
                tokenError(o);
            }
        } catch (Exception e) {
            HeaderValues.TOKEN_TYPE = "";
            HeaderValues.ACESS_TOKEN = "";
            tokenError(o);
        }
    }

    private void productDetails(Object o) {
        try {
            ProductDetailsResponseModel responseModel = (ProductDetailsResponseModel) o;
            if (responseModel != null) {
                view.successFromServer(responseModel);
            } else {
                failureAction(mContext.getString(R.string.some_thing_went_wrong));
            }
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    @Override
    public void onFailure(String body) {
        failureAction(body);
    }

    @Override
    public void onErrorBody(Object o, int apiFrom) {
        loadingDialog(false);
        switch (apiFrom) {
            case -1:
                tokenError(o);
                break;
            case 1:
                productDetailsError(o);
                break;
            case 2:
                productDetailsError(o);
                break;
        }
        //   }
    }


    private void tokenError(Object o) {
        loadingDialog(false);
        if (view != null)
            view.sessionError("");
    }

    private void productDetailsError(Object o) {
        failureAction(mContext.getString(R.string.some_thing_went_wrong));
    }

    private String cutNull(Object o) {
        return Utils.cutNull(o);
    }


    public NaturalWaistRequestModel getWaist() {
        NaturalWaistRequestModel waist = new NaturalWaistRequestModel();
        waist.setSizeId(view.getHeightRequest().getValue());
        waist.setValue(view.getHeightRequest().getValue());
        waist.setType("naturalwaist");
        return waist;
    }


    public void waiseCircumference(String id) {
        this.waiseCircumferenceId = id;
        apiCall(6);
    }

    public void waistFloor(String id) {
        this.waistFloorId = id;
        apiCall(7);
    }

    public void addToCart(CommonDetail productCommonDetails, Object selectedItem, MeasurementOptions mesurementOptions, String productId, ProductSizeItem productSizeItem, boolean isBuyNow, Object height, Object naturalWaist, Object circumFerence, Object floor, List<String> selectedAddon) {
        String measurementFlag = "";
        this.isBuyNow = isBuyNow;
        if (mesurementOptions != null && mesurementOptions.getItems() != null) {
            for (ItemsItem item : mesurementOptions.getItems()) {
                if (item.getDescription().equalsIgnoreCase("Ready Size")) {
                    measurementFlag = item.getCode();
                }
            }
        }
        addToCartResquestModel = new AddToCartRequestModel();
        //need to check ready size or not
        addToCartResquestModel.setCustomization("2");
        addToCartResquestModel.setQuantity(Integer.parseInt(selectedItem.toString()));
        addToCartResquestModel.setMeasurementFlag(measurementFlag);
        addToCartResquestModel.setProductCode(productId);
        if (productSizeItem != null && productSizeItem.getValue() != null)
            addToCartResquestModel.setProductSize(productSizeItem.getValue());
        String addOns = "";
        if (selectedAddon != null) {
            addOns = selectedAddon.toString().replace(",", "|").replace("[", "").replace("]", "");
        }
        addToCartResquestModel.setSupplementaryProductCodes(addOns);
        if (height != null)
            addToCartResquestModel.setBodyHeight(height.toString());
        if (naturalWaist != null)
            addToCartResquestModel.setNaturalWaist(naturalWaist.toString());
        if (circumFerence != null)
            addToCartResquestModel.setWaistCircumference(circumFerence.toString());
        if (floor != null)
            addToCartResquestModel.setWaistToFloor(floor.toString());
        apiCall(8);
    }
}
