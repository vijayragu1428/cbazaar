package vogue.ethno.cbazaar.view.payment.viewdetailsdialog;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.classmodel.requestmodel.confirmcart.ShippingAddress;
import vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart.ConfirmCartResponseModel;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class ViewDetailsFragment extends DialogFragment {


  //  @BindView(R.id.tv_summary)
    TextView tvSummary;
  //  @BindView(R.id.rel_discount)
    RelativeLayout relativeLayout;
  //  @BindView(R.id.tv_currency)
    TextView tvCurrency;
   // @BindView(R.id.txt_hide_details)
    TextView txtHideDetails;
 //   @BindView(R.id.txt_gross_order_total)
    TextView txtGrossOrderTotal;
 //   @BindView(R.id.txt_gross_order_total_amount)
    TextView txtGrossOrderTotalAmount;
   // @BindView(R.id.img_hint)
    ImageView imgHint;
 //   @BindView(R.id.txt_product_discount)
    TextView txtProductDiscount;
  //  @BindView(R.id.txt_product_discount_amount)
    TextView txtProductDiscountAmount;
 //   @BindView(R.id.txt_order_sub_total)
    TextView txtOrderSubTotal;
//    @BindView(R.id.txt_order_sub_total_amount)
    TextView txtOrderSubTotalAmount;
 //   @BindView(R.id.txt_shipping_charges)
    TextView txtShippingCharges;
 //   @BindView(R.id.txt_shipping_charges_amount)
    TextView txtShippingChargesAmount;
 //   @BindView(R.id.txt_order_total)
    TextView txtOrderTotal;
 //   @BindView(R.id.txt_order_total_amount)
    TextView txtOrderTotalAmount;
 //   @BindView(R.id.tv_you_have_saved)
    TextView tvYouHaveSaved;
 //   @BindView(R.id.tv_total_cretit)
    TextView tvTotalCretit;
 //   @BindView(R.id.txt_total_payment)
    TextView txtTotalPayment;
  //  @BindView(R.id.txt_total_payment_amount)
    TextView txtTotalPaymentAmount;
  //  @BindView(R.id.txt_adress_details)
    TextView txtAdressDetails;
  //  @BindView(R.id.lin_summary)
    LinearLayout linSummary;
    Unbinder unbinder;

    private ConfirmCartResponseModel getCartResponseModel;
    private String address = "";


    @SuppressLint("ValidFragment")
    public ViewDetailsFragment(ConfirmCartResponseModel model, String s, ShippingAddress shippingAddress) {
        this.getCartResponseModel = model;
        this.address = s;
    }
    /*@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_view_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }*/

  /*  @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }
*/
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final  Dialog dialog = new  Dialog(getActivity());
        dialog.setContentView(R.layout.fragment_view_details);
        setViewId(dialog);initUi();
         dialog.setCancelable(true);
        return dialog;
    }

    private void setViewId( Dialog dialog) {
           tvSummary=dialog.findViewById(R.id.tv_summary);
           relativeLayout=dialog.findViewById(R.id.rel_discount);
           tvCurrency=dialog.findViewById( R.id.tv_currency);
           txtHideDetails=dialog.findViewById(R.id.txt_hide_details);
           txtGrossOrderTotal=dialog.findViewById(R.id.txt_gross_order_total);
           txtGrossOrderTotalAmount=dialog.findViewById(R.id.txt_gross_order_total_amount);
           imgHint=dialog.findViewById(R.id.img_hint);
           txtProductDiscount=dialog.findViewById(R.id.txt_product_discount);
           txtProductDiscountAmount=dialog.findViewById(R.id.txt_product_discount_amount);
           txtOrderSubTotal=dialog.findViewById(R.id.txt_order_sub_total);
           txtOrderSubTotalAmount=dialog.findViewById(R.id.txt_order_sub_total_amount);
           txtShippingCharges=dialog.findViewById(R.id.txt_shipping_charges);
           txtShippingChargesAmount=dialog.findViewById(R.id.txt_shipping_charges_amount);
           txtOrderTotal=dialog.findViewById(R.id.txt_order_total);
           txtOrderTotalAmount=dialog.findViewById(R.id.txt_order_total_amount);
           tvYouHaveSaved=dialog.findViewById(R.id.tv_you_have_saved);
           tvTotalCretit=dialog.findViewById(R.id.tv_total_cretit);
           txtTotalPayment=dialog.findViewById( R.id.txt_total_payment);
           txtTotalPaymentAmount=dialog.findViewById(R.id.txt_total_payment_amount);
           txtAdressDetails=dialog.findViewById(R.id.txt_adress_details);
         linSummary=dialog.findViewById(R.id.lin_summary);;
    }

  /*  @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.AppTheme);
    }*/

    private void initUi() {
        setData();
    }

    @SuppressLint("SetTextI18n")
    private void setData() {
        if (getCartResponseModel != null && getCartResponseModel.getCartdetail().getCartSummaries() != null) {
            tvTotalCretit.setText(getSpanableString());
            txtAdressDetails.setText(address);
            final vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart.CartSummaries summaries = getCartResponseModel.getCartdetail().getCartSummaries();
            txtGrossOrderTotalAmount.setText(Utils.getAmountString(summaries.getGrossOrderTotal()));

            try {
                int i = Integer.parseInt(String.valueOf(summaries.getProductDiscounts()));
                if (i == 0) {
                    relativeLayout.setVisibility(View.GONE);
                    tvYouHaveSaved.setVisibility(View.GONE);
                } else {
                    relativeLayout.setVisibility(View.VISIBLE);
                    tvYouHaveSaved.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            txtProductDiscountAmount.setText(Utils.getAmountString(summaries.getProductDiscounts()));
            txtOrderSubTotalAmount.setText(Utils.getAmountString(summaries.getSubTotal()));
            txtOrderTotalAmount.setText(Utils.getAmountString(summaries.getGrandTotal()));
            txtTotalPaymentAmount.setText(Utils.getAmountString(summaries.getGrandTotal()));
            tvCurrency.setText(getString(R.string.currency) + " : " + Utils.cutNull(getCartResponseModel.getCartdetail().getInfomations().getCurrencyCode()));
            if (getCartResponseModel.getCartdetail().getShippingCost() != null) {
                if (getCartResponseModel.getCartdetail().getShippingCost().getShippingCost() == 0.0 || getCartResponseModel.getCartdetail().getShippingCost().getShippingCost() == 0)
                    txtShippingChargesAmount.setText(getString(R.string.free));
                else
                    txtShippingChargesAmount.setText(Utils.getAmountString(getCartResponseModel.getCartdetail().getShippingCost().getShippingCost()));
                if (summaries.getSavings() == 0 || summaries.getSavings() == 0.0)
                    tvYouHaveSaved.setText(getString(R.string.you_have_saves) + " " + Utils.getAmountString(0));
                else
                    tvYouHaveSaved.setText(getString(R.string.you_have_saves) + " " + Utils.getAmountString(summaries.getSavings()));
            }

        }
    }

    private SpannableString getSpanableString() {
        SpannableString wordtoSpan = new SpannableString(getString(R.string.you_have_total) + " " + amount() + " " + getString(R.string.store_credits_available));
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), getString(R.string.you_have_total).length() + 1, (getString(R.string.you_have_total).length() + 1) + (amount().length() + 1), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return wordtoSpan;
    }

    private String amount() {
        if (getCartResponseModel.getCartdetail().getCartSummaries() != null)
            return Utils.getAmountString(getCartResponseModel.getCartdetail().getCartSummaries().getStoreCredit());
        return "0";
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        unbinder.unbind();
    }

    @OnClick(R.id.txt_hide_details)
    public void onViewClicked() {
        dismiss();
    }
}
