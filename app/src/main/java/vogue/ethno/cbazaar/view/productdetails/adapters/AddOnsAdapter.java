package vogue.ethno.cbazaar.view.productdetails.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.AddOnItem;

public class AddOnsAdapter extends RecyclerView.Adapter<AddOnsAdapter.ViewHolder> {


    private Context mContext;
    private List<AddOnItem> addOnItemList;
    private OnChecked checked;

    public AddOnsAdapter(Context mContext, OnChecked onChecked) {
        this.mContext = mContext;
        this.addOnItemList = new ArrayList<>(0);
        this.checked = onChecked;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_add_on, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final AddOnItem addOnItem = addOnItemList.get(position);
        holder.txtTitlePrice.setText(Utils.cutNull(addOnItem.getDescription() + " (" + Utils.getAmountString(addOnItem.getCost()) + ")"));
        holder.txtTiltle.setText(Utils.cutNull(addOnItem.getDetail()));
        Glide.with(mContext).load(Utils.cutNull(addOnItem.getImage()))
                .apply(RequestOptions.placeholderOf(R.drawable.ev_logo))
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .into(holder.imgItemImg);
        holder.radioBtnYes.setChecked(true);
        checked.onCheckedChangeListener(true,addOnItem);

        holder.radioBtnYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                     checked.onCheckedChangeListener(isChecked,addOnItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return addOnItemList == null ? 0 : addOnItemList.size();
    }

    public void loadData(List<AddOnItem> addOn) {
        this.addOnItemList = addOn;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_title_price)
        TextView txtTitlePrice;
        @BindView(R.id.txt_tiltle)
        TextView txtTiltle;
        @BindView(R.id.img_item_img)
        ImageView imgItemImg;
        @BindView(R.id.radio_btn_yes)
        RadioButton radioBtnYes;
        @BindView(R.id.radio_btn_no)
        RadioButton radioBtnNo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnChecked {
        void onCheckedChangeListener(boolean status, AddOnItem onItem);
    }
}
