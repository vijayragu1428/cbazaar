package vogue.ethno.cbazaar.view.homescreen.fragment.home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import vogue.ethno.cbazaar.apputils.ItemDecorationGridColumns;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.model.BannerModel;
import vogue.ethno.cbazaar.model.PatternModel;
import vogue.ethno.cbazaar.model.ProductModel;
import vogue.ethno.cbazaar.model.adapter.PatternAdapter;
import vogue.ethno.cbazaar.model.adapter.ProductAdapter;
import vogue.ethno.cbazaar.view.homescreen.FragmentListener;

public class ProductTypeFragment extends Fragment {


    @BindView(R.id.rv_banner)
    RecyclerView rvBanner;
    @BindView(R.id.rv_pattern)
    RecyclerView rvPattern;
    @BindView(R.id.rv_product)
    RecyclerView rvProduct;

    ArrayList<BannerModel> bannerList;
    ArrayList<PatternModel> patternList;
    ArrayList<ProductModel> productList;

    PatternAdapter patternAdapter;
    ProductAdapter productAdapter;

    int spanCount = 3;

    private FragmentListener mListener;
    private Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_type_product, container, false);
        ButterKnife.bind(this, view);
        initUi();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void initUi() {
        mContext = getContext();
        initRv();
    }

    private void initRv() {

        bannerList = new ArrayList<>();
        patternList = new ArrayList<>();
        productList = new ArrayList<>();

        bannerList = getBannerList();
        patternList = getPatternList();
        productList = getProductList();

        rvBanner.setHasFixedSize(true);
        rvPattern.setHasFixedSize(true);
        rvProduct.setHasFixedSize(true);

        rvProduct.setNestedScrollingEnabled(false);

        LinearLayoutManager bannerLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager patternLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        GridLayoutManager productLayoutManager = new GridLayoutManager(mContext, spanCount, GridLayoutManager.VERTICAL, false);

        rvBanner.setLayoutManager(bannerLayoutManager);
        rvPattern.setLayoutManager(patternLayoutManager);
        rvProduct.setLayoutManager(productLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvBanner.getContext(), bannerLayoutManager.getOrientation());
        rvBanner.addItemDecoration(dividerItemDecoration);

        rvProduct.addItemDecoration(new ItemDecorationGridColumns(15, 3));

        patternAdapter = new PatternAdapter(patternList, mContext);
        productAdapter = new ProductAdapter(productList, mContext);

        rvPattern.setAdapter(patternAdapter);
        rvProduct.setAdapter(productAdapter);
    }

    private ArrayList<BannerModel> getBannerList() {
        for (int i = 0; i < 10; i++) {
            BannerModel banner = new BannerModel(R.drawable.banner, "banner " + i);
            bannerList.add(banner);
        }
        return bannerList;
    }

    private ArrayList<PatternModel> getPatternList() {
        for (int i = 0; i < 10; i++) {
            PatternModel pattern = new PatternModel(R.drawable.fustan, "pattern " + i);
            patternList.add(pattern);
        }
        return patternList;
    }

    private ArrayList<ProductModel> getProductList() {
        for (int i = 0; i < 30; i++) {
            ProductModel product = new ProductModel(R.drawable.indo_western, "product " + i);
            productList.add(product);
        }
        return productList;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            mListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
