package vogue.ethno.cbazaar.view.productdetails.adapters;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.classmodel.responsemodel.productdetails.ProductSizeItem;

public class ProductSizeAdapter extends RecyclerView.Adapter<ProductSizeAdapter.ViewHolder> {


    private List<ProductSizeItem> addOnItemList;
    private int selectedPosition = 0;
    private RecyclerView view;
    private OnSizeClick onSizeClick;
    private int firstTime = 0;

    public ProductSizeAdapter(OnSizeClick mContext, RecyclerView rvSize) {
        this.onSizeClick = mContext;
        this.addOnItemList = new ArrayList<>(0);
        this.view = rvSize;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_size_cart, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ProductSizeItem addOnItem = addOnItemList.get(position);
        holder.onItemBind(addOnItem);
       /* try {
            LinearLayoutManager layoutManager = ((LinearLayoutManager) view.getLayoutManager());
            int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
            int centeredItemPosition = totalVisibleItems / 2;
            view.smoothScrollToPosition(position);
            view.setScrollY(centeredItemPosition);
         */
        holder.txtItemSize.setText(addOnItem.getValue());

        if (selectedPosition == position) {
            holder.txtItemSize.setBackgroundResource(R.drawable.item_size_circle);
            holder.txtItemSize.setTextColor(Color.parseColor("#ffffff"));
        } else
            holder.txtItemSize.setBackgroundResource(R.color.sizeTop);
        if (position > 0 && firstTime == 0) {
            firstTime = 1;
            selectedPosition = 0;
            onSizeClick.onSizeClickEvent(addOnItemList.get(0));
        }
    }

    @Override
    public int getItemCount() {
        return addOnItemList == null ? 0 : addOnItemList.size();
    }

    public void loadData(List<ProductSizeItem> productSize) {
        this.addOnItemList = productSize;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_item_size)
        TextView txtItemSize;
        private ProductSizeItem productSizeItem;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.txt_item_size)
        public void onViewClicked() {
            selectedPosition = getAdapterPosition();
            onSizeClick.onSizeClickEvent(productSizeItem);
            // onSizeClick.onSizeClickEvent(productSizeItem);
            // notifyDataSetChanged();
        }

        public void onItemBind(ProductSizeItem addOnItem) {
            this.productSizeItem = addOnItem;
        }
    }

    public interface OnSizeClick {
        void onSizeClickEvent(ProductSizeItem productSizeItem);
    }
}
