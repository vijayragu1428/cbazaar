package vogue.ethno.cbazaar.view.homescreen;

public interface OnItemSelectedCallback {
    void onItemSelected(String name, String redirectUrl);
}
