package vogue.ethno.cbazaar.view.homescreen;

import java.util.List;

import vogue.ethno.cbazaar.base.LoadDataView;
import vogue.ethno.cbazaar.classmodel.responsemodel.rsakey.RsaKeyResponseModel;
import vogue.ethno.cbazaar.model.HomeDetailsResponseModel;

public interface HomeDetailsListener extends LoadDataView {
    void successFromServer(HomeDetailsResponseModel responseModel);

    void successFromServerRSA(List<RsaKeyResponseModel> rsaKeyResponseModel);
}
