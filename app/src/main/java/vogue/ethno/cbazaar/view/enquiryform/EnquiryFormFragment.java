package vogue.ethno.cbazaar.view.enquiryform;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.classmodel.requestmodel.enquiryform.EnquiryFormRequest;
import vogue.ethno.cbazaar.classmodel.responsemodel.getstates.StateResponseModel;
import vogue.ethno.cbazaar.view.homescreen.HomeActivity;


public class EnquiryFormFragment extends Fragment implements EnquiryFormListener {


    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.et_full_name)
    EditText etFullName;
    @BindView(R.id.et_email_address)
    EditText etEmailAddress;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.sp_state)
    Spinner spState;
    /*@BindView(R.id.sp_district)
    Spinner spDistrict;*/
    @BindView(R.id.et_district)
    EditText etDistrict;
    @BindView(R.id.et_city)
    EditText etCity;
    @BindView(R.id.et_purpose)
    EditText etPurpose;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
 @BindView(R.id.rel_root)
 RelativeLayout rel_root;

    private HomeActivity homeActivity;
    private EnquiryFormPresenter enquiryFormPresenter;
    private EnquiryFormRequest formRequest;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_enquiry_form, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        intUi();
    }

    private void intUi() {
        enquiryFormPresenter = new EnquiryFormPresenter();
        enquiryFormPresenter.setView(this);
        homeActivity = (HomeActivity) getActivity();
        homeActivity.showHideToolbar(true);
        toolBar.setNavigationIcon(getContext().getResources().getDrawable(R.drawable.ic_back_arrow));
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        enquiryFormPresenter.apiCall(2);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        homeActivity.showHideToolbar(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.btn_submit)
    public void onViewClicked() {
        if (validation()) {
            formRequest = new EnquiryFormRequest();
            formRequest.setFirstName(etFullName.getText().toString());
            formRequest.setEmailAddress(etEmailAddress.getText().toString());
            formRequest.setPhone(etPhone.getText().toString());
            formRequest.setState(spState.getSelectedItem().toString());
            formRequest.setDistrict(etDistrict.getText().toString());
            formRequest.setAddress1(etCity.getText().toString() + ", " + etPurpose.getText().toString());
            enquiryFormPresenter.apiCall(1);
        }
    }

    @Override
    public EnquiryFormRequest getSubmitData() {
        return formRequest;
    }

    @Override
    public void stateList(ArrayList<StateResponseModel> stateList) {
        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add("--Select State--");
        for (StateResponseModel s : stateList)
            arrayList.add(s.getStateName().replaceAll("\"", ""));

        ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, arrayList);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spState.setAdapter(stateAdapter);
    }

    @Override
    public void showLoading() {
        homeActivity.showDialog("", false,rel_root);
    }

    @Override
    public void hideLoading(boolean status) {
        homeActivity.hideDialog(status);
    }

    @Override
    public Context getActivityContext() {
        return null;
    }

    @Override
    public void showError(String string) {
        Toast.makeText(getContext(), string, Toast.LENGTH_LONG).show();

    }

    @Override
    public void sessionError(String string) {

    }

    private boolean validation() {
        boolean flag = true;

        if (etFullName.getText().toString().isEmpty()) {
            flag = false;
            etFullName.setError("");
        }

        if (etEmailAddress.getText().toString().isEmpty()) {
            flag = false;
            etEmailAddress.setError("");

        }
        if (etPhone.getText().toString().isEmpty()) {
            flag = false;
            etPhone.setError("");

        }
        if (spState.getSelectedItem().toString().contains("--Select State--")) {
            flag = false;
            showError("Please select a state");

        }
        if (etDistrict.getText().toString().isEmpty()) {
            flag = false;
            etDistrict.setError("");

        }
        if (etCity.getText().toString().isEmpty()) {
            flag = false;
            etCity.setError("");

        }
        if (etPurpose.getText().toString().isEmpty()) {
            flag = false;
            etPurpose.setError("");
        }

        return flag;
    }
}
