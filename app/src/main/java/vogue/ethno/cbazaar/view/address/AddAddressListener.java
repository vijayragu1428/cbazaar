package vogue.ethno.cbazaar.view.address;

import java.util.List;

import vogue.ethno.cbazaar.base.LoadDataView;
import vogue.ethno.cbazaar.classmodel.requestmodel.address.AddressRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.confirmcart.ShippingAddress;
import vogue.ethno.cbazaar.classmodel.responsemodel.addaddress.AddAddressResponse;
import vogue.ethno.cbazaar.classmodel.responsemodel.addaddress.RelationInfoItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart.ConfirmCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.getaddress.AddressResponse;

public interface AddAddressListener extends LoadDataView {
    void successAddress(AddressResponse responseModel);

    AddressRequestModel model();

    void addAddressResponse(AddAddressResponse addAddressResponse);

    void removeAddress(List<RelationInfoItem> relationInfo);

    void cartResponseModel(ConfirmCartResponseModel cartResponseModel, ShippingAddress shippingAddress);
}
