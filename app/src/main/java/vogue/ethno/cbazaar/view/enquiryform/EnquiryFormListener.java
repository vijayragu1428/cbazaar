package vogue.ethno.cbazaar.view.enquiryform;

import java.util.ArrayList;

import vogue.ethno.cbazaar.base.LoadDataView;
import vogue.ethno.cbazaar.classmodel.requestmodel.enquiryform.EnquiryFormRequest;
import vogue.ethno.cbazaar.classmodel.responsemodel.getstates.StateResponseModel;

public interface EnquiryFormListener extends LoadDataView {

    EnquiryFormRequest getSubmitData();

    void stateList(ArrayList<StateResponseModel> stateList);
}
