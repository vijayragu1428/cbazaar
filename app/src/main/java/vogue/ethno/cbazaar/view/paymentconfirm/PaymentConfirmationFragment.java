package vogue.ethno.cbazaar.view.paymentconfirm;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.classmodel.responsemodel.verifyotp.VerifyOtpResponse;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class PaymentConfirmationFragment extends Fragment {


    public  PaymentConfirmationFragment(VerifyOtpResponse responseModel) {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_confirm_payment, container, false);
    }

}
