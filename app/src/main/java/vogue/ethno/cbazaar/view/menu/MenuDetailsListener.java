package vogue.ethno.cbazaar.view.menu;

import vogue.ethno.cbazaar.base.LoadDataView;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;

public interface MenuDetailsListener extends LoadDataView {
    void successFromServer(MenuModel responseModel);
    void onMenuItemClick(String path);

    void updateCartCount(GetCartResponseModel responseModel);
}
