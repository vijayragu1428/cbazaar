package vogue.ethno.cbazaar.view.payment;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.base.AbstractBasePresenter;
import vogue.ethno.cbazaar.classmodel.requestmodel.getcartdetails.GetCartRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.storecreatepay.StoreCreatePayRequest;
import vogue.ethno.cbazaar.classmodel.responsemodel.accesstoken.TokenResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.cashondelivery.CashOnDelivryResponse;
import vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart.ConfirmCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.rsakey.RsaKeyResponseModel;
import vogue.ethno.cbazaar.data.ApiClient;
import vogue.ethno.cbazaar.data.network.HeaderValues;
import vogue.ethno.cbazaar.data.network.NetworkCallback;

public class PaymentPresenter extends AbstractBasePresenter<PaymentListener> implements NetworkCallback {
    private PaymentListener view;
    private Context mContext;
    private int apiFrom = 0;
    private ApiClient apiClient;
    private GetCartRequestModel model;
    private HashMap<String, Object> hashMap;
    private StoreCreatePayRequest storeCreatePayRequest;

    @Override
    public void setView(PaymentListener view) {
        super.setView(view);
        this.view = view;
        this.mContext = view.getActivityContext();
        this.apiClient = new ApiClient(this);
    }


    public void apiCall(int whichApi) {
        loadingDialog(true);
        this.apiFrom = whichApi;
        switch (whichApi) {
            case 1:
                apiClient.getAddress(whichApi, getCartAddressData());
                break;
            case 2:
                apiClient.cashOndelivery(whichApi, hashMap);
                break;
            case 3:
                apiClient.storeCretePayment(whichApi, storeCreatePayRequest);
                break;
            case 4:
                apiClient.getRsaKey(whichApi, view.orderId());
                break;

        }
    }

    private HashMap<String, Object> getCartAddressData() {
        HashMap<String, Object> hashMap = new HashMap<>(0);
        hashMap.put("ShippingCountryID", 250);
        hashMap.put("IsGuest", true);
        hashMap.put("TemporaryGuestAddress", null);
        return hashMap;
    }


    @Override
    public void onSuccess(Object o, int api) {
        if (apiFrom != -1)
            loadingDialog(false);
        try {
            if (o != null) {
                switch (apiFrom) {
                    case -1:
                        accessToken(o);
                        break;
                    case 1:
                        getCartCount(o);
                        break;
                    case 2:
                        cashOndelivery(o);
                        break;
                    case 3:
                        storeCreate(o);
                        break;
                    case 4:
                        getRasKey(o);
                        break;
                }
            } else failureAction(mContext.getString(R.string.some_thing_went_wrong));
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void getRasKey(Object o) {
        try {

            if (o instanceof JsonArray) {
                JsonArray jsonArr = (JsonArray) o;
                Type listType = new TypeToken<List<RsaKeyResponseModel>>() {
                }.getType();
                List<RsaKeyResponseModel> rsaKeyResponseModel = new Gson().fromJson(jsonArr, listType);

                if (rsaKeyResponseModel != null && rsaKeyResponseModel.size() > 0)
                    view.successFromServerRSA(rsaKeyResponseModel);
            } else failureAction(mContext.getString(R.string.some_thing_went_wrong));
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));

        }
    }

    private void storeCreate(Object o) {
        try {
            CashOnDelivryResponse responseModel = (CashOnDelivryResponse) o;
            if (responseModel != null) {
                view.cashOndeliverySuccess(responseModel);
            } else failureAction(mContext.getString(R.string.some_thing_went_wrong));
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void cashOndelivery(Object o) {
        try {
            CashOnDelivryResponse responseModel = (CashOnDelivryResponse) o;
            if (responseModel != null) {
                view.cashOndeliverySuccess(responseModel);
            } else failureAction(mContext.getString(R.string.some_thing_went_wrong));
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void getCartCount(Object o) {
        try {
            GetCartResponseModel responseModel = (GetCartResponseModel) o;
            if (responseModel != null && responseModel.isIsSuccess()) {
                //  view.updateCartCount(responseModel);
            }
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void accessToken(Object o) {
        try {
            TokenResponseModel responseModel = (TokenResponseModel) o;
            if (responseModel != null && !cutNull(responseModel.getAccessToken()).isEmpty() && !cutNull(responseModel.getTokenType()).isEmpty()) {
                HeaderValues.ACESS_TOKEN = responseModel.getAccessToken();
                HeaderValues.TOKEN_TYPE = responseModel.getTokenType();
                apiCall(apiFrom);
            } else {
                HeaderValues.ACESS_TOKEN = "";
                HeaderValues.TOKEN_TYPE = "";
                tokenError(o);
            }
        } catch (Exception e) {
            HeaderValues.TOKEN_TYPE = "";
            HeaderValues.ACESS_TOKEN = "";
            tokenError(o);
        }
    }

    private void tokenError(Object o) {
        loadingDialog(false);
        if (view != null)
            view.sessionError("");
    }

    private String cutNull(Object o) {
        return Utils.cutNull(o);
    }

    @Override
    public void onFailure(String body) {
        failureAction(body);
    }

    @Override
    public void onErrorBody(Object o, int api) {
        loadingDialog(false);
        switch (apiFrom) {
            case -1:
                tokenError(o);
                break;
            case 1:
                failureAction(mContext.getString(R.string.some_thing_went_wrong));
                break;
            case 2:
                failureAction(mContext.getString(R.string.some_thing_went_wrong));
                break;
        }
    }

    private void loadingDialog(boolean status) {
        if (view != null) {
            if (status)
                view.showLoading();
            else view.hideLoading(status);
        }
    }

    private void failureAction(String s) {
        loadingDialog(false);
        if (view != null) {
            view.showError(s);
        }
    }

    public void cashOndelivery(int index, String pref, int trackNumber) {
        hashMap = new HashMap<>();
        hashMap.put("MobileNumber", pref);
        hashMap.put("PaymentGateway", 18);
        hashMap.put("TrackNumber", trackNumber);
        apiCall(2);

    }

    public void storeCreatePay(ConfirmCartResponseModel model) {
        if (model.getTemporaryTransaction() != null) {
            storeCreatePayRequest = new StoreCreatePayRequest();
            storeCreatePayRequest.setTrackNumber(model.getTemporaryTransaction().getTrackNumber());
            storeCreatePayRequest.setTotalStoreCredited(model.getTotalStoreCreditAmount());
            storeCreatePayRequest.setGrandTotal(model.getGrandTotal());
            storeCreatePayRequest.setIsUsedCredit("false");
            storeCreatePayRequest.setStoreCreditDetail(new ArrayList<>(model.getStoreCreditDetail()));
            apiCall(3);
        }
    }
}
