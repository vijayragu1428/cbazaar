package vogue.ethno.cbazaar.view.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.base.BaseActivity;
import vogue.ethno.cbazaar.view.homescreen.HomeActivity;

public class LoginActivity extends BaseActivity implements LoginListener {

    @BindView(R.id.img_logo)
    ImageView imgLogo;
    @BindView(R.id.txt_login_title)
    TextView txtLoginTitle;
    @BindView(R.id.edt_user_id)
    EditText edtUserId;
    @BindView(R.id.ti_edt_user_id)
    TextInputLayout tiEdtUserId;
    @BindView(R.id.view_name)
    View viewName;
    @BindView(R.id.edt_password)
    EditText edtPassword;
    @BindView(R.id.ti_edt_password)
    TextInputLayout tiEdtPassword;
    @BindView(R.id.view_password)
    View viewPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.txt_register)
    TextView txtRegister;
    @BindView(R.id.txt_sign_up)
    TextView txtSignUp;
   @BindView(R.id.rel_root)
   ScrollView rel_root;

    private LoginPresenter loginPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initUI();
    }

    private void initUI() {
        loginPresenter = new LoginPresenter();
        loginPresenter.setView(this);
    }

    @OnClick({R.id.btn_login, R.id.txt_register, R.id.txt_sign_up})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_login: {
                if (validation())
                    loginPresenter.apiCall(1);
                break;
            }
            case R.id.txt_register: {
                break;
            }
            case R.id.txt_sign_up: {
                break;
            }
        }
    }

    private boolean validation() {
       /* if (edtUserId.getText().toString().isEmpty()) {
            showError(getString(R.string.please_enter_email));
            return false;
        }
        if (!Utils.isValidEmail(edtUserId.getText().toString())) {
            showError(getString(R.string.please_enter_valid_email));
            return false;
        }*/
        if (edtPassword.getText().toString().isEmpty()) {
            showError(getString(R.string.please_enter_pass));
            return false;
        }
        if (Utils.cutNull(edtPassword.getText().toString()).length() < 5) {
            showError(getString(R.string.please_enter_pass_6));
            return false;
        }
        return true;
    }

    @Override
    public void showLoading() {
        showDialog("", false,rel_root);

    }

    @Override
    public void hideLoading(boolean status) {
        hideDialog(status);

    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public void showError(String string) {
        showToast(string);

    }

    @Override
    public void sessionError(String string) {

    }

    @Override
    public String getLoginId() {
        return edtUserId.getText().toString();
    }

    @Override
    public String getPassword() {
        return edtPassword.getText().toString();
    }

    @Override
    public boolean getIsGuest() {
        return false;
    }

    @Override
    public void loginSuccess() {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
