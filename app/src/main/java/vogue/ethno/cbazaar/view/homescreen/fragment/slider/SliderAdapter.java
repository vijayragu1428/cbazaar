package vogue.ethno.cbazaar.view.homescreen.fragment.slider;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

public class SliderAdapter extends RecyclerView.Adapter<SliderAdapter.ViewHolder> {
    private OnViewClickListener clickListener;

    public SliderAdapter(OnViewClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public void loadData() {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    public interface OnViewClickListener {
        void onItemClick(int pos);
    }
}
