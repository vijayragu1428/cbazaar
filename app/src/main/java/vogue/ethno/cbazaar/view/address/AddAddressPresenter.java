package vogue.ethno.cbazaar.view.address;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.base.AbstractBasePresenter;
import vogue.ethno.cbazaar.classmodel.requestmodel.address.AddressRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.confirmcart.AppliedPromoCodeDetailsItem;
import vogue.ethno.cbazaar.classmodel.requestmodel.confirmcart.BillingAddress;
import vogue.ethno.cbazaar.classmodel.requestmodel.confirmcart.ConfirmCartRequest;
import vogue.ethno.cbazaar.classmodel.requestmodel.confirmcart.ShippingAddress;
import vogue.ethno.cbazaar.classmodel.responsemodel.accesstoken.TokenResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.addaddress.AddAddressResponse;
import vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart.ConfirmCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.getaddress.AddressResponse;
import vogue.ethno.cbazaar.classmodel.responsemodel.getaddress.RelationItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.CartDetailItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.CartSummaries;
import vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails.GetCartResponseModel;
import vogue.ethno.cbazaar.data.ApiClient;
import vogue.ethno.cbazaar.data.network.HeaderValues;
import vogue.ethno.cbazaar.data.network.NetworkCallback;

public class AddAddressPresenter extends AbstractBasePresenter<AddAddressListener> implements NetworkCallback {

    private AddAddressListener view;
    private int apiFrom = 0;
    private Context mContext;
    private ApiClient apiClient;
    private AddressRequestModel addressRequestModel;
    private RelationItem relationItemConfirm;
    private ConfirmCartRequest confirmCartRequest;

    @Override
    public void setView(AddAddressListener view) {
        super.setView(view);
        this.view = view;
        this.mContext = view.getActivityContext();
        this.apiClient = new ApiClient(this);
    }

    public void apiCall(int whichApi) {
        loadingDialog(true);
        this.apiFrom = whichApi;
        switch (whichApi) {
            case 1:
                apiClient.getAddress(whichApi, getCartAddressData());
                break;
            case 2:
                apiClient.addressOperation(whichApi, addressRequestModel);
                break;
            case 3:
                apiClient.addressOperation(whichApi, view.model());
                break;
            case 4:
                apiClient.confirmCart(whichApi, confirmCartRequest);
                break;
        }
    }


    public void deleteAddress(RelationItem relationItem) {
        addressRequestModel = new AddressRequestModel();
        addressRequestModel.setCommand(2);
        addressRequestModel.setRelationID(relationItem.getRelationID());
        addressRequestModel.setIsGuest(false);
        addressRequestModel.setAddress(null);
        apiCall(2);
    }
    private HashMap<String, Object> getCartAddressData() {
        HashMap<String, Object> hashMap = new HashMap<>(0);
        hashMap.put("ShippingCountryID", 250);
        hashMap.put("IsGuest", false);
        return hashMap;
    }

    @Override
    public void onSuccess(Object o, int api) {
        if (apiFrom != -1)
            loadingDialog(false);
        try {
            if (o != null) {
                switch (apiFrom) {
                    case -1:
                        accessToken(o);
                        break;
                    case 1:
                        addressDetails(o);
                        break;
                    case 2:
                        deleteAddressSuccess(o);
                        break;
                    case 3:
                        addAddressSuccess(o);
                        break;
                    case 4:
                        confirmCartResponse(o);
                        break;

                }
            } else failureAction(mContext.getString(R.string.some_thing_went_wrong));
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void confirmCartResponse(Object o) {
        try {
            ConfirmCartResponseModel cartResponseModel = (ConfirmCartResponseModel) o;
            view.cartResponseModel(cartResponseModel,confirmCartRequest.getShippingAddress());
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void addAddressSuccess(Object o) {
        try {
            AddAddressResponse addAddressResponse = (AddAddressResponse) o;
            view.addAddressResponse(addAddressResponse);
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void deleteAddressSuccess(Object o) {
        try {
            AddAddressResponse addAddressResponse = (AddAddressResponse) o;
            view.removeAddress(addAddressResponse.getRelationInfo());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addressDetails(Object o) {

        try {
            AddressResponse responseModel = (AddressResponse) o;
            if (responseModel != null) {
                view.successAddress(responseModel);
            }
        } catch (Exception e) {
            failureAction(mContext.getString(R.string.some_thing_went_wrong));
        }
    }

    private void accessToken(Object o) {
        try {
            TokenResponseModel responseModel = (TokenResponseModel) o;
            if (responseModel != null && !cutNull(responseModel.getAccessToken()).isEmpty() && !cutNull(responseModel.getTokenType()).isEmpty()) {
                HeaderValues.ACESS_TOKEN = responseModel.getAccessToken();
                HeaderValues.TOKEN_TYPE = responseModel.getTokenType();
                apiCall(apiFrom);
            } else {
                HeaderValues.ACESS_TOKEN = "";
                HeaderValues.TOKEN_TYPE = "";
                tokenError(o);
            }
        } catch (Exception e) {
            HeaderValues.TOKEN_TYPE = "";
            HeaderValues.ACESS_TOKEN = "";
            tokenError(o);
        }
    }

    @Override
    public void onFailure(String body) {
        failureAction(body);
    }

    @Override
    public void onErrorBody(Object o, int api) {
        loadingDialog(false);
        switch (apiFrom) {
            case -1:
                tokenError(o);
                break;
            default:
                addressError(o);
                break;
        }
    }

    private void addressError(Object o) {
        view.successAddress(null);    }


    private void loadingDialog(boolean status) {
        if (view != null) {
            if (status)
                view.showLoading();
            else view.hideLoading(status);
        }
    }

    private void failureAction(String s) {
        loadingDialog(false);
        if (view != null) {
            view.showError(s);
        }
    }

    private void tokenError(Object o) {
        loadingDialog(false);
        if (view != null)
            view.sessionError("");
    }

    private String cutNull(Object o) {
        return Utils.cutNull(o);
    }

    public void confirmCart(RelationItem relationDetais, RelationItem address, GetCartResponseModel model, AddressResponse addressResponse) {
        confirmCartRequest = new ConfirmCartRequest();
        confirmCartRequest.setIsShippingBillingSame(address == null);
        confirmCartRequest.setIsGuest("false");

        ShippingAddress shippingAddress = new ShippingAddress();
        shippingAddress.setRelationID(relationDetais.getRelationID());
        shippingAddress.setRelationTypeId(relationDetais.getRelationTypeId());
        shippingAddress.setRelationFirstName(relationDetais.getRelationFirstName());
        shippingAddress.setRelationLastName(relationDetais.getRelationLastName());
        shippingAddress.setAddress1(relationDetais.getAddress1());
        shippingAddress.setAddress2(relationDetais.getAddress2());
        shippingAddress.setCity(relationDetais.getCity());
        shippingAddress.setState(relationDetais.getState());
        shippingAddress.setRelationCountryId(relationDetais.getRelationCountryId());
        shippingAddress.setRelationCountryCode(relationDetais.getCountry());
        shippingAddress.setCountry(relationDetais.getCountry());
        shippingAddress.setZipcode(relationDetais.getZipcode());
        shippingAddress.setPhoneNumber(relationDetais.getPhoneNumber());
        shippingAddress.setCellNumber(relationDetais.getCellNumber());
        shippingAddress.setRelationEmailId(relationDetais.getRelationEmailId());
        shippingAddress.setHasDeletePermission(true);

        BillingAddress billingAddress = new BillingAddress();
        billingAddress.setRelationID(address==null?relationDetais.getRelationID():address.getRelationID());
        billingAddress.setRelationTypeId(address==null?relationDetais.getRelationTypeId():address.getRelationTypeId());
        billingAddress.setRelationFirstName(address==null?relationDetais.getRelationFirstName():address.getRelationFirstName());
        billingAddress.setRelationLastName(address==null?relationDetais.getRelationLastName():address.getRelationLastName());
        billingAddress.setAddress1(address==null?relationDetais.getAddress1():address.getAddress1());
        billingAddress.setAddress2(address==null?relationDetais.getAddress2():address.getAddress2());
        billingAddress.setCity(address==null?relationDetais.getCity():address.getCity());
        billingAddress.setState(address==null?relationDetais.getState():address.getState());
        billingAddress.setRelationCountryId(address==null?relationDetais.getRelationCountryId():address.getRelationCountryId());
        billingAddress.setRelationCountryCode(address==null?relationDetais.getCountry():address.getCountry());
        billingAddress.setCountry(address==null?relationDetais.getCountry():address.getCountry());
        billingAddress.setZipcode(address==null?relationDetais.getZipcode():address.getZipcode());
        billingAddress.setPhoneNumber(address==null?relationDetais.getPhoneNumber():address.getPhoneNumber());
        billingAddress.setCellNumber(address==null?relationDetais.getCellNumber():address.getCellNumber());
        billingAddress.setRelationEmailId(address==null?relationDetais.getRelationEmailId():address.getRelationEmailId());
        billingAddress.setHasDeletePermission(true);
        confirmCartRequest.setShippingAddress(shippingAddress);
        confirmCartRequest.setBillingAddress(billingAddress);
        if (addressResponse.getTimeZones() != null && addressResponse.getTimeZones() != null && addressResponse.getTimeZones().size() > 0)
            confirmCartRequest.setTimeZone(addressResponse.getTimeZones().get(0));
        final CartSummaries summaries = model.getCartSummaries();
        confirmCartRequest.setGrandTotal(String.valueOf(summaries.getGrandTotal()));
        confirmCartRequest.setGiftText("");
        confirmCartRequest.setFromTime("");
        confirmCartRequest.setToTime("");
        confirmCartRequest.setOccasion("");
        confirmCartRequest.setSpecialInstruction("");
        confirmCartRequest.setIs7DD("false");
        confirmCartRequest.setIsEarlyDispatch("false");
        confirmCartRequest.setIsReadyToShip("false");
        confirmCartRequest.setIsReadyToShip("false");
        List<AppliedPromoCodeDetailsItem> appliedPromoCodeDetailsItems = new ArrayList<>();
        List<CartDetailItem> cartDetailItems = model.getCartDetail();
        if (cartDetailItems != null && cartDetailItems.size() > 0) {
            for (CartDetailItem detailItem : cartDetailItems) {
                AppliedPromoCodeDetailsItem detailsItem = new AppliedPromoCodeDetailsItem();
                detailsItem.setPromotionCode("");
                detailsItem.setMessages("");
                detailsItem.setPromotionValue(0);
                detailsItem.setPromotionType("");
                detailsItem.setPromotionMinimumValue(0);
                detailsItem.setIsValid(false);
                detailsItem.setPromotionDiscountPercentage(detailItem.getProductDiscountPercentage());
                appliedPromoCodeDetailsItems.add(detailsItem);
            }
        }
        confirmCartRequest.setAppliedPromoCodeDetails(appliedPromoCodeDetailsItems);
        apiCall(4);
     }
}
