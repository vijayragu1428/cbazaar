package vogue.ethno.cbazaar.view.addaddress;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import vogue.ethno.cbazaar.R;
import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.classmodel.requestmodel.address.Address;
import vogue.ethno.cbazaar.classmodel.requestmodel.address.AddressRequestModel;
import vogue.ethno.cbazaar.classmodel.requestmodel.confirmcart.ShippingAddress;
import vogue.ethno.cbazaar.classmodel.responsemodel.addaddress.AddAddressResponse;
import vogue.ethno.cbazaar.classmodel.responsemodel.addaddress.RelationInfoItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart.ConfirmCartResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.getaddress.AddressResponse;
import vogue.ethno.cbazaar.classmodel.responsemodel.getaddress.RelationItem;
import vogue.ethno.cbazaar.classmodel.responsemodel.getaddress.ShippingCountryListItem;
import vogue.ethno.cbazaar.view.address.AddAddressListener;
import vogue.ethno.cbazaar.view.address.AddAddressModified;
import vogue.ethno.cbazaar.view.address.AddAddressPresenter;
import vogue.ethno.cbazaar.view.homescreen.FragmentListener;
import vogue.ethno.cbazaar.view.homescreen.HomeActivity;
import vogue.ethno.cbazaar.view.homescreen.fragment.home.HomeFragment;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")

public class AddAddressFragment extends Fragment implements AddAddressListener {
    @BindView(R.id.spin_type)
    Spinner spinType;

    @BindView(R.id.spin_country)
    Spinner spinCountry;
    @BindView(R.id.spin_state)
    Spinner spinState;
    @BindView(R.id.edt_first_name)
    EditText edtFirstName;
    @BindView(R.id.edt_last_name)
    EditText edtLastName;
    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_addres_line_one)
    EditText edtAddresLineOne;
    @BindView(R.id.edt_addres_line_two)
    EditText edtAddresLineTwo;
    @BindView(R.id.edt_code)
    EditText edtCode;
    @BindView(R.id.edt_city)
    EditText edtCity;
    @BindView(R.id.edt_county)
    EditText edtCounty;
    @BindView(R.id.edt_state)
    EditText edtState;
    @BindView(R.id.edt_mobile_number)
    EditText edtMobileNumber;
    @BindView(R.id.edt_alter_number)
    EditText edtAlterNumber;
    @BindView(R.id.lin_bottom)
    LinearLayout linBottom;
    @BindView(R.id.rel_root)
    RelativeLayout rel_root;



    Unbinder unbinder;
    private RelationItem relationItem;
    private AddressResponse addressResponse;
    private boolean isEditAddress;
    private AddAddressModified addAddressModified;

    public AddAddressFragment(AddressResponse addressResponse, RelationItem relationItem, AddAddressModified addressFragment) {
        this.relationItem = relationItem;
        this.addressResponse = addressResponse;
        this.addAddressModified = addressFragment;
    }

    private AddAddressPresenter presenter;
    private HomeActivity activity;
    private FragmentListener fragmentListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_new_address, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            fragmentListener = (FragmentListener) context;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (fragmentListener != null)
            fragmentListener = null;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        activity = (HomeActivity) getActivity();
        presenter = new AddAddressPresenter();
        presenter.setView(this);
        initRv();
        isEditAddress = false;
        if (relationItem != null)
            setData();
        else {
            setSpinerData();
            isEditAddress = false;
        }
    }

    private void setSpinerData() {
        if (addressResponse == null) {
            activity.showToast(getString(R.string.some_thing_went_wrong));
           // activity.onBackPressed();
            return;
        }
        if (addressResponse.getShippingStateList() != null && addressResponse.getShippingStateList().size() > 0) {
            ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, addressResponse.getShippingStateList());
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinState.setAdapter(stateAdapter);

        }
        if (addressResponse.getShippingCountryList() != null && addressResponse.getShippingCountryList().size() > 0) {
            List<String> list = new ArrayList<>();
            for (ShippingCountryListItem shippingCountryListItem : addressResponse.getShippingCountryList()) {
                list.add(shippingCountryListItem.getCountry());
            }
            ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, list);
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinCountry.setAdapter(stateAdapter);

        }
        if (addressResponse.getAddressType() != null && addressResponse.getAddressType().size() > 0) {
            ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, addressResponse.getAddressType());
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinType.setAdapter(stateAdapter);
        }
    }

    private void setData() {

        if (relationItem != null) {
            isEditAddress = true;
            edtFirstName.setText(relationItem.getRelationFirstName());
            edtLastName.setText(relationItem.getRelationLastName());
            edtEmail.setText(relationItem.getRelationEmailId());
            edtAddresLineOne.setText(relationItem.getAddress1());
            edtAddresLineTwo.setText(relationItem.getAddress2());
            edtCode.setText(relationItem.getZipcode());
            edtMobileNumber.setText(relationItem.getPhoneNumber());
            edtCity.setText(relationItem.getCity());
            if (addressResponse.getShippingStateList() != null && addressResponse.getShippingStateList().size() > 0) {
                ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, addressResponse.getShippingStateList());
                stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinState.setAdapter(stateAdapter);
                int selectPos = 0;
                for (String s : addressResponse.getShippingStateList()) {

                    if (s.equalsIgnoreCase(relationItem.getState()))
                        break;
                    selectPos++;
                }
                spinState.setSelection(selectPos);
            }
            if (addressResponse.getShippingCountryList() != null && addressResponse.getShippingCountryList().size() > 0) {
                List<String> list = new ArrayList<>();
                for (ShippingCountryListItem shippingCountryListItem : addressResponse.getShippingCountryList()) {
                    list.add(shippingCountryListItem.getCountry());
                }
                ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, list);
                stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinCountry.setAdapter(stateAdapter);
                int selectPos = 0;
                for (String s : list) {

                    if (s.equalsIgnoreCase(relationItem.getCountry()))
                        break;
                    selectPos++;
                }
                spinCountry.setSelection(selectPos);
            }
            if (addressResponse.getAddressType() != null && addressResponse.getAddressType().size() > 0) {
                ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, addressResponse.getAddressType());
                stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinType.setAdapter(stateAdapter);
                int selectPos = 0;
                for (String s : addressResponse.getAddressType()) {

                    if (s.equalsIgnoreCase(relationItem.getAddressType()))
                        break;
                    selectPos++;
                }
                spinType.setSelection(selectPos);
            }
        }

        // edtAlterNumber.setText(relationItem.get);
    }

    private void initRv() {

    }

    @Override
    public void onResume() {
        super.onResume();
        fragmentListener.setTitle(getString(R.string.add_address));//.toUpperCase());
    }

    @Override
    public void showLoading() {
        activity.showDialog("", false, rel_root);
    }

    @Override
    public void hideLoading(boolean status) {
        activity.hideDialog(status);
    }

    @Override
    public Context getActivityContext() {
        return getContext();
    }

    @Override
    public void showError(String string) {
        activity.showToast(string);
    }


    @Override
    public void sessionError(String string) {
        fragmentListener.nextFragment(new HomeFragment());
    }

    @Override
    public void successAddress(AddressResponse responseModel) {
        if (responseModel.getRelation() != null && responseModel.getRelation().size() > 0) {
            addAddressModified.isAddressModified(responseModel.getRelation());
            getActivity().onBackPressed();
        }
    }

    @Override
    public AddressRequestModel model() {
        AddressRequestModel addressRequestModel = new AddressRequestModel();
        if (!isEditAddress) {
            addressRequestModel.setRelationID(0);
            addressRequestModel.setCommand(1);
            addressRequestModel.setIsGuest(false);
        } else {
            addressRequestModel.setRelationID(relationItem.getRelationID());
            addressRequestModel.setCommand(3);
            addressRequestModel.setIsGuest(false);
        }
        Address address = new Address();
        if (!isEditAddress) {
            address.setRelationTypeId(0);
        } else {
            address.setRelationTypeId(relationItem.getRelationTypeId());
            address.setRelationID(relationItem.getRelationID());
        }

        address.setRelationFirstName(edtFirstName.getText().toString());
        address.setRelationLastName(edtFirstName.getText().toString());
        address.setAddress1(edtAddresLineOne.getText().toString());
        address.setAddress2(edtAddresLineTwo.getText().toString());
        address.setCity(edtCity.getText().toString());
        address.setState(spinState.getSelectedItem().toString());

        for (ShippingCountryListItem countryListItem : addressResponse.getShippingCountryList()) {
            if (countryListItem.getCountry().equalsIgnoreCase(spinCountry.getSelectedItem().toString())) {
                address.setRelationCountryId(countryListItem.getCountryId());
                address.setRelationCountryCode(countryListItem.getCountryCode());
                break;
            }
        }

        address.setCountry(spinCountry.getSelectedItem().toString());
        address.setZipcode(edtCode.getText().toString());
        address.setPhoneNumber(edtMobileNumber.getText().toString());
        address.setCellNumber(edtAlterNumber.getText().toString());
        address.setRelationEmailId(edtEmail.getText().toString());
        address.setTitle(spinType.getSelectedItem().toString());
        addressRequestModel.setAddress(address);
        return addressRequestModel;

    }

    @Override
    public void addAddressResponse(AddAddressResponse addAddressResponse) {
        if (addAddressResponse != null && addAddressResponse.getRelationInfo() != null && addAddressResponse.getRelationInfo().size() > 0) {
            addAddressModified.isAddressModified(null);
            getActivity().onBackPressed();
        }
    }

    @Override
    public void removeAddress(List<RelationInfoItem> relationInfo) {

    }

    @Override
    public void cartResponseModel(ConfirmCartResponseModel cartResponseModel, ShippingAddress shippingAddress) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.txt_cancel, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txt_cancel:
                getActivity().onBackPressed();
                break;
            case R.id.btn_submit:
                submitAction();
                break;
        }
    }

    private void submitAction() {
        if (validation())
            presenter.apiCall(3);
    }

        private boolean validation() {
            if (Utils.cutNull(edtFirstName.getText().toString()).isEmpty()) {
                showError(getString(R.string.please_enter_first_name));
                return false;
            }
            if (Utils.cutNull(edtLastName.getText().toString()).isEmpty()) {
                showError(getString(R.string.please_enter_last_name));
                return false;
            }
            if (Utils.cutNull(edtEmail.getText().toString()).isEmpty()) {
                showError(getString(R.string.please_enter_email));
                return false;
            }
            if (!Utils.isValidEmail(edtEmail.getText().toString())) {
                showError(getString(R.string.please_enter_valid_email));
                return false;
            }
            if (Utils.cutNull(edtAddresLineOne.getText().toString()).isEmpty()) {
                showError(getString(R.string.please_enter_address1));
                return false;
            }
            if (Utils.cutNull(edtAddresLineTwo.getText().toString()).isEmpty()) {
                showError(getString(R.string.please_enter_address2));
                return false;
            }
            if (Utils.cutNull(edtCode.getText().toString()).isEmpty()) {
                showError(getString(R.string.please_enter_post));
                return false;
            }
            if (edtCode.getText().toString().length() < 5) {
                showError(getString(R.string.please_zip_6));
                return false;
            }
            if (Utils.cutNull(edtCity.getText().toString()).isEmpty()) {
                showError(getString(R.string.please_enter_city));
                return false;
            }
            if (Utils.cutNull(edtMobileNumber.getText().toString()).isEmpty()) {
                showError(getString(R.string.please_enter_mobilet));
                return false;
            }
            if (edtMobileNumber.getText().toString().length() < 9) {
                showError(getString(R.string.please_enter_mobilet_10));
                return false;
            }
            if (!Utils.cutNull(edtMobileNumber.getText().toString()).isEmpty() && edtAlterNumber.getText().toString().length() > 0) {
                if (edtAlterNumber.getText().toString().length() < 9) {
                    showError(getString(R.string.please_enter_mobilet_10));
                    return false;
                }
            }
            return true;
        }
}
