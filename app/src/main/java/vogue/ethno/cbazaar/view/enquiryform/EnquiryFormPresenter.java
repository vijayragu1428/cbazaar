package vogue.ethno.cbazaar.view.enquiryform;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import vogue.ethno.cbazaar.apputils.Utils;
import vogue.ethno.cbazaar.base.AbstractBasePresenter;
import vogue.ethno.cbazaar.classmodel.responsemodel.accesstoken.TokenResponseModel;
import vogue.ethno.cbazaar.classmodel.responsemodel.getstates.StateResponseModel;
import vogue.ethno.cbazaar.data.ApiClient;
import vogue.ethno.cbazaar.data.localdb.pref.AppPreference;
import vogue.ethno.cbazaar.data.network.HeaderValues;
import vogue.ethno.cbazaar.data.network.NetworkCallback;
import vogue.ethno.cbazaar.view.productdetails.ProductDetailsListener;

public class EnquiryFormPresenter extends AbstractBasePresenter<EnquiryFormListener> implements NetworkCallback {

    private EnquiryFormListener views;
    private int apiFrom;
    private Context mContext;
    private ApiClient apiClient;

    @Override
    public void setView(EnquiryFormListener view) {
        super.setView(view);
        this.views = view;
        this.mContext = view.getActivityContext();
        this.apiClient = new ApiClient(this);

    }

    public void apiCall(int item) {
        loadingDialog(true);
        apiFrom = item;
        switch (item) {
            case 1:
                apiClient.submitEnquiry(item, views.getSubmitData());
                break;
            case 2:
                apiClient.getStateList(item);
                break;
        }

    }

    @Override
    public void onSuccess(Object o, int api) {
        loadingDialog(false);

        if (apiFrom != -1)
            loadingDialog(false);
        try {
            if (o != null) {
                switch (apiFrom) {
                    case -1:
                        accessToken(o);
                        break;
                    case 1:
                        enquiryFormResponse(o);
                        break;
                    case 2:
                        stateListResponse(o);
                        break;

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void enquiryFormResponse(Object o) {
        if (o instanceof JsonObject) {
            try {
                onFailure(((JsonObject) o).get("Status").toString());
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(String body) {
        loadingDialog(false);

        views.showError(body);
    }

    @Override
    public void onErrorBody(Object o, int api) {
        loadingDialog(false);
        switch (apiFrom) {
            case -1:
                tokenError(o);
                break;

        }
    }

    private void loadingDialog(boolean status) {
        if (views != null) {
            if (status)
                views.showLoading();
            else views.hideLoading(status);
        }
    }

    private void tokenError(Object o) {
        loadingDialog(false);
        if (views != null)
            views.sessionError("");
    }

    private void accessToken(Object o) {
        try {
            TokenResponseModel responseModel = (TokenResponseModel) o;
            if (responseModel != null && !Utils.cutNull(responseModel.getAccessToken()).isEmpty() && !Utils.cutNull(responseModel.getTokenType()).isEmpty()) {
                HeaderValues.ACESS_TOKEN = responseModel.getAccessToken();
                HeaderValues.TOKEN_TYPE = responseModel.getTokenType();
                apiCall(apiFrom);
            } else {
                HeaderValues.ACESS_TOKEN = "";
                HeaderValues.TOKEN_TYPE = "";
                tokenError(o);
            }
        } catch (Exception e) {
            HeaderValues.TOKEN_TYPE = "";
            HeaderValues.ACESS_TOKEN = "";
            tokenError(o);
        }
    }

    private void stateListResponse(Object o) {
        try {
            JsonArray array = (JsonArray) o;
            ArrayList<StateResponseModel> stateList = new ArrayList<StateResponseModel>();
            for (int i = 0; i < array.size(); i++) {
                JsonObject obj = array.get(i).getAsJsonObject();
                StateResponseModel stateObj= new StateResponseModel();
                stateObj.setStateId(Integer.parseInt(String.valueOf(obj.get("StateId"))));
                stateObj.setStateName(String.valueOf(obj.get("StateName")));
                stateObj.setIsCODEligible(Boolean.parseBoolean(String.valueOf(obj.get("IsCODEligible"))));
                stateList.add(stateObj);
            }
            views.stateList(stateList);
        } catch (JsonIOException e) {
            e.printStackTrace();
        }

    }
}
