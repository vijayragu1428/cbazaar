package vogue.ethno.cbazaar.classmodel.responsemodel.getaddress;

 import com.google.gson.annotations.SerializedName;

 public class BillingCountryListItem{

	@SerializedName("CountryId")
	private int countryId;

	@SerializedName("Alpha3CountryCode")
	private String alpha3CountryCode;

	@SerializedName("Region")
	private String region;

	@SerializedName("Country")
	private String country;

	@SerializedName("CountryCode")
	private String countryCode;

	@SerializedName("EStoreID")
	private int eStoreID;

	public void setCountryId(int countryId){
		this.countryId = countryId;
	}

	public int getCountryId(){
		return countryId;
	}

	public void setAlpha3CountryCode(String alpha3CountryCode){
		this.alpha3CountryCode = alpha3CountryCode;
	}

	public String getAlpha3CountryCode(){
		return alpha3CountryCode;
	}

	public void setRegion(String region){
		this.region = region;
	}

	public String getRegion(){
		return region;
	}

	public void setCountry(String country){
		this.country = country;
	}

	public String getCountry(){
		return country;
	}

	public void setCountryCode(String countryCode){
		this.countryCode = countryCode;
	}

	public String getCountryCode(){
		return countryCode;
	}

	public void setEStoreID(int eStoreID){
		this.eStoreID = eStoreID;
	}

	public int getEStoreID(){
		return eStoreID;
	}
}