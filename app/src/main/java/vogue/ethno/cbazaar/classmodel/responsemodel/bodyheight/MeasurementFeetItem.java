package vogue.ethno.cbazaar.classmodel.responsemodel.bodyheight;

 import com.google.gson.annotations.SerializedName;

 public class MeasurementFeetItem{

	@SerializedName("Size")
	private String size;

	@SerializedName("Value")
	private String value;

	@SerializedName("Text")
	private String text;

	public void setSize(String size){
		this.size = size;
	}

	public String getSize(){
		return size;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}
}