package vogue.ethno.cbazaar.classmodel.responsemodel.userlogin;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class UserLoginResponse{

	@SerializedName("Status")
	private String status;

	@SerializedName("IsRegisteredWithPassword")
	private boolean isRegisteredWithPassword;

	@SerializedName("Message")
	private String message;

	@SerializedName("TrackNumbers")
	private List<Object> trackNumbers;

	@SerializedName("ID")
	private int iD;

	@SerializedName("RfmValue")
	private String rfmValue;

	@SerializedName("IsGuideShopUser")
	private boolean isGuideShopUser;

	@SerializedName("Name")
	private String name;

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setIsRegisteredWithPassword(boolean isRegisteredWithPassword){
		this.isRegisteredWithPassword = isRegisteredWithPassword;
	}

	public boolean isIsRegisteredWithPassword(){
		return isRegisteredWithPassword;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setTrackNumbers(List<Object> trackNumbers){
		this.trackNumbers = trackNumbers;
	}

	public List<Object> getTrackNumbers(){
		return trackNumbers;
	}

	public void setID(int iD){
		this.iD = iD;
	}

	public int getID(){
		return iD;
	}

	public void setRfmValue(String rfmValue){
		this.rfmValue = rfmValue;
	}

	public String getRfmValue(){
		return rfmValue;
	}

	public void setIsGuideShopUser(boolean isGuideShopUser){
		this.isGuideShopUser = isGuideShopUser;
	}

	public boolean isIsGuideShopUser(){
		return isGuideShopUser;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}
}