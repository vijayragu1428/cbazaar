package vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart;


import com.google.gson.annotations.SerializedName;


public class PaymentGatewayItem{

	@SerializedName("ShowStoreCreditLoginMessage")
	private boolean showStoreCreditLoginMessage;

	@SerializedName("IsEnabled")
	private boolean isEnabled;

	@SerializedName("PaymentGateway")
	private int paymentGateway;

	@SerializedName("DisplayName")
	private String displayName;

	@SerializedName("Index")
	private int index;

	@SerializedName("CallPaymentGatewaySelectionApi")
	private boolean callPaymentGatewaySelectionApi;

	@SerializedName("OfflineMessage")
	private Object offlineMessage;

	public void setShowStoreCreditLoginMessage(boolean showStoreCreditLoginMessage){
		this.showStoreCreditLoginMessage = showStoreCreditLoginMessage;
	}

	public boolean isShowStoreCreditLoginMessage(){
		return showStoreCreditLoginMessage;
	}

	public void setIsEnabled(boolean isEnabled){
		this.isEnabled = isEnabled;
	}

	public boolean isIsEnabled(){
		return isEnabled;
	}

	public void setPaymentGateway(int paymentGateway){
		this.paymentGateway = paymentGateway;
	}

	public int getPaymentGateway(){
		return paymentGateway;
	}

	public void setDisplayName(String displayName){
		this.displayName = displayName;
	}

	public String getDisplayName(){
		return displayName;
	}

	public void setIndex(int index){
		this.index = index;
	}

	public int getIndex(){
		return index;
	}

	public void setCallPaymentGatewaySelectionApi(boolean callPaymentGatewaySelectionApi){
		this.callPaymentGatewaySelectionApi = callPaymentGatewaySelectionApi;
	}

	public boolean isCallPaymentGatewaySelectionApi(){
		return callPaymentGatewaySelectionApi;
	}

	public void setOfflineMessage(Object offlineMessage){
		this.offlineMessage = offlineMessage;
	}

	public Object getOfflineMessage(){
		return offlineMessage;
	}
}