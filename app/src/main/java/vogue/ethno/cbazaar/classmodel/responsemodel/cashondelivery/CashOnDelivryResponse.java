package vogue.ethno.cbazaar.classmodel.responsemodel.cashondelivery;

import java.util.List;
 import com.google.gson.annotations.SerializedName;

 public class 	CashOnDelivryResponse{

	@SerializedName("IsCartValid")
	private boolean isCartValid;

	@SerializedName("IsSuccess")
	private boolean isSuccess;

	@SerializedName("Messages")
	private List<Object> messages;

	@SerializedName("StoreCreditPopupMessage")
	private Object storeCreditPopupMessage;

	@SerializedName("Token")
	private Object token;

	@SerializedName("PayableAmount")
	private double payableAmount;

	@SerializedName("Url")
	private Object url;

	@SerializedName("Is3DSecureActive")
	private boolean is3DSecureActive;

	@SerializedName("PromoCodeExpiredMessage")
	private String promoCodeExpiredMessage;

	@SerializedName("IsCartItemsEmpty")
	private boolean isCartItemsEmpty;

	@SerializedName("PaymentGateway")
	private int paymentGateway;

	@SerializedName("OTPMobileNumer")
	private Object oTPMobileNumer;

	@SerializedName("UsedStoreCreditAmount")
	private double usedStoreCreditAmount;

	@SerializedName("ExpireMessage")
	private Object expireMessage;

	public void setIsCartValid(boolean isCartValid){
		this.isCartValid = isCartValid;
	}

	public boolean isIsCartValid(){
		return isCartValid;
	}

	public void setIsSuccess(boolean isSuccess){
		this.isSuccess = isSuccess;
	}

	public boolean isIsSuccess(){
		return isSuccess;
	}

	public void setMessages(List<Object> messages){
		this.messages = messages;
	}

	public List<Object> getMessages(){
		return messages;
	}

	public void setStoreCreditPopupMessage(Object storeCreditPopupMessage){
		this.storeCreditPopupMessage = storeCreditPopupMessage;
	}

	public Object getStoreCreditPopupMessage(){
		return storeCreditPopupMessage;
	}

	public void setToken(Object token){
		this.token = token;
	}

	public Object getToken(){
		return token;
	}

	public void setPayableAmount(double payableAmount){
		this.payableAmount = payableAmount;
	}

	public double getPayableAmount(){
		return payableAmount;
	}

	public void setUrl(Object url){
		this.url = url;
	}

	public Object getUrl(){
		return url;
	}

	public void setIs3DSecureActive(boolean is3DSecureActive){
		this.is3DSecureActive = is3DSecureActive;
	}

	public boolean isIs3DSecureActive(){
		return is3DSecureActive;
	}

	public void setPromoCodeExpiredMessage(String promoCodeExpiredMessage){
		this.promoCodeExpiredMessage = promoCodeExpiredMessage;
	}

	public String getPromoCodeExpiredMessage(){
		return promoCodeExpiredMessage;
	}

	public void setIsCartItemsEmpty(boolean isCartItemsEmpty){
		this.isCartItemsEmpty = isCartItemsEmpty;
	}

	public boolean isIsCartItemsEmpty(){
		return isCartItemsEmpty;
	}

	public void setPaymentGateway(int paymentGateway){
		this.paymentGateway = paymentGateway;
	}

	public int getPaymentGateway(){
		return paymentGateway;
	}

	public void setOTPMobileNumer(Object oTPMobileNumer){
		this.oTPMobileNumer = oTPMobileNumer;
	}

	public Object getOTPMobileNumer(){
		return oTPMobileNumer;
	}

	public void setUsedStoreCreditAmount(double usedStoreCreditAmount){
		this.usedStoreCreditAmount = usedStoreCreditAmount;
	}

	public double getUsedStoreCreditAmount(){
		return usedStoreCreditAmount;
	}

	public void setExpireMessage(Object expireMessage){
		this.expireMessage = expireMessage;
	}

	public Object getExpireMessage(){
		return expireMessage;
	}
}