package vogue.ethno.cbazaar.classmodel.requestmodel.addtocart;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddToCartRequestModel {

    @SerializedName("ProductSize")
    private String productSize;

    @SerializedName("MeasurementFlag")
    private String measurementFlag;

    @SerializedName("IsProductStitched")
    private int isProductStitched;

    @SerializedName("KameezStyle")
    private List<Object> kameezStyle;

    @SerializedName("ProductCode")
    private String productCode;

    @SerializedName("Customization")
    private String customization;

    @SerializedName("SupplementaryProductCodes")
    private String supplementaryProductCodes;

    @SerializedName("Quantity")
    private int quantity;

    @SerializedName("BodyHeight")
    private String bodyHeight;

    @SerializedName("NaturalWaist")
    private String naturalWaist;

    @SerializedName("WaistCircumference")
    private String waistCircumference;

    @SerializedName("WaistToFloor")
    private String waistToFloor;

    public String getBodyHeight() {
        return bodyHeight;
    }

    public void setBodyHeight(String bodyHeight) {
        this.bodyHeight = bodyHeight;
    }

    public String getNaturalWaist() {
        return naturalWaist;
    }

    public void setNaturalWaist(String naturalWaist) {
        this.naturalWaist = naturalWaist;
    }

    public String getWaistCircumference() {
        return waistCircumference;
    }

    public void setWaistCircumference(String waistCircumference) {
        this.waistCircumference = waistCircumference;
    }

    public String getWaistToFloor() {
        return waistToFloor;
    }

    public void setWaistToFloor(String waistToFloor) {
        this.waistToFloor = waistToFloor;
    }

    public void setProductSize(String productSize) {
        this.productSize = productSize;
    }

    public String getProductSize() {
        return productSize;
    }

    public void setMeasurementFlag(String measurementFlag) {
        this.measurementFlag = measurementFlag;
    }

    public String getMeasurementFlag() {
        return measurementFlag;
    }

    public void setIsProductStitched(int isProductStitched) {
        this.isProductStitched = isProductStitched;
    }

    public int getIsProductStitched() {
        return isProductStitched;
    }

    public void setKameezStyle(List<Object> kameezStyle) {
        this.kameezStyle = kameezStyle;
    }

    public List<Object> getKameezStyle() {
        return kameezStyle;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setCustomization(String customization) {
        this.customization = customization;
    }

    public String getCustomization() {
        return customization;
    }

    public void setSupplementaryProductCodes(String supplementaryProductCodes) {
        this.supplementaryProductCodes = supplementaryProductCodes;
    }

    public String getSupplementaryProductCodes() {
        return supplementaryProductCodes;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }
}