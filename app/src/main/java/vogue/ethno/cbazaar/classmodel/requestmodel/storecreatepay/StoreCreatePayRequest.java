package vogue.ethno.cbazaar.classmodel.requestmodel.storecreatepay;

import java.util.List;
 import com.google.gson.annotations.SerializedName;

 public class StoreCreatePayRequest {

	@SerializedName("IsUsedCredit")
	private String isUsedCredit;

	@SerializedName("StoreCreditDetail")
	private List<StoreCreditDetailItem> storeCreditDetail;

	@SerializedName("GrandTotal")
	private int grandTotal;

	@SerializedName("TrackNumber")
	private int trackNumber;

	@SerializedName("TotalStoreCredited")
	private int totalStoreCredited;

	public void setIsUsedCredit(String isUsedCredit){
		this.isUsedCredit = isUsedCredit;
	}

	public String getIsUsedCredit(){
		return isUsedCredit;
	}

	public void setStoreCreditDetail(List<StoreCreditDetailItem> storeCreditDetail){
		this.storeCreditDetail = storeCreditDetail;
	}

	public List<StoreCreditDetailItem> getStoreCreditDetail(){
		return storeCreditDetail;
	}

	public void setGrandTotal(int grandTotal){
		this.grandTotal = grandTotal;
	}

	public int getGrandTotal(){
		return grandTotal;
	}

	public void setTrackNumber(int trackNumber){
		this.trackNumber = trackNumber;
	}

	public int getTrackNumber(){
		return trackNumber;
	}

	public void setTotalStoreCredited(int totalStoreCredited){
		this.totalStoreCredited = totalStoreCredited;
	}

	public int getTotalStoreCredited(){
		return totalStoreCredited;
	}
}