package vogue.ethno.cbazaar.classmodel.responsemodel.productdetails;

import java.io.Serializable;
import java.util.List;
 import com.google.gson.annotations.SerializedName;

 public class CommonDetail implements Serializable {

	@SerializedName("OutOfStockImageFabricsPatten")
	private String outOfStockImageFabricsPatten;

	@SerializedName("IsQuantity")
	private boolean isQuantity;

	@SerializedName("SupperClassUrl")
	private Object supperClassUrl;

	@SerializedName("IsReadyToShip")
	private boolean isReadyToShip;

	@SerializedName("Size")
	private String size;

	@SerializedName("BodyHeight")
	private List<Object> bodyHeight;

	@SerializedName("SizeGuideLink")
	private String sizeGuideLink;

	@SerializedName("InterLink")
	private String interLink;

	@SerializedName("MaximumKameezLength")
	private String maximumKameezLength;

	@SerializedName("DeliveryDateNote")
	private String deliveryDateNote;

	@SerializedName("ProductOffer")
	private String productOffer;

	@SerializedName("IsUnstitchedDefaultSelection")
	private boolean isUnstitchedDefaultSelection;

	@SerializedName("ShippingMode")
	private String shippingMode;

	@SerializedName("MegaOfferProductQuantity")
	private int megaOfferProductQuantity;

	@SerializedName("ShortListLink")
	private String shortListLink;

	@SerializedName("ShowCustomCost")
	private String showCustomCost;

	@SerializedName("OutOfStockMessage")
	private Object outOfStockMessage;

	@SerializedName("IsShortListedFlag")
	private boolean isShortListedFlag;

	@SerializedName("ShortListCount")
	private int shortListCount;

	@SerializedName("ShortJacketBlouseLength")
	private Object shortJacketBlouseLength;

	@SerializedName("DispatchDate")
	private String dispatchDate;

	@SerializedName("IsShowWaistCircumference")
	private boolean isShowWaistCircumference;

	@SerializedName("OfferMessage")
	private Object offerMessage;

	@SerializedName("IsBodyHeight")
	private boolean isBodyHeight;

	@SerializedName("DiscountPrice")
	private String discountPrice;

	@SerializedName("ShippingCost")
	private String shippingCost;

	@SerializedName("ProductStyle")
	private String productStyle;

	@SerializedName("FreeGiftCode")
	private String freeGiftCode;

	@SerializedName("LongJacketBustSize")
	private Object longJacketBustSize;

	@SerializedName("CommercialMessage")
	private Object commercialMessage;

	@SerializedName("CaptionName")
	private String captionName;

	@SerializedName("OutOfStockImage")
	private Object outOfStockImage;

	@SerializedName("UnstitchedNote")
	private String unstitchedNote;

	@SerializedName("IsCommercial")
	private boolean isCommercial;

	@SerializedName("LongJacketBlouseLength")
	private Object longJacketBlouseLength;

	@SerializedName("AlluringDupionSilkWinterBlouseSize")
	private Object alluringDupionSilkWinterBlouseSize;

	@SerializedName("SleeveType")
	private String sleeveType;

	@SerializedName("IsStrikeOutAmount")
	private boolean isStrikeOutAmount;

	@SerializedName("Is7DD")
	private int is7DD;

	@SerializedName("IsShowUnstitched")
	private boolean isShowUnstitched;

	@SerializedName("IsShowNaturalWaist")
	private boolean isShowNaturalWaist;

	@SerializedName("RawSilkBlouseColors")
	private Object rawSilkBlouseColors;

	@SerializedName("IsShipping")
	private boolean isShipping;

	@SerializedName("CustomReadysizeActive")
	private boolean customReadysizeActive;

	@SerializedName("CashBackAmount")
	private int cashBackAmount;

	@SerializedName("ShortJacketBustSize")
	private Object shortJacketBustSize;

	@SerializedName("IsKameezStyle")
	private boolean isKameezStyle;

	@SerializedName("IsSelectedQuantity")
	private int isSelectedQuantity;

	@SerializedName("IsSize")
	private boolean isSize;

	@SerializedName("SizeGuide")
	private String sizeGuide;

	@SerializedName("ExpressDeliveryNote")
	private Object expressDeliveryNote;

	@SerializedName("Quantity")
	private List<String> quantity;

	@SerializedName("TotalAmount")
	private String totalAmount;

	@SerializedName("CommercialDescription")
	private Object commercialDescription;

	@SerializedName("IsAccessories")
	private boolean isAccessories;

	@SerializedName("IsShowWaistToFloor")
	private boolean isShowWaistToFloor;

	@SerializedName("ProductSize")
	private List<ProductSizeItem> productSize;

	@SerializedName("ExtendedMessage")
	private String extendedMessage;

	@SerializedName("IsProductStitched")
	private boolean isProductStitched;

	@SerializedName("DiscountPercentage")
	private String discountPercentage;

	@SerializedName("IsUnstitched")
	private boolean isUnstitched;

	@SerializedName("BodyStyle")
	private List<Object> bodyStyle;

	@SerializedName("VatMessage")
	private Object vatMessage;

	public void setOutOfStockImageFabricsPatten(String outOfStockImageFabricsPatten){
		this.outOfStockImageFabricsPatten = outOfStockImageFabricsPatten;
	}

	public String getOutOfStockImageFabricsPatten(){
		return outOfStockImageFabricsPatten;
	}

	public void setIsQuantity(boolean isQuantity){
		this.isQuantity = isQuantity;
	}

	public boolean isIsQuantity(){
		return isQuantity;
	}

	public void setSupperClassUrl(Object supperClassUrl){
		this.supperClassUrl = supperClassUrl;
	}

	public Object getSupperClassUrl(){
		return supperClassUrl;
	}

	public void setIsReadyToShip(boolean isReadyToShip){
		this.isReadyToShip = isReadyToShip;
	}

	public boolean isIsReadyToShip(){
		return isReadyToShip;
	}

	public void setSize(String size){
		this.size = size;
	}

	public String getSize(){
		return size;
	}

	public void setBodyHeight(List<Object> bodyHeight){
		this.bodyHeight = bodyHeight;
	}

	public List<Object> getBodyHeight(){
		return bodyHeight;
	}

	public void setSizeGuideLink(String sizeGuideLink){
		this.sizeGuideLink = sizeGuideLink;
	}

	public String getSizeGuideLink(){
		return sizeGuideLink;
	}

	public void setInterLink(String interLink){
		this.interLink = interLink;
	}

	public String getInterLink(){
		return interLink;
	}

	public void setMaximumKameezLength(String maximumKameezLength){
		this.maximumKameezLength = maximumKameezLength;
	}

	public String getMaximumKameezLength(){
		return maximumKameezLength;
	}

	public void setDeliveryDateNote(String deliveryDateNote){
		this.deliveryDateNote = deliveryDateNote;
	}

	public String getDeliveryDateNote(){
		return deliveryDateNote;
	}

	public void setProductOffer(String productOffer){
		this.productOffer = productOffer;
	}

	public String getProductOffer(){
		return productOffer;
	}

	public void setIsUnstitchedDefaultSelection(boolean isUnstitchedDefaultSelection){
		this.isUnstitchedDefaultSelection = isUnstitchedDefaultSelection;
	}

	public boolean isIsUnstitchedDefaultSelection(){
		return isUnstitchedDefaultSelection;
	}

	public void setShippingMode(String shippingMode){
		this.shippingMode = shippingMode;
	}

	public String getShippingMode(){
		return shippingMode;
	}

	public void setMegaOfferProductQuantity(int megaOfferProductQuantity){
		this.megaOfferProductQuantity = megaOfferProductQuantity;
	}

	public int getMegaOfferProductQuantity(){
		return megaOfferProductQuantity;
	}

	public void setShortListLink(String shortListLink){
		this.shortListLink = shortListLink;
	}

	public String getShortListLink(){
		return shortListLink;
	}

	public void setShowCustomCost(String showCustomCost){
		this.showCustomCost = showCustomCost;
	}

	public String getShowCustomCost(){
		return showCustomCost;
	}

	public void setOutOfStockMessage(Object outOfStockMessage){
		this.outOfStockMessage = outOfStockMessage;
	}

	public Object getOutOfStockMessage(){
		return outOfStockMessage;
	}

	public void setIsShortListedFlag(boolean isShortListedFlag){
		this.isShortListedFlag = isShortListedFlag;
	}

	public boolean isIsShortListedFlag(){
		return isShortListedFlag;
	}

	public void setShortListCount(int shortListCount){
		this.shortListCount = shortListCount;
	}

	public int getShortListCount(){
		return shortListCount;
	}

	public void setShortJacketBlouseLength(Object shortJacketBlouseLength){
		this.shortJacketBlouseLength = shortJacketBlouseLength;
	}

	public Object getShortJacketBlouseLength(){
		return shortJacketBlouseLength;
	}

	public void setDispatchDate(String dispatchDate){
		this.dispatchDate = dispatchDate;
	}

	public String getDispatchDate(){
		return dispatchDate;
	}

	public void setIsShowWaistCircumference(boolean isShowWaistCircumference){
		this.isShowWaistCircumference = isShowWaistCircumference;
	}

	public boolean isIsShowWaistCircumference(){
		return isShowWaistCircumference;
	}

	public void setOfferMessage(Object offerMessage){
		this.offerMessage = offerMessage;
	}

	public Object getOfferMessage(){
		return offerMessage;
	}

	public void setIsBodyHeight(boolean isBodyHeight){
		this.isBodyHeight = isBodyHeight;
	}

	public boolean isIsBodyHeight(){
		return isBodyHeight;
	}

	public void setDiscountPrice(String discountPrice){
		this.discountPrice = discountPrice;
	}

	public String getDiscountPrice(){
		return discountPrice;
	}

	public void setShippingCost(String shippingCost){
		this.shippingCost = shippingCost;
	}

	public String getShippingCost(){
		return shippingCost;
	}

	public void setProductStyle(String productStyle){
		this.productStyle = productStyle;
	}

	public String getProductStyle(){
		return productStyle;
	}

	public void setFreeGiftCode(String freeGiftCode){
		this.freeGiftCode = freeGiftCode;
	}

	public String getFreeGiftCode(){
		return freeGiftCode;
	}

	public void setLongJacketBustSize(Object longJacketBustSize){
		this.longJacketBustSize = longJacketBustSize;
	}

	public Object getLongJacketBustSize(){
		return longJacketBustSize;
	}

	public void setCommercialMessage(Object commercialMessage){
		this.commercialMessage = commercialMessage;
	}

	public Object getCommercialMessage(){
		return commercialMessage;
	}

	public void setCaptionName(String captionName){
		this.captionName = captionName;
	}

	public String getCaptionName(){
		return captionName;
	}

	public void setOutOfStockImage(Object outOfStockImage){
		this.outOfStockImage = outOfStockImage;
	}

	public Object getOutOfStockImage(){
		return outOfStockImage;
	}

	public void setUnstitchedNote(String unstitchedNote){
		this.unstitchedNote = unstitchedNote;
	}

	public String getUnstitchedNote(){
		return unstitchedNote;
	}

	public void setIsCommercial(boolean isCommercial){
		this.isCommercial = isCommercial;
	}

	public boolean isIsCommercial(){
		return isCommercial;
	}

	public void setLongJacketBlouseLength(Object longJacketBlouseLength){
		this.longJacketBlouseLength = longJacketBlouseLength;
	}

	public Object getLongJacketBlouseLength(){
		return longJacketBlouseLength;
	}

	public void setAlluringDupionSilkWinterBlouseSize(Object alluringDupionSilkWinterBlouseSize){
		this.alluringDupionSilkWinterBlouseSize = alluringDupionSilkWinterBlouseSize;
	}

	public Object getAlluringDupionSilkWinterBlouseSize(){
		return alluringDupionSilkWinterBlouseSize;
	}

	public void setSleeveType(String sleeveType){
		this.sleeveType = sleeveType;
	}

	public String getSleeveType(){
		return sleeveType;
	}

	public void setIsStrikeOutAmount(boolean isStrikeOutAmount){
		this.isStrikeOutAmount = isStrikeOutAmount;
	}

	public boolean isIsStrikeOutAmount(){
		return isStrikeOutAmount;
	}

	public void setIs7DD(int is7DD){
		this.is7DD = is7DD;
	}

	public int getIs7DD(){
		return is7DD;
	}

	public void setIsShowUnstitched(boolean isShowUnstitched){
		this.isShowUnstitched = isShowUnstitched;
	}

	public boolean isIsShowUnstitched(){
		return isShowUnstitched;
	}

	public void setIsShowNaturalWaist(boolean isShowNaturalWaist){
		this.isShowNaturalWaist = isShowNaturalWaist;
	}

	public boolean isIsShowNaturalWaist(){
		return isShowNaturalWaist;
	}

	public void setRawSilkBlouseColors(Object rawSilkBlouseColors){
		this.rawSilkBlouseColors = rawSilkBlouseColors;
	}

	public Object getRawSilkBlouseColors(){
		return rawSilkBlouseColors;
	}

	public void setIsShipping(boolean isShipping){
		this.isShipping = isShipping;
	}

	public boolean isIsShipping(){
		return isShipping;
	}

	public void setCustomReadysizeActive(boolean customReadysizeActive){
		this.customReadysizeActive = customReadysizeActive;
	}

	public boolean isCustomReadysizeActive(){
		return customReadysizeActive;
	}

	public void setCashBackAmount(int cashBackAmount){
		this.cashBackAmount = cashBackAmount;
	}

	public int getCashBackAmount(){
		return cashBackAmount;
	}

	public void setShortJacketBustSize(Object shortJacketBustSize){
		this.shortJacketBustSize = shortJacketBustSize;
	}

	public Object getShortJacketBustSize(){
		return shortJacketBustSize;
	}

	public void setIsKameezStyle(boolean isKameezStyle){
		this.isKameezStyle = isKameezStyle;
	}

	public boolean isIsKameezStyle(){
		return isKameezStyle;
	}

	public void setIsSelectedQuantity(int isSelectedQuantity){
		this.isSelectedQuantity = isSelectedQuantity;
	}

	public int getIsSelectedQuantity(){
		return isSelectedQuantity;
	}

	public void setIsSize(boolean isSize){
		this.isSize = isSize;
	}

	public boolean isIsSize(){
		return isSize;
	}

	public void setSizeGuide(String sizeGuide){
		this.sizeGuide = sizeGuide;
	}

	public String getSizeGuide(){
		return sizeGuide;
	}

	public void setExpressDeliveryNote(Object expressDeliveryNote){
		this.expressDeliveryNote = expressDeliveryNote;
	}

	public Object getExpressDeliveryNote(){
		return expressDeliveryNote;
	}

	public void setQuantity(List<String> quantity){
		this.quantity = quantity;
	}

	public List<String> getQuantity(){
		return quantity;
	}

	public void setTotalAmount(String totalAmount){
		this.totalAmount = totalAmount;
	}

	public String getTotalAmount(){
		return totalAmount;
	}

	public void setCommercialDescription(Object commercialDescription){
		this.commercialDescription = commercialDescription;
	}

	public Object getCommercialDescription(){
		return commercialDescription;
	}

	public void setIsAccessories(boolean isAccessories){
		this.isAccessories = isAccessories;
	}

	public boolean isIsAccessories(){
		return isAccessories;
	}

	public void setIsShowWaistToFloor(boolean isShowWaistToFloor){
		this.isShowWaistToFloor = isShowWaistToFloor;
	}

	public boolean isIsShowWaistToFloor(){
		return isShowWaistToFloor;
	}

	public void setProductSize(List<ProductSizeItem> productSize){
		this.productSize = productSize;
	}

	public List<ProductSizeItem> getProductSize(){
		return productSize;
	}

	public void setExtendedMessage(String extendedMessage){
		this.extendedMessage = extendedMessage;
	}

	public String getExtendedMessage(){
		return extendedMessage;
	}

	public void setIsProductStitched(boolean isProductStitched){
		this.isProductStitched = isProductStitched;
	}

	public boolean isIsProductStitched(){
		return isProductStitched;
	}

	public void setDiscountPercentage(String discountPercentage){
		this.discountPercentage = discountPercentage;
	}

	public String getDiscountPercentage(){
		return discountPercentage;
	}

	public void setIsUnstitched(boolean isUnstitched){
		this.isUnstitched = isUnstitched;
	}

	public boolean isIsUnstitched(){
		return isUnstitched;
	}

	public void setBodyStyle(List<Object> bodyStyle){
		this.bodyStyle = bodyStyle;
	}

	public List<Object> getBodyStyle(){
		return bodyStyle;
	}

	public void setVatMessage(Object vatMessage){
		this.vatMessage = vatMessage;
	}

	public Object getVatMessage(){
		return vatMessage;
	}
}