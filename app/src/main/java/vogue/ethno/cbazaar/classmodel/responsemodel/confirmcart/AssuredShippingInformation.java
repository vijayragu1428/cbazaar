package vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart;


import com.google.gson.annotations.SerializedName;


public class AssuredShippingInformation{

	@SerializedName("AssuredShippingID")
	private int assuredShippingID;

	@SerializedName("Title")
	private Object title;

	@SerializedName("ToolTipDescription")
	private Object toolTipDescription;

	@SerializedName("WarningDescription")
	private Object warningDescription;

	@SerializedName("UnprocessedAssuredShippingProducts")
	private Object unprocessedAssuredShippingProducts;

	@SerializedName("FinalShippingDate")
	private Object finalShippingDate;

	@SerializedName("DisclaimerTilte")
	private Object disclaimerTilte;

	@SerializedName("ActionStripMessage")
	private Object actionStripMessage;

	@SerializedName("RemovePopupMessage")
	private Object removePopupMessage;

	@SerializedName("IsAssuredActive")
	private boolean isAssuredActive;

	@SerializedName("IsAssuredShipping")
	private boolean isAssuredShipping;

	@SerializedName("ToolTipTitle")
	private Object toolTipTitle;

	@SerializedName("DisclaimerDescription")
	private Object disclaimerDescription;

	public void setAssuredShippingID(int assuredShippingID){
		this.assuredShippingID = assuredShippingID;
	}

	public int getAssuredShippingID(){
		return assuredShippingID;
	}

	public void setTitle(Object title){
		this.title = title;
	}

	public Object getTitle(){
		return title;
	}

	public void setToolTipDescription(Object toolTipDescription){
		this.toolTipDescription = toolTipDescription;
	}

	public Object getToolTipDescription(){
		return toolTipDescription;
	}

	public void setWarningDescription(Object warningDescription){
		this.warningDescription = warningDescription;
	}

	public Object getWarningDescription(){
		return warningDescription;
	}

	public void setUnprocessedAssuredShippingProducts(Object unprocessedAssuredShippingProducts){
		this.unprocessedAssuredShippingProducts = unprocessedAssuredShippingProducts;
	}

	public Object getUnprocessedAssuredShippingProducts(){
		return unprocessedAssuredShippingProducts;
	}

	public void setFinalShippingDate(Object finalShippingDate){
		this.finalShippingDate = finalShippingDate;
	}

	public Object getFinalShippingDate(){
		return finalShippingDate;
	}

	public void setDisclaimerTilte(Object disclaimerTilte){
		this.disclaimerTilte = disclaimerTilte;
	}

	public Object getDisclaimerTilte(){
		return disclaimerTilte;
	}

	public void setActionStripMessage(Object actionStripMessage){
		this.actionStripMessage = actionStripMessage;
	}

	public Object getActionStripMessage(){
		return actionStripMessage;
	}

	public void setRemovePopupMessage(Object removePopupMessage){
		this.removePopupMessage = removePopupMessage;
	}

	public Object getRemovePopupMessage(){
		return removePopupMessage;
	}

	public void setIsAssuredActive(boolean isAssuredActive){
		this.isAssuredActive = isAssuredActive;
	}

	public boolean isIsAssuredActive(){
		return isAssuredActive;
	}

	public void setIsAssuredShipping(boolean isAssuredShipping){
		this.isAssuredShipping = isAssuredShipping;
	}

	public boolean isIsAssuredShipping(){
		return isAssuredShipping;
	}

	public void setToolTipTitle(Object toolTipTitle){
		this.toolTipTitle = toolTipTitle;
	}

	public Object getToolTipTitle(){
		return toolTipTitle;
	}

	public void setDisclaimerDescription(Object disclaimerDescription){
		this.disclaimerDescription = disclaimerDescription;
	}

	public Object getDisclaimerDescription(){
		return disclaimerDescription;
	}
}