package vogue.ethno.cbazaar.classmodel.responsemodel.addaddress;

import java.util.List;
 import com.google.gson.annotations.SerializedName;

 public class AddAddressResponse{

	@SerializedName("Command")
	private int command;

	@SerializedName("RelationInfo")
	private List<RelationInfoItem> relationInfo;

	@SerializedName("RelationId")
	private int relationId;

	public void setCommand(int command){
		this.command = command;
	}

	public int getCommand(){
		return command;
	}

	public void setRelationInfo(List<RelationInfoItem> relationInfo){
		this.relationInfo = relationInfo;
	}

	public List<RelationInfoItem> getRelationInfo(){
		return relationInfo;
	}

	public void setRelationId(int relationId){
		this.relationId = relationId;
	}

	public int getRelationId(){
		return relationId;
	}
}