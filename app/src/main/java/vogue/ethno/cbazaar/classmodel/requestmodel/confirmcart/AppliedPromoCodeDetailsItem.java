package vogue.ethno.cbazaar.classmodel.requestmodel.confirmcart;

 import com.google.gson.annotations.SerializedName;

 public class AppliedPromoCodeDetailsItem{

	@SerializedName("PromotionDiscountPercentage")
	private int promotionDiscountPercentage;

	@SerializedName("PromotionType")
	private String promotionType;

	@SerializedName("Messages")
	private Object messages;

	@SerializedName("PromotionMinimumValue")
	private int promotionMinimumValue;

	@SerializedName("PromotionCode")
	private String promotionCode;

	@SerializedName("PromotionValue")
	private int promotionValue;

	@SerializedName("IsValid")
	private boolean isValid;

	public void setPromotionDiscountPercentage(int promotionDiscountPercentage){
		this.promotionDiscountPercentage = promotionDiscountPercentage;
	}

	public int getPromotionDiscountPercentage(){
		return promotionDiscountPercentage;
	}

	public void setPromotionType(String promotionType){
		this.promotionType = promotionType;
	}

	public String getPromotionType(){
		return promotionType;
	}

	public void setMessages(Object messages){
		this.messages = messages;
	}

	public Object getMessages(){
		return messages;
	}

	public void setPromotionMinimumValue(int promotionMinimumValue){
		this.promotionMinimumValue = promotionMinimumValue;
	}

	public int getPromotionMinimumValue(){
		return promotionMinimumValue;
	}

	public void setPromotionCode(String promotionCode){
		this.promotionCode = promotionCode;
	}

	public String getPromotionCode(){
		return promotionCode;
	}

	public void setPromotionValue(int promotionValue){
		this.promotionValue = promotionValue;
	}

	public int getPromotionValue(){
		return promotionValue;
	}

	public void setIsValid(boolean isValid){
		this.isValid = isValid;
	}

	public boolean isIsValid(){
		return isValid;
	}
}