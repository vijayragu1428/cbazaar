package vogue.ethno.cbazaar.classmodel.responsemodel.productdetails;

 import com.google.gson.annotations.SerializedName;

 import java.io.Serializable;

public class ItemsItem implements Serializable {

	@SerializedName("SpecialInstruction")
	private String specialInstruction;

	@SerializedName("DeliveryDays")
	private int deliveryDays;

	@SerializedName("PromotionType")
	private int promotionType;

	@SerializedName("Description")
	private String description;

	@SerializedName("CashBackCost")
	private int cashBackCost;

	@SerializedName("IsEthnoStylish")
	private boolean isEthnoStylish;

	@SerializedName("DiscountCost")
	private int discountCost;

	@SerializedName("IsDefaultSelection")
	private boolean isDefaultSelection;

	@SerializedName("Code")
	private String code;

	@SerializedName("Cost")
	private int cost;

	@SerializedName("ActualCost")
	private int actualCost;

	@SerializedName("DefaultSelection")
	private int defaultSelection;

	@SerializedName("ID")
	private int iD;

	@SerializedName("PromotionPercentage")
	private int promotionPercentage;

	public void setSpecialInstruction(String specialInstruction){
		this.specialInstruction = specialInstruction;
	}

	public String getSpecialInstruction(){
		return specialInstruction;
	}

	public void setDeliveryDays(int deliveryDays){
		this.deliveryDays = deliveryDays;
	}

	public int getDeliveryDays(){
		return deliveryDays;
	}

	public void setPromotionType(int promotionType){
		this.promotionType = promotionType;
	}

	public int getPromotionType(){
		return promotionType;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setCashBackCost(int cashBackCost){
		this.cashBackCost = cashBackCost;
	}

	public int getCashBackCost(){
		return cashBackCost;
	}

	public void setIsEthnoStylish(boolean isEthnoStylish){
		this.isEthnoStylish = isEthnoStylish;
	}

	public boolean isIsEthnoStylish(){
		return isEthnoStylish;
	}

	public void setDiscountCost(int discountCost){
		this.discountCost = discountCost;
	}

	public int getDiscountCost(){
		return discountCost;
	}

	public void setIsDefaultSelection(boolean isDefaultSelection){
		this.isDefaultSelection = isDefaultSelection;
	}

	public boolean isIsDefaultSelection(){
		return isDefaultSelection;
	}

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setCost(int cost){
		this.cost = cost;
	}

	public int getCost(){
		return cost;
	}

	public void setActualCost(int actualCost){
		this.actualCost = actualCost;
	}

	public int getActualCost(){
		return actualCost;
	}

	public void setDefaultSelection(int defaultSelection){
		this.defaultSelection = defaultSelection;
	}

	public int getDefaultSelection(){
		return defaultSelection;
	}

	public void setID(int iD){
		this.iD = iD;
	}

	public int getID(){
		return iD;
	}

	public void setPromotionPercentage(int promotionPercentage){
		this.promotionPercentage = promotionPercentage;
	}

	public int getPromotionPercentage(){
		return promotionPercentage;
	}
}