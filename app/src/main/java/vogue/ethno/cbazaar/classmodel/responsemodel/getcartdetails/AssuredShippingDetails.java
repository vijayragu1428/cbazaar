package vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails;

 import com.google.gson.annotations.SerializedName;

 public class AssuredShippingDetails{

	@SerializedName("AssuredShippingID")
	private int assuredShippingID;

	@SerializedName("Title")
	private String title;

	@SerializedName("ToolTipDescription")
	private String toolTipDescription;

	@SerializedName("WarningDescription")
	private String warningDescription;

	@SerializedName("UnprocessedAssuredShippingProducts")
	private Object unprocessedAssuredShippingProducts;

	@SerializedName("FinalShippingDate")
	private String finalShippingDate;

	@SerializedName("DisclaimerTilte")
	private Object disclaimerTilte;

	@SerializedName("ActionStripMessage")
	private String actionStripMessage;

	@SerializedName("RemovePopupMessage")
	private String removePopupMessage;

	@SerializedName("IsAssuredActive")
	private boolean isAssuredActive;

	@SerializedName("IsAssuredShipping")
	private boolean isAssuredShipping;

	@SerializedName("ToolTipTitle")
	private String toolTipTitle;

	@SerializedName("DisclaimerDescription")
	private Object disclaimerDescription;

	public void setAssuredShippingID(int assuredShippingID){
		this.assuredShippingID = assuredShippingID;
	}

	public int getAssuredShippingID(){
		return assuredShippingID;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setToolTipDescription(String toolTipDescription){
		this.toolTipDescription = toolTipDescription;
	}

	public String getToolTipDescription(){
		return toolTipDescription;
	}

	public void setWarningDescription(String warningDescription){
		this.warningDescription = warningDescription;
	}

	public String getWarningDescription(){
		return warningDescription;
	}

	public void setUnprocessedAssuredShippingProducts(Object unprocessedAssuredShippingProducts){
		this.unprocessedAssuredShippingProducts = unprocessedAssuredShippingProducts;
	}

	public Object getUnprocessedAssuredShippingProducts(){
		return unprocessedAssuredShippingProducts;
	}

	public void setFinalShippingDate(String finalShippingDate){
		this.finalShippingDate = finalShippingDate;
	}

	public String getFinalShippingDate(){
		return finalShippingDate;
	}

	public void setDisclaimerTilte(Object disclaimerTilte){
		this.disclaimerTilte = disclaimerTilte;
	}

	public Object getDisclaimerTilte(){
		return disclaimerTilte;
	}

	public void setActionStripMessage(String actionStripMessage){
		this.actionStripMessage = actionStripMessage;
	}

	public String getActionStripMessage(){
		return actionStripMessage;
	}

	public void setRemovePopupMessage(String removePopupMessage){
		this.removePopupMessage = removePopupMessage;
	}

	public String getRemovePopupMessage(){
		return removePopupMessage;
	}

	public void setIsAssuredActive(boolean isAssuredActive){
		this.isAssuredActive = isAssuredActive;
	}

	public boolean isIsAssuredActive(){
		return isAssuredActive;
	}

	public void setIsAssuredShipping(boolean isAssuredShipping){
		this.isAssuredShipping = isAssuredShipping;
	}

	public boolean isIsAssuredShipping(){
		return isAssuredShipping;
	}

	public void setToolTipTitle(String toolTipTitle){
		this.toolTipTitle = toolTipTitle;
	}

	public String getToolTipTitle(){
		return toolTipTitle;
	}

	public void setDisclaimerDescription(Object disclaimerDescription){
		this.disclaimerDescription = disclaimerDescription;
	}

	public Object getDisclaimerDescription(){
		return disclaimerDescription;
	}
}