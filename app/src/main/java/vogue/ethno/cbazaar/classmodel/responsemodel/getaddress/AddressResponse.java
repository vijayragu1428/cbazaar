package vogue.ethno.cbazaar.classmodel.responsemodel.getaddress;

import java.util.List;
 import com.google.gson.annotations.SerializedName;

 public class AddressResponse{

	@SerializedName("UserDetail")
	private UserDetail userDetail;

	@SerializedName("ShowLoginLink")
	private boolean showLoginLink;

	@SerializedName("Relation")
	private List<RelationItem> relation;

	@SerializedName("TimeZones")
	private List<String> timeZones;

	@SerializedName("ShippingLoginInformation")
	private Object shippingLoginInformation;

	@SerializedName("ShippingStateList")
	private List<String> shippingStateList;

	@SerializedName("ShippingCountryList")
	private List<ShippingCountryListItem> shippingCountryList;

	@SerializedName("ShippingLoginMessage")
	private Object shippingLoginMessage;

	@SerializedName("Time")
	private List<String> time;

	@SerializedName("AddressType")
	private List<String> addressType;

	@SerializedName("BillingCountryList")
	private List<BillingCountryListItem> billingCountryList;

	@SerializedName("BillingStateList")
	private List<String> billingStateList;

	public void setUserDetail(UserDetail userDetail){
		this.userDetail = userDetail;
	}

	public UserDetail getUserDetail(){
		return userDetail;
	}

	public void setShowLoginLink(boolean showLoginLink){
		this.showLoginLink = showLoginLink;
	}

	public boolean isShowLoginLink(){
		return showLoginLink;
	}

	public void setRelation(List<RelationItem> relation){
		this.relation = relation;
	}

	public List<RelationItem> getRelation(){
		return relation;
	}

	public void setTimeZones(List<String> timeZones){
		this.timeZones = timeZones;
	}

	public List<String> getTimeZones(){
		return timeZones;
	}

	public void setShippingLoginInformation(Object shippingLoginInformation){
		this.shippingLoginInformation = shippingLoginInformation;
	}

	public Object getShippingLoginInformation(){
		return shippingLoginInformation;
	}

	public void setShippingStateList(List<String> shippingStateList){
		this.shippingStateList = shippingStateList;
	}

	public List<String> getShippingStateList(){
		return shippingStateList;
	}

	public void setShippingCountryList(List<ShippingCountryListItem> shippingCountryList){
		this.shippingCountryList = shippingCountryList;
	}

	public List<ShippingCountryListItem> getShippingCountryList(){
		return shippingCountryList;
	}

	public void setShippingLoginMessage(Object shippingLoginMessage){
		this.shippingLoginMessage = shippingLoginMessage;
	}

	public Object getShippingLoginMessage(){
		return shippingLoginMessage;
	}

	public void setTime(List<String> time){
		this.time = time;
	}

	public List<String> getTime(){
		return time;
	}

	public void setAddressType(List<String> addressType){
		this.addressType = addressType;
	}

	public List<String> getAddressType(){
		return addressType;
	}

	public void setBillingCountryList(List<BillingCountryListItem> billingCountryList){
		this.billingCountryList = billingCountryList;
	}

	public List<BillingCountryListItem> getBillingCountryList(){
		return billingCountryList;
	}

	public void setBillingStateList(List<String> billingStateList){
		this.billingStateList = billingStateList;
	}

	public List<String> getBillingStateList(){
		return billingStateList;
	}
}