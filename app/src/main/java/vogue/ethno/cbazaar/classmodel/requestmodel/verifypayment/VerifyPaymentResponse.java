package vogue.ethno.cbazaar.classmodel.requestmodel.verifypayment;

 import com.google.gson.annotations.SerializedName;

 public class VerifyPaymentResponse{

	@SerializedName("BillingAddress")
	private BillingAddress billingAddress;

	@SerializedName("Nonce")
	private String nonce;

	@SerializedName("OrderValue")
	private String orderValue;

	@SerializedName("SelectedPaymentGateway")
	private int selectedPaymentGateway;

	@SerializedName("OrderNumber")
	private String orderNumber;

	public void setBillingAddress(BillingAddress billingAddress){
		this.billingAddress = billingAddress;
	}

	public BillingAddress getBillingAddress(){
		return billingAddress;
	}

	public void setNonce(String nonce){
		this.nonce = nonce;
	}

	public String getNonce(){
		return nonce;
	}

	public void setOrderValue(String orderValue){
		this.orderValue = orderValue;
	}

	public String getOrderValue(){
		return orderValue;
	}

	public void setSelectedPaymentGateway(int selectedPaymentGateway){
		this.selectedPaymentGateway = selectedPaymentGateway;
	}

	public int getSelectedPaymentGateway(){
		return selectedPaymentGateway;
	}

	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber(){
		return orderNumber;
	}
}