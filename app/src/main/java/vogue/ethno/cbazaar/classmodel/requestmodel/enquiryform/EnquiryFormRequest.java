package vogue.ethno.cbazaar.classmodel.requestmodel.enquiryform;

import com.google.gson.annotations.SerializedName;

public class EnquiryFormRequest {

	@SerializedName("FirstName")
	private String firstName;

	@SerializedName("Phone")
	private String phone;

	@SerializedName("State")
	private String state;

	@SerializedName("Purpose")
	private String purpose;

	@SerializedName("LastName")
	private String lastName;

	@SerializedName("Address1")
	private String address1;

	@SerializedName("IsGuest")
	private boolean isGuest;

	@SerializedName("District")
	private String district;

	@SerializedName("EmailAddress")
	private String emailAddress;

	@SerializedName("Other")
	private String other;

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setPurpose(String purpose){
		this.purpose = purpose;
	}

	public String getPurpose(){
		return purpose;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setAddress1(String address1){
		this.address1 = address1;
	}

	public String getAddress1(){
		return address1;
	}

	public void setIsGuest(boolean isGuest){
		this.isGuest = isGuest;
	}

	public boolean isIsGuest(){
		return isGuest;
	}

	public void setDistrict(String district){
		this.district = district;
	}

	public String getDistrict(){
		return district;
	}

	public void setEmailAddress(String emailAddress){
		this.emailAddress = emailAddress;
	}

	public String getEmailAddress(){
		return emailAddress;
	}

	public void setOther(String other){
		this.other = other;
	}

	public String getOther(){
		return other;
	}
}