package vogue.ethno.cbazaar.classmodel.responsemodel.productdetails;

import java.io.Serializable;
import java.util.List;
 import com.google.gson.annotations.SerializedName;

 public class ProductDetailsResponseModel implements Serializable {

	@SerializedName("CustomTailoringOptions")
	private CustomTailoringOptions customTailoringOptions;
	@SerializedName("ErrorMessage")
	private String errorMessage;

	 public String getErrorMessage() {
		 return errorMessage;
	 }

	 public void setErrorMessage(String errorMessage) {
		 this.errorMessage = errorMessage;
	 }

	 @SerializedName("AdditionalInfo")
	private List<AdditionalInfoItem> additionalInfo;

	@SerializedName("MinimumCost")
	private int minimumCost;

	@SerializedName("PatternProducts")
	private Object patternProducts;

	@SerializedName("AddOn")
	private List<AddOnItem> addOn;

	@SerializedName("ProductDetail")
	private List<ProductDetailItem> productDetail;

	@SerializedName("MetaTitle")
	private String metaTitle;

	@SerializedName("AssuredShippingDetail")
	private AssuredShippingDetail assuredShippingDetail;

	@SerializedName("LongSleeveOptions")
	private LongSleeveOptions longSleeveOptions;

	@SerializedName("SupplementaryOptions")
	private List<Object> supplementaryOptions;

	@SerializedName("Ratingstars")
	private Ratingstars ratingstars;

	@SerializedName("Count")
	private int count;

	@SerializedName("ProductSize")
	private List<Object> productSize;

	@SerializedName("PageContent")
	private String pageContent;

	@SerializedName("MeasurementOptions")
	private MeasurementOptions measurementOptions;

	@SerializedName("CommonDetail")
	private CommonDetail commonDetail;

	@SerializedName("ReadyMadeColors")
	private Object readyMadeColors;

	@SerializedName("MaximumCost")
	private int maximumCost;

	@SerializedName("PatternProductDetail")
	private List<PatternProductDetailItem> patternProductDetail;

	@SerializedName("LongBlouseOptions")
	private LongBlouseOptions longBlouseOptions;

	@SerializedName("MetaDescription")
	private String metaDescription;

	@SerializedName("CustomerReview")
	private List<CustomerReviewItem> customerReview;

	public void setCustomTailoringOptions(CustomTailoringOptions customTailoringOptions){
		this.customTailoringOptions = customTailoringOptions;
	}

	public CustomTailoringOptions getCustomTailoringOptions(){
		return customTailoringOptions;
	}

	public void setAdditionalInfo(List<AdditionalInfoItem> additionalInfo){
		this.additionalInfo = additionalInfo;
	}

	public List<AdditionalInfoItem> getAdditionalInfo(){
		return additionalInfo;
	}

	public void setMinimumCost(int minimumCost){
		this.minimumCost = minimumCost;
	}

	public int getMinimumCost(){
		return minimumCost;
	}

	public void setPatternProducts(Object patternProducts){
		this.patternProducts = patternProducts;
	}

	public Object getPatternProducts(){
		return patternProducts;
	}

	public void setAddOn(List<AddOnItem> addOn){
		this.addOn = addOn;
	}

	public List<AddOnItem> getAddOn(){
		return addOn;
	}

	public void setProductDetail(List<ProductDetailItem> productDetail){
		this.productDetail = productDetail;
	}

	public List<ProductDetailItem> getProductDetail(){
		return productDetail;
	}

	public void setMetaTitle(String metaTitle){
		this.metaTitle = metaTitle;
	}

	public String getMetaTitle(){
		return metaTitle;
	}

	public void setAssuredShippingDetail(AssuredShippingDetail assuredShippingDetail){
		this.assuredShippingDetail = assuredShippingDetail;
	}

	public AssuredShippingDetail getAssuredShippingDetail(){
		return assuredShippingDetail;
	}

	public void setLongSleeveOptions(LongSleeveOptions longSleeveOptions){
		this.longSleeveOptions = longSleeveOptions;
	}

	public LongSleeveOptions getLongSleeveOptions(){
		return longSleeveOptions;
	}

	public void setSupplementaryOptions(List<Object> supplementaryOptions){
		this.supplementaryOptions = supplementaryOptions;
	}

	public List<Object> getSupplementaryOptions(){
		return supplementaryOptions;
	}

	public void setRatingstars(Ratingstars ratingstars){
		this.ratingstars = ratingstars;
	}

	public Ratingstars getRatingstars(){
		return ratingstars;
	}

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}

	public void setProductSize(List<Object> productSize){
		this.productSize = productSize;
	}

	public List<Object> getProductSize(){
		return productSize;
	}

	public void setPageContent(String pageContent){
		this.pageContent = pageContent;
	}

	public String getPageContent(){
		return pageContent;
	}

	public void setMeasurementOptions(MeasurementOptions measurementOptions){
		this.measurementOptions = measurementOptions;
	}

	public MeasurementOptions getMeasurementOptions(){
		return measurementOptions;
	}

	public void setCommonDetail(CommonDetail commonDetail){
		this.commonDetail = commonDetail;
	}

	public CommonDetail getCommonDetail(){
		return commonDetail;
	}

	public void setReadyMadeColors(Object readyMadeColors){
		this.readyMadeColors = readyMadeColors;
	}

	public Object getReadyMadeColors(){
		return readyMadeColors;
	}

	public void setMaximumCost(int maximumCost){
		this.maximumCost = maximumCost;
	}

	public int getMaximumCost(){
		return maximumCost;
	}

	public void setPatternProductDetail(List<PatternProductDetailItem> patternProductDetail){
		this.patternProductDetail = patternProductDetail;
	}

	public List<PatternProductDetailItem> getPatternProductDetail(){
		return patternProductDetail;
	}

	public void setLongBlouseOptions(LongBlouseOptions longBlouseOptions){
		this.longBlouseOptions = longBlouseOptions;
	}

	public LongBlouseOptions getLongBlouseOptions(){
		return longBlouseOptions;
	}

	public void setMetaDescription(String metaDescription){
		this.metaDescription = metaDescription;
	}

	public String getMetaDescription(){
		return metaDescription;
	}

	public void setCustomerReview(List<CustomerReviewItem> customerReview){
		this.customerReview = customerReview;
	}

	public List<CustomerReviewItem> getCustomerReview(){
		return customerReview;
	}
}