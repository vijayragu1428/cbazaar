package vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart;


import com.google.gson.annotations.SerializedName;


public class ShippingCost{

	@SerializedName("EDSShippingCost")
	private int eDSShippingCost;

	@SerializedName("FinalShippingCost")
	private int finalShippingCost;

	@SerializedName("HasPartialShipping")
	private boolean hasPartialShipping;

	@SerializedName("ShippingCost")
	private int shippingCost;

	@SerializedName("ShippingCountry")
	private Object shippingCountry;

	@SerializedName("FabricUnitCost")
	private int fabricUnitCost;

	@SerializedName("ShippingCostPerQuantity")
	private int shippingCostPerQuantity;

	public void setEDSShippingCost(int eDSShippingCost){
		this.eDSShippingCost = eDSShippingCost;
	}

	public int getEDSShippingCost(){
		return eDSShippingCost;
	}

	public void setFinalShippingCost(int finalShippingCost){
		this.finalShippingCost = finalShippingCost;
	}

	public int getFinalShippingCost(){
		return finalShippingCost;
	}

	public void setHasPartialShipping(boolean hasPartialShipping){
		this.hasPartialShipping = hasPartialShipping;
	}

	public boolean isHasPartialShipping(){
		return hasPartialShipping;
	}

	public void setShippingCost(int shippingCost){
		this.shippingCost = shippingCost;
	}

	public int getShippingCost(){
		return shippingCost;
	}

	public void setShippingCountry(Object shippingCountry){
		this.shippingCountry = shippingCountry;
	}

	public Object getShippingCountry(){
		return shippingCountry;
	}

	public void setFabricUnitCost(int fabricUnitCost){
		this.fabricUnitCost = fabricUnitCost;
	}

	public int getFabricUnitCost(){
		return fabricUnitCost;
	}

	public void setShippingCostPerQuantity(int shippingCostPerQuantity){
		this.shippingCostPerQuantity = shippingCostPerQuantity;
	}

	public int getShippingCostPerQuantity(){
		return shippingCostPerQuantity;
	}
}