package vogue.ethno.cbazaar.classmodel.responsemodel.productdetails;

 import com.google.gson.annotations.SerializedName;

 import java.io.Serializable;

public class AssuredShippingDetail implements Serializable {

	@SerializedName("DisclaimerTitle")
	private String disclaimerTitle;

	@SerializedName("AssuredTitle")
	private String assuredTitle;

	@SerializedName("IsAssuredShip")
	private boolean isAssuredShip;

	@SerializedName("AssuredDoodleImage")
	private String assuredDoodleImage;

	@SerializedName("IsShowDisclaimer")
	private boolean isShowDisclaimer;

	@SerializedName("IOSAssuredDoodleImage")
	private String iOSAssuredDoodleImage;

	@SerializedName("ToolTipTitle")
	private String toolTipTitle;

	@SerializedName("IsAssuredStrike")
	private boolean isAssuredStrike;

	@SerializedName("DisclaimerDescription")
	private String disclaimerDescription;

	@SerializedName("AssuredDescription")
	private String assuredDescription;

	public void setDisclaimerTitle(String disclaimerTitle){
		this.disclaimerTitle = disclaimerTitle;
	}

	public String getDisclaimerTitle(){
		return disclaimerTitle;
	}

	public void setAssuredTitle(String assuredTitle){
		this.assuredTitle = assuredTitle;
	}

	public String getAssuredTitle(){
		return assuredTitle;
	}

	public void setIsAssuredShip(boolean isAssuredShip){
		this.isAssuredShip = isAssuredShip;
	}

	public boolean isIsAssuredShip(){
		return isAssuredShip;
	}

	public void setAssuredDoodleImage(String assuredDoodleImage){
		this.assuredDoodleImage = assuredDoodleImage;
	}

	public String getAssuredDoodleImage(){
		return assuredDoodleImage;
	}

	public void setIsShowDisclaimer(boolean isShowDisclaimer){
		this.isShowDisclaimer = isShowDisclaimer;
	}

	public boolean isIsShowDisclaimer(){
		return isShowDisclaimer;
	}

	public void setIOSAssuredDoodleImage(String iOSAssuredDoodleImage){
		this.iOSAssuredDoodleImage = iOSAssuredDoodleImage;
	}

	public String getIOSAssuredDoodleImage(){
		return iOSAssuredDoodleImage;
	}

	public void setToolTipTitle(String toolTipTitle){
		this.toolTipTitle = toolTipTitle;
	}

	public String getToolTipTitle(){
		return toolTipTitle;
	}

	public void setIsAssuredStrike(boolean isAssuredStrike){
		this.isAssuredStrike = isAssuredStrike;
	}

	public boolean isIsAssuredStrike(){
		return isAssuredStrike;
	}

	public void setDisclaimerDescription(String disclaimerDescription){
		this.disclaimerDescription = disclaimerDescription;
	}

	public String getDisclaimerDescription(){
		return disclaimerDescription;
	}

	public void setAssuredDescription(String assuredDescription){
		this.assuredDescription = assuredDescription;
	}

	public String getAssuredDescription(){
		return assuredDescription;
	}
}