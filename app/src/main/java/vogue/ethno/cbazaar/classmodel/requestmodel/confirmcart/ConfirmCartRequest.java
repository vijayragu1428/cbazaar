package vogue.ethno.cbazaar.classmodel.requestmodel.confirmcart;

import java.util.List;
 import com.google.gson.annotations.SerializedName;

 public class ConfirmCartRequest {

	@SerializedName("SpecialInstruction")
	private String specialInstruction;

	@SerializedName("IsEarlyDispatch")
	private String isEarlyDispatch;

	@SerializedName("Occasion")
	private String occasion;

	@SerializedName("IsReadyToShip")
	private String isReadyToShip;

	@SerializedName("GiftText")
	private String giftText;

	@SerializedName("PromotionCode")
	private String promotionCode;

	@SerializedName("IsGuest")
	private String isGuest;

	@SerializedName("BillingAddress")
	private BillingAddress billingAddress;

	@SerializedName("AppliedPromoCodeDetails")
	private List<AppliedPromoCodeDetailsItem> appliedPromoCodeDetails;

	@SerializedName("TimeZone")
	private String timeZone;

	@SerializedName("ToTime")
	private String toTime;

	@SerializedName("Is7DD")
	private String is7DD;

	@SerializedName("ShippingAddress")
	private ShippingAddress shippingAddress;

	@SerializedName("GrandTotal")
	private String grandTotal;

	@SerializedName("IsShippingBillingSame")
	private boolean isShippingBillingSame;

	@SerializedName("FromTime")
	private String fromTime;

	public void setSpecialInstruction(String specialInstruction){
		this.specialInstruction = specialInstruction;
	}

	public String getSpecialInstruction(){
		return specialInstruction;
	}

	public void setIsEarlyDispatch(String isEarlyDispatch){
		this.isEarlyDispatch = isEarlyDispatch;
	}

	public String getIsEarlyDispatch(){
		return isEarlyDispatch;
	}

	public void setOccasion(String occasion){
		this.occasion = occasion;
	}

	public String getOccasion(){
		return occasion;
	}

	public void setIsReadyToShip(String isReadyToShip){
		this.isReadyToShip = isReadyToShip;
	}

	public String getIsReadyToShip(){
		return isReadyToShip;
	}

	public void setGiftText(String giftText){
		this.giftText = giftText;
	}

	public String getGiftText(){
		return giftText;
	}

	public void setPromotionCode(String promotionCode){
		this.promotionCode = promotionCode;
	}

	public String getPromotionCode(){
		return promotionCode;
	}

	public void setIsGuest(String isGuest){
		this.isGuest = isGuest;
	}

	public String getIsGuest(){
		return isGuest;
	}

	public void setBillingAddress(BillingAddress billingAddress){
		this.billingAddress = billingAddress;
	}

	public BillingAddress getBillingAddress(){
		return billingAddress;
	}

	public void setAppliedPromoCodeDetails(List<AppliedPromoCodeDetailsItem> appliedPromoCodeDetails){
		this.appliedPromoCodeDetails = appliedPromoCodeDetails;
	}

	public List<AppliedPromoCodeDetailsItem> getAppliedPromoCodeDetails(){
		return appliedPromoCodeDetails;
	}

	public void setTimeZone(String timeZone){
		this.timeZone = timeZone;
	}

	public String getTimeZone(){
		return timeZone;
	}

	public void setToTime(String toTime){
		this.toTime = toTime;
	}

	public String getToTime(){
		return toTime;
	}

	public void setIs7DD(String is7DD){
		this.is7DD = is7DD;
	}

	public String getIs7DD(){
		return is7DD;
	}

	public void setShippingAddress(ShippingAddress shippingAddress){
		this.shippingAddress = shippingAddress;
	}

	public ShippingAddress getShippingAddress(){
		return shippingAddress;
	}

	public void setGrandTotal(String grandTotal){
		this.grandTotal = grandTotal;
	}

	public String getGrandTotal(){
		return grandTotal;
	}

	public void setIsShippingBillingSame(boolean isShippingBillingSame){
		this.isShippingBillingSame = isShippingBillingSame;
	}

	public boolean isIsShippingBillingSame(){
		return isShippingBillingSame;
	}

	public void setFromTime(String fromTime){
		this.fromTime = fromTime;
	}

	public String getFromTime(){
		return fromTime;
	}
}