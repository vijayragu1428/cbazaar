package vogue.ethno.cbazaar.classmodel.requestmodel.getcartdetails;

 import com.google.gson.annotations.SerializedName;

 public class GetCartRequestModel{

	@SerializedName("IsEDS")
	private boolean isEDS;

	@SerializedName("IsCODEligible")
	private boolean isCODEligible;

	@SerializedName("Command")
	private int command;

	@SerializedName("ProductCode")
	private int productCode;

	@SerializedName("GrandTotal")
	private double grandTotal;

	@SerializedName("PromotionCode")
	private int promotionCode;

	@SerializedName("DeliveryMode")
	private int deliveryMode;

	@SerializedName("CartItemID")
	private int cartItemID;

	@SerializedName("PromoDiscountPercentage")
	private int promoDiscountPercentage;

	@SerializedName("IsMoveToShortlist")
	private boolean isMoveToShortlist;

	 @SerializedName("Quantity")
	 private int quantity;

	 @SerializedName("ShippingCountryID")
	 private int shippingCountryID;

	 public int getShippingCountryID() {
		 return shippingCountryID;
	 }

	 public void setShippingCountryID(int shippingCountryID) {
		 this.shippingCountryID = shippingCountryID;
	 }

	 public boolean isEDS() {
		 return isEDS;
	 }

	 public void setEDS(boolean EDS) {
		 isEDS = EDS;
	 }

	 public boolean isCODEligible() {
		 return isCODEligible;
	 }

	 public void setCODEligible(boolean CODEligible) {
		 isCODEligible = CODEligible;
	 }

	 public boolean isMoveToShortlist() {
		 return isMoveToShortlist;
	 }

	 public void setMoveToShortlist(boolean moveToShortlist) {
		 isMoveToShortlist = moveToShortlist;
	 }

	 public int getQuantity() {
		 return quantity;
	 }

	 public void setQuantity(int quantity) {
		 this.quantity = quantity;
	 }

	 public void setIsEDS(boolean isEDS){
		this.isEDS = isEDS;
	}

	public boolean isIsEDS(){
		return isEDS;
	}

	public void setIsCODEligible(boolean isCODEligible){
		this.isCODEligible = isCODEligible;
	}

	public boolean isIsCODEligible(){
		return isCODEligible;
	}

	public void setCommand(int command){
		this.command = command;
	}

	public int getCommand(){
		return command;
	}

	public void setProductCode(int productCode){
		this.productCode = productCode;
	}

	public int getProductCode(){
		return productCode;
	}

	public void setGrandTotal(double grandTotal){
		this.grandTotal = grandTotal;
	}

	public double getGrandTotal(){
		return grandTotal;
	}

	public void setPromotionCode(int promotionCode){
		this.promotionCode = promotionCode;
	}

	public int getPromotionCode(){
		return promotionCode;
	}

	public void setDeliveryMode(int deliveryMode){
		this.deliveryMode = deliveryMode;
	}

	public int getDeliveryMode(){
		return deliveryMode;
	}

	public void setCartItemID(int cartItemID){
		this.cartItemID = cartItemID;
	}

	public int getCartItemID(){
		return cartItemID;
	}

	public void setPromoDiscountPercentage(int promoDiscountPercentage){
		this.promoDiscountPercentage = promoDiscountPercentage;
	}

	public int getPromoDiscountPercentage(){
		return promoDiscountPercentage;
	}

	public void setIsMoveToShortlist(boolean isMoveToShortlist){
		this.isMoveToShortlist = isMoveToShortlist;
	}

	public boolean isIsMoveToShortlist(){
		return isMoveToShortlist;
	}
}