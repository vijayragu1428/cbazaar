package vogue.ethno.cbazaar.classmodel.requestmodel.address;

 import com.google.gson.annotations.SerializedName;

 public class Address{

	@SerializedName("RelationCountryId")
	private int relationCountryId;

	@SerializedName("RelationTypeId")
	private int relationTypeId;

	@SerializedName("Address2")
	private String address2;

	@SerializedName("Title")
	private String title;

	@SerializedName("Address1")
	private String address1;

	@SerializedName("City")
	private String city;

	@SerializedName("State")
	private String state;

	@SerializedName("RelationID")
	private int relationID;

	@SerializedName("Zipcode")
	private String zipcode;

	@SerializedName("RelationEmailId")
	private String relationEmailId;

	@SerializedName("Country")
	private String country;

	@SerializedName("PhoneNumber")
	private String phoneNumber;

	@SerializedName("RelationLastName")
	private String relationLastName;

	@SerializedName("AddressType")
	private String addressType;

	@SerializedName("CellNumber")
	private String cellNumber;

	@SerializedName("RelationFirstName")
	private String relationFirstName;

	@SerializedName("RelationCountryCode")
	private String relationCountryCode;

	public void setRelationCountryId(int relationCountryId){
		this.relationCountryId = relationCountryId;
	}

	public int getRelationCountryId(){
		return relationCountryId;
	}

	public void setRelationTypeId(int relationTypeId){
		this.relationTypeId = relationTypeId;
	}

	public int getRelationTypeId(){
		return relationTypeId;
	}

	public void setAddress2(String address2){
		this.address2 = address2;
	}

	public String getAddress2(){
		return address2;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setAddress1(String address1){
		this.address1 = address1;
	}

	public String getAddress1(){
		return address1;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setRelationID(int relationID){
		this.relationID = relationID;
	}

	public int getRelationID(){
		return relationID;
	}

	public void setZipcode(String zipcode){
		this.zipcode = zipcode;
	}

	public String getZipcode(){
		return zipcode;
	}

	public void setRelationEmailId(String relationEmailId){
		this.relationEmailId = relationEmailId;
	}

	public String getRelationEmailId(){
		return relationEmailId;
	}

	public void setCountry(String country){
		this.country = country;
	}

	public String getCountry(){
		return country;
	}

	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber(){
		return phoneNumber;
	}

	public void setRelationLastName(String relationLastName){
		this.relationLastName = relationLastName;
	}

	public String getRelationLastName(){
		return relationLastName;
	}

	public void setAddressType(String addressType){
		this.addressType = addressType;
	}

	public String getAddressType(){
		return addressType;
	}

	public void setCellNumber(String cellNumber){
		this.cellNumber = cellNumber;
	}

	public String getCellNumber(){
		return cellNumber;
	}

	public void setRelationFirstName(String relationFirstName){
		this.relationFirstName = relationFirstName;
	}

	public String getRelationFirstName(){
		return relationFirstName;
	}

	public void setRelationCountryCode(String relationCountryCode){
		this.relationCountryCode = relationCountryCode;
	}

	public String getRelationCountryCode(){
		return relationCountryCode;
	}
}