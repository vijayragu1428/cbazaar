package vogue.ethno.cbazaar.classmodel.requestmodel.topstyle;

 import com.google.gson.annotations.SerializedName;

 public class TopStyleRequestModel{

	@SerializedName("ProductCode")
	private String productCode;

	@SerializedName("SizeValue")
	private String sizeValue;

	@SerializedName("FeetAndInches")
	private String feetAndInches;

	public void setProductCode(String productCode){
		this.productCode = productCode;
	}

	public String getProductCode(){
		return productCode;
	}

	public void setSizeValue(String sizeValue){
		this.sizeValue = sizeValue;
	}

	public String getSizeValue(){
		return sizeValue;
	}

	public void setFeetAndInches(String feetAndInches){
		this.feetAndInches = feetAndInches;
	}

	public String getFeetAndInches(){
		return feetAndInches;
	}
}