package vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails;

import java.util.List;
 import com.google.gson.annotations.SerializedName;

 public class Information{

	@SerializedName("RTSDeliveryDate")
	private String rTSDeliveryDate;

	@SerializedName("PromotionDiscountPercentage")
	private int promotionDiscountPercentage;

	@SerializedName("PromotionOfferMessage")
	private List<Object> promotionOfferMessage;

	@SerializedName("MoveToWishlistMessage")
	private String moveToWishlistMessage;

	@SerializedName("EDSDescription")
	private String eDSDescription;

	@SerializedName("EsCourierdays")
	private String esCourierdays;

	@SerializedName("CurrencySymbol")
	private String currencySymbol;

	@SerializedName("DelivaryDate")
	private String delivaryDate;

	@SerializedName("CurrencyCode")
	private String currencyCode;

	@SerializedName("Is7DD")
	private boolean is7DD;

	@SerializedName("RemoveStitchedProductMessage")
	private String removeStitchedProductMessage;

	@SerializedName("UserID")
	private int userID;

	@SerializedName("Applicable7DD")
	private String applicable7DD;

	@SerializedName("NotApplicable7DD")
	private String notApplicable7DD;

	@SerializedName("HasCustomItems")
	private boolean hasCustomItems;

	@SerializedName("QuantityForShippingcost")
	private int quantityForShippingcost;

	@SerializedName("DeliveryDays")
	private int deliveryDays;

	@SerializedName("NextPercentageForStepSale")
	private int nextPercentageForStepSale;

	@SerializedName("IsEDSApplicable")
	private boolean isEDSApplicable;

	@SerializedName("DeliveryDescription")
	private Object deliveryDescription;

	@SerializedName("PartialDeliveryDescriptionInfo")
	private Object partialDeliveryDescriptionInfo;

	@SerializedName("NonPartialDeliveryInfo")
	private Object nonPartialDeliveryInfo;

	@SerializedName("InformationMessages")
	private List<InformationMessagesItem> informationMessages;

	@SerializedName("ShortlistCount")
	private int shortlistCount;

	@SerializedName("ShippingOfferMessage")
	private List<Object> shippingOfferMessage;

	@SerializedName("IsCODEligible")
	private boolean isCODEligible;

	@SerializedName("CartQuantity")
	private int cartQuantity;

	@SerializedName("IsShowEDSFee")
	private boolean isShowEDSFee;

	@SerializedName("StepUpSaleProductCount")
	private int stepUpSaleProductCount;

	@SerializedName("SessionID")
	private String sessionID;

	public void setRTSDeliveryDate(String rTSDeliveryDate){
		this.rTSDeliveryDate = rTSDeliveryDate;
	}

	public String getRTSDeliveryDate(){
		return rTSDeliveryDate;
	}

	public void setPromotionDiscountPercentage(int promotionDiscountPercentage){
		this.promotionDiscountPercentage = promotionDiscountPercentage;
	}

	public int getPromotionDiscountPercentage(){
		return promotionDiscountPercentage;
	}

	public void setPromotionOfferMessage(List<Object> promotionOfferMessage){
		this.promotionOfferMessage = promotionOfferMessage;
	}

	public List<Object> getPromotionOfferMessage(){
		return promotionOfferMessage;
	}

	public void setMoveToWishlistMessage(String moveToWishlistMessage){
		this.moveToWishlistMessage = moveToWishlistMessage;
	}

	public String getMoveToWishlistMessage(){
		return moveToWishlistMessage;
	}

	public void setEDSDescription(String eDSDescription){
		this.eDSDescription = eDSDescription;
	}

	public String getEDSDescription(){
		return eDSDescription;
	}

	public void setEsCourierdays(String esCourierdays){
		this.esCourierdays = esCourierdays;
	}

	public String getEsCourierdays(){
		return esCourierdays;
	}

	public void setCurrencySymbol(String currencySymbol){
		this.currencySymbol = currencySymbol;
	}

	public String getCurrencySymbol(){
		return currencySymbol;
	}

	public void setDelivaryDate(String delivaryDate){
		this.delivaryDate = delivaryDate;
	}

	public String getDelivaryDate(){
		return delivaryDate;
	}

	public void setCurrencyCode(String currencyCode){
		this.currencyCode = currencyCode;
	}

	public String getCurrencyCode(){
		return currencyCode;
	}

	public void setIs7DD(boolean is7DD){
		this.is7DD = is7DD;
	}

	public boolean isIs7DD(){
		return is7DD;
	}

	public void setRemoveStitchedProductMessage(String removeStitchedProductMessage){
		this.removeStitchedProductMessage = removeStitchedProductMessage;
	}

	public String getRemoveStitchedProductMessage(){
		return removeStitchedProductMessage;
	}

	public void setUserID(int userID){
		this.userID = userID;
	}

	public int getUserID(){
		return userID;
	}

	public void setApplicable7DD(String applicable7DD){
		this.applicable7DD = applicable7DD;
	}

	public String getApplicable7DD(){
		return applicable7DD;
	}

	public void setNotApplicable7DD(String notApplicable7DD){
		this.notApplicable7DD = notApplicable7DD;
	}

	public String getNotApplicable7DD(){
		return notApplicable7DD;
	}

	public void setHasCustomItems(boolean hasCustomItems){
		this.hasCustomItems = hasCustomItems;
	}

	public boolean isHasCustomItems(){
		return hasCustomItems;
	}

	public void setQuantityForShippingcost(int quantityForShippingcost){
		this.quantityForShippingcost = quantityForShippingcost;
	}

	public int getQuantityForShippingcost(){
		return quantityForShippingcost;
	}

	public void setDeliveryDays(int deliveryDays){
		this.deliveryDays = deliveryDays;
	}

	public int getDeliveryDays(){
		return deliveryDays;
	}

	public void setNextPercentageForStepSale(int nextPercentageForStepSale){
		this.nextPercentageForStepSale = nextPercentageForStepSale;
	}

	public int getNextPercentageForStepSale(){
		return nextPercentageForStepSale;
	}

	public void setIsEDSApplicable(boolean isEDSApplicable){
		this.isEDSApplicable = isEDSApplicable;
	}

	public boolean isIsEDSApplicable(){
		return isEDSApplicable;
	}

	public void setDeliveryDescription(Object deliveryDescription){
		this.deliveryDescription = deliveryDescription;
	}

	public Object getDeliveryDescription(){
		return deliveryDescription;
	}

	public void setPartialDeliveryDescriptionInfo(Object partialDeliveryDescriptionInfo){
		this.partialDeliveryDescriptionInfo = partialDeliveryDescriptionInfo;
	}

	public Object getPartialDeliveryDescriptionInfo(){
		return partialDeliveryDescriptionInfo;
	}

	public void setNonPartialDeliveryInfo(Object nonPartialDeliveryInfo){
		this.nonPartialDeliveryInfo = nonPartialDeliveryInfo;
	}

	public Object getNonPartialDeliveryInfo(){
		return nonPartialDeliveryInfo;
	}

	public void setInformationMessages(List<InformationMessagesItem> informationMessages){
		this.informationMessages = informationMessages;
	}

	public List<InformationMessagesItem> getInformationMessages(){
		return informationMessages;
	}

	public void setShortlistCount(int shortlistCount){
		this.shortlistCount = shortlistCount;
	}

	public int getShortlistCount(){
		return shortlistCount;
	}

	public void setShippingOfferMessage(List<Object> shippingOfferMessage){
		this.shippingOfferMessage = shippingOfferMessage;
	}

	public List<Object> getShippingOfferMessage(){
		return shippingOfferMessage;
	}

	public void setIsCODEligible(boolean isCODEligible){
		this.isCODEligible = isCODEligible;
	}

	public boolean isIsCODEligible(){
		return isCODEligible;
	}

	public void setCartQuantity(int cartQuantity){
		this.cartQuantity = cartQuantity;
	}

	public int getCartQuantity(){
		return cartQuantity;
	}

	public void setIsShowEDSFee(boolean isShowEDSFee){
		this.isShowEDSFee = isShowEDSFee;
	}

	public boolean isIsShowEDSFee(){
		return isShowEDSFee;
	}

	public void setStepUpSaleProductCount(int stepUpSaleProductCount){
		this.stepUpSaleProductCount = stepUpSaleProductCount;
	}

	public int getStepUpSaleProductCount(){
		return stepUpSaleProductCount;
	}

	public void setSessionID(String sessionID){
		this.sessionID = sessionID;
	}

	public String getSessionID(){
		return sessionID;
	}
}