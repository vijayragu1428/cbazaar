package vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails;

 import com.google.gson.annotations.SerializedName;

 public class InformationMessagesItem{

	@SerializedName("Value")
	private String value;

	@SerializedName("Key")
	private String key;

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	public void setKey(String key){
		this.key = key;
	}

	public String getKey(){
		return key;
	}
}