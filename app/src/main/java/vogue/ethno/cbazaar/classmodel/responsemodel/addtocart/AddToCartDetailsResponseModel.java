package vogue.ethno.cbazaar.classmodel.responsemodel.addtocart;

 import com.google.gson.annotations.SerializedName;

 public class AddToCartDetailsResponseModel {

	@SerializedName("Message")
	private String message;

	@SerializedName("LineItemID")
	private int lineItemID;

	@SerializedName("StatusCode")
	private String statusCode;

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setLineItemID(int lineItemID){
		this.lineItemID = lineItemID;
	}

	public int getLineItemID(){
		return lineItemID;
	}

	public void setStatusCode(String statusCode){
		this.statusCode = statusCode;
	}

	public String getStatusCode(){
		return statusCode;
	}
}