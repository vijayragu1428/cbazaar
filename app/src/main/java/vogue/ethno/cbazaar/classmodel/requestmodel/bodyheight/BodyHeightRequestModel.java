package vogue.ethno.cbazaar.classmodel.requestmodel.bodyheight;

 import com.google.gson.annotations.SerializedName;

 public class BodyHeightRequestModel{

	@SerializedName("ProductCode")
	private String productCode;

	@SerializedName("Value")
	private String value;

	public void setProductCode(String productCode){
		this.productCode = productCode;
	}

	public String getProductCode(){
		return productCode;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}
}