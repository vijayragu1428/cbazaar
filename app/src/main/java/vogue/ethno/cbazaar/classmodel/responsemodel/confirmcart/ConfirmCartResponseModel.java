package vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import vogue.ethno.cbazaar.classmodel.requestmodel.storecreatepay.StoreCreditDetailItem;


public class ConfirmCartResponseModel{

	@SerializedName("IsSuccess")
	private boolean isSuccess;

	@SerializedName("Years")
	private Object years;

	@SerializedName("AppliedPromoCodeDetail")
	private List<AppliedPromoCodeDetailItem> appliedPromoCodeDetail;

	@SerializedName("Message")
	private String message;

	@SerializedName("StoreCreditMessage")
	private String storeCreditMessage;

	@SerializedName("IsStoreCreditEnable")
	private boolean isStoreCreditEnable;

	@SerializedName("StoreCreditDetail")
	private List<StoreCreditDetailItem> storeCreditDetail;

	@SerializedName("PaymentLoginInformation")
	private Object paymentLoginInformation;

	@SerializedName("OrderNumber")
	private String orderNumber="";

	@SerializedName("OutOfStock")
	private boolean outOfStock;

	@SerializedName("BalanceStoreCredit")
	private String balanceStoreCredit;

	@SerializedName("Success")
	private Object success;

	@SerializedName("TotalStoreCreditAmount")
	private int totalStoreCreditAmount;

	@SerializedName("PaymentLoginMessage")
	private Object paymentLoginMessage;

	@SerializedName("TemporaryTransaction")
	private TemporaryTransaction temporaryTransaction;

	@SerializedName("PaymentGateway")
	private List<PaymentGatewayItem> paymentGateway;

	@SerializedName("GrandTotal")
	private int grandTotal;

	@SerializedName("CardTypes")
	private Object cardTypes;

	@SerializedName("Cartdetail")
	private Cartdetail cartdetail;

	@SerializedName("ProceedToPaymentGateway")
	private boolean proceedToPaymentGateway;

	@SerializedName("ShowPaymentPopupMessage")
	private String showPaymentPopupMessage;

	@SerializedName("IsFailure")
	private boolean isFailure;

	public void setIsSuccess(boolean isSuccess){
		this.isSuccess = isSuccess;
	}

	public boolean isIsSuccess(){
		return isSuccess;
	}

	public void setYears(Object years){
		this.years = years;
	}

	public Object getYears(){
		return years;
	}

	public void setAppliedPromoCodeDetail(List<AppliedPromoCodeDetailItem> appliedPromoCodeDetail){
		this.appliedPromoCodeDetail = appliedPromoCodeDetail;
	}

	public List<AppliedPromoCodeDetailItem> getAppliedPromoCodeDetail(){
		return appliedPromoCodeDetail;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStoreCreditMessage(String storeCreditMessage){
		this.storeCreditMessage = storeCreditMessage;
	}

	public String getStoreCreditMessage(){
		return storeCreditMessage;
	}

	public void setIsStoreCreditEnable(boolean isStoreCreditEnable){
		this.isStoreCreditEnable = isStoreCreditEnable;
	}

	public boolean isIsStoreCreditEnable(){
		return isStoreCreditEnable;
	}

	public void setStoreCreditDetail(List<StoreCreditDetailItem> storeCreditDetail){
		this.storeCreditDetail = storeCreditDetail;
	}

	public List<StoreCreditDetailItem> getStoreCreditDetail(){
		return storeCreditDetail;
	}

	public void setPaymentLoginInformation(Object paymentLoginInformation){
		this.paymentLoginInformation = paymentLoginInformation;
	}

	public Object getPaymentLoginInformation(){
		return paymentLoginInformation;
	}

	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public void setOutOfStock(boolean outOfStock){
		this.outOfStock = outOfStock;
	}

	public boolean isOutOfStock(){
		return outOfStock;
	}

	public void setBalanceStoreCredit(String balanceStoreCredit){
		this.balanceStoreCredit = balanceStoreCredit;
	}

	public String getBalanceStoreCredit(){
		return balanceStoreCredit;
	}

	public void setSuccess(Object success){
		this.success = success;
	}

	public Object getSuccess(){
		return success;
	}

	public void setTotalStoreCreditAmount(int totalStoreCreditAmount){
		this.totalStoreCreditAmount = totalStoreCreditAmount;
	}

	public int getTotalStoreCreditAmount(){
		return totalStoreCreditAmount;
	}

	public void setPaymentLoginMessage(Object paymentLoginMessage){
		this.paymentLoginMessage = paymentLoginMessage;
	}

	public Object getPaymentLoginMessage(){
		return paymentLoginMessage;
	}

	public void setTemporaryTransaction(TemporaryTransaction temporaryTransaction){
		this.temporaryTransaction = temporaryTransaction;
	}

	public TemporaryTransaction getTemporaryTransaction(){
		return temporaryTransaction;
	}

	public void setPaymentGateway(List<PaymentGatewayItem> paymentGateway){
		this.paymentGateway = paymentGateway;
	}

	public List<PaymentGatewayItem> getPaymentGateway(){
		return paymentGateway;
	}

	public void setGrandTotal(int grandTotal){
		this.grandTotal = grandTotal;
	}

	public int getGrandTotal(){
		return grandTotal;
	}

	public void setCardTypes(Object cardTypes){
		this.cardTypes = cardTypes;
	}

	public Object getCardTypes(){
		return cardTypes;
	}

	public void setCartdetail(Cartdetail cartdetail){
		this.cartdetail = cartdetail;
	}

	public Cartdetail getCartdetail(){
		return cartdetail;
	}

	public void setProceedToPaymentGateway(boolean proceedToPaymentGateway){
		this.proceedToPaymentGateway = proceedToPaymentGateway;
	}

	public boolean isProceedToPaymentGateway(){
		return proceedToPaymentGateway;
	}

	public void setShowPaymentPopupMessage(String showPaymentPopupMessage){
		this.showPaymentPopupMessage = showPaymentPopupMessage;
	}

	public String getShowPaymentPopupMessage(){
		return showPaymentPopupMessage;
	}

	public void setIsFailure(boolean isFailure){
		this.isFailure = isFailure;
	}

	public boolean isIsFailure(){
		return isFailure;
	}
}