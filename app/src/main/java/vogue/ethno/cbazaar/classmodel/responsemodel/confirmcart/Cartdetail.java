package vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Cartdetail{

	@SerializedName("Infomations")
	private Infomations infomations;

	@SerializedName("Commercial")
	private Commercial commercial;

	@SerializedName("CartSummaries")
	private CartSummaries cartSummaries;

	@SerializedName("EDSProducts")
	private Object eDSProducts;

	@SerializedName("InvalidProducts")
	private List<InvalidProductsItem> invalidProducts;

	@SerializedName("AssuredShippingInformation")
	private AssuredShippingInformation assuredShippingInformation;

	@SerializedName("ShippingCost")
	private ShippingCost shippingCost;

	@SerializedName("PromotionDetails")
	private List<PromotionDetailsItem> promotionDetails;

	@SerializedName("Occasions")
	private List<String> occasions;

	@SerializedName("CartItems")
	private List<CartItemsItem> cartItems;

	@SerializedName("NonEDSProducts")
	private List<NonEDSProductsItem> nonEDSProducts;

	public void setInfomations(Infomations infomations){
		this.infomations = infomations;
	}

	public Infomations getInfomations(){
		return infomations;
	}

	public void setCommercial(Commercial commercial){
		this.commercial = commercial;
	}

	public Commercial getCommercial(){
		return commercial;
	}

	public void setCartSummaries(CartSummaries cartSummaries){
		this.cartSummaries = cartSummaries;
	}

	public CartSummaries getCartSummaries(){
		return cartSummaries;
	}

	public void setEDSProducts(Object eDSProducts){
		this.eDSProducts = eDSProducts;
	}

	public Object getEDSProducts(){
		return eDSProducts;
	}

	public void setInvalidProducts(List<InvalidProductsItem> invalidProducts){
		this.invalidProducts = invalidProducts;
	}

	public List<InvalidProductsItem> getInvalidProducts(){
		return invalidProducts;
	}

	public void setAssuredShippingInformation(AssuredShippingInformation assuredShippingInformation){
		this.assuredShippingInformation = assuredShippingInformation;
	}

	public AssuredShippingInformation getAssuredShippingInformation(){
		return assuredShippingInformation;
	}

	public void setShippingCost(ShippingCost shippingCost){
		this.shippingCost = shippingCost;
	}

	public ShippingCost getShippingCost(){
		return shippingCost;
	}

	public void setPromotionDetails(List<PromotionDetailsItem> promotionDetails){
		this.promotionDetails = promotionDetails;
	}

	public List<PromotionDetailsItem> getPromotionDetails(){
		return promotionDetails;
	}

	public void setOccasions(List<String> occasions){
		this.occasions = occasions;
	}

	public List<String> getOccasions(){
		return occasions;
	}

	public void setCartItems(List<CartItemsItem> cartItems){
		this.cartItems = cartItems;
	}

	public List<CartItemsItem> getCartItems(){
		return cartItems;
	}

	public void setNonEDSProducts(List<NonEDSProductsItem> nonEDSProducts){
		this.nonEDSProducts = nonEDSProducts;
	}

	public List<NonEDSProductsItem> getNonEDSProducts(){
		return nonEDSProducts;
	}
}