package vogue.ethno.cbazaar.classmodel.requestmodel.storecreatepay;

 import com.google.gson.annotations.SerializedName;

 public class 	StoreCreditDetailItem{

	@SerializedName("LineBalanceAmount")
	private double lineBalanceAmount;

	@SerializedName("Type")
	private String type;

	@SerializedName("ExpiryDate")
	private String expiryDate;

	@SerializedName("LineCreditedAmount")
	private double lineCreditedAmount;

	@SerializedName("LineAmount")
	private double lineAmount;

	@SerializedName("Code")
	private int code;

	@SerializedName("Voucher")
	private String voucher;

	@SerializedName("IsChecked")
	private boolean isChecked;

	 public double getLineBalanceAmount() {
		 return lineBalanceAmount;
	 }

	 public void setLineBalanceAmount(double lineBalanceAmount) {
		 this.lineBalanceAmount = lineBalanceAmount;
	 }

	 public String getType() {
		 return type;
	 }

	 public void setType(String type) {
		 this.type = type;
	 }

	 public String getExpiryDate() {
		 return expiryDate;
	 }

	 public void setExpiryDate(String expiryDate) {
		 this.expiryDate = expiryDate;
	 }

	 public double getLineCreditedAmount() {
		 return lineCreditedAmount;
	 }

	 public void setLineCreditedAmount(double lineCreditedAmount) {
		 this.lineCreditedAmount = lineCreditedAmount;
	 }

	 public double getLineAmount() {
		 return lineAmount;
	 }

	 public void setLineAmount(double lineAmount) {
		 this.lineAmount = lineAmount;
	 }

	 public int getCode() {
		 return code;
	 }

	 public void setCode(int code) {
		 this.code = code;
	 }

	 public String getVoucher() {
		 return voucher;
	 }

	 public void setVoucher(String voucher) {
		 this.voucher = voucher;
	 }

	 public boolean isChecked() {
		 return isChecked;
	 }

	 public void setChecked(boolean checked) {
		 isChecked = checked;
	 }
 }