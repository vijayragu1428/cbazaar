package vogue.ethno.cbazaar.classmodel.responsemodel.topstyle;

import java.util.List;
 import com.google.gson.annotations.SerializedName;

 public class TopStyleResponse{

	@SerializedName("KameezStyle")
	private List<Object> kameezStyle;

	public void setKameezStyle(List<Object> kameezStyle){
		this.kameezStyle = kameezStyle;
	}

	public List<Object> getKameezStyle(){
		return kameezStyle;
	}
}