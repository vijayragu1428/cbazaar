package vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Infomations{

	@SerializedName("PromotionDiscountPercentage")
	private int promotionDiscountPercentage;

	@SerializedName("PromotionOfferMessage")
	private List<Object> promotionOfferMessage;

	@SerializedName("UniqueID")
	private Object uniqueID;

	@SerializedName("Alpha3CountryCode")
	private Object alpha3CountryCode;

	@SerializedName("Email")
	private Object email;

	@SerializedName("ProductDiscountPercentage")
	private int productDiscountPercentage;

	@SerializedName("EDSDescription")
	private String eDSDescription;

	@SerializedName("StatusCode")
	private Object statusCode;

	@SerializedName("DelivaryDate")
	private String delivaryDate;

	@SerializedName("CurrencySymbol")
	private String currencySymbol;

	@SerializedName("CurrencyCode")
	private String currencyCode;

	@SerializedName("IsRequestFromApp")
	private boolean isRequestFromApp;

	@SerializedName("AccessToken")
	private Object accessToken;

	@SerializedName("Applicable7DD")
	private String applicable7DD;

	@SerializedName("RfmValue")
	private Object rfmValue;

	@SerializedName("NextPercentageForStepSale")
	private int nextPercentageForStepSale;

	@SerializedName("ProductCode")
	private Object productCode;

	@SerializedName("IsEDSApplicable")
	private boolean isEDSApplicable;

	@SerializedName("DeliveryDescription")
	private Object deliveryDescription;

	@SerializedName("TimeStamp")
	private String timeStamp;

	@SerializedName("EStoreID")
	private int eStoreID;

	@SerializedName("UserBusinessType")
	private int userBusinessType;

	@SerializedName("IsGuideShopUser")
	private boolean isGuideShopUser;

	@SerializedName("InformationMessages")
	private List<InformationMessagesItem> informationMessages;

	@SerializedName("ShippingOfferMessage")
	private List<Object> shippingOfferMessage;

	@SerializedName("TopupDiscountPercentage")
	private int topupDiscountPercentage;

	@SerializedName("IsShowEDSFee")
	private boolean isShowEDSFee;

	@SerializedName("StepUpSaleProductCount")
	private int stepUpSaleProductCount;

	@SerializedName("DiscountType")
	private int discountType;

	@SerializedName("CustomerIP")
	private Object customerIP;

	@SerializedName("CustomizationDiscountType")
	private int customizationDiscountType;

	@SerializedName("Region")
	private Object region;

	@SerializedName("SessionID")
	private String sessionID;

	@SerializedName("RTSDeliveryDate")
	private String rTSDeliveryDate;

	@SerializedName("CurrencyFactor")
	private int currencyFactor;

	@SerializedName("MoveToWishlistMessage")
	private String moveToWishlistMessage;

	@SerializedName("Message")
	private Object message;

	@SerializedName("ProductCost")
	private int productCost;

	@SerializedName("AppSessionID")
	private Object appSessionID;

	@SerializedName("LoginSource")
	private Object loginSource;

	@SerializedName("EsCourierdays")
	private String esCourierdays;

	@SerializedName("AppVersion")
	private Object appVersion;

	@SerializedName("IsGetCartRequired")
	private boolean isGetCartRequired;

	@SerializedName("Is7DD")
	private boolean is7DD;

	@SerializedName("RemoveStitchedProductMessage")
	private String removeStitchedProductMessage;

	@SerializedName("UserID")
	private int userID;

	@SerializedName("ProductOriginalCost")
	private int productOriginalCost;

	@SerializedName("CountryCode")
	private Object countryCode;

	@SerializedName("NotApplicable7DD")
	private String notApplicable7DD;

	@SerializedName("QuantityForShippingcost")
	private int quantityForShippingcost;

	@SerializedName("HasCustomItems")
	private boolean hasCustomItems;

	@SerializedName("DeliveryDays")
	private int deliveryDays;

	@SerializedName("DeviceID")
	private Object deviceID;

	@SerializedName("PartialDeliveryDescriptionInfo")
	private Object partialDeliveryDescriptionInfo;

	@SerializedName("NonPartialDeliveryInfo")
	private Object nonPartialDeliveryInfo;

	@SerializedName("AppSource")
	private int appSource;

	@SerializedName("ShortlistCount")
	private int shortlistCount;

	@SerializedName("CustomizationPercentage")
	private int customizationPercentage;

	@SerializedName("IsCODEligible")
	private boolean isCODEligible;

	@SerializedName("CartQuantity")
	private int cartQuantity;

	public void setPromotionDiscountPercentage(int promotionDiscountPercentage){
		this.promotionDiscountPercentage = promotionDiscountPercentage;
	}

	public int getPromotionDiscountPercentage(){
		return promotionDiscountPercentage;
	}

	public void setPromotionOfferMessage(List<Object> promotionOfferMessage){
		this.promotionOfferMessage = promotionOfferMessage;
	}

	public List<Object> getPromotionOfferMessage(){
		return promotionOfferMessage;
	}

	public void setUniqueID(Object uniqueID){
		this.uniqueID = uniqueID;
	}

	public Object getUniqueID(){
		return uniqueID;
	}

	public void setAlpha3CountryCode(Object alpha3CountryCode){
		this.alpha3CountryCode = alpha3CountryCode;
	}

	public Object getAlpha3CountryCode(){
		return alpha3CountryCode;
	}

	public void setEmail(Object email){
		this.email = email;
	}

	public Object getEmail(){
		return email;
	}

	public void setProductDiscountPercentage(int productDiscountPercentage){
		this.productDiscountPercentage = productDiscountPercentage;
	}

	public int getProductDiscountPercentage(){
		return productDiscountPercentage;
	}

	public void setEDSDescription(String eDSDescription){
		this.eDSDescription = eDSDescription;
	}

	public String getEDSDescription(){
		return eDSDescription;
	}

	public void setStatusCode(Object statusCode){
		this.statusCode = statusCode;
	}

	public Object getStatusCode(){
		return statusCode;
	}

	public void setDelivaryDate(String delivaryDate){
		this.delivaryDate = delivaryDate;
	}

	public String getDelivaryDate(){
		return delivaryDate;
	}

	public void setCurrencySymbol(String currencySymbol){
		this.currencySymbol = currencySymbol;
	}

	public String getCurrencySymbol(){
		return currencySymbol;
	}

	public void setCurrencyCode(String currencyCode){
		this.currencyCode = currencyCode;
	}

	public String getCurrencyCode(){
		return currencyCode;
	}

	public void setIsRequestFromApp(boolean isRequestFromApp){
		this.isRequestFromApp = isRequestFromApp;
	}

	public boolean isIsRequestFromApp(){
		return isRequestFromApp;
	}

	public void setAccessToken(Object accessToken){
		this.accessToken = accessToken;
	}

	public Object getAccessToken(){
		return accessToken;
	}

	public void setApplicable7DD(String applicable7DD){
		this.applicable7DD = applicable7DD;
	}

	public String getApplicable7DD(){
		return applicable7DD;
	}

	public void setRfmValue(Object rfmValue){
		this.rfmValue = rfmValue;
	}

	public Object getRfmValue(){
		return rfmValue;
	}

	public void setNextPercentageForStepSale(int nextPercentageForStepSale){
		this.nextPercentageForStepSale = nextPercentageForStepSale;
	}

	public int getNextPercentageForStepSale(){
		return nextPercentageForStepSale;
	}

	public void setProductCode(Object productCode){
		this.productCode = productCode;
	}

	public Object getProductCode(){
		return productCode;
	}

	public void setIsEDSApplicable(boolean isEDSApplicable){
		this.isEDSApplicable = isEDSApplicable;
	}

	public boolean isIsEDSApplicable(){
		return isEDSApplicable;
	}

	public void setDeliveryDescription(Object deliveryDescription){
		this.deliveryDescription = deliveryDescription;
	}

	public Object getDeliveryDescription(){
		return deliveryDescription;
	}

	public void setTimeStamp(String timeStamp){
		this.timeStamp = timeStamp;
	}

	public String getTimeStamp(){
		return timeStamp;
	}

	public void setEStoreID(int eStoreID){
		this.eStoreID = eStoreID;
	}

	public int getEStoreID(){
		return eStoreID;
	}

	public void setUserBusinessType(int userBusinessType){
		this.userBusinessType = userBusinessType;
	}

	public int getUserBusinessType(){
		return userBusinessType;
	}

	public void setIsGuideShopUser(boolean isGuideShopUser){
		this.isGuideShopUser = isGuideShopUser;
	}

	public boolean isIsGuideShopUser(){
		return isGuideShopUser;
	}

	public void setInformationMessages(List<InformationMessagesItem> informationMessages){
		this.informationMessages = informationMessages;
	}

	public List<InformationMessagesItem> getInformationMessages(){
		return informationMessages;
	}

	public void setShippingOfferMessage(List<Object> shippingOfferMessage){
		this.shippingOfferMessage = shippingOfferMessage;
	}

	public List<Object> getShippingOfferMessage(){
		return shippingOfferMessage;
	}

	public void setTopupDiscountPercentage(int topupDiscountPercentage){
		this.topupDiscountPercentage = topupDiscountPercentage;
	}

	public int getTopupDiscountPercentage(){
		return topupDiscountPercentage;
	}

	public void setIsShowEDSFee(boolean isShowEDSFee){
		this.isShowEDSFee = isShowEDSFee;
	}

	public boolean isIsShowEDSFee(){
		return isShowEDSFee;
	}

	public void setStepUpSaleProductCount(int stepUpSaleProductCount){
		this.stepUpSaleProductCount = stepUpSaleProductCount;
	}

	public int getStepUpSaleProductCount(){
		return stepUpSaleProductCount;
	}

	public void setDiscountType(int discountType){
		this.discountType = discountType;
	}

	public int getDiscountType(){
		return discountType;
	}

	public void setCustomerIP(Object customerIP){
		this.customerIP = customerIP;
	}

	public Object getCustomerIP(){
		return customerIP;
	}

	public void setCustomizationDiscountType(int customizationDiscountType){
		this.customizationDiscountType = customizationDiscountType;
	}

	public int getCustomizationDiscountType(){
		return customizationDiscountType;
	}

	public void setRegion(Object region){
		this.region = region;
	}

	public Object getRegion(){
		return region;
	}

	public void setSessionID(String sessionID){
		this.sessionID = sessionID;
	}

	public String getSessionID(){
		return sessionID;
	}

	public void setRTSDeliveryDate(String rTSDeliveryDate){
		this.rTSDeliveryDate = rTSDeliveryDate;
	}

	public String getRTSDeliveryDate(){
		return rTSDeliveryDate;
	}

	public void setCurrencyFactor(int currencyFactor){
		this.currencyFactor = currencyFactor;
	}

	public int getCurrencyFactor(){
		return currencyFactor;
	}

	public void setMoveToWishlistMessage(String moveToWishlistMessage){
		this.moveToWishlistMessage = moveToWishlistMessage;
	}

	public String getMoveToWishlistMessage(){
		return moveToWishlistMessage;
	}

	public void setMessage(Object message){
		this.message = message;
	}

	public Object getMessage(){
		return message;
	}

	public void setProductCost(int productCost){
		this.productCost = productCost;
	}

	public int getProductCost(){
		return productCost;
	}

	public void setAppSessionID(Object appSessionID){
		this.appSessionID = appSessionID;
	}

	public Object getAppSessionID(){
		return appSessionID;
	}

	public void setLoginSource(Object loginSource){
		this.loginSource = loginSource;
	}

	public Object getLoginSource(){
		return loginSource;
	}

	public void setEsCourierdays(String esCourierdays){
		this.esCourierdays = esCourierdays;
	}

	public String getEsCourierdays(){
		return esCourierdays;
	}

	public void setAppVersion(Object appVersion){
		this.appVersion = appVersion;
	}

	public Object getAppVersion(){
		return appVersion;
	}

	public void setIsGetCartRequired(boolean isGetCartRequired){
		this.isGetCartRequired = isGetCartRequired;
	}

	public boolean isIsGetCartRequired(){
		return isGetCartRequired;
	}

	public void setIs7DD(boolean is7DD){
		this.is7DD = is7DD;
	}

	public boolean isIs7DD(){
		return is7DD;
	}

	public void setRemoveStitchedProductMessage(String removeStitchedProductMessage){
		this.removeStitchedProductMessage = removeStitchedProductMessage;
	}

	public String getRemoveStitchedProductMessage(){
		return removeStitchedProductMessage;
	}

	public void setUserID(int userID){
		this.userID = userID;
	}

	public int getUserID(){
		return userID;
	}

	public void setProductOriginalCost(int productOriginalCost){
		this.productOriginalCost = productOriginalCost;
	}

	public int getProductOriginalCost(){
		return productOriginalCost;
	}

	public void setCountryCode(Object countryCode){
		this.countryCode = countryCode;
	}

	public Object getCountryCode(){
		return countryCode;
	}

	public void setNotApplicable7DD(String notApplicable7DD){
		this.notApplicable7DD = notApplicable7DD;
	}

	public String getNotApplicable7DD(){
		return notApplicable7DD;
	}

	public void setQuantityForShippingcost(int quantityForShippingcost){
		this.quantityForShippingcost = quantityForShippingcost;
	}

	public int getQuantityForShippingcost(){
		return quantityForShippingcost;
	}

	public void setHasCustomItems(boolean hasCustomItems){
		this.hasCustomItems = hasCustomItems;
	}

	public boolean isHasCustomItems(){
		return hasCustomItems;
	}

	public void setDeliveryDays(int deliveryDays){
		this.deliveryDays = deliveryDays;
	}

	public int getDeliveryDays(){
		return deliveryDays;
	}

	public void setDeviceID(Object deviceID){
		this.deviceID = deviceID;
	}

	public Object getDeviceID(){
		return deviceID;
	}

	public void setPartialDeliveryDescriptionInfo(Object partialDeliveryDescriptionInfo){
		this.partialDeliveryDescriptionInfo = partialDeliveryDescriptionInfo;
	}

	public Object getPartialDeliveryDescriptionInfo(){
		return partialDeliveryDescriptionInfo;
	}

	public void setNonPartialDeliveryInfo(Object nonPartialDeliveryInfo){
		this.nonPartialDeliveryInfo = nonPartialDeliveryInfo;
	}

	public Object getNonPartialDeliveryInfo(){
		return nonPartialDeliveryInfo;
	}

	public void setAppSource(int appSource){
		this.appSource = appSource;
	}

	public int getAppSource(){
		return appSource;
	}

	public void setShortlistCount(int shortlistCount){
		this.shortlistCount = shortlistCount;
	}

	public int getShortlistCount(){
		return shortlistCount;
	}

	public void setCustomizationPercentage(int customizationPercentage){
		this.customizationPercentage = customizationPercentage;
	}

	public int getCustomizationPercentage(){
		return customizationPercentage;
	}

	public void setIsCODEligible(boolean isCODEligible){
		this.isCODEligible = isCODEligible;
	}

	public boolean isIsCODEligible(){
		return isCODEligible;
	}

	public void setCartQuantity(int cartQuantity){
		this.cartQuantity = cartQuantity;
	}

	public int getCartQuantity(){
		return cartQuantity;
	}
}