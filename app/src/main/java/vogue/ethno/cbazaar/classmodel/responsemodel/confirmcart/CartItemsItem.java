package vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class CartItemsItem{

	@SerializedName("ProductDiscountPercentage")
	private int productDiscountPercentage;

	@SerializedName("IsFreeShipProduct")
	private boolean isFreeShipProduct;

	@SerializedName("HasPartialShipping")
	private boolean hasPartialShipping;

	@SerializedName("ProductDesigner")
	private String productDesigner;

	@SerializedName("HasDelete")
	private boolean hasDelete;

	@SerializedName("IsRTSProduct")
	private boolean isRTSProduct;

	@SerializedName("DefaultDeliveryMode")
	private boolean defaultDeliveryMode;

	@SerializedName("PartialDeliveryCost")
	private int partialDeliveryCost;

	@SerializedName("DeliveryOptions")
	private List<DeliveryOptionsItem> deliveryOptions;

	@SerializedName("L4Name")
	private String l4Name;

	@SerializedName("ProductCode")
	private String productCode;

	@SerializedName("PromotionIcon")
	private String promotionIcon;

	@SerializedName("OutOfStockMessage")
	private Object outOfStockMessage;

	@SerializedName("IsEditQuantity")
	private boolean isEditQuantity;

	@SerializedName("ProductDiscount")
	private int productDiscount;

	@SerializedName("SKUSubTotal")
	private int sKUSubTotal;

	@SerializedName("IsCashBackProduct")
	private boolean isCashBackProduct;

	@SerializedName("DispatchDate")
	private String dispatchDate;

	@SerializedName("ProductAmount")
	private int productAmount;

	@SerializedName("TopupDiscountPercentage")
	private int topupDiscountPercentage;

	@SerializedName("PromotionTypeID")
	private int promotionTypeID;

	@SerializedName("CashbackDiscountAmount")
	private int cashbackDiscountAmount;

	@SerializedName("ShippingCost")
	private int shippingCost;

	@SerializedName("CartID")
	private int cartID;

	@SerializedName("AssuredShippingTitle")
	private Object assuredShippingTitle;

	@SerializedName("CashbackDiscountPercentage")
	private int cashbackDiscountPercentage;

	@SerializedName("ProductOrderType")
	private String productOrderType;

	@SerializedName("ProductQuantity")
	private int productQuantity;

	@SerializedName("EssentialDiscount")
	private int essentialDiscount;

	@SerializedName("EssentialNetAmount")
	private int essentialNetAmount;

	@SerializedName("SavingOfferMessage")
	private String savingOfferMessage;

	@SerializedName("ProductImage")
	private String productImage;

	@SerializedName("IsActive")
	private boolean isActive;

	@SerializedName("SKUNetAmount")
	private int sKUNetAmount;

	@SerializedName("IsMultiOffer")
	private boolean isMultiOffer;

	@SerializedName("DeductedMultiOfferQuantity")
	private int deductedMultiOfferQuantity;

	@SerializedName("MeasurementURL")
	private String measurementURL;

	@SerializedName("MultiOfferDiscountPercentage")
	private int multiOfferDiscountPercentage;

	@SerializedName("IsAssuredExpressActive")
	private boolean isAssuredExpressActive;

	@SerializedName("AssuredInformation")
	private Object assuredInformation;

	@SerializedName("ProductRate")
	private int productRate;

	@SerializedName("ProductBrand")
	private String productBrand;

	@SerializedName("IsMeasurementSubmitted")
	private boolean isMeasurementSubmitted;

	@SerializedName("IsPattern")
	private boolean isPattern;

	@SerializedName("SKUDiscount")
	private int sKUDiscount;

	@SerializedName("IsAssuredActive")
	private boolean isAssuredActive;

	@SerializedName("CashBackOfferMessage")
	private String cashBackOfferMessage;

	@SerializedName("ProductUniqueID")
	private String productUniqueID;

	@SerializedName("IsAssuredStanderdStrike")
	private boolean isAssuredStanderdStrike;

	@SerializedName("IsFabricProduct")
	private boolean isFabricProduct;

	@SerializedName("ProductNetAmount")
	private int productNetAmount;

	@SerializedName("Quantity")
	private List<String> quantity;

	@SerializedName("IsAssuredStanderdActive")
	private boolean isAssuredStanderdActive;

	@SerializedName("ProductVendorType")
	private String productVendorType;

	@SerializedName("SupplementaryItems")
	private List<SupplementaryItemsItem> supplementaryItems;

	@SerializedName("IsAssuredStrike")
	private boolean isAssuredStrike;

	@SerializedName("RefrencedPatternImage")
	private String refrencedPatternImage;

	@SerializedName("ProductSize")
	private String productSize;

	@SerializedName("EssentialGrossAmount")
	private int essentialGrossAmount;

	@SerializedName("TopupOfferMessage")
	private String topupOfferMessage;

	@SerializedName("TopupDiscountAmount")
	private int topupDiscountAmount;

	@SerializedName("ProductAvailableQuantity")
	private int productAvailableQuantity;

	@SerializedName("ProductTypeShortCode")
	private String productTypeShortCode;

	@SerializedName("ProductDescription")
	private String productDescription;

	@SerializedName("IsOutOfStock")
	private boolean isOutOfStock;

	public void setProductDiscountPercentage(int productDiscountPercentage){
		this.productDiscountPercentage = productDiscountPercentage;
	}

	public int getProductDiscountPercentage(){
		return productDiscountPercentage;
	}

	public void setIsFreeShipProduct(boolean isFreeShipProduct){
		this.isFreeShipProduct = isFreeShipProduct;
	}

	public boolean isIsFreeShipProduct(){
		return isFreeShipProduct;
	}

	public void setHasPartialShipping(boolean hasPartialShipping){
		this.hasPartialShipping = hasPartialShipping;
	}

	public boolean isHasPartialShipping(){
		return hasPartialShipping;
	}

	public void setProductDesigner(String productDesigner){
		this.productDesigner = productDesigner;
	}

	public String getProductDesigner(){
		return productDesigner;
	}

	public void setHasDelete(boolean hasDelete){
		this.hasDelete = hasDelete;
	}

	public boolean isHasDelete(){
		return hasDelete;
	}

	public void setIsRTSProduct(boolean isRTSProduct){
		this.isRTSProduct = isRTSProduct;
	}

	public boolean isIsRTSProduct(){
		return isRTSProduct;
	}

	public void setDefaultDeliveryMode(boolean defaultDeliveryMode){
		this.defaultDeliveryMode = defaultDeliveryMode;
	}

	public boolean isDefaultDeliveryMode(){
		return defaultDeliveryMode;
	}

	public void setPartialDeliveryCost(int partialDeliveryCost){
		this.partialDeliveryCost = partialDeliveryCost;
	}

	public int getPartialDeliveryCost(){
		return partialDeliveryCost;
	}

	public void setDeliveryOptions(List<DeliveryOptionsItem> deliveryOptions){
		this.deliveryOptions = deliveryOptions;
	}

	public List<DeliveryOptionsItem> getDeliveryOptions(){
		return deliveryOptions;
	}

	public void setL4Name(String l4Name){
		this.l4Name = l4Name;
	}

	public String getL4Name(){
		return l4Name;
	}

	public void setProductCode(String productCode){
		this.productCode = productCode;
	}

	public String getProductCode(){
		return productCode;
	}

	public void setPromotionIcon(String promotionIcon){
		this.promotionIcon = promotionIcon;
	}

	public String getPromotionIcon(){
		return promotionIcon;
	}

	public void setOutOfStockMessage(Object outOfStockMessage){
		this.outOfStockMessage = outOfStockMessage;
	}

	public Object getOutOfStockMessage(){
		return outOfStockMessage;
	}

	public void setIsEditQuantity(boolean isEditQuantity){
		this.isEditQuantity = isEditQuantity;
	}

	public boolean isIsEditQuantity(){
		return isEditQuantity;
	}

	public void setProductDiscount(int productDiscount){
		this.productDiscount = productDiscount;
	}

	public int getProductDiscount(){
		return productDiscount;
	}

	public void setSKUSubTotal(int sKUSubTotal){
		this.sKUSubTotal = sKUSubTotal;
	}

	public int getSKUSubTotal(){
		return sKUSubTotal;
	}

	public void setIsCashBackProduct(boolean isCashBackProduct){
		this.isCashBackProduct = isCashBackProduct;
	}

	public boolean isIsCashBackProduct(){
		return isCashBackProduct;
	}

	public void setDispatchDate(String dispatchDate){
		this.dispatchDate = dispatchDate;
	}

	public String getDispatchDate(){
		return dispatchDate;
	}

	public void setProductAmount(int productAmount){
		this.productAmount = productAmount;
	}

	public int getProductAmount(){
		return productAmount;
	}

	public void setTopupDiscountPercentage(int topupDiscountPercentage){
		this.topupDiscountPercentage = topupDiscountPercentage;
	}

	public int getTopupDiscountPercentage(){
		return topupDiscountPercentage;
	}

	public void setPromotionTypeID(int promotionTypeID){
		this.promotionTypeID = promotionTypeID;
	}

	public int getPromotionTypeID(){
		return promotionTypeID;
	}

	public void setCashbackDiscountAmount(int cashbackDiscountAmount){
		this.cashbackDiscountAmount = cashbackDiscountAmount;
	}

	public int getCashbackDiscountAmount(){
		return cashbackDiscountAmount;
	}

	public void setShippingCost(int shippingCost){
		this.shippingCost = shippingCost;
	}

	public int getShippingCost(){
		return shippingCost;
	}

	public void setCartID(int cartID){
		this.cartID = cartID;
	}

	public int getCartID(){
		return cartID;
	}

	public void setAssuredShippingTitle(Object assuredShippingTitle){
		this.assuredShippingTitle = assuredShippingTitle;
	}

	public Object getAssuredShippingTitle(){
		return assuredShippingTitle;
	}

	public void setCashbackDiscountPercentage(int cashbackDiscountPercentage){
		this.cashbackDiscountPercentage = cashbackDiscountPercentage;
	}

	public int getCashbackDiscountPercentage(){
		return cashbackDiscountPercentage;
	}

	public void setProductOrderType(String productOrderType){
		this.productOrderType = productOrderType;
	}

	public String getProductOrderType(){
		return productOrderType;
	}

	public void setProductQuantity(int productQuantity){
		this.productQuantity = productQuantity;
	}

	public int getProductQuantity(){
		return productQuantity;
	}

	public void setEssentialDiscount(int essentialDiscount){
		this.essentialDiscount = essentialDiscount;
	}

	public int getEssentialDiscount(){
		return essentialDiscount;
	}

	public void setEssentialNetAmount(int essentialNetAmount){
		this.essentialNetAmount = essentialNetAmount;
	}

	public int getEssentialNetAmount(){
		return essentialNetAmount;
	}

	public void setSavingOfferMessage(String savingOfferMessage){
		this.savingOfferMessage = savingOfferMessage;
	}

	public String getSavingOfferMessage(){
		return savingOfferMessage;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public void setSKUNetAmount(int sKUNetAmount){
		this.sKUNetAmount = sKUNetAmount;
	}

	public int getSKUNetAmount(){
		return sKUNetAmount;
	}

	public void setIsMultiOffer(boolean isMultiOffer){
		this.isMultiOffer = isMultiOffer;
	}

	public boolean isIsMultiOffer(){
		return isMultiOffer;
	}

	public void setDeductedMultiOfferQuantity(int deductedMultiOfferQuantity){
		this.deductedMultiOfferQuantity = deductedMultiOfferQuantity;
	}

	public int getDeductedMultiOfferQuantity(){
		return deductedMultiOfferQuantity;
	}

	public void setMeasurementURL(String measurementURL){
		this.measurementURL = measurementURL;
	}

	public String getMeasurementURL(){
		return measurementURL;
	}

	public void setMultiOfferDiscountPercentage(int multiOfferDiscountPercentage){
		this.multiOfferDiscountPercentage = multiOfferDiscountPercentage;
	}

	public int getMultiOfferDiscountPercentage(){
		return multiOfferDiscountPercentage;
	}

	public void setIsAssuredExpressActive(boolean isAssuredExpressActive){
		this.isAssuredExpressActive = isAssuredExpressActive;
	}

	public boolean isIsAssuredExpressActive(){
		return isAssuredExpressActive;
	}

	public void setAssuredInformation(Object assuredInformation){
		this.assuredInformation = assuredInformation;
	}

	public Object getAssuredInformation(){
		return assuredInformation;
	}

	public void setProductRate(int productRate){
		this.productRate = productRate;
	}

	public int getProductRate(){
		return productRate;
	}

	public void setProductBrand(String productBrand){
		this.productBrand = productBrand;
	}

	public String getProductBrand(){
		return productBrand;
	}

	public void setIsMeasurementSubmitted(boolean isMeasurementSubmitted){
		this.isMeasurementSubmitted = isMeasurementSubmitted;
	}

	public boolean isIsMeasurementSubmitted(){
		return isMeasurementSubmitted;
	}

	public void setIsPattern(boolean isPattern){
		this.isPattern = isPattern;
	}

	public boolean isIsPattern(){
		return isPattern;
	}

	public void setSKUDiscount(int sKUDiscount){
		this.sKUDiscount = sKUDiscount;
	}

	public int getSKUDiscount(){
		return sKUDiscount;
	}

	public void setIsAssuredActive(boolean isAssuredActive){
		this.isAssuredActive = isAssuredActive;
	}

	public boolean isIsAssuredActive(){
		return isAssuredActive;
	}

	public void setCashBackOfferMessage(String cashBackOfferMessage){
		this.cashBackOfferMessage = cashBackOfferMessage;
	}

	public String getCashBackOfferMessage(){
		return cashBackOfferMessage;
	}

	public void setProductUniqueID(String productUniqueID){
		this.productUniqueID = productUniqueID;
	}

	public String getProductUniqueID(){
		return productUniqueID;
	}

	public void setIsAssuredStanderdStrike(boolean isAssuredStanderdStrike){
		this.isAssuredStanderdStrike = isAssuredStanderdStrike;
	}

	public boolean isIsAssuredStanderdStrike(){
		return isAssuredStanderdStrike;
	}

	public void setIsFabricProduct(boolean isFabricProduct){
		this.isFabricProduct = isFabricProduct;
	}

	public boolean isIsFabricProduct(){
		return isFabricProduct;
	}

	public void setProductNetAmount(int productNetAmount){
		this.productNetAmount = productNetAmount;
	}

	public int getProductNetAmount(){
		return productNetAmount;
	}

	public void setQuantity(List<String> quantity){
		this.quantity = quantity;
	}

	public List<String> getQuantity(){
		return quantity;
	}

	public void setIsAssuredStanderdActive(boolean isAssuredStanderdActive){
		this.isAssuredStanderdActive = isAssuredStanderdActive;
	}

	public boolean isIsAssuredStanderdActive(){
		return isAssuredStanderdActive;
	}

	public void setProductVendorType(String productVendorType){
		this.productVendorType = productVendorType;
	}

	public String getProductVendorType(){
		return productVendorType;
	}

	public void setSupplementaryItems(List<SupplementaryItemsItem> supplementaryItems){
		this.supplementaryItems = supplementaryItems;
	}

	public List<SupplementaryItemsItem> getSupplementaryItems(){
		return supplementaryItems;
	}

	public void setIsAssuredStrike(boolean isAssuredStrike){
		this.isAssuredStrike = isAssuredStrike;
	}

	public boolean isIsAssuredStrike(){
		return isAssuredStrike;
	}

	public void setRefrencedPatternImage(String refrencedPatternImage){
		this.refrencedPatternImage = refrencedPatternImage;
	}

	public String getRefrencedPatternImage(){
		return refrencedPatternImage;
	}

	public void setProductSize(String productSize){
		this.productSize = productSize;
	}

	public String getProductSize(){
		return productSize;
	}

	public void setEssentialGrossAmount(int essentialGrossAmount){
		this.essentialGrossAmount = essentialGrossAmount;
	}

	public int getEssentialGrossAmount(){
		return essentialGrossAmount;
	}

	public void setTopupOfferMessage(String topupOfferMessage){
		this.topupOfferMessage = topupOfferMessage;
	}

	public String getTopupOfferMessage(){
		return topupOfferMessage;
	}

	public void setTopupDiscountAmount(int topupDiscountAmount){
		this.topupDiscountAmount = topupDiscountAmount;
	}

	public int getTopupDiscountAmount(){
		return topupDiscountAmount;
	}

	public void setProductAvailableQuantity(int productAvailableQuantity){
		this.productAvailableQuantity = productAvailableQuantity;
	}

	public int getProductAvailableQuantity(){
		return productAvailableQuantity;
	}

	public void setProductTypeShortCode(String productTypeShortCode){
		this.productTypeShortCode = productTypeShortCode;
	}

	public String getProductTypeShortCode(){
		return productTypeShortCode;
	}

	public void setProductDescription(String productDescription){
		this.productDescription = productDescription;
	}

	public String getProductDescription(){
		return productDescription;
	}

	public void setIsOutOfStock(boolean isOutOfStock){
		this.isOutOfStock = isOutOfStock;
	}

	public boolean isIsOutOfStock(){
		return isOutOfStock;
	}
}