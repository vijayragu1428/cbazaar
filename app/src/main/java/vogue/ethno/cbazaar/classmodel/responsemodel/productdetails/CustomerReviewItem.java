package vogue.ethno.cbazaar.classmodel.responsemodel.productdetails;

 import com.google.gson.annotations.SerializedName;

 import java.io.Serializable;

public class CustomerReviewItem implements Serializable {

	@SerializedName("SerialNo")
	private int serialNo;

	@SerializedName("FeedbackType")
	private String feedbackType;

	@SerializedName("UserName")
	private String userName;

	@SerializedName("FeedBackImage")
	private String feedBackImage;

	@SerializedName("Feedback")
	private String feedback;

	@SerializedName("FeedbackDate")
	private String feedbackDate;

	@SerializedName("RatingOn")
	private int ratingOn;

	@SerializedName("ProductImage")
	private String productImage;

	@SerializedName("RatingName")
	private String ratingName;

	@SerializedName("RatingOff")
	private int ratingOff;

	@SerializedName("UserPlace")
	private String userPlace;

	@SerializedName("Image")
	private String image;

	public void setSerialNo(int serialNo){
		this.serialNo = serialNo;
	}

	public int getSerialNo(){
		return serialNo;
	}

	public void setFeedbackType(String feedbackType){
		this.feedbackType = feedbackType;
	}

	public String getFeedbackType(){
		return feedbackType;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setFeedBackImage(String feedBackImage){
		this.feedBackImage = feedBackImage;
	}

	public String getFeedBackImage(){
		return feedBackImage;
	}

	public void setFeedback(String feedback){
		this.feedback = feedback;
	}

	public String getFeedback(){
		return feedback;
	}

	public void setFeedbackDate(String feedbackDate){
		this.feedbackDate = feedbackDate;
	}

	public String getFeedbackDate(){
		return feedbackDate;
	}

	public void setRatingOn(int ratingOn){
		this.ratingOn = ratingOn;
	}

	public int getRatingOn(){
		return ratingOn;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setRatingName(String ratingName){
		this.ratingName = ratingName;
	}

	public String getRatingName(){
		return ratingName;
	}

	public void setRatingOff(int ratingOff){
		this.ratingOff = ratingOff;
	}

	public int getRatingOff(){
		return ratingOff;
	}

	public void setUserPlace(String userPlace){
		this.userPlace = userPlace;
	}

	public String getUserPlace(){
		return userPlace;
	}

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}
}