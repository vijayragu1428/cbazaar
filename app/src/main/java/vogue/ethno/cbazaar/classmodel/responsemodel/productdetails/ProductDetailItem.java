package vogue.ethno.cbazaar.classmodel.responsemodel.productdetails;

 import com.google.gson.annotations.SerializedName;

 import java.io.Serializable;

public class ProductDetailItem implements Serializable {

	@SerializedName("currencyType")
	private String currencyType;

	@SerializedName("New")
	private String news;

	@SerializedName("Occasion")
	private String occasion;

	@SerializedName("IsReadyToShip")
	private boolean isReadyToShip;

	@SerializedName("KameezAlterable")
	private String kameezAlterable;

	@SerializedName("BusinessType")
	private String businessType;

	@SerializedName("IsAnimation")
	private String isAnimation;

	@SerializedName("DeliveryDate")
	private String deliveryDate;

	@SerializedName("Gender")
	private String gender;

	@SerializedName("LowerThumbImage")
	private String lowerThumbImage;

	@SerializedName("VisitCount")
	private int visitCount;

	@SerializedName("CurrencySymbol")
	private String currencySymbol;

	@SerializedName("Name")
	private String name;

	@SerializedName("IsPatternProduct")
	private boolean isPatternProduct;

	@SerializedName("MaxDeliveryDays")
	private int maxDeliveryDays;

	@SerializedName("OfferImageUrl")
	private String offerImageUrl;

	@SerializedName("UnitOfLabel")
	private String unitOfLabel;

	@SerializedName("SizeChartImg")
	private String sizeChartImg;

	@SerializedName("HasFlavour")
	private String hasFlavour;

	@SerializedName("MaximumSizeChart")
	private String maximumSizeChart;

	@SerializedName("GifLargeImage")
	private String gifLargeImage;

	@SerializedName("YardMessage")
	private Object yardMessage;

	@SerializedName("Color")
	private String color;

	@SerializedName("ImageUrl")
	private String imageUrl;

	@SerializedName("SupplierLeadDays")
	private int supplierLeadDays;

	@SerializedName("OutOfStock")
	private String outOfStock;

	@SerializedName("UpperThumbImage")
	private String upperThumbImage;

	@SerializedName("Code")
	private String code;

	@SerializedName("PromotionsIcon")
	private String promotionsIcon;

	@SerializedName("SuperClassification")
	private String superClassification;

	@SerializedName("IsInWishlist")
	private boolean isInWishlist;

	@SerializedName("Brand")
	private String brand;

	@SerializedName("Specialty")
	private String specialty;

	@SerializedName("TVSerial")
	private String tVSerial;

	@SerializedName("PromotionTypeID")
	private int promotionTypeID;

	@SerializedName("HasColor")
	private String hasColor;

	@SerializedName("CustLeadDays")
	private int custLeadDays;

	@SerializedName("Fabric")
	private String fabric;

	@SerializedName("HasCity")
	private String hasCity;

	@SerializedName("FreeShip")
	private String freeShip;

	@SerializedName("DummySampleCost")
	private int dummySampleCost;

	@SerializedName("Discount")
	private int discount;

	@SerializedName("ArtStyle")
	private String artStyle;

	@SerializedName("VideoPath")
	private String videoPath;

	@SerializedName("Films")
	private String films;

	@SerializedName("Promotions")
	private String promotions;

	@SerializedName("IsActive")
	private String isActive;

	@SerializedName("StockQuantity")
	private int stockQuantity;

	@SerializedName("Rating")
	private int rating;

	@SerializedName("TypeShortCode")
	private String typeShortCode;

	@SerializedName("DiscountCost")
	private int discountCost;

	@SerializedName("IsFabricPatternProduct")
	private boolean isFabricPatternProduct;

	@SerializedName("Cost")
	private double cost;

	@SerializedName("Bufferdays")
	private int bufferdays;

	@SerializedName("Designer")
	private String designer;

	@SerializedName("IsSupplier")
	private String isSupplier;

	@SerializedName("Is7DD")
	private int is7DD;

	@SerializedName("AddCost")
	private int addCost;

	@SerializedName("InSufficientSupplierStockQuantity")
	private int inSufficientSupplierStockQuantity;

	@SerializedName("UpperLargeImage")
	private String upperLargeImage;

	@SerializedName("KameezMaxLength")
	private String kameezMaxLength;

	@SerializedName("Style")
	private String style;

	@SerializedName("HasSize")
	private String hasSize;

	@SerializedName("MinimumSizeChart")
	private String minimumSizeChart;

	@SerializedName("SmallImage")
	private String smallImage;

	@SerializedName("LowerLargeImageUrl")
	private String lowerLargeImageUrl;

	@SerializedName("Nature")
	private String nature;

	@SerializedName("CashBackAmt")
	private int cashBackAmt;

	@SerializedName("DeliveryDays")
	private int deliveryDays;

	@SerializedName("GifSmallImage")
	private String gifSmallImage;

	@SerializedName("HasQuantityDisplay")
	private String hasQuantityDisplay;

	@SerializedName("BufferStockQuantity")
	private int bufferStockQuantity;

	@SerializedName("OffCost")
	private int offCost;

	@SerializedName("KameezMinLength")
	private String kameezMinLength;

	@SerializedName("DetailUrl")
	private String detailUrl;

	@SerializedName("ProductID")
	private int productID;

	@SerializedName("SupplyDays")
	private int supplyDays;

	@SerializedName("Type")
	private String type;

	@SerializedName("CanonicalUrl")
	private String canonicalUrl;

	@SerializedName("VendorType")
	private String vendorType;

	@SerializedName("LargeImageUrl")
	private String largeImageUrl;

	@SerializedName("LowerLargeImage")
	private String lowerLargeImage;

	@SerializedName("DesignNumber")
	private String designNumber;

	@SerializedName("EsCourierDays")
	private String esCourierDays;

	@SerializedName("Delivery")
	private String delivery;

	public void setCurrencyType(String currencyType){
		this.currencyType = currencyType;
	}

	public String getCurrencyType(){
		return currencyType;
	}

	public void setNew(String news){
		this.news = news;
	}

	public String getNew(){
		return news;
	}

	public void setOccasion(String occasion){
		this.occasion = occasion;
	}

	public String getOccasion(){
		return occasion;
	}

	public void setIsReadyToShip(boolean isReadyToShip){
		this.isReadyToShip = isReadyToShip;
	}

	public boolean isIsReadyToShip(){
		return isReadyToShip;
	}

	public void setKameezAlterable(String kameezAlterable){
		this.kameezAlterable = kameezAlterable;
	}

	public String getKameezAlterable(){
		return kameezAlterable;
	}

	public void setBusinessType(String businessType){
		this.businessType = businessType;
	}

	public String getBusinessType(){
		return businessType;
	}

	public void setIsAnimation(String isAnimation){
		this.isAnimation = isAnimation;
	}

	public String getIsAnimation(){
		return isAnimation;
	}

	public void setDeliveryDate(String deliveryDate){
		this.deliveryDate = deliveryDate;
	}

	public String getDeliveryDate(){
		return deliveryDate;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setLowerThumbImage(String lowerThumbImage){
		this.lowerThumbImage = lowerThumbImage;
	}

	public String getLowerThumbImage(){
		return lowerThumbImage;
	}

	public void setVisitCount(int visitCount){
		this.visitCount = visitCount;
	}

	public int getVisitCount(){
		return visitCount;
	}

	public void setCurrencySymbol(String currencySymbol){
		this.currencySymbol = currencySymbol;
	}

	public String getCurrencySymbol(){
		return currencySymbol;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setIsPatternProduct(boolean isPatternProduct){
		this.isPatternProduct = isPatternProduct;
	}

	public boolean isIsPatternProduct(){
		return isPatternProduct;
	}

	public void setMaxDeliveryDays(int maxDeliveryDays){
		this.maxDeliveryDays = maxDeliveryDays;
	}

	public int getMaxDeliveryDays(){
		return maxDeliveryDays;
	}

	public void setOfferImageUrl(String offerImageUrl){
		this.offerImageUrl = offerImageUrl;
	}

	public String getOfferImageUrl(){
		return offerImageUrl;
	}

	public void setUnitOfLabel(String unitOfLabel){
		this.unitOfLabel = unitOfLabel;
	}

	public String getUnitOfLabel(){
		return unitOfLabel;
	}

	public void setSizeChartImg(String sizeChartImg){
		this.sizeChartImg = sizeChartImg;
	}

	public String getSizeChartImg(){
		return sizeChartImg;
	}

	public void setHasFlavour(String hasFlavour){
		this.hasFlavour = hasFlavour;
	}

	public String getHasFlavour(){
		return hasFlavour;
	}

	public void setMaximumSizeChart(String maximumSizeChart){
		this.maximumSizeChart = maximumSizeChart;
	}

	public String getMaximumSizeChart(){
		return maximumSizeChart;
	}

	public void setGifLargeImage(String gifLargeImage){
		this.gifLargeImage = gifLargeImage;
	}

	public String getGifLargeImage(){
		return gifLargeImage;
	}

	public void setYardMessage(Object yardMessage){
		this.yardMessage = yardMessage;
	}

	public Object getYardMessage(){
		return yardMessage;
	}

	public void setColor(String color){
		this.color = color;
	}

	public String getColor(){
		return color;
	}

	public void setImageUrl(String imageUrl){
		this.imageUrl = imageUrl;
	}

	public String getImageUrl(){
		return imageUrl;
	}

	public void setSupplierLeadDays(int supplierLeadDays){
		this.supplierLeadDays = supplierLeadDays;
	}

	public int getSupplierLeadDays(){
		return supplierLeadDays;
	}

	public void setOutOfStock(String outOfStock){
		this.outOfStock = outOfStock;
	}

	public String getOutOfStock(){
		return outOfStock;
	}

	public void setUpperThumbImage(String upperThumbImage){
		this.upperThumbImage = upperThumbImage;
	}

	public String getUpperThumbImage(){
		return upperThumbImage;
	}

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setPromotionsIcon(String promotionsIcon){
		this.promotionsIcon = promotionsIcon;
	}

	public String getPromotionsIcon(){
		return promotionsIcon;
	}

	public void setSuperClassification(String superClassification){
		this.superClassification = superClassification;
	}

	public String getSuperClassification(){
		return superClassification;
	}

	public void setIsInWishlist(boolean isInWishlist){
		this.isInWishlist = isInWishlist;
	}

	public boolean isIsInWishlist(){
		return isInWishlist;
	}

	public void setBrand(String brand){
		this.brand = brand;
	}

	public String getBrand(){
		return brand;
	}

	public void setSpecialty(String specialty){
		this.specialty = specialty;
	}

	public String getSpecialty(){
		return specialty;
	}

	public void setTVSerial(String tVSerial){
		this.tVSerial = tVSerial;
	}

	public String getTVSerial(){
		return tVSerial;
	}

	public void setPromotionTypeID(int promotionTypeID){
		this.promotionTypeID = promotionTypeID;
	}

	public int getPromotionTypeID(){
		return promotionTypeID;
	}

	public void setHasColor(String hasColor){
		this.hasColor = hasColor;
	}

	public String getHasColor(){
		return hasColor;
	}

	public void setCustLeadDays(int custLeadDays){
		this.custLeadDays = custLeadDays;
	}

	public int getCustLeadDays(){
		return custLeadDays;
	}

	public void setFabric(String fabric){
		this.fabric = fabric;
	}

	public String getFabric(){
		return fabric;
	}

	public void setHasCity(String hasCity){
		this.hasCity = hasCity;
	}

	public String getHasCity(){
		return hasCity;
	}

	public void setFreeShip(String freeShip){
		this.freeShip = freeShip;
	}

	public String getFreeShip(){
		return freeShip;
	}

	public void setDummySampleCost(int dummySampleCost){
		this.dummySampleCost = dummySampleCost;
	}

	public int getDummySampleCost(){
		return dummySampleCost;
	}

	public void setDiscount(int discount){
		this.discount = discount;
	}

	public int getDiscount(){
		return discount;
	}

	public void setArtStyle(String artStyle){
		this.artStyle = artStyle;
	}

	public String getArtStyle(){
		return artStyle;
	}

	public void setVideoPath(String videoPath){
		this.videoPath = videoPath;
	}

	public String getVideoPath(){
		return videoPath;
	}

	public void setFilms(String films){
		this.films = films;
	}

	public String getFilms(){
		return films;
	}

	public void setPromotions(String promotions){
		this.promotions = promotions;
	}

	public String getPromotions(){
		return promotions;
	}

	public void setIsActive(String isActive){
		this.isActive = isActive;
	}

	public String getIsActive(){
		return isActive;
	}

	public void setStockQuantity(int stockQuantity){
		this.stockQuantity = stockQuantity;
	}

	public int getStockQuantity(){
		return stockQuantity;
	}

	public void setRating(int rating){
		this.rating = rating;
	}

	public int getRating(){
		return rating;
	}

	public void setTypeShortCode(String typeShortCode){
		this.typeShortCode = typeShortCode;
	}

	public String getTypeShortCode(){
		return typeShortCode;
	}

	public void setDiscountCost(int discountCost){
		this.discountCost = discountCost;
	}

	public int getDiscountCost(){
		return discountCost;
	}

	public void setIsFabricPatternProduct(boolean isFabricPatternProduct){
		this.isFabricPatternProduct = isFabricPatternProduct;
	}

	public boolean isIsFabricPatternProduct(){
		return isFabricPatternProduct;
	}

	public void setCost(double cost){
		this.cost = cost;
	}

	public double getCost(){
		return cost;
	}

	public void setBufferdays(int bufferdays){
		this.bufferdays = bufferdays;
	}

	public int getBufferdays(){
		return bufferdays;
	}

	public void setDesigner(String designer){
		this.designer = designer;
	}

	public String getDesigner(){
		return designer;
	}

	public void setIsSupplier(String isSupplier){
		this.isSupplier = isSupplier;
	}

	public String getIsSupplier(){
		return isSupplier;
	}

	public void setIs7DD(int is7DD){
		this.is7DD = is7DD;
	}

	public int getIs7DD(){
		return is7DD;
	}

	public void setAddCost(int addCost){
		this.addCost = addCost;
	}

	public int getAddCost(){
		return addCost;
	}

	public void setInSufficientSupplierStockQuantity(int inSufficientSupplierStockQuantity){
		this.inSufficientSupplierStockQuantity = inSufficientSupplierStockQuantity;
	}

	public int getInSufficientSupplierStockQuantity(){
		return inSufficientSupplierStockQuantity;
	}

	public void setUpperLargeImage(String upperLargeImage){
		this.upperLargeImage = upperLargeImage;
	}

	public String getUpperLargeImage(){
		return upperLargeImage;
	}

	public void setKameezMaxLength(String kameezMaxLength){
		this.kameezMaxLength = kameezMaxLength;
	}

	public String getKameezMaxLength(){
		return kameezMaxLength;
	}

	public void setStyle(String style){
		this.style = style;
	}

	public String getStyle(){
		return style;
	}

	public void setHasSize(String hasSize){
		this.hasSize = hasSize;
	}

	public String getHasSize(){
		return hasSize;
	}

	public void setMinimumSizeChart(String minimumSizeChart){
		this.minimumSizeChart = minimumSizeChart;
	}

	public String getMinimumSizeChart(){
		return minimumSizeChart;
	}

	public void setSmallImage(String smallImage){
		this.smallImage = smallImage;
	}

	public String getSmallImage(){
		return smallImage;
	}

	public void setLowerLargeImageUrl(String lowerLargeImageUrl){
		this.lowerLargeImageUrl = lowerLargeImageUrl;
	}

	public String getLowerLargeImageUrl(){
		return lowerLargeImageUrl;
	}

	public void setNature(String nature){
		this.nature = nature;
	}

	public String getNature(){
		return nature;
	}

	public void setCashBackAmt(int cashBackAmt){
		this.cashBackAmt = cashBackAmt;
	}

	public int getCashBackAmt(){
		return cashBackAmt;
	}

	public void setDeliveryDays(int deliveryDays){
		this.deliveryDays = deliveryDays;
	}

	public int getDeliveryDays(){
		return deliveryDays;
	}

	public void setGifSmallImage(String gifSmallImage){
		this.gifSmallImage = gifSmallImage;
	}

	public String getGifSmallImage(){
		return gifSmallImage;
	}

	public void setHasQuantityDisplay(String hasQuantityDisplay){
		this.hasQuantityDisplay = hasQuantityDisplay;
	}

	public String getHasQuantityDisplay(){
		return hasQuantityDisplay;
	}

	public void setBufferStockQuantity(int bufferStockQuantity){
		this.bufferStockQuantity = bufferStockQuantity;
	}

	public int getBufferStockQuantity(){
		return bufferStockQuantity;
	}

	public void setOffCost(int offCost){
		this.offCost = offCost;
	}

	public int getOffCost(){
		return offCost;
	}

	public void setKameezMinLength(String kameezMinLength){
		this.kameezMinLength = kameezMinLength;
	}

	public String getKameezMinLength(){
		return kameezMinLength;
	}

	public void setDetailUrl(String detailUrl){
		this.detailUrl = detailUrl;
	}

	public String getDetailUrl(){
		return detailUrl;
	}

	public void setProductID(int productID){
		this.productID = productID;
	}

	public int getProductID(){
		return productID;
	}

	public void setSupplyDays(int supplyDays){
		this.supplyDays = supplyDays;
	}

	public int getSupplyDays(){
		return supplyDays;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setCanonicalUrl(String canonicalUrl){
		this.canonicalUrl = canonicalUrl;
	}

	public String getCanonicalUrl(){
		return canonicalUrl;
	}

	public void setVendorType(String vendorType){
		this.vendorType = vendorType;
	}

	public String getVendorType(){
		return vendorType;
	}

	public void setLargeImageUrl(String largeImageUrl){
		this.largeImageUrl = largeImageUrl;
	}

	public String getLargeImageUrl(){
		return largeImageUrl;
	}

	public void setLowerLargeImage(String lowerLargeImage){
		this.lowerLargeImage = lowerLargeImage;
	}

	public String getLowerLargeImage(){
		return lowerLargeImage;
	}

	public void setDesignNumber(String designNumber){
		this.designNumber = designNumber;
	}

	public String getDesignNumber(){
		return designNumber;
	}

	public void setEsCourierDays(String esCourierDays){
		this.esCourierDays = esCourierDays;
	}

	public String getEsCourierDays(){
		return esCourierDays;
	}

	public void setDelivery(String delivery){
		this.delivery = delivery;
	}

	public String getDelivery(){
		return delivery;
	}
}