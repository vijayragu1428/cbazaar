package vogue.ethno.cbazaar.classmodel.responsemodel.storecreate;

import java.util.List;
 import com.google.gson.annotations.SerializedName;

 public class StoreCreateResponse{

	@SerializedName("IsSuccess")
	private boolean isSuccess;

	@SerializedName("StoreCreditDiscount")
	private double storeCreditDiscount;

	@SerializedName("Message")
	private String message;

	@SerializedName("StoreCreditMessage")
	private String storeCreditMessage;

	@SerializedName("StoreCreditDetail")
	private Object storeCreditDetail;

	@SerializedName("StoreCreditResponseDetails")
	private List<Object> storeCreditResponseDetails;

	@SerializedName("BalanceStoreCredit")
	private String balanceStoreCredit;

	@SerializedName("Success")
	private Object success;

	@SerializedName("PayableAmount")
	private double payableAmount;

	@SerializedName("Url")
	private Object url;

	@SerializedName("GrandTotal")
	private double grandTotal;

	@SerializedName("ProceedToPaymentGateway")
	private boolean proceedToPaymentGateway;

	@SerializedName("UsedStoreCreditAmount")
	private double usedStoreCreditAmount;

	@SerializedName("IsFailure")
	private boolean isFailure;

	public void setIsSuccess(boolean isSuccess){
		this.isSuccess = isSuccess;
	}

	public boolean isIsSuccess(){
		return isSuccess;
	}

	public void setStoreCreditDiscount(double storeCreditDiscount){
		this.storeCreditDiscount = storeCreditDiscount;
	}

	public double getStoreCreditDiscount(){
		return storeCreditDiscount;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStoreCreditMessage(String storeCreditMessage){
		this.storeCreditMessage = storeCreditMessage;
	}

	public String getStoreCreditMessage(){
		return storeCreditMessage;
	}

	public void setStoreCreditDetail(Object storeCreditDetail){
		this.storeCreditDetail = storeCreditDetail;
	}

	public Object getStoreCreditDetail(){
		return storeCreditDetail;
	}

	public void setStoreCreditResponseDetails(List<Object> storeCreditResponseDetails){
		this.storeCreditResponseDetails = storeCreditResponseDetails;
	}

	public List<Object> getStoreCreditResponseDetails(){
		return storeCreditResponseDetails;
	}

	public void setBalanceStoreCredit(String balanceStoreCredit){
		this.balanceStoreCredit = balanceStoreCredit;
	}

	public String getBalanceStoreCredit(){
		return balanceStoreCredit;
	}

	public void setSuccess(Object success){
		this.success = success;
	}

	public Object getSuccess(){
		return success;
	}

	public void setPayableAmount(double payableAmount){
		this.payableAmount = payableAmount;
	}

	public double getPayableAmount(){
		return payableAmount;
	}

	public void setUrl(Object url){
		this.url = url;
	}

	public Object getUrl(){
		return url;
	}

	public void setGrandTotal(double grandTotal){
		this.grandTotal = grandTotal;
	}

	public double getGrandTotal(){
		return grandTotal;
	}

	public void setProceedToPaymentGateway(boolean proceedToPaymentGateway){
		this.proceedToPaymentGateway = proceedToPaymentGateway;
	}

	public boolean isProceedToPaymentGateway(){
		return proceedToPaymentGateway;
	}

	public void setUsedStoreCreditAmount(double usedStoreCreditAmount){
		this.usedStoreCreditAmount = usedStoreCreditAmount;
	}

	public double getUsedStoreCreditAmount(){
		return usedStoreCreditAmount;
	}

	public void setIsFailure(boolean isFailure){
		this.isFailure = isFailure;
	}

	public boolean isIsFailure(){
		return isFailure;
	}
}