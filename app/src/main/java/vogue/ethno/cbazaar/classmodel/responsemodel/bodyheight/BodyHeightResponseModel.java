package vogue.ethno.cbazaar.classmodel.responsemodel.bodyheight;

import java.util.List;
 import com.google.gson.annotations.SerializedName;

 public class BodyHeightResponseModel {

	@SerializedName("CashBackAmount")
	private int cashBackAmount;

	@SerializedName("MeasurementFeet")
	private List<MeasurementFeetItem> measurementFeet;

	@SerializedName("TopStyle")
	private List<Object> topStyle;

	@SerializedName("ReadyToShip")
	private boolean readyToShip;

	@SerializedName("DispatchDate")
	private String dispatchDate;

	@SerializedName("DiscountPercentage")
	private String discountPercentage;

	@SerializedName("OfferMessage")
	private Object offerMessage;

	@SerializedName("DiscountPrice")
	private String discountPrice;

	@SerializedName("Quantity")
	private List<String> quantity;

	@SerializedName("IsTopStyle")
	private boolean isTopStyle;

	@SerializedName("TotalAmount")
	private String totalAmount;

	@SerializedName("DeliveryDateNote")
	private String deliveryDateNote;

	public void setCashBackAmount(int cashBackAmount){
		this.cashBackAmount = cashBackAmount;
	}

	public int getCashBackAmount(){
		return cashBackAmount;
	}

	public void setMeasurementFeet(List<MeasurementFeetItem> measurementFeet){
		this.measurementFeet = measurementFeet;
	}

	public List<MeasurementFeetItem> getMeasurementFeet(){
		return measurementFeet;
	}

	public void setTopStyle(List<Object> topStyle){
		this.topStyle = topStyle;
	}

	public List<Object> getTopStyle(){
		return topStyle;
	}

	public void setReadyToShip(boolean readyToShip){
		this.readyToShip = readyToShip;
	}

	public boolean isReadyToShip(){
		return readyToShip;
	}

	public void setDispatchDate(String dispatchDate){
		this.dispatchDate = dispatchDate;
	}

	public String getDispatchDate(){
		return dispatchDate;
	}

	public void setDiscountPercentage(String discountPercentage){
		this.discountPercentage = discountPercentage;
	}

	public String getDiscountPercentage(){
		return discountPercentage;
	}

	public void setOfferMessage(Object offerMessage){
		this.offerMessage = offerMessage;
	}

	public Object getOfferMessage(){
		return offerMessage;
	}

	public void setDiscountPrice(String discountPrice){
		this.discountPrice = discountPrice;
	}

	public String getDiscountPrice(){
		return discountPrice;
	}

	public void setQuantity(List<String> quantity){
		this.quantity = quantity;
	}

	public List<String> getQuantity(){
		return quantity;
	}

	public void setIsTopStyle(boolean isTopStyle){
		this.isTopStyle = isTopStyle;
	}

	public boolean isIsTopStyle(){
		return isTopStyle;
	}

	public void setTotalAmount(String totalAmount){
		this.totalAmount = totalAmount;
	}

	public String getTotalAmount(){
		return totalAmount;
	}

	public void setDeliveryDateNote(String deliveryDateNote){
		this.deliveryDateNote = deliveryDateNote;
	}

	public String getDeliveryDateNote(){
		return deliveryDateNote;
	}
}