package vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails;

import java.util.List;
 import com.google.gson.annotations.SerializedName;

 public class GetCartResponseModel {

	@SerializedName("IsSuccess")
	private boolean isSuccess;

	@SerializedName("CartSummaries")
	private CartSummaries cartSummaries;

	@SerializedName("Message")
	private List<String> message;

	@SerializedName("EDSProducts")
	private List<Object> eDSProducts;

	@SerializedName("Occasions")
	private List<String> occasions;

	@SerializedName("Information")
	private Information information;

	@SerializedName("Commercial")
	private Commercial commercial;

	@SerializedName("AssuredShippingDetails")
	private AssuredShippingDetails assuredShippingDetails;

	@SerializedName("Command")
	private int command;

	@SerializedName("InvalidProducts")
	private List<Object> invalidProducts;

	@SerializedName("ShippingCost")
	private ShippingCost shippingCost;

	@SerializedName("PromotionDetail")
	private List<Object> promotionDetail;

	@SerializedName("CartDetail")
	private List<CartDetailItem> cartDetail;

	@SerializedName("NonEDSProducts")
	private List<NonEDSProductsItem> nonEDSProducts;

	public void setIsSuccess(boolean isSuccess){
		this.isSuccess = isSuccess;
	}

	public boolean isIsSuccess(){
		return isSuccess;
	}

	public void setCartSummaries(CartSummaries cartSummaries){
		this.cartSummaries = cartSummaries;
	}

	public CartSummaries getCartSummaries(){
		return cartSummaries;
	}

	public void setMessage(List<String> message){
		this.message = message;
	}

	public List<String> getMessage(){
		return message;
	}

	public void setEDSProducts(List<Object> eDSProducts){
		this.eDSProducts = eDSProducts;
	}

	public List<Object> getEDSProducts(){
		return eDSProducts;
	}

	public void setOccasions(List<String> occasions){
		this.occasions = occasions;
	}

	public List<String> getOccasions(){
		return occasions;
	}

	public void setInformation(Information information){
		this.information = information;
	}

	public Information getInformation(){
		return information;
	}

	public void setCommercial(Commercial commercial){
		this.commercial = commercial;
	}

	public Commercial getCommercial(){
		return commercial;
	}

	public void setAssuredShippingDetails(AssuredShippingDetails assuredShippingDetails){
		this.assuredShippingDetails = assuredShippingDetails;
	}

	public AssuredShippingDetails getAssuredShippingDetails(){
		return assuredShippingDetails;
	}

	public void setCommand(int command){
		this.command = command;
	}

	public int getCommand(){
		return command;
	}

	public void setInvalidProducts(List<Object> invalidProducts){
		this.invalidProducts = invalidProducts;
	}

	public List<Object> getInvalidProducts(){
		return invalidProducts;
	}

	public void setShippingCost(ShippingCost shippingCost){
		this.shippingCost = shippingCost;
	}

	public ShippingCost getShippingCost(){
		return shippingCost;
	}

	public void setPromotionDetail(List<Object> promotionDetail){
		this.promotionDetail = promotionDetail;
	}

	public List<Object> getPromotionDetail(){
		return promotionDetail;
	}

	public void setCartDetail(List<CartDetailItem> cartDetail){
		this.cartDetail = cartDetail;
	}

	public List<CartDetailItem> getCartDetail(){
		return cartDetail;
	}

	public void setNonEDSProducts(List<NonEDSProductsItem> nonEDSProducts){
		this.nonEDSProducts = nonEDSProducts;
	}

	public List<NonEDSProductsItem> getNonEDSProducts(){
		return nonEDSProducts;
	}
}