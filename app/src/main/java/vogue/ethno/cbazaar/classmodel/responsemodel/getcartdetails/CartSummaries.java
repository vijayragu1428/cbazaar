package vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails;

 import com.google.gson.annotations.SerializedName;

 public class CartSummaries{

	@SerializedName("CashBackAmount")
	private int cashBackAmount;

	@SerializedName("Savings")
	private double savings;

	@SerializedName("StoreCredit")
	private double storeCredit;

	@SerializedName("IGSTMaxPercentage")
	private int iGSTMaxPercentage;

	@SerializedName("TopupDiscount")
	private int topupDiscount;

	@SerializedName("ProductDiscounts")
	private double productDiscounts;

	@SerializedName("SubTotal")
	private double subTotal;

	@SerializedName("GVAmount")
	private int gVAmount;

	@SerializedName("ProductCashBack")
	private int productCashBack;

	@SerializedName("SGSTMaxPercentage")
	private int sGSTMaxPercentage;

	@SerializedName("GrandTotal")
	private int grandTotal;

	@SerializedName("GrossOrderTotal")
	private double grossOrderTotal;

	@SerializedName("MultiOfferDiscount")
	private int multiOfferDiscount;

	@SerializedName("PartialDeliveryCost")
	private int partialDeliveryCost;

	@SerializedName("CGSTCost")
	private int cGSTCost;

	@SerializedName("IsShowGst")
	private boolean isShowGst;

	@SerializedName("SGSTCost")
	private int sGSTCost;

	@SerializedName("PromotionDiscount")
	private int promotionDiscount;

	@SerializedName("IGSTCost")
	private int iGSTCost;

	@SerializedName("CustomizationDiscount")
	private int customizationDiscount;

	public void setCashBackAmount(int cashBackAmount){
		this.cashBackAmount = cashBackAmount;
	}

	public int getCashBackAmount(){
		return cashBackAmount;
	}

	public void setSavings(double savings){
		this.savings = savings;
	}

	public double getSavings(){
		return savings;
	}

	public void setStoreCredit(double storeCredit){
		this.storeCredit = storeCredit;
	}

	public double getStoreCredit(){
		return storeCredit;
	}

	public void setIGSTMaxPercentage(int iGSTMaxPercentage){
		this.iGSTMaxPercentage = iGSTMaxPercentage;
	}

	public int getIGSTMaxPercentage(){
		return iGSTMaxPercentage;
	}

	public void setTopupDiscount(int topupDiscount){
		this.topupDiscount = topupDiscount;
	}

	public int getTopupDiscount(){
		return topupDiscount;
	}

	public void setProductDiscounts(double productDiscounts){
		this.productDiscounts = productDiscounts;
	}

	public double getProductDiscounts(){
		return productDiscounts;
	}

	public void setSubTotal(double subTotal){
		this.subTotal = subTotal;
	}

	public double getSubTotal(){
		return subTotal;
	}

	public void setGVAmount(int gVAmount){
		this.gVAmount = gVAmount;
	}

	public int getGVAmount(){
		return gVAmount;
	}

	public void setProductCashBack(int productCashBack){
		this.productCashBack = productCashBack;
	}

	public int getProductCashBack(){
		return productCashBack;
	}

	public void setSGSTMaxPercentage(int sGSTMaxPercentage){
		this.sGSTMaxPercentage = sGSTMaxPercentage;
	}

	public int getSGSTMaxPercentage(){
		return sGSTMaxPercentage;
	}

	public void setGrandTotal(int grandTotal){
		this.grandTotal = grandTotal;
	}

	public int getGrandTotal(){
		return grandTotal;
	}

	public void setGrossOrderTotal(double grossOrderTotal){
		this.grossOrderTotal = grossOrderTotal;
	}

	public double getGrossOrderTotal(){
		return grossOrderTotal;
	}

	public void setMultiOfferDiscount(int multiOfferDiscount){
		this.multiOfferDiscount = multiOfferDiscount;
	}

	public int getMultiOfferDiscount(){
		return multiOfferDiscount;
	}

	public void setPartialDeliveryCost(int partialDeliveryCost){
		this.partialDeliveryCost = partialDeliveryCost;
	}

	public int getPartialDeliveryCost(){
		return partialDeliveryCost;
	}

	public void setCGSTCost(int cGSTCost){
		this.cGSTCost = cGSTCost;
	}

	public int getCGSTCost(){
		return cGSTCost;
	}

	public void setIsShowGst(boolean isShowGst){
		this.isShowGst = isShowGst;
	}

	public boolean isIsShowGst(){
		return isShowGst;
	}

	public void setSGSTCost(int sGSTCost){
		this.sGSTCost = sGSTCost;
	}

	public int getSGSTCost(){
		return sGSTCost;
	}

	public void setPromotionDiscount(int promotionDiscount){
		this.promotionDiscount = promotionDiscount;
	}

	public int getPromotionDiscount(){
		return promotionDiscount;
	}

	public void setIGSTCost(int iGSTCost){
		this.iGSTCost = iGSTCost;
	}

	public int getIGSTCost(){
		return iGSTCost;
	}

	public void setCustomizationDiscount(int customizationDiscount){
		this.customizationDiscount = customizationDiscount;
	}

	public int getCustomizationDiscount(){
		return customizationDiscount;
	}
}