package vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails;

 import com.google.gson.annotations.SerializedName;

 public class Commercial{

	@SerializedName("Description")
	private String description;

	@SerializedName("Message")
	private String message;

	@SerializedName("Region")
	private String region;

	@SerializedName("IsMessageDisplay")
	private boolean isMessageDisplay;

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setRegion(String region){
		this.region = region;
	}

	public String getRegion(){
		return region;
	}

	public void setIsMessageDisplay(boolean isMessageDisplay){
		this.isMessageDisplay = isMessageDisplay;
	}

	public boolean isIsMessageDisplay(){
		return isMessageDisplay;
	}
}