package vogue.ethno.cbazaar.classmodel.responsemodel.getstates;

import com.google.gson.annotations.SerializedName;

public class StateResponseModel{

	@SerializedName("IsCODEligible")
	private boolean isCODEligible;

	@SerializedName("StateName")
	private String stateName;

	@SerializedName("StateId")
	private int stateId;

	public void setIsCODEligible(boolean isCODEligible){
		this.isCODEligible = isCODEligible;
	}

	public boolean isIsCODEligible(){
		return isCODEligible;
	}

	public void setStateName(String stateName){
		this.stateName = stateName;
	}

	public String getStateName(){
		return stateName;
	}

	public void setStateId(int stateId){
		this.stateId = stateId;
	}

	public int getStateId(){
		return stateId;
	}
}