package vogue.ethno.cbazaar.classmodel.responsemodel.productdetails;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Ratingstars implements Serializable {

    @SerializedName("SumOfRating")
    private int sumOfRating;

    @SerializedName("OverallRatingOn")
    private int overallRatingOn;

    @SerializedName("OverallRatingOff")
    private int overallRatingOff;

    @SerializedName("CountOfRating")
    private int countOfRating;

    public void setSumOfRating(int sumOfRating) {
        this.sumOfRating = sumOfRating;
    }

    public int getSumOfRating() {
        return sumOfRating;
    }

    public void setOverallRatingOn(int overallRatingOn) {
        this.overallRatingOn = overallRatingOn;
    }

    public int getOverallRatingOn() {
        return overallRatingOn;
    }

    public void setOverallRatingOff(int overallRatingOff) {
        this.overallRatingOff = overallRatingOff;
    }

    public int getOverallRatingOff() {
        return overallRatingOff;
    }

    public void setCountOfRating(int countOfRating) {
        this.countOfRating = countOfRating;
    }

    public int getCountOfRating() {
        return countOfRating;
    }
}