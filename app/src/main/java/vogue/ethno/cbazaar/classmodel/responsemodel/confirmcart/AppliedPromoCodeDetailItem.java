package vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart;

 import com.google.gson.annotations.SerializedName;

 public class AppliedPromoCodeDetailItem{

	@SerializedName("PromotionDiscountPercentage")
	private int promotionDiscountPercentage;

	@SerializedName("IsExpired")
	private boolean isExpired;

	@SerializedName("PromotionType")
	private String promotionType;

	@SerializedName("Category")
	private int category;

	@SerializedName("Messages")
	private Object messages;

	@SerializedName("HasRestriction")
	private Object hasRestriction;

	@SerializedName("IsActive")
	private boolean isActive;

	@SerializedName("PromotionCode")
	private String promotionCode;

	@SerializedName("HasDelete")
	private boolean hasDelete;

	@SerializedName("LoginSourceCategory")
	private Object loginSourceCategory;

	@SerializedName("UserBusinessType")
	private int userBusinessType;

	@SerializedName("PromotionMinimumValue")
	private int promotionMinimumValue;

	@SerializedName("DiscountType")
	private int discountType;

	@SerializedName("PromotionValue")
	private int promotionValue;

	@SerializedName("LoginSourceName")
	private Object loginSourceName;

	@SerializedName("TargetPercentage")
	private int targetPercentage;

	@SerializedName("CustomizationSKUs")
	private Object customizationSKUs;

	@SerializedName("AgentId")
	private int agentId;

	@SerializedName("PromocodeTaggedLoginSource")
	private Object promocodeTaggedLoginSource;

	@SerializedName("IsValid")
	private boolean isValid;

	public void setPromotionDiscountPercentage(int promotionDiscountPercentage){
		this.promotionDiscountPercentage = promotionDiscountPercentage;
	}

	public int getPromotionDiscountPercentage(){
		return promotionDiscountPercentage;
	}

	public void setIsExpired(boolean isExpired){
		this.isExpired = isExpired;
	}

	public boolean isIsExpired(){
		return isExpired;
	}

	public void setPromotionType(String promotionType){
		this.promotionType = promotionType;
	}

	public String getPromotionType(){
		return promotionType;
	}

	public void setCategory(int category){
		this.category = category;
	}

	public int getCategory(){
		return category;
	}

	public void setMessages(Object messages){
		this.messages = messages;
	}

	public Object getMessages(){
		return messages;
	}

	public void setHasRestriction(Object hasRestriction){
		this.hasRestriction = hasRestriction;
	}

	public Object getHasRestriction(){
		return hasRestriction;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public void setPromotionCode(String promotionCode){
		this.promotionCode = promotionCode;
	}

	public String getPromotionCode(){
		return promotionCode;
	}

	public void setHasDelete(boolean hasDelete){
		this.hasDelete = hasDelete;
	}

	public boolean isHasDelete(){
		return hasDelete;
	}

	public void setLoginSourceCategory(Object loginSourceCategory){
		this.loginSourceCategory = loginSourceCategory;
	}

	public Object getLoginSourceCategory(){
		return loginSourceCategory;
	}

	public void setUserBusinessType(int userBusinessType){
		this.userBusinessType = userBusinessType;
	}

	public int getUserBusinessType(){
		return userBusinessType;
	}

	public void setPromotionMinimumValue(int promotionMinimumValue){
		this.promotionMinimumValue = promotionMinimumValue;
	}

	public int getPromotionMinimumValue(){
		return promotionMinimumValue;
	}

	public void setDiscountType(int discountType){
		this.discountType = discountType;
	}

	public int getDiscountType(){
		return discountType;
	}

	public void setPromotionValue(int promotionValue){
		this.promotionValue = promotionValue;
	}

	public int getPromotionValue(){
		return promotionValue;
	}

	public void setLoginSourceName(Object loginSourceName){
		this.loginSourceName = loginSourceName;
	}

	public Object getLoginSourceName(){
		return loginSourceName;
	}

	public void setTargetPercentage(int targetPercentage){
		this.targetPercentage = targetPercentage;
	}

	public int getTargetPercentage(){
		return targetPercentage;
	}

	public void setCustomizationSKUs(Object customizationSKUs){
		this.customizationSKUs = customizationSKUs;
	}

	public Object getCustomizationSKUs(){
		return customizationSKUs;
	}

	public void setAgentId(int agentId){
		this.agentId = agentId;
	}

	public int getAgentId(){
		return agentId;
	}

	public void setPromocodeTaggedLoginSource(Object promocodeTaggedLoginSource){
		this.promocodeTaggedLoginSource = promocodeTaggedLoginSource;
	}

	public Object getPromocodeTaggedLoginSource(){
		return promocodeTaggedLoginSource;
	}

	public void setIsValid(boolean isValid){
		this.isValid = isValid;
	}

	public boolean isIsValid(){
		return isValid;
	}
}