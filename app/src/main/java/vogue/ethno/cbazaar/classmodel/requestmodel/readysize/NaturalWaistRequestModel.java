package vogue.ethno.cbazaar.classmodel.requestmodel.readysize;

import com.google.gson.annotations.SerializedName;

public class NaturalWaistRequestModel {

    @SerializedName("Type")
    private String type;

    @SerializedName("Value")
    private String value;

    @SerializedName("SizeID")
    private String sizeId;

    public String getSizeId() {
        return sizeId;
    }

    public void setSizeId(String sizeId) {
        this.sizeId = sizeId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}