package vogue.ethno.cbazaar.classmodel.responsemodel.verifyotp;

 import com.google.gson.annotations.SerializedName;

 public class VerifyOtpResponse{

	@SerializedName("IsProceedToCOD")
	private boolean isProceedToCOD;

	@SerializedName("Message")
	private String message;

	@SerializedName("ExpireMessage")
	private String expireMessage;

	public void setIsProceedToCOD(boolean isProceedToCOD){
		this.isProceedToCOD = isProceedToCOD;
	}

	public boolean isIsProceedToCOD(){
		return isProceedToCOD;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setExpireMessage(String expireMessage){
		this.expireMessage = expireMessage;
	}

	public String getExpireMessage(){
		return expireMessage;
	}
}