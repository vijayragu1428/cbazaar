package vogue.ethno.cbazaar.classmodel.requestmodel.resent;

 import com.google.gson.annotations.SerializedName;
 public class ReSendOtpRequest {

	@SerializedName("Action")
	private String action;

	@SerializedName("MobileNumber")
	private String mobileNumber;

	@SerializedName("Mode")
	private String mode;

	@SerializedName("OtpVerificationCode")
	private String otpVerificationCode;

	@SerializedName("Operation")
	private String operation;

	@SerializedName("TrackNumber")
	private String trackNumber;

	public void setAction(String action){
		this.action = action;
	}

	public String getAction(){
		return action;
	}

	public void setMobileNumber(String mobileNumber){
		this.mobileNumber = mobileNumber;
	}

	public String getMobileNumber(){
		return mobileNumber;
	}

	public void setMode(String mode){
		this.mode = mode;
	}

	public String getMode(){
		return mode;
	}

	public void setOtpVerificationCode(String otpVerificationCode){
		this.otpVerificationCode = otpVerificationCode;
	}

	public String getOtpVerificationCode(){
		return otpVerificationCode;
	}

	public void setOperation(String operation){
		this.operation = operation;
	}

	public String getOperation(){
		return operation;
	}

	public void setTrackNumber(String trackNumber){
		this.trackNumber = trackNumber;
	}

	public String getTrackNumber(){
		return trackNumber;
	}
}