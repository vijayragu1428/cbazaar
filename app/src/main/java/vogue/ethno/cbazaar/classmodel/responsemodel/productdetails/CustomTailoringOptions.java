package vogue.ethno.cbazaar.classmodel.responsemodel.productdetails;

 import com.google.gson.annotations.SerializedName;

 import java.io.Serializable;

public class CustomTailoringOptions implements Serializable {

	@SerializedName("Note")
	private Object note;

	@SerializedName("Title")
	private Object title;

	@SerializedName("Items")
	private Object items;

	@SerializedName("IsSingleSelect")
	private boolean isSingleSelect;

	public void setNote(Object note){
		this.note = note;
	}

	public Object getNote(){
		return note;
	}

	public void setTitle(Object title){
		this.title = title;
	}

	public Object getTitle(){
		return title;
	}

	public void setItems(Object items){
		this.items = items;
	}

	public Object getItems(){
		return items;
	}

	public void setIsSingleSelect(boolean isSingleSelect){
		this.isSingleSelect = isSingleSelect;
	}

	public boolean isIsSingleSelect(){
		return isSingleSelect;
	}
}