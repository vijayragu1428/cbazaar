package vogue.ethno.cbazaar.classmodel.responsemodel.productdetails;

 import com.google.gson.annotations.SerializedName;

 import java.io.Serializable;

public class ProductSizeItem implements Serializable {

	@SerializedName("Value")
	private String value;

	@SerializedName("Key")
	private int key;

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	public void setKey(int key){
		this.key = key;
	}

	public int getKey(){
		return key;
	}
}