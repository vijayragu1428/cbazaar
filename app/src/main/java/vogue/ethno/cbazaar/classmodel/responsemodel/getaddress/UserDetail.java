package vogue.ethno.cbazaar.classmodel.responsemodel.getaddress;

 import com.google.gson.annotations.SerializedName;

 public class UserDetail{

	@SerializedName("FirstName")
	private String firstName;

	@SerializedName("ZipCode")
	private String zipCode;

	@SerializedName("Address2")
	private String address2;

	@SerializedName("Anniversary")
	private String anniversary;

	@SerializedName("CBNews")
	private Object cBNews;

	@SerializedName("Address1")
	private String address1;

	@SerializedName("City")
	private String city;

	@SerializedName("Gender")
	private String gender;

	@SerializedName("EmailAddress")
	private String emailAddress;

	@SerializedName("Industry")
	private String industry;

	@SerializedName("EmailSubsYN")
	private String emailSubsYN;

	@SerializedName("SMSAlertYN")
	private String sMSAlertYN;

	@SerializedName("State")
	private String state;

	@SerializedName("DOB")
	private String dOB;

	@SerializedName("Phone")
	private String phone;

	@SerializedName("News")
	private Object news;

	@SerializedName("Country")
	private String country;

	@SerializedName("LastName")
	private String lastName;

	@SerializedName("Cell")
	private String cell;

	@SerializedName("HearAboutUs")
	private String hearAboutUs;

	@SerializedName("Password")
	private String password;

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setZipCode(String zipCode){
		this.zipCode = zipCode;
	}

	public String getZipCode(){
		return zipCode;
	}

	public void setAddress2(String address2){
		this.address2 = address2;
	}

	public String getAddress2(){
		return address2;
	}

	public void setAnniversary(String anniversary){
		this.anniversary = anniversary;
	}

	public String getAnniversary(){
		return anniversary;
	}

	public void setCBNews(Object cBNews){
		this.cBNews = cBNews;
	}

	public Object getCBNews(){
		return cBNews;
	}

	public void setAddress1(String address1){
		this.address1 = address1;
	}

	public String getAddress1(){
		return address1;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setEmailAddress(String emailAddress){
		this.emailAddress = emailAddress;
	}

	public String getEmailAddress(){
		return emailAddress;
	}

	public void setIndustry(String industry){
		this.industry = industry;
	}

	public String getIndustry(){
		return industry;
	}

	public void setEmailSubsYN(String emailSubsYN){
		this.emailSubsYN = emailSubsYN;
	}

	public String getEmailSubsYN(){
		return emailSubsYN;
	}

	public void setSMSAlertYN(String sMSAlertYN){
		this.sMSAlertYN = sMSAlertYN;
	}

	public String getSMSAlertYN(){
		return sMSAlertYN;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setDOB(String dOB){
		this.dOB = dOB;
	}

	public String getDOB(){
		return dOB;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setNews(Object news){
		this.news = news;
	}

	public Object getNews(){
		return news;
	}

	public void setCountry(String country){
		this.country = country;
	}

	public String getCountry(){
		return country;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setCell(String cell){
		this.cell = cell;
	}

	public String getCell(){
		return cell;
	}

	public void setHearAboutUs(String hearAboutUs){
		this.hearAboutUs = hearAboutUs;
	}

	public String getHearAboutUs(){
		return hearAboutUs;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}
}