package vogue.ethno.cbazaar.classmodel.responsemodel.getcartdetails;

 import com.google.gson.annotations.SerializedName;

 public class DeliveryOptionsItem{

	@SerializedName("StartDate")
	private String startDate;

	@SerializedName("Description")
	private String description;

	@SerializedName("Value")
	private String value;

	@SerializedName("IsSelected")
	private boolean isSelected;

	@SerializedName("Key")
	private int key;

	public void setStartDate(String startDate){
		this.startDate = startDate;
	}

	public String getStartDate(){
		return startDate;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	public void setIsSelected(boolean isSelected){
		this.isSelected = isSelected;
	}

	public boolean isIsSelected(){
		return isSelected;
	}

	public void setKey(int key){
		this.key = key;
	}

	public int getKey(){
		return key;
	}
}