package vogue.ethno.cbazaar.classmodel.requestmodel.address;

 import com.google.gson.annotations.SerializedName;

 public class AddressRequestModel{

	@SerializedName("Address")
	private Address address;

	@SerializedName("Command")
	private int command;

	@SerializedName("RelationID")
	private int relationID;

	@SerializedName("IsGuest")
	private boolean isGuest;

	public void setAddress(Address address){
		this.address = address;
	}

	public Address getAddress(){
		return address;
	}

	public void setCommand(int command){
		this.command = command;
	}

	public int getCommand(){
		return command;
	}

	public void setRelationID(int relationID){
		this.relationID = relationID;
	}

	public int getRelationID(){
		return relationID;
	}

	public void setIsGuest(boolean isGuest){
		this.isGuest = isGuest;
	}

	public boolean getIsGuest(){
		return isGuest;
	}
}