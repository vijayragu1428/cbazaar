package vogue.ethno.cbazaar.classmodel.responsemodel.productdetails;

import java.io.Serializable;
import java.util.List;
 import com.google.gson.annotations.SerializedName;

 public class MeasurementOptions implements Serializable {

	@SerializedName("SelectedIndex")
	private int selectedIndex;

	@SerializedName("Note")
	private String note;

	@SerializedName("Title")
	private Object title;

	@SerializedName("Items")
	private List<ItemsItem> items;

	@SerializedName("IsSingleSelect")
	private boolean isSingleSelect;

	public void setSelectedIndex(int selectedIndex){
		this.selectedIndex = selectedIndex;
	}

	public int getSelectedIndex(){
		return selectedIndex;
	}

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setTitle(Object title){
		this.title = title;
	}

	public Object getTitle(){
		return title;
	}

	public void setItems(List<ItemsItem> items){
		this.items = items;
	}

		public List<ItemsItem> getItems(){
		return items;
	}

	public void setIsSingleSelect(boolean isSingleSelect){
		this.isSingleSelect = isSingleSelect;
	}

	public boolean isIsSingleSelect(){
		return isSingleSelect;
	}
}