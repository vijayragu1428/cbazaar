package vogue.ethno.cbazaar.classmodel.responsemodel.productdetails;

 import com.google.gson.annotations.SerializedName;

 import java.io.Serializable;

public class PatternProductDetailItem implements Serializable {

	@SerializedName("ComponentDisplay")
	private int componentDisplay;

	@SerializedName("MeasurementNature")
	private Object measurementNature;

	@SerializedName("ComponentDescription")
	private String componentDescription;

	@SerializedName("ProductCode")
	private String productCode;

	@SerializedName("ComponentCost")
	private int componentCost;

	@SerializedName("ProductHisImage")
	private String productHisImage;

	@SerializedName("Quantity")
	private int quantity;

	@SerializedName("DispatchDate")
	private Object dispatchDate;

	@SerializedName("UpperImage")
	private String upperImage;

	@SerializedName("UpperThumpnailImage")
	private String upperThumpnailImage;

	@SerializedName("IsFabric")
	private boolean isFabric;

	@SerializedName("ComponentName")
	private String componentName;

	@SerializedName("MappedProductCode")
	private Object mappedProductCode;

	@SerializedName("ComponentId")
	private int componentId;

	@SerializedName("IsPartiallyOrdered")
	private boolean isPartiallyOrdered;

	public void setComponentDisplay(int componentDisplay){
		this.componentDisplay = componentDisplay;
	}

	public int getComponentDisplay(){
		return componentDisplay;
	}

	public void setMeasurementNature(Object measurementNature){
		this.measurementNature = measurementNature;
	}

	public Object getMeasurementNature(){
		return measurementNature;
	}

	public void setComponentDescription(String componentDescription){
		this.componentDescription = componentDescription;
	}

	public String getComponentDescription(){
		return componentDescription;
	}

	public void setProductCode(String productCode){
		this.productCode = productCode;
	}

	public String getProductCode(){
		return productCode;
	}

	public void setComponentCost(int componentCost){
		this.componentCost = componentCost;
	}

	public int getComponentCost(){
		return componentCost;
	}

	public void setProductHisImage(String productHisImage){
		this.productHisImage = productHisImage;
	}

	public String getProductHisImage(){
		return productHisImage;
	}

	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public int getQuantity(){
		return quantity;
	}

	public void setDispatchDate(Object dispatchDate){
		this.dispatchDate = dispatchDate;
	}

	public Object getDispatchDate(){
		return dispatchDate;
	}

	public void setUpperImage(String upperImage){
		this.upperImage = upperImage;
	}

	public String getUpperImage(){
		return upperImage;
	}

	public void setUpperThumpnailImage(String upperThumpnailImage){
		this.upperThumpnailImage = upperThumpnailImage;
	}

	public String getUpperThumpnailImage(){
		return upperThumpnailImage;
	}

	public void setIsFabric(boolean isFabric){
		this.isFabric = isFabric;
	}

	public boolean isIsFabric(){
		return isFabric;
	}

	public void setComponentName(String componentName){
		this.componentName = componentName;
	}

	public String getComponentName(){
		return componentName;
	}

	public void setMappedProductCode(Object mappedProductCode){
		this.mappedProductCode = mappedProductCode;
	}

	public Object getMappedProductCode(){
		return mappedProductCode;
	}

	public void setComponentId(int componentId){
		this.componentId = componentId;
	}

	public int getComponentId(){
		return componentId;
	}

	public void setIsPartiallyOrdered(boolean isPartiallyOrdered){
		this.isPartiallyOrdered = isPartiallyOrdered;
	}

	public boolean isIsPartiallyOrdered(){
		return isPartiallyOrdered;
	}
}