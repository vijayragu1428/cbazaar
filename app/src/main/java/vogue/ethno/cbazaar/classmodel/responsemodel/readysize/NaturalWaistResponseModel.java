package vogue.ethno.cbazaar.classmodel.responsemodel.readysize;

 import com.google.gson.annotations.SerializedName;

 public class NaturalWaistResponseModel {

	@SerializedName("Inches")
	private String inches;

	@SerializedName("Id")
	private String id;

	public void setInches(String inches){
		this.inches = inches;
	}

	public String getInches(){
		return inches;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}
}