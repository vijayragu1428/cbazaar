package vogue.ethno.cbazaar.classmodel.responsemodel.confirmcart;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class TemporaryTransaction{

	@SerializedName("ModeOfPayment")
	private Object modeOfPayment;

	@SerializedName("TotalPurchaseAmount")
	private int totalPurchaseAmount;

	@SerializedName("BillingCountryCode")
	private String billingCountryCode;

	@SerializedName("UserGiftMessage")
	private String userGiftMessage;

	@SerializedName("UserSpecialInstruction")
	private String userSpecialInstruction;

	@SerializedName("DeliveryDate")
	private String deliveryDate;

	@SerializedName("TrackNumber")
	private int trackNumber;

	@SerializedName("IsGVProduct")
	private boolean isGVProduct;

	@SerializedName("CustomerType")
	private Object customerType;

	@SerializedName("OrderDate")
	private String orderDate;

	@SerializedName("ShippingCountryCode")
	private String shippingCountryCode;

	@SerializedName("BillingAddress")
	private String billingAddress;

	@SerializedName("HasCashBack")
	private boolean hasCashBack;

	@SerializedName("ShopOccasion")
	private String shopOccasion;

	@SerializedName("UserID")
	private int userID;

	@SerializedName("ShippingAddress")
	private List<ShippingAddressItem> shippingAddress;

	@SerializedName("StripeCustomerID")
	private Object stripeCustomerID;

	@SerializedName("ShippingCost")
	private int shippingCost;

	@SerializedName("ClientID")
	private String clientID;

	@SerializedName("VatMessage")
	private boolean vatMessage;

	public void setModeOfPayment(Object modeOfPayment){
		this.modeOfPayment = modeOfPayment;
	}

	public Object getModeOfPayment(){
		return modeOfPayment;
	}

	public void setTotalPurchaseAmount(int totalPurchaseAmount){
		this.totalPurchaseAmount = totalPurchaseAmount;
	}

	public int getTotalPurchaseAmount(){
		return totalPurchaseAmount;
	}

	public void setBillingCountryCode(String billingCountryCode){
		this.billingCountryCode = billingCountryCode;
	}

	public String getBillingCountryCode(){
		return billingCountryCode;
	}

	public void setUserGiftMessage(String userGiftMessage){
		this.userGiftMessage = userGiftMessage;
	}

	public String getUserGiftMessage(){
		return userGiftMessage;
	}

	public void setUserSpecialInstruction(String userSpecialInstruction){
		this.userSpecialInstruction = userSpecialInstruction;
	}

	public String getUserSpecialInstruction(){
		return userSpecialInstruction;
	}

	public void setDeliveryDate(String deliveryDate){
		this.deliveryDate = deliveryDate;
	}

	public String getDeliveryDate(){
		return deliveryDate;
	}

	public void setTrackNumber(int trackNumber){
		this.trackNumber = trackNumber;
	}

	public int getTrackNumber(){
		return trackNumber;
	}

	public void setIsGVProduct(boolean isGVProduct){
		this.isGVProduct = isGVProduct;
	}

	public boolean isIsGVProduct(){
		return isGVProduct;
	}

	public void setCustomerType(Object customerType){
		this.customerType = customerType;
	}

	public Object getCustomerType(){
		return customerType;
	}

	public void setOrderDate(String orderDate){
		this.orderDate = orderDate;
	}

	public String getOrderDate(){
		return orderDate;
	}

	public void setShippingCountryCode(String shippingCountryCode){
		this.shippingCountryCode = shippingCountryCode;
	}

	public String getShippingCountryCode(){
		return shippingCountryCode;
	}

	public void setBillingAddress(String billingAddress){
		this.billingAddress = billingAddress;
	}

	public String getBillingAddress(){
		return billingAddress;
	}

	public void setHasCashBack(boolean hasCashBack){
		this.hasCashBack = hasCashBack;
	}

	public boolean isHasCashBack(){
		return hasCashBack;
	}

	public void setShopOccasion(String shopOccasion){
		this.shopOccasion = shopOccasion;
	}

	public String getShopOccasion(){
		return shopOccasion;
	}

	public void setUserID(int userID){
		this.userID = userID;
	}

	public int getUserID(){
		return userID;
	}

	public void setShippingAddress(List<ShippingAddressItem> shippingAddress){
		this.shippingAddress = shippingAddress;
	}

	public List<ShippingAddressItem> getShippingAddress(){
		return shippingAddress;
	}

	public void setStripeCustomerID(Object stripeCustomerID){
		this.stripeCustomerID = stripeCustomerID;
	}

	public Object getStripeCustomerID(){
		return stripeCustomerID;
	}

	public void setShippingCost(int shippingCost){
		this.shippingCost = shippingCost;
	}

	public int getShippingCost(){
		return shippingCost;
	}

	public void setClientID(String clientID){
		this.clientID = clientID;
	}

	public String getClientID(){
		return clientID;
	}

	public void setVatMessage(boolean vatMessage){
		this.vatMessage = vatMessage;
	}

	public boolean isVatMessage(){
		return vatMessage;
	}
}