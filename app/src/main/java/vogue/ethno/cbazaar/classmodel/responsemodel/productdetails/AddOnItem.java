package vogue.ethno.cbazaar.classmodel.responsemodel.productdetails;

 import com.google.gson.annotations.SerializedName;

 import java.io.Serializable;

public class AddOnItem implements Serializable {

	@SerializedName("SpecialInstruction")
	private String specialInstruction;

	@SerializedName("DeliveryDays")
	private int deliveryDays;

	@SerializedName("PromotionType")
	private int promotionType;

	@SerializedName("Description")
	private String description;

	@SerializedName("CashBackCost")
	private int cashBackCost;

	@SerializedName("IsEthnoStylish")
	private boolean isEthnoStylish;

	@SerializedName("DiscountCost")
	private int discountCost;

	@SerializedName("Image")
	private String image;

	@SerializedName("Code")
	private String code;

	@SerializedName("Cost")
	private double cost;

	@SerializedName("ActualCost")
	private int actualCost;

	@SerializedName("Name")
	private String name;

	@SerializedName("IsRequired")
	private boolean isRequired;

	@SerializedName("ID")
	private int iD;

	@SerializedName("PromotionPercentage")
	private int promotionPercentage;

	@SerializedName("Detail")
	private String detail;

	public void setSpecialInstruction(String specialInstruction){
		this.specialInstruction = specialInstruction;
	}

	public String getSpecialInstruction(){
		return specialInstruction;
	}

	public void setDeliveryDays(int deliveryDays){
		this.deliveryDays = deliveryDays;
	}

	public int getDeliveryDays(){
		return deliveryDays;
	}

	public void setPromotionType(int promotionType){
		this.promotionType = promotionType;
	}

	public int getPromotionType(){
		return promotionType;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setCashBackCost(int cashBackCost){
		this.cashBackCost = cashBackCost;
	}

	public int getCashBackCost(){
		return cashBackCost;
	}

	public void setIsEthnoStylish(boolean isEthnoStylish){
		this.isEthnoStylish = isEthnoStylish;
	}

	public boolean isIsEthnoStylish(){
		return isEthnoStylish;
	}

	public void setDiscountCost(int discountCost){
		this.discountCost = discountCost;
	}

	public int getDiscountCost(){
		return discountCost;
	}

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setCost(double cost){
		this.cost = cost;
	}

	public double getCost(){
		return cost;
	}

	public void setActualCost(int actualCost){
		this.actualCost = actualCost;
	}

	public int getActualCost(){
		return actualCost;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setIsRequired(boolean isRequired){
		this.isRequired = isRequired;
	}

	public boolean isIsRequired(){
		return isRequired;
	}

	public void setID(int iD){
		this.iD = iD;
	}

	public int getID(){
		return iD;
	}

	public void setPromotionPercentage(int promotionPercentage){
		this.promotionPercentage = promotionPercentage;
	}

	public int getPromotionPercentage(){
		return promotionPercentage;
	}

	public void setDetail(String detail){
		this.detail = detail;
	}

	public String getDetail(){
		return detail;
	}
}